#ifndef KURISUMESHINSTANCEIMPL_HPP
#define KURISUMESHINSTANCEIMPL_HPP

#include <KawaiiRenderer/Geometry/KawaiiMeshInstanceImpl.hpp>
#include "KurisuRenderer_global.hpp"
#include "Vulkan/VulkanBuffer.hpp"
#include <vector>

class KurisuRootImpl;

class KURISURENDERER_SHARED_EXPORT KurisuMeshInstanceImpl: public KawaiiMeshInstanceImpl
{
public:
  KurisuMeshInstanceImpl(KawaiiMeshInstance *model);
  ~KurisuMeshInstanceImpl();

  void draw() override;



  //IMPLEMENT
private:
  KurisuRootImpl *root;
  std::vector<VulkanBuffer> indirectDrawBuf; //one per device
  VkDrawIndexedIndirectCommand drawIndexedIndirectCommand;

  void setInstanceCount(uint32_t instanceCount);

  // KawaiiRendererImpl interface
private:
  void initConnections() override;
};

#endif // KURISUMESHINSTANCEIMPL_HPP
