#include "VulkanFbo.hpp"

VulkanFbo::VulkanFbo(VulkanBaseRenderpass &renderpass, const QSize &sz, KawaiiTextureFormat colorFormat, const VulkanTexture::UsageFlags &attachmentsUsage):
  fbo(VK_NULL_HANDLE),
  dev(&renderpass.getDevice())
{
  attachedTextures.emplace_back(renderpass.getDevice(), attachmentsUsage | VulkanTexture::UsageAttachment);
  attachedTextures.emplace_back(renderpass.getDevice(), attachmentsUsage | VulkanTexture::UsageAttachment);

  getColorTex().setExtent(sz.width(), sz.height());
  getColorTex().setFormat_8bpc(colorFormat);
  getColorTex().createImage(nullptr, true);
  getColorTex().createImageView(VkImageViewType::VK_IMAGE_VIEW_TYPE_2D);

  getDepthTex().setExtent(sz.width(), sz.height());
  getDepthTex().setFormat_32bpc(KawaiiTextureFormat::Depth);
  getDepthTex().createImage(nullptr, true);
  getDepthTex().createImageView(VkImageViewType::VK_IMAGE_VIEW_TYPE_2D);

  const std::array<VkImageView, 2> attachements = { getColorTexture().getVkImageView(), getDepthTexture().getVkImageView() };

  const VkFramebufferCreateInfo createInfo {
    .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
    .renderPass = renderpass.getRenderpass(),
    .attachmentCount = static_cast<uint32_t>(attachements.size()),
    .pAttachments = attachements.data(),
    .width = static_cast<uint32_t>(sz.width()),
    .height = static_cast<uint32_t>(sz.height()),
    .layers = 1
  };

  if (vkCreateFramebuffer(dev->vkDev, &createInfo, nullptr, &fbo) != VK_SUCCESS)
    throw std::runtime_error("failed to create framebuffer!");
}

VulkanFbo::VulkanFbo(VulkanBaseRenderpass &renderpass, const QSize &sz, const std::vector<std::pair<VulkanTexture *, int> > &externalAttachements):
  fbo(VK_NULL_HANDLE),
  dev(&renderpass.getDevice())
{
  for(size_t i = 0; i < externalAttachements.size(); ++i)
    {
      if(externalAttachements[i].first || i == 1)
        {
          attachedTextures.emplace_back(*dev, VulkanTexture::UsageAttachment);
          int layer = externalAttachements[i].second;
          
          if(externalAttachements[i].first)
            attachedTextures.back().pointToImage(*externalAttachements[i].first);
          else if(i == 1)
            {
              attachedTextures.back().setFormat_32bpc(KawaiiTextureFormat::Depth);
              attachedTextures.back().setExtent(sz.width(), sz.height());
              attachedTextures.back().createImage(nullptr, false);
              layer = 0;
            }
          
          attachedLayers.emplace_back(attachedTextures.back(), layer);
        }
    }

  std::vector<VkImageView> attachements(attachedLayers.size(), VK_NULL_HANDLE);
  for(size_t i = 0; i < attachements.size(); ++i)
    attachements[i] = attachedLayers[i].getImgView();

  const VkFramebufferCreateInfo createInfo {
    VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO, // sType
    nullptr, // pNext
    0, // flags
    renderpass.getRenderpass(), // renderPass
    static_cast<uint32_t>(attachements.size()), // attachmentCount
    attachements.data(), // pAttachments
    static_cast<uint32_t>(sz.width()), // width
    static_cast<uint32_t>(sz.height()), // height
    1 // layers
  };

  if (vkCreateFramebuffer(dev->vkDev, &createInfo, nullptr, &fbo) != VK_SUCCESS)
    throw std::runtime_error("failed to create framebuffer!");
}

VulkanFbo::~VulkanFbo()
{
  destroy();
}

VulkanFbo::VulkanFbo(VulkanFbo &&orig):
  attachedTextures(std::move(orig.attachedTextures)),
  attachedLayers(std::move(orig.attachedLayers)),
  fbo(orig.fbo),
  dev(orig.dev)
{
  orig.fbo = VK_NULL_HANDLE;
}

VulkanFbo &VulkanFbo::operator=(VulkanFbo &&orig)
{
  destroy();
  attachedTextures = std::move(orig.attachedTextures);
  attachedLayers = std::move(orig.attachedLayers);
  fbo = orig.fbo;
  dev = orig.dev;
  orig.fbo = VK_NULL_HANDLE;
  return *this;
}

const VulkanDevice &VulkanFbo::getDevice() const
{
  return *dev;
}

VkFramebuffer VulkanFbo::getFbo() const
{
  return fbo;
}

const VkDescriptorImageInfo *VulkanFbo::getColorImgDescriptor()
{
  return getColorTexture().descriptorInfo();
}

const VkDescriptorImageInfo *VulkanFbo::getDepthImgDescriptor()
{
  return getDepthTexture().descriptorInfo();
}

VkFormat VulkanFbo::getColorVkFormat() const
{
  return getColorTexture().getVkFormat();
}

VkFormat VulkanFbo::getDepthVkFormat() const
{
  return getDepthTexture().getVkFormat();
}

const VulkanTexture &VulkanFbo::getColorTexture() const
{
  Q_ASSERT(attachedTextures.size() >= 1);
  return attachedTextures[0];
}

const VulkanTexture &VulkanFbo::getDepthTexture() const
{
  Q_ASSERT(attachedTextures.size() >= 2);
  return attachedTextures[1];
}

VulkanTexture &VulkanFbo::getColorTex()
{
  Q_ASSERT(attachedTextures.size() >= 1);
  return attachedTextures[0];
}

VulkanTexture &VulkanFbo::getDepthTex()
{
  Q_ASSERT(attachedTextures.size() >= 2);
  return attachedTextures[1];
}

void VulkanFbo::destroy()
{
  attachedLayers.clear();
  attachedTextures.clear();
  if(fbo)
    {
      dev->waitIdle();
      vkDestroyFramebuffer(dev->vkDev, fbo, nullptr);
    }
}
