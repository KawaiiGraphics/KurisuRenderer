#include "KurisuSceneLayerImpl.hpp"
#include "KurisuRenderpassImpl.hpp"
#include "KurisuRootImpl.hpp"

namespace {
  inline const glm::mat4 vulkanToGlClipCorrection(1.0f,  0.0f, 0.0f, 0.0f,
                                                  0.0f, -1.0f, 0.0f, 0.0f,
                                                  0.0f,  0.0f, 0.5f, 0.0f,
                                                  0.0f,  0.0f, 0.5f, 1.0f);
}

KurisuSceneLayerImpl::KurisuSceneLayerImpl(KawaiiSceneLayer *model):
  KawaiiImageSourceImpl(model),
  KurisuImageSourceImpl(model),
  KawaiiSceneLayerImpl(model),
  root(nullptr)
{
  model->setClipCorrection(vulkanToGlClipCorrection);
}

KurisuSceneLayerImpl::~KurisuSceneLayerImpl()
{
  preDestruct();
}

void KurisuSceneLayerImpl::drawQuad()
{ }

void KurisuSceneLayerImpl::writeCmdBuf(RenderingCommands &cmdBuf, size_t dev_index)
{
  if(Q_LIKELY(root))
    {
      root->activateCommands(&cmdBuf, dev_index);
      KawaiiSceneLayerImpl::directDraw();
      root->activateCommands(nullptr, dev_index);
    }
}

void KurisuSceneLayerImpl::initConnections()
{
  auto modelRoot = KawaiiRoot::getRoot(getModel());
  if(Q_UNLIKELY(!modelRoot)) return;

  auto newRoot = static_cast<KurisuRootImpl*>(modelRoot->getRendererImpl());
  if(newRoot != root)
    {
      disconnect(onRootBufferBindingChanged);
      onRootBufferBindingChanged = connect(newRoot, &KurisuRootImpl::bufferBindingChanged, this, [this] {
          refreshNeeded = true;
        });
      root = newRoot;
    }
}
