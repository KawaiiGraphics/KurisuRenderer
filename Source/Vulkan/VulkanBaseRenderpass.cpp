#include "VulkanBaseRenderpass.hpp"
#include <QPointer>

VulkanBaseRenderpass::VulkanBaseRenderpass(VulkanDevice &dev):
  dev(&dev),
  renderPass(VK_NULL_HANDLE),
  dependencies(),
  attachments(),
  clearValues(),
  inputAttachments(),
  colorAttachments(),
  preserveAttachments(),
  depthAttachments(),
  subpasses(),
  createInfo {
    .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
    .attachmentCount = static_cast<uint32_t>(attachments.size()),
    .pAttachments = attachments.data(),
    .subpassCount = static_cast<uint32_t>(subpasses.size()),
    .pSubpasses = subpasses.data(),
    .dependencyCount = static_cast<uint32_t>(dependencies.size()),
    .pDependencies = dependencies.data()
    },
  dirty(true)
{
}

VulkanBaseRenderpass::VulkanBaseRenderpass(VulkanDevice &dev, VkFormat colorFmt):
  VulkanBaseRenderpass(dev)
{
  addAttachement(VkAttachmentDescription {
                   .format = colorFmt,
                   .samples = VK_SAMPLE_COUNT_1_BIT,
                   .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
                   .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
                   .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                   .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                   .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                   .finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                 },
                 VkClearValue { .color = { .float32 = { 0, 0, 0, 0 } } });

  addAttachement(VkAttachmentDescription {
                   .format = dev.depthFormat,
                   .samples = VK_SAMPLE_COUNT_1_BIT,
                   .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
                   .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
                   .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                   .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                   .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                   .finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                 },
                 VkClearValue { .depthStencil = { .depth = 1.0f, .stencil = 0 } });

  appendSubpass(VkAttachmentReference {
                  .attachment = 0,
                  .layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
                },
                VkAttachmentReference {
                  .attachment = 1,
                  .layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
                });
}

namespace {
  VkFormat getFmt(VulkanTexture *tex)
  {
    if(tex)
      return tex->getVkFormat();
    else
      return VK_FORMAT_UNDEFINED;
  }
}

VulkanBaseRenderpass::VulkanBaseRenderpass(VulkanDevice &dev, const std::vector<std::pair<VulkanTexture *, int> > &renderBufs):
  VulkanBaseRenderpass(dev, getFmt(renderBufs.front().first))
{
  uint32_t removedSubpasses = 0;
  if(renderBufs.front().first == nullptr)
    {
      attachments.erase(attachments.begin());
      clearValues.erase(clearValues.begin());
      colorAttachments.front().pop_back();
      subpasses.front().pColorAttachments = nullptr;
      subpasses.front().colorAttachmentCount = 0;
      depthAttachments.front().attachment = 0;
      createInfo.attachmentCount = 1;
      createInfo.pAttachments = attachments.data();
      removedSubpasses = 1;
    }

  for(size_t i = 2; i < renderBufs.size(); ++i)
    {
      addAttachement(VkAttachmentDescription {
                          .format = getFmt(renderBufs[i].first),
                          .samples = VK_SAMPLE_COUNT_1_BIT,
                          .loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR,
                          .storeOp = VK_ATTACHMENT_STORE_OP_STORE,
                          .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                          .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
                          .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                          .finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                        },
                     VkClearValue { .color = { .float32 = {0,0,0,0} } });

      addColorAttachment(i - removedSubpasses, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, 0);
    }
}

VulkanBaseRenderpass::VulkanBaseRenderpass(VulkanBaseRenderpass &&orig):
  dev(orig.dev),
  renderPass(orig.renderPass),
  dependencies(std::move(orig.dependencies)),
  attachments(std::move(orig.attachments)),
  inputAttachments(std::move(orig.inputAttachments)),
  colorAttachments(std::move(orig.colorAttachments)),
  depthAttachments(std::move(orig.depthAttachments)),
  subpasses(std::move(orig.subpasses)),

  createInfo {
    .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
    .attachmentCount = static_cast<uint32_t>(attachments.size()),
    .pAttachments = attachments.data(),
    .subpassCount = static_cast<uint32_t>(subpasses.size()),
    .pSubpasses = subpasses.data(),
    .dependencyCount = static_cast<uint32_t>(dependencies.size()),
    .pDependencies = dependencies.data()
    },

  dirty(orig.dirty)
{
  orig.renderPass = VK_NULL_HANDLE;
  onAttachementsRelocated();
  dirty = orig.dirty;
}

VulkanBaseRenderpass &VulkanBaseRenderpass::operator=(VulkanBaseRenderpass &&orig)
{
  dev->waitIdle();

  if(renderPass)
    vkDestroyRenderPass(dev->vkDev, renderPass, nullptr);

  dev = orig.dev;
  renderPass = orig.renderPass;
  dependencies = std::move(orig.dependencies);
  attachments = std::move(orig.attachments);
  inputAttachments = std::move(orig.inputAttachments);
  colorAttachments = std::move(orig.colorAttachments);
  depthAttachments = std::move(orig.depthAttachments);
  subpasses = std::move(orig.subpasses);

  createInfo = VkRenderPassCreateInfo {
      .sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
      .attachmentCount = static_cast<uint32_t>(attachments.size()),
      .pAttachments = attachments.data(),
      .subpassCount = static_cast<uint32_t>(subpasses.size()),
      .pSubpasses = subpasses.data(),
      .dependencyCount = static_cast<uint32_t>(dependencies.size()),
      .pDependencies = dependencies.data() };

  orig.renderPass = VK_NULL_HANDLE;
  onAttachementsRelocated();
  dirty = orig.dirty;

  return *this;
}

VulkanBaseRenderpass::~VulkanBaseRenderpass()
{
  dev->waitIdle();

  if(renderPass)
    vkDestroyRenderPass(dev->vkDev, renderPass, nullptr);
}

VulkanDevice &VulkanBaseRenderpass::getDevice() const
{
  return *dev;
}

VkRenderPass VulkanBaseRenderpass::getRenderpass()
{
  if(dirty)
    {
      destroyResources();

      if (vkCreateRenderPass(dev->vkDev, &createInfo, nullptr, &renderPass) != VK_SUCCESS) {
          renderPass = VK_NULL_HANDLE;
          throw std::runtime_error("failed to create render pass!");
        }

      dirty = false;
    }

  return renderPass;
}

uint32_t VulkanBaseRenderpass::addAttachement(const VkAttachmentDescription &att, const VkClearValue &clearValue)
{
  const uint32_t result = static_cast<uint32_t>(attachments.size());
  attachments.push_back(att);
  clearValues.push_back(clearValue);
  onAttachementsRelocated();
  return result;
}

void VulkanBaseRenderpass::addInputAttachment(uint32_t attachmentIndex, VkImageLayout imgLayout, size_t subpass)
{
  const VkAttachmentReference attRef = {
    .attachment = attachmentIndex,
    .layout = imgLayout
  };

  inputAttachments[subpass].push_back(attRef);
  onAttachementsRelocated();
}

void VulkanBaseRenderpass::addColorAttachment(uint32_t attachmentIndex, VkImageLayout imgLayout, size_t subpass)
{
  const VkAttachmentReference attRef = {
    .attachment = attachmentIndex,
    .layout = imgLayout
  };

  colorAttachments[subpass].push_back(attRef);
  onAttachementsRelocated();
}

void VulkanBaseRenderpass::addPreserveAttachment(uint32_t attachmentIndex, size_t subpass)
{
  preserveAttachments[subpass].push_back(attachmentIndex);
  onAttachementsRelocated();
}

size_t VulkanBaseRenderpass::appendSubpass(const VkAttachmentReference &colorAttachment, const VkAttachmentReference &depthAttachment)
{
  size_t index = subpasses.size();

  if(colorAttachment.attachment != VK_ATTACHMENT_UNUSED)
    colorAttachments.push_back(std::vector<VkAttachmentReference>(1, colorAttachment));
  else
    colorAttachments.emplace_back();
  depthAttachments.push_back(depthAttachment);
  inputAttachments.push_back(std::vector<VkAttachmentReference>());
  preserveAttachments.push_back(std::vector<uint32_t>());

  const VkSubpassDescription subpass = {
    .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS
  };

  subpasses.push_back(subpass);
  onSubpassesRelocated();
  onAttachementsRelocated();

  return index;
}

void VulkanBaseRenderpass::prependSubpass(const VkAttachmentReference &colorAttachment, const VkAttachmentReference &depthAttachment)
{
  colorAttachments.insert(colorAttachments.begin(), std::vector<VkAttachmentReference>(1, colorAttachment));
  depthAttachments.insert(depthAttachments.begin(), depthAttachment);
  inputAttachments.insert(inputAttachments.begin(), std::vector<VkAttachmentReference>());
  preserveAttachments.insert(preserveAttachments.begin(), std::vector<uint32_t>());

  const VkSubpassDescription subpass = {
    .pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS
  };

  subpasses.insert(subpasses.begin(), subpass);
  onSubpassesRelocated();
  onAttachementsRelocated();

  for(auto &i: dependencies)
    {
      if(i.srcSubpass != VK_SUBPASS_EXTERNAL)
        ++i.srcSubpass;

      if(i.dstSubpass != VK_SUBPASS_EXTERNAL)
        ++i.dstSubpass;
    }
}

void VulkanBaseRenderpass::addSubpassDependency(const VkSubpassDependency &dependency)
{
  dependencies.push_back(dependency);
  onDependenciesRelocated();
}

VkAttachmentDescription &VulkanBaseRenderpass::getAttachment(size_t i)
{
  Q_ASSERT(i < attachments.size());
  return attachments[i];
}

const VkAttachmentDescription &VulkanBaseRenderpass::getAttachment(size_t i) const
{
  Q_ASSERT(i < attachments.size());
  return attachments[i];
}

const VkAttachmentReference &VulkanBaseRenderpass::getInputAttachmentRef(uint32_t subpass, uint32_t inputAttachmentIndex)
{
  return inputAttachments[subpass][inputAttachmentIndex];
}

const std::vector<VkClearValue> &VulkanBaseRenderpass::getClearValues() const
{
  return clearValues;
}

void VulkanBaseRenderpass::onSubpassesRelocated()
{
  createInfo.subpassCount = subpasses.size();
  createInfo.pSubpasses = subpasses.data();
  dirty = true;
}

void VulkanBaseRenderpass::onAttachementsRelocated()
{
  createInfo.attachmentCount = attachments.size();
  createInfo.pAttachments = attachments.data();
  for(size_t i = 0; i < subpasses.size(); ++i)
    {
      subpasses[i].inputAttachmentCount = inputAttachments[i].size();
      subpasses[i].pInputAttachments = inputAttachments[i].empty()? nullptr: inputAttachments[i].data();
      subpasses[i].colorAttachmentCount = colorAttachments[i].size();
      subpasses[i].pColorAttachments = colorAttachments[i].data();
      subpasses[i].pDepthStencilAttachment = &depthAttachments[i];
      subpasses[i].preserveAttachmentCount = preserveAttachments[i].size();
      subpasses[i].pPreserveAttachments = preserveAttachments[i].empty()? nullptr: preserveAttachments[i].data();
    }

  dirty = true;
}

void VulkanBaseRenderpass::onDependenciesRelocated()
{
  createInfo.dependencyCount = dependencies.size();
  createInfo.pDependencies = dependencies.data();
  dirty = true;
}

void VulkanBaseRenderpass::destroyResources()
{
  if(renderPass) {
      dev->waitIdle();
      vkDestroyRenderPass(dev->vkDev, renderPass, nullptr);
      renderPass = VK_NULL_HANDLE;
    }
}
