#include "VulkanRenderTarget.hpp"

VulkanRenderTarget::VulkanRenderTarget(VulkanDevice *dev):
  dev(dev),
  renderpass(VK_NULL_HANDLE),
  fbo(VK_NULL_HANDLE),
  width(1),
  height(1),
  fboDirty(false)
{
}

VulkanRenderTarget::VulkanRenderTarget(VulkanRenderTarget &&orig):
  dev(orig.dev),
  renderpass(orig.renderpass),
  imgs(std::move(orig.imgs)),
  fbo(orig.fbo),
  width(orig.width),
  height(orig.height),
  fboDirty(orig.fboDirty)
{
  orig.fboDirty = false;
  orig.width = orig.height = 0;
  orig.fbo = VK_NULL_HANDLE;
}

VulkanRenderTarget &VulkanRenderTarget::operator=(VulkanRenderTarget &&orig)
{
  destroyFramebuffer();
  dev = orig.dev;
  renderpass = orig.renderpass;
  imgs = std::move(orig.imgs);
  fbo = orig.fbo;
  width = orig.width;
  height = orig.height;

  orig.fboDirty = false;
  orig.width = orig.height = 0;
  orig.fbo = VK_NULL_HANDLE;

  return *this;
}

VulkanRenderTarget::~VulkanRenderTarget()
{
  destroyFramebuffer();
}

const std::vector<VkImageView> &VulkanRenderTarget::getImgs() const
{
  return imgs;
}

void VulkanRenderTarget::setImgs(const std::vector<VkImageView> &newImgs)
{
  if(newImgs != imgs)
    {
      imgs = newImgs;
      fboDirty = true;
    }
}

VkFramebuffer VulkanRenderTarget::getFbo()
{
  if(fboDirty)
    {
      destroyFramebuffer();
      VkFramebufferCreateInfo createInfo = {
        .sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,
        .renderPass = renderpass,
        .attachmentCount = static_cast<uint32_t>(imgs.size()),
        .pAttachments = imgs.data(),
        .width = width,
        .height = height,
        .layers = 1
      };
      const VkResult result = vkCreateFramebuffer(dev->vkDev, &createInfo, nullptr, &fbo);
      if(Q_UNLIKELY(result != VK_SUCCESS))
        {
          fbo = VK_NULL_HANDLE;
          throw std::runtime_error("failed to create framebuffer!");
        }
      fboDirty = false;
    }
  return fbo;
}

uint32_t VulkanRenderTarget::getHeight() const
{
  return height;
}

void VulkanRenderTarget::setHeight(uint32_t newHeight)
{
  if(newHeight != height)
    {
      height = newHeight;
      fboDirty = true;
    }
}

void VulkanRenderTarget::replaceAttachement(VkImageView oldImg, VkImageView newImg)
{
  for(VkImageView &img: imgs)
    if(img == oldImg)
      {
        img = newImg;
        fboDirty = true;
      }
}

uint32_t VulkanRenderTarget::getWidth() const
{
  return width;
}

void VulkanRenderTarget::setWidth(uint32_t newWidth)
{
  if(newWidth != width)
    {
      width = newWidth;
      fboDirty = true;
    }
}

VkRenderPass VulkanRenderTarget::getRenderpass() const
{
  return renderpass;
}

void VulkanRenderTarget::setRenderpass(VkRenderPass newRenderpass)
{
  if(newRenderpass != renderpass)
    {
      renderpass = newRenderpass;
      fboDirty = true;
    }
}

void VulkanRenderTarget::destroyFramebuffer()
{
  if(fbo)
    {
      vkDestroyFramebuffer(dev->vkDev, fbo, nullptr);
      fbo = VK_NULL_HANDLE;
    }
}
