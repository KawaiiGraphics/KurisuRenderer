#include "VulkanSemaphore.hpp"

namespace {
# if defined(Q_OS_UNIX)
  constexpr VkExternalSemaphoreHandleTypeFlagBits externalSemaphoreBits = VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_FD_BIT;
# elif defined(Q_OS_WINDOWS)
  constexpr VkExternalSemaphoreHandleTypeFlagBits externalSemaphoreBits = VK_EXTERNAL_SEMAPHORE_HANDLE_TYPE_OPAQUE_WIN32_KMT_BIT;
# endif

  const VkExportSemaphoreCreateInfo semExportInfo = {
    .sType = VK_STRUCTURE_TYPE_EXPORT_SEMAPHORE_CREATE_INFO,
    .handleTypes = externalSemaphoreBits
  };

  const VkSemaphoreCreateInfo semCreateInfo = {
    .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO
  };

  const VkSemaphoreCreateInfo semWithHandleCreateInfo = {
    .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
    .pNext = &semExportInfo
  };

}

VulkanSemaphore::VulkanSemaphore(VulkanDevice &dev, bool allowOpaqueHandle):
  dev(&dev)
{
  if(vkCreateSemaphore(dev.vkDev, allowOpaqueHandle? &semWithHandleCreateInfo: &semCreateInfo, nullptr, &sem) != VK_SUCCESS)
    throw std::runtime_error("failed to create semaphore!");
}

VulkanSemaphore::VulkanSemaphore(VulkanSemaphore &&orig):
  dev(orig.dev),
  sem(orig.sem)
{
  orig.sem = VK_NULL_HANDLE;
}

VulkanSemaphore &VulkanSemaphore::operator=(VulkanSemaphore &&orig)
{
  destroy();
  dev = orig.dev;
  sem = orig.sem;
  orig.sem = VK_NULL_HANDLE;
  return *this;
}

VulkanSemaphore::~VulkanSemaphore()
{
  destroy();
}

VkSemaphore VulkanSemaphore::getSemaphore() const
{
  return sem;
}

std::optional<VulkanDevice::Handle> VulkanSemaphore::getOpaqueHandle() const
{
  return dev->getSemHandle(sem, externalSemaphoreBits);
}

void VulkanSemaphore::reset()
{
  destroy();
  if(vkCreateSemaphore(dev->vkDev, &semCreateInfo, nullptr, &sem) != VK_SUCCESS)
    throw std::runtime_error("failed to recreate semaphore!");
}

void VulkanSemaphore::destroy()
{
  if(sem)
    {
      dev->waitIdle();
      vkDestroySemaphore(dev->vkDev, sem, nullptr);
    }
}
