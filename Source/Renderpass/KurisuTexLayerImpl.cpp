#include "KurisuTexLayerImpl.hpp"
#include "KurisuRenderpassImpl.hpp"
#include "KurisuRootImpl.hpp"
#include "Textures/KurisuTextureHandle.hpp"
#include <KawaiiRenderer/Textures/KawaiiTextureImpl.hpp>

KurisuTexLayerImpl::KurisuTexLayerImpl(KawaiiTexLayer *model):
  KawaiiImageSourceImpl(model),
  KurisuImageSourceImpl(model),
  layers(0)
{
  needsGBufs = false;
}

KurisuTexLayerImpl::~KurisuTexLayerImpl()
{
  disconnect(onTextureHandleChanged);
  preDestruct();
}

void KurisuTexLayerImpl::directDraw()
{ }

void KurisuTexLayerImpl::drawQuad()
{ }

void KurisuTexLayerImpl::writeCmdBuf(RenderingCommands &cmdBuf, size_t dev_index)
{
  disconnect(onTextureHandleChanged);
  auto modelObj = static_cast<KawaiiTexLayer*>(getModel());
  if(!modelObj->getTexture() || modelObj->getTexture()->getResolution().at(2) != layers)
    {
      pipeline.clear();
      views.clear();
    }
  if(!modelObj->getTexture())
    {
      layers = 0;
      return;
    }
  auto *tex = static_cast<KawaiiTextureImpl*>(modelObj->getTexture()->getRendererImpl());
  onTextureHandleChanged = connect(tex, &KawaiiTextureImpl::handleChanged, this, [this] { refreshNeeded = true; });

  auto *rp = getModel()->getOwner();
  auto *rp_impl = static_cast<KurisuRenderpassImpl*>(rp->getRendererImpl());
  auto *root = rp_impl->getRoot();

  if(pipeline.empty())
    {
      static const constexpr VkPipelineVertexInputStateCreateInfo vertexInputState = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .vertexBindingDescriptionCount = 0,
        .pVertexBindingDescriptions = nullptr,
        .vertexAttributeDescriptionCount = 0,
        .pVertexAttributeDescriptions = nullptr
      };

      root->forallDevices([this, root, modelObj] (VulkanDevice &dev, size_t dev_index) {
        pipeline.push_back(std::make_unique<VulkanPipeline>(dev, root->getOverlayPipelineLayout(dev_index)));
        pipeline.back()->setShaderStages(root->getArrayedOverlayShaders(dev_index, modelObj->getTexture()->getResolution().at(2)));
        pipeline.back()->setVertexInputState(vertexInputState);
        pipeline.back()->setCullMode(VK_CULL_MODE_BACK_BIT);
      });
    }
  std::vector<VkSpecializationInfo> specialization;
  float aspectRatioCorrection = 1.0;
  if(modelObj->isKeepAspectRatio())
    aspectRatioCorrection = static_cast<double>(modelObj->getTexture()->getResolution().at(1)) * static_cast<double>(getModel()->getRenderableSize().x)
        / (static_cast<double>(modelObj->getTexture()->getResolution().at(0)) * static_cast<double>(getModel()->getRenderableSize().y));

  if(aspectRatioCorrection != 1)
    {
      static const constexpr VkSpecializationMapEntry specMapEntry = {
        .constantID = 0,
        .offset = 0,
        .size = sizeof(float)
      };
      specialization.push_back(VkSpecializationInfo {
                                 .mapEntryCount = 1,
                                 .pMapEntries = &specMapEntry,
                                 .dataSize = sizeof(float),
                                 .pData = &aspectRatioCorrection
                               });
      specialization.push_back(VkSpecializationInfo {
                                 .mapEntryCount = 0,
                                 .pMapEntries = nullptr,
                                 .dataSize = 0,
                                 .pData = nullptr
                               });
    }

  VkPipeline vkPipeline;
  vkPipeline = pipeline[dev_index]->getPipeline(&cmdBuf,
                                                rp_impl->getRenderpass(dev_index).getRenderpass(),
                                                getModel()->getSubpass(),
                                                root->getColorBlendAttachmentState(),
                                                root->getDepthTest(),
                                                root->getDepthWrite(),
                                                root->getDepthCompareOp(), specialization);

  auto &pipelineLayout = root->getOverlayPipelineLayout(dev_index);
  VkDescriptorSetLayout descrSetLayout = pipelineLayout.getDescriptorSetLayouts()[0];

  auto *texHandle = static_cast<KurisuTextureHandle*>(tex->getHandle());
  const uint64_t descrSetName = static_cast<uint64_t>(reinterpret_cast<size_t>(this));

  if(modelObj->getTexture()->getResolution().at(2) == 1)
    {
      cmdBuf.createDescriptorSet(descrSetName,
                                 {}, { VulkanDescriptorPool::ImgBinding {
                                         .descrImg = *texHandle->getTexture(dev_index).descriptorInfo(),
                                         .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                                         .bindingPoint = 0
                                       } }, descrSetLayout, pipelineLayout.getDescriptorCount(0));
    } else
    {
      if(views.empty())
        root->forallDevices([this, texHandle] (VulkanDevice&, size_t dev_index) {
            views.emplace_back(std::ref(texHandle->getTexture(dev_index)));
          });
      cmdBuf.createDescriptorSet(descrSetName,
                                 {}, { VulkanDescriptorPool::ImgBinding {
                                         .descrImg = VkDescriptorImageInfo {
                                           .sampler = texHandle->getTexture(dev_index).descriptorInfo()->sampler,
                                           .imageView = views[dev_index].getImgView(),
                                           .imageLayout = texHandle->getTexture(dev_index).descriptorInfo()->imageLayout
                                         },
                                         .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                                         .bindingPoint = 0
                                       } }, descrSetLayout, pipelineLayout.getDescriptorCount(0));
    }
  cmdBuf.bindDescriptorSet(descrSetName, 0, pipelineLayout.getPipelineLayout());
  cmdBuf.bindGraphicsPipeline(vkPipeline);
  cmdBuf.draw(6, 1, 0, 0);
  layers = modelObj->getTexture()->getResolution().at(2);
}
