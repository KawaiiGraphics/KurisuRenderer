#ifndef VULKANCOMMANDPOOL_HPP
#define VULKANCOMMANDPOOL_HPP

#include <vulkan/vulkan.h>
#include <QFuture>
#include <QMutex>
#include <memory>
#include <list>

class VulkanCommandPool
{
  VulkanCommandPool(const VulkanCommandPool &) = delete;
  VulkanCommandPool& operator=(const VulkanCommandPool &) = delete;

public:
  using OnSubmitElement = std::list<std::function<void()>>::iterator;

  VulkanCommandPool(VkDevice dev, uint32_t queueFamily);
  ~VulkanCommandPool();

  VulkanCommandPool(VulkanCommandPool &&orig);

  void writeCommands(const std::vector<std::function<void(VkCommandBuffer)>> &commands);
  bool submit(VkQueue queue);
  bool submitAsync(VkQueue queue, VkSemaphore finished);

  void waitAsyncTask() const;
  VkFence getFence() const;

  OnSubmitElement addOnSubmit(const std::function<void()> &func);
  void removeOnSubmit(const OnSubmitElement &el);



  //IMPLEMENT
private:
  VkDevice dev;
  VkCommandPool pool;
  VkCommandBuffer cmdBuf;
  VkFence fence;
  QFuture<void> asyncSubmit;
  std::unique_ptr<QRecursiveMutex> mutex;

  std::list<std::function<void()>> onSubmit;

  size_t commandsCount;
};

#endif // VULKANCOMMANDPOOL_HPP
