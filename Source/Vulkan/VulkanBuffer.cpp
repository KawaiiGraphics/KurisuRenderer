#include "VulkanBuffer.hpp"
#include "SingleTimeCommands.hpp"

#include <QtConcurrent>
#include <cstring>
#include <array>

#include <Kawaii3D/KawaiiConfig.hpp>

VulkanBuffer::VulkanBuffer(VulkanDevice &dev, VkBufferUsageFlags usage):
  dev(&dev),
  data(nullptr),
  length(0),
  detachedLock(std::make_unique<QReadWriteLock>()),
  usage(usage)
{ }

VulkanBuffer::~VulkanBuffer()
{
  QReadLocker l0(detachedLock.get());
  for(auto &i: detached)
    i.second->waitForGpu();
  l0.unlock();

  QWriteLocker l(detachedLock.get());
  detached.clear();
  l.unlock();

  detachedLock.reset();
}

VulkanBuffer::VulkanBuffer(VulkanBuffer &&orig):
  dev(orig.dev),
  data(orig.data),
  length(orig.length),
  detached(std::move(orig.detached)),
  detachedLock(std::move(orig.detachedLock)),
  usage(orig.usage)
{
  orig.dev = nullptr;
  orig.data = nullptr;
  orig.length = 0;
}

VulkanBuffer &VulkanBuffer::operator=(VulkanBuffer &&orig)
{
  dev = orig.dev;
  data = orig.data;
  length = orig.length;
  detachedLock = std::move(orig.detachedLock);
  detached = std::move(orig.detached);
  usage = orig.usage;

  orig.dev = nullptr;
  orig.data = nullptr;
  orig.length = 0;

  return *this;
}

bool VulkanBuffer::setData(const void *ptr, size_t bytes)
{
  if(bytes == 0) return true;

  data = ptr;
  length = bytes;

  dev->waitIdle();
  QReadLocker l(detachedLock.get());
  for(auto &i: detached)
    {
      try
      {
        i.second->setHostData(ptr, bytes);
      } catch(const std::invalid_argument &)
      {
        return false;
      }
    }

  return true;
}

bool VulkanBuffer::mergeRg(const VkBufferCopy &rgA, VkBufferCopy &rgB)
{
  VkDeviceSize aStart = rgA.srcOffset,
      aEnd = aStart + rgA.size,

      bStart = rgB.srcOffset,
      bEnd = bStart + rgB.size;

  if(aEnd <= bEnd && aEnd >= bStart)
    {
      if(aStart < bStart)
        {
          rgB.dstOffset = rgB.srcOffset = aStart;
          rgB.size = bEnd - aStart;
        }

      return true;
    } else if(aStart <= bEnd && aStart >= bStart)
    {
      if(aEnd > bEnd)
        rgB.size = aEnd - bStart;

      return true;
    }
  return false;
}

void VulkanBuffer::updateSubData(size_t bytes, size_t offset)
{
  const VkBufferCopy rg = {
    .srcOffset = offset,
    .dstOffset = offset,
    .size = bytes
  };

  QReadLocker l(detachedLock.get());
  for(auto &i: detached)
    i.second->markDirty(rg);
}

VulkanBuffer::Instance &VulkanBuffer::getDetached(QObject *owner, size_t frame_count)
{
  Q_ASSERT(dev != nullptr);

  QWriteLocker l(detachedLock.get());
  auto el = detached.find(owner);
  if(el == detached.end())
    {
      el = detached.emplace(std::piecewise_construct,
                            std::forward_as_tuple(owner),
                            std::forward_as_tuple(std::make_unique<Instance>(*dev, frame_count, usage, true))).first;
      el->second->setHostData(data, length);
      el->second->onDestroyed = QObject::connect(owner, &QObject::destroyed, [owner, this] {
          QWriteLocker l(detachedLock.get());
          auto el = detached.find(owner);
          if(el != detached.end())
            {
              el->second.reset();
              detached.erase(el);
            }
        });
    }

  return *el->second;
}

VkBuffer VulkanBuffer::getDetachedBuffer(QObject *owner, size_t frameIndex, size_t frame_count)
{
  return getDetached(owner, frame_count).getBuffer(frameIndex);
}

void VulkanBuffer::setUsedInGpu(QObject *owner, size_t frame_index)
{
  QReadLocker l(detachedLock.get());
  auto el = detached.find(owner);
  if(el != detached.end())
    el->second->setUsedInGpu(frame_index);
}

void VulkanBuffer::setUnusedInGpu(QObject *owner, size_t frame_index)
{
  QReadLocker l(detachedLock.get());
  auto el = detached.find(owner);
  if(el != detached.end())
    el->second->setUnusedInGpu(frame_index);
}

const VkDescriptorBufferInfo *VulkanBuffer::descrInfo(QObject *owner, size_t frameIndex, size_t frame_count)
{
  return getDetached(owner, frame_count).getDescriptor(frameIndex);
}

void VulkanBuffer::syncDetachedBuffer(QObject *owner, size_t frameIndex)
{
  QReadLocker l(detachedLock.get());
  auto el = detached.find(owner);
  if(el != detached.end())
    el->second->sendToDevice(frameIndex);
}

VulkanDevice &VulkanBuffer::getDevice() const
{
  return *dev;
}

bool VulkanBuffer::empty() const
{
  return (length == 0);
}

QThreadPool &VulkanBuffer::memcpyToGpuBufThreadPool()
{
  static QThreadPool pool;
  static bool initialized = false;
  if(!initialized)
    {
      pool.setMaxThreadCount(24);
      pool.setThreadPriority(QThread::IdlePriority);
    }
  return pool;
}

VulkanBuffer::Instance::Instance(VulkanDevice &dev, size_t frameCount, VkBufferUsageFlags usage, bool useStaging):
  dev(&dev),
  stagingMem(VK_NULL_HANDLE),
  mem(VK_NULL_HANDLE),
  mappedMem(nullptr),
  bufOffset(frameCount, 0),
  stagingBuf(useStaging? frameCount: 0, VK_NULL_HANDLE),
  buf(frameCount,  VK_NULL_HANDLE),
  descr(frameCount),
  dirtyRg(frameCount),
  dirtyRgLock(std::make_unique<QMutex>()),
  hostData(nullptr),
  length(0),
  isInGpuUse(frameCount, false),
  usage(usage)
{
}

VulkanBuffer::Instance::~Instance()
{
  QObject::disconnect(onDestroyed);
  waitForGpu();
  destroy();
}

VulkanBuffer::Instance::Instance(VulkanBuffer::Instance &&orig):
  dev(orig.dev),
  stagingMem(orig.stagingMem),
  mem(orig.mem),
  mappedMem(orig.mappedMem),
  bufOffset(std::move(orig.bufOffset)),
  stagingBuf(std::move(orig.stagingBuf)),
  buf(std::move(orig.buf)),
  descr(std::move(orig.descr)),
  dirtyRg(std::move(orig.dirtyRg)),
  dirtyRgLock(std::make_unique<QMutex>()),
  hostData(orig.hostData),
  length(orig.length),
  isInGpuUse(std::move(orig.isInGpuUse)),
  usage(orig.usage)
{
  orig.stagingMem = VK_NULL_HANDLE;
  orig.mem = VK_NULL_HANDLE;
}

VulkanBuffer::Instance &VulkanBuffer::Instance::operator=(VulkanBuffer::Instance &&orig)
{
  waitForGpu();
  destroy();

  dev = orig.dev;
  stagingMem = orig.stagingMem;
  mem = orig.mem;
  mappedMem = orig.mappedMem;
  stagingBuf = std::move(orig.stagingBuf);
  buf = std::move(orig.buf);
  descr = std::move(orig.descr);

  QMutexLocker l0(orig.dirtyRgLock.get()),
      l1(dirtyRgLock.get());
  dirtyRg = std::move(orig.dirtyRg);
  l0.unlock();
  l1.unlock();

  hostData = orig.hostData;
  length = orig.length;
  isInGpuUse = std::move(orig.isInGpuUse);
  usage = orig.usage;

  orig.stagingMem = VK_NULL_HANDLE;
  orig.mem = VK_NULL_HANDLE;

  return *this;
}

void VulkanBuffer::Instance::setHostData(const void *hostData, size_t length)
{
  this->hostData = hostData;
  if(this->length != length)
    {
      if(this->length > 0)
        throw std::invalid_argument("VulkanBuffer::Instance::setHostData: length must be equal to this->length");

      this->length = length;
      init();
    }
}

namespace {
  void unite(std::vector<VkBufferCopy> &vec, const VkBufferCopy &val)
  {
    for(VkBufferCopy &i: vec)
      if(VulkanBuffer::mergeRg(val, i))
        return;
    vec.push_back(val);
  }
}

void VulkanBuffer::Instance::markDirty(const VkBufferCopy &rg)
{
  Q_ASSERT(rg.dstOffset == rg.srcOffset);
  Q_ASSERT(rg.srcOffset + rg.size <= length);

  const std::byte *src = static_cast<const std::byte*>(hostData) + rg.srcOffset;
  for(size_t frameIndex = 0; frameIndex < bufOffset.size(); ++frameIndex)
    if(!isInGpuUse[frameIndex])
      {
        std::byte *dest = static_cast<std::byte*>(mappedMem) + bufOffset[frameIndex] + rg.dstOffset;
        std::memcpy(dest, src, rg.size);
      }

  QMutexLocker l(dirtyRgLock.get());
  for(size_t frameIndex = 0; frameIndex < dirtyRg.size(); ++frameIndex)
    if(isInGpuUse[frameIndex] || !stagingBuf.empty())
      unite(dirtyRg[frameIndex], rg);
}

void VulkanBuffer::Instance::sendToDevice(size_t frame_index)
{
  QMutexLocker l(dirtyRgLock.get());
  if(dirtyRg[frame_index].empty()) return;

  auto rgArray = std::move(dirtyRg[frame_index]);
  l.unlock();

  if(!stagingBuf.empty())
    {
      SingleTimeCommands commands(*dev);
      commands.copyBuffer(stagingBuf[frame_index], buf[frame_index], rgArray);
      commands.write().waitForFinished();
    }
}

VkBuffer VulkanBuffer::Instance::getBuffer(size_t frame_index) const
{
  return buf[frame_index];
}

const VkDescriptorBufferInfo *VulkanBuffer::Instance::getDescriptor(size_t frame_index) const
{
  return &descr[frame_index];
}

void VulkanBuffer::Instance::setUsedInGpu(size_t frame_index)
{
  isInGpuUse[frame_index] = true;
}

void VulkanBuffer::Instance::setUnusedInGpu(size_t frame_index)
{
  if(isInGpuUse[frame_index])
    {
      QMutexLocker l(dirtyRgLock.get());
      const auto &rgArray = dirtyRg[frame_index];
      if(!rgArray.empty() && hostData && mappedMem)
        {
          const std::byte *src = static_cast<const std::byte*>(hostData);
          std::byte *dest = static_cast<std::byte*>(mappedMem) + bufOffset[frame_index];
          QtConcurrent::map(rgArray, [src, dest] (const VkBufferCopy &rg) {
              std::memcpy(dest + rg.dstOffset, src + rg.srcOffset, rg.size);
            }).waitForFinished();
        }
      isInGpuUse[frame_index] = false;
    }
}

void VulkanBuffer::Instance::waitForGpu()
{
  for(bool &i: isInGpuUse)
    while(i)
      QThread::msleep(5);
}

void VulkanBuffer::Instance::createBuffers(std::vector<VkBuffer> &buf, VkDeviceMemory &mem, VkMemoryPropertyFlags memProperties, VkBufferUsageFlags usage, bool &deviceLocal)
{
  const std::array<uint32_t, 2> queueFamilyIndices = { dev->queueFamilies.transferFamily.value(), dev->queueFamilies.graphicsFamily.value() };
  const bool exclusive = queueFamilyIndices[0]==queueFamilyIndices[1];

  const VkBufferCreateInfo createInfo = {
    .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
    .size = length,
    .usage = usage,
    .sharingMode = exclusive? VK_SHARING_MODE_EXCLUSIVE: VK_SHARING_MODE_CONCURRENT,
    .queueFamilyIndexCount = static_cast<uint32_t>(exclusive? 1: queueFamilyIndices.size()),
    .pQueueFamilyIndices = queueFamilyIndices.data()
    };

  VkMemoryRequirements memRequirements = {
    .size = 0,
    .alignment = 0,
    .memoryTypeBits = 0
  };
  for(size_t i = 0; i < buf.size(); ++i)
    {
      if(vkCreateBuffer(dev->vkDev, &createInfo, nullptr, &buf[i]) != VK_SUCCESS)
        throw std::runtime_error("Can not create vkBuffer");

      VkMemoryRequirements memReq;
      vkGetBufferMemoryRequirements(dev->vkDev, buf[i], &memReq);

      if(!memRequirements.memoryTypeBits)
        memRequirements.memoryTypeBits = memReq.memoryTypeBits;

      Q_ASSERT(memRequirements.memoryTypeBits == memReq.memoryTypeBits);

      if(memRequirements.alignment < memReq.alignment)
        memRequirements.alignment = memReq.alignment;

      bufOffset[i] = std::ceil(static_cast<double>(memRequirements.size) / static_cast<double>(memReq.alignment)) * memReq.alignment;
      memRequirements.size = bufOffset[i] + memReq.size;
    }

  const auto memInfo = dev->allocateMemory(memRequirements, memProperties);
  mem = memInfo.first;
  deviceLocal = memInfo.second;

  for(size_t i = 0; i < buf.size(); ++i)
    vkBindBufferMemory(dev->vkDev, buf[i], mem, bufOffset[i]);
}

void VulkanBuffer::Instance::init()
{
  auto useHostVisibleBuffers = [this] (std::vector<VkDeviceSize> &&offsets) {
      dev->waitIdle();
      for(VkBuffer buf: buf)
        if(buf)
          vkDestroyBuffer(dev->vkDev, buf, nullptr);
      if(mem)
        vkFreeMemory(dev->vkDev, mem, nullptr);

      buf = std::move(stagingBuf);
      mem = stagingMem;
      stagingMem = VK_NULL_HANDLE;
      bufOffset = std::move(offsets);
    };

  if(length)
    {
      bool deviceLocal;
      if(!stagingBuf.empty())
        {
          createBuffers(stagingBuf, stagingMem, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, usage | VK_BUFFER_USAGE_TRANSFER_SRC_BIT, deviceLocal);
          vkMapMemory(dev->vkDev, stagingMem, 0, VK_WHOLE_SIZE, 0, &mappedMem);
          auto offsets = std::move(bufOffset);
          if(deviceLocal)
            useHostVisibleBuffers(std::move(offsets));
          else
            {
              bufOffset.resize(buf.size());
              createBuffers(buf, mem, 0, usage | VK_BUFFER_USAGE_TRANSFER_DST_BIT, deviceLocal);
              if(!deviceLocal)
                {
                  useHostVisibleBuffers(std::move(offsets));
                  qWarning("VulkanBuffer: switched to host visible memory -- probably out of device memory");
                }
            }
        } else
        {
          createBuffers(buf, mem, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, usage, deviceLocal);
          vkMapMemory(dev->vkDev, mem, 0, VK_WHOLE_SIZE, 0, &mappedMem);
        }
      markDirty({ .srcOffset = 0, .dstOffset = 0, .size = length });

      for(size_t i = 0; i < buf.size(); ++i)
        {
          descr[i] = {
            .buffer = buf[i],
            .offset = 0,
            .range = VK_WHOLE_SIZE
          };
        }

      if(KawaiiConfig::getInstance().getDebugLevel() > 2)
        qDebug("KurisuRenderer: created %lu buffers, length = %lu, use staging buffers = %d, use device local memory = %d",
               buf.size(), length, !stagingBuf.empty(), deviceLocal);
    }
}

void VulkanBuffer::Instance::destroy()
{
  QWriteLocker l(&dev->resLock);
  for(VkBuffer &buf: stagingBuf)
    if(buf)
      {
        vkDestroyBuffer(dev->vkDev, buf, nullptr);
        buf = VK_NULL_HANDLE;
      }

  if(stagingMem)
    {
      vkUnmapMemory(dev->vkDev, stagingMem);
      vkFreeMemory(dev->vkDev, stagingMem, nullptr);
      stagingMem = VK_NULL_HANDLE;
    } else
    if(mem)
      vkUnmapMemory(dev->vkDev, mem);

  mappedMem = nullptr;

  for(VkBuffer &buf: buf)
    if(buf)
      {
        vkDestroyBuffer(dev->vkDev, buf, nullptr);
        buf = VK_NULL_HANDLE;
      }
  if(mem)
    {
      vkFreeMemory(dev->vkDev, mem, nullptr);
      mem = VK_NULL_HANDLE;
    }
}
