#version 450 core

layout (set = 0, binding = 0) uniform sampler2D inputTex;

layout (location = 0) in vec2 texcoord;
layout (location = 0) out vec4 outColor;

void main(void)
{
    outColor = texture(inputTex, texcoord);
}
