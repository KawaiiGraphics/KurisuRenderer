#ifndef KURISUSHADERIMPL_HPP
#define KURISUSHADERIMPL_HPP

#include "../KurisuRenderer_global.hpp"

#include <KawaiiRenderer/Shaders/KawaiiShaderImpl.hpp>

#include <glslang/Public/ShaderLang.h>
#include <vulkan/vulkan.h>
#include <vector>
#include <array>

class KurisuRootImpl;
class KURISURENDERER_SHARED_EXPORT KurisuShaderImpl : public KawaiiShaderImpl
{
public:
  KurisuShaderImpl(KawaiiShader *model);
  ~KurisuShaderImpl();

  KawaiiShaderType getShaderType() const;

  QString getModuleFName() const;
  inline const std::string &getGlslCode() const
  { return glsl; }

  inline VkShaderStageFlagBits getStage() const
  { return stage; }

  inline EShLanguage getKind() const
  { return kind; }

  std::unique_ptr<glslang::TShader> compile() const;

  const std::vector<uint32_t>& getLinkableSpirv();

  bool isAttributeEnabled(size_t attribute) const;

  // KawaiiShaderImpl interface
private:
  void createShader() override final;
  void deleteShader() override final;
  void recompile() override final;



  //IMPLEMENT
private:
  KawaiiShader *modelObj;
  std::string glsl;

  std::vector<uint32_t> spirv;
  std::array<bool, 3> enabledVertexAttributes;

  VkShaderStageFlagBits stage;
  EShLanguage kind;
  bool spirv_dirty;

  QString preprocessGlsl(const QString &code);
  KurisuRootImpl* rootImpl() const;
};

#endif // KURISUSHADERIMPL_HPP
