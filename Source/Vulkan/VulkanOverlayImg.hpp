#ifndef VULKANOVERLAYIMG_HPP
#define VULKANOVERLAYIMG_HPP

#include "VulkanOverlayCtx.hpp"
#include "VulkanSemaphore.hpp"
#include "VulkanTexture.hpp"
#include "VulkanGBuf.hpp"

#include <QOpenGLFramebufferObject>
#include <QOpenGLPaintDevice>

class VulkanOverlayImg
{
public:
  VulkanOverlayImg(VulkanDevice &dev, const QSize &sz, VulkanOverlayCtx &ctx);
  ~VulkanOverlayImg();

  void init();
  void destroy();

  void resize(const QSize &sz);

  QPaintDevice& qPaintDev();
  const VkDescriptorImageInfo* descriptorInfo() const;

  VkSemaphore getCompleteSemaphore() const;

  void flush();
  void prepare();
  void clear();

  void yank(size_t frameSrc, size_t frameDst);

  bool isZeroCopy() const;



  //IMPLEMENT
private:
  VulkanDevice &dev;
  VulkanOverlayCtx &ctx;

  VulkanSemaphore imagePrepared;
  VulkanSemaphore completeRendering;
  VulkanTexture texture;
  GlExternalTexture glTex;
  QSize size;
  GLuint glPrepared;
  GLuint glComplete;

  GLuint fbo;
  GLuint renderbuff;

  std::unique_ptr<QOpenGLPaintDevice> paintDev;

  void destroyGlFbo();

  void initTexture(const QSize &sz, VulkanTexture &tex);
  void initFbo(const QSize &sz);
};

#endif // VULKANOVERLAYIMG_HPP
