#ifndef KURISUOFFSCRRENDERER_HPP
#define KURISUOFFSCRRENDERER_HPP

#include "../KurisuRenderer_global.hpp"
#include "../Vulkan/VulkanFbo.hpp"
#include "../Vulkan/VulkanBaseRenderpass.hpp"
#include <vector>

class KURISURENDERER_SHARED_EXPORT KurisuOffscrRenderer
{
public:
  virtual VulkanBaseRenderpass &getRp(size_t device_index) const = 0;
};

#endif // KURISUOFFSCRRENDERER_HPP
