#ifndef KURISUTEXTUREHANDLE_HPP
#define KURISUTEXTUREHANDLE_HPP

#include <KawaiiRenderer/Textures/KawaiiTextureHandle.hpp>
#include "../KurisuRenderer_global.hpp"
#include "Vulkan/VulkanTexture.hpp"

class KurisuRootImpl;

class KURISURENDERER_SHARED_EXPORT KurisuTextureHandle : public KawaiiTextureHandle
{
  KurisuTextureHandle(const KurisuTextureHandle &orig) = delete;
  KurisuTextureHandle& operator=(const KurisuTextureHandle &orig) = delete;

public:
  KurisuTextureHandle(KurisuRootImpl &root, KawaiiTexture *model);
  ~KurisuTextureHandle();

  VulkanTexture &getTexture(size_t device_index);
  VulkanTexture &getFboTexture(size_t device_index);

  bool isAttachable() const;

  void askRecreate();

  int getWidth() const;
  int getHeight() const;
  int getDepth() const;

  // KawaiiTextureHandle interface
public:
  void setData1D(int width, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr) override final;
  void setData1D(int width, int layers, KawaiiTextureFormat fmt, const float *ptr) override final;
  void setData2D(int width, int heigh, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr) override final;
  void setData2D(int width, int heigh, int layers, KawaiiTextureFormat fmt, const float *ptr) override final;
  void setData3D(int width, int heigh, int depth, KawaiiTextureFormat fmt, const uint8_t *ptr) override final;
  void setData3D(int width, int heigh, int depth, KawaiiTextureFormat fmt, const float *ptr) override final;
  void setDataCube(int width, int heigh, int layers, KawaiiTextureFormat fmt, const uint8_t *ptr) override final;
  void setDataCube(int width, int heigh, int layers, KawaiiTextureFormat fmt, const float *ptr) override final;

  void setMinFilter(KawaiiTextureFilter filter) override final;
  void setMagFilter(KawaiiTextureFilter filter) override final;
  void setWrapModeS(KawaiiTextureWrapMode mode) override final;
  void setWrapModeT(KawaiiTextureWrapMode mode) override final;
  void setWrapModeR(KawaiiTextureWrapMode mode) override final;

  void setCompareOperation(KawaiiDepthCompareOperation op) override;



  //IMPLEMENT
private:
  enum class StorageType: uint8_t {
    Data1D,
    Data2D,
    Data3D,
    None
  };

  KurisuRootImpl &r;

  std::vector<VulkanTexture> textures;
  std::vector<VulkanTexture> fboTextures;

  int w;
  int h;
  int d;

  KawaiiTextureFormat storageFmt;
  StorageType storageType;
  bool mipmap_needed;
  bool attachable;

  bool checkMipmapIsNeeded(KawaiiTextureFilter filter) const;

  bool checkStorageCompatibility(int width, KawaiiTextureFormat fmt) const;
  bool checkStorageCompatibility(int width, int height, KawaiiTextureFormat fmt) const;
  bool checkStorageCompatibility(int width, int height, int depth, KawaiiTextureFormat fmt) const;
};

#endif // KURISUTEXTUREHANDLE_HPP
