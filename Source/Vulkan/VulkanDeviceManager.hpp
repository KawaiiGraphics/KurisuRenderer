#ifndef VULKANHELPER_HPP
#define VULKANHELPER_HPP

#include "VulkanDevice.hpp"
#include <QByteArrayList>
#include <QSize>

#include <vector>
#include <string>
#include <unordered_set>

class VulkanDeviceManager
{
public:
  VulkanDeviceManager(VkInstance instance, const std::vector<std::string> &device_extensions,
                      const std::vector<std::string> &optional_device_extensions);
  ~VulkanDeviceManager();

  VulkanDevice &device(size_t i);
  const VulkanDevice &device(size_t i) const;

  const std::vector<VulkanDevice> &allDevices() const;

private:
  static VulkanDevice::QueueFamilyIndices findQueueFamilies(VkPhysicalDevice device);

  VkInstance inst;

  std::vector<VulkanDevice> devices;

  const std::vector<std::string> extensions;
  const std::vector<const char*> extensions_cstr;

  const std::unordered_set<std::string> optional_extensions;

  void createDevices();
};

#endif // VULKANHELPER_HPP
