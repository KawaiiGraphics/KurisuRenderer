#ifndef KURISUGPUBUFIMPL_HPP
#define KURISUGPUBUFIMPL_HPP

#include <KawaiiRenderer/KawaiiGpuBufImpl.hpp>
#include "KurisuBufferHandle.hpp"
#include <unordered_map>
#include <qopengl.h>
#include <QPointer>

class KURISURENDERER_SHARED_EXPORT KurisuGpuBufImpl : public KawaiiGpuBufImpl
{
public:
  KurisuGpuBufImpl(KawaiiGpuBuf *model);
  ~KurisuGpuBufImpl();

  // KawaiiGpuBufImpl interface
  void bindTexture(const QString &fieldName, KawaiiTexture *tex);

  // KawaiiRendererImpl interface
private:
  void initConnections() override final;



  //IMPLEMENT
private:
  std::unordered_map<std::string, QMetaObject::Connection> onBindedTexHandleChanged;

  void rebindTextures();
  KurisuBufferHandle *handle();
};

#endif // KURISUGPUBUFIMPL_HPP
