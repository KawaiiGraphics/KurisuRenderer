#include "KurisuGlPaintedLayerImpl.hpp"

KurisuGlPaintedLayerImpl::KurisuGlPaintedLayerImpl(KawaiiGlPaintedLayer *model):
  KawaiiImageSourceImpl(model),
  KurisuGlInteropLayer(model),
  KawaiiGlPaintedLayerImpl(model)
{
  ctxGetter = std::bind(&KurisuGlPaintedLayerImpl::getGlCtx, this);
}

void KurisuGlPaintedLayerImpl::refreshPixmaps()
{
  KurisuGlInteropLayer::refreshPixmaps();
}

QOpenGLContext *KurisuGlPaintedLayerImpl::getGlCtx()
{
  getCurrentOverlayImg().prepare();
  getCurrentOverlayImg().clear();
  return QOpenGLContext::currentContext();
}
