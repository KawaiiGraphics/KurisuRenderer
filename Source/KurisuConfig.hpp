#ifndef KURISUCONFIG_HPP
#define KURISUCONFIG_HPP

#include <memory>
#include <vector>
#include <QReadWriteLock>
#include "Vulkan/VulkanDeviceManager.hpp"

class KurisuConfig
{
public:
  KurisuConfig(const VulkanDeviceManager &devices);
  ~KurisuConfig();

  uint64_t getComposerGpu() const;
  const std::vector<uint64_t> &getUsingGpus() const;
  static uint64_t getMaxCachedStagingBuffers();
  static bool isGlInteropAllowed();


  //IMPLEMENT
private:
  std::vector<uint64_t> usingGpus;
  uint64_t composerGpu;

  struct StaticConfig {
    std::vector<uint64_t> usingGpus;
    uint64_t preferredComposerGpu;
    uint64_t maxCachedStagingBuffers;
    bool glInterop;

    StaticConfig(const QStringList &usingGpus, uint64_t composerGpu, uint64_t maxCachedStagingBuffers, bool glInterop);
  };

  static StaticConfig sharedCfg;
};

#endif // KURISUCONFIG_HPP
