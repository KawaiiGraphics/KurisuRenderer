#ifndef VULKANDESCRIPTORPOOL_HPP
#define VULKANDESCRIPTORPOOL_HPP

#include "VulkanDevice.hpp"
#include "VulkanBuffer.hpp"
#include <unordered_map>
#include <QObject>
#include <list>

bool operator<(const VkDescriptorBufferInfo &a, const VkDescriptorBufferInfo &b);
bool operator<(const VkDescriptorImageInfo &a, const VkDescriptorImageInfo &b);

class VulkanDescriptorPool: public QObject
{
  Q_OBJECT

  VulkanDescriptorPool(const VulkanDescriptorPool &) = delete;
  VulkanDescriptorPool& operator=(const VulkanDescriptorPool &) = delete;

public:
  struct BufBinding {
    VulkanBuffer *buf;
    VkDescriptorType descriptorType;
    uint32_t bindingPoint;
  };

  struct ImgBinding {
    VkDescriptorImageInfo descrImg;
    VkDescriptorType descriptorType;
    uint32_t bindingPoint;
  };

  VulkanDescriptorPool(VulkanDevice &dev, size_t frame_index, size_t frame_count);
  ~VulkanDescriptorPool();

  void createDescriptorSet(uint64_t name, const std::vector<BufBinding> &bufBindings, const std::vector<ImgBinding> &imgBindings, VkDescriptorSetLayout layout, const std::unordered_map<VkDescriptorType, uint32_t> &descriptorCount);
  VkDescriptorSet getDescriptorSet(uint64_t name);
  void releaseDescriptorSet(uint64_t name);
  void forgetUsedBuffers();
  void useDescriptorSet(uint64_t name);
  void collectGarbage();

  void prepareResources();

  void setUsedInGpu();
  void setUnusedInGpu();

signals:
  void descriptorSetUpdated(quint64 name);
  void recreated();



  //IMPLEMENT
private:
  struct DescrSetInfo {
    std::vector<VkWriteDescriptorSet> writes;
    VkDescriptorSetLayout layout;
    std::unordered_map<VkDescriptorType, uint32_t> descriptorCount;
    std::vector<std::list<VkDescriptorBufferInfo>::iterator> usedBufs;
    std::vector<std::list<VkDescriptorImageInfo>::iterator> usedImgs;

    VkDescriptorSet descrSet;
    uint32_t uses;

    inline DescrSetInfo():
      descrSet(VK_NULL_HANDLE),
      uses(1)
    {}

  };

  VulkanDevice &dev;

  size_t frame_index;
  size_t frame_count;

  //needed to keep each item of InternalDescrSetCreator::writes valid
  std::list<VkDescriptorBufferInfo> descrBufInfo;
  std::list<VkDescriptorImageInfo> descrImgInfo;

  std::unordered_map<uint64_t, DescrSetInfo> descrSets;
  std::vector<VulkanBuffer*> usedBuffers;

  VkDescriptorPool pool;

  bool dirty;

  void destroyPool();
  void recreateDescriptorSets();
  void updateDescriptorSet(uint64_t name, DescrSetInfo &descrSet, const std::vector<BufBinding> &bufBindings, const std::vector<ImgBinding> &imgBindings, VkDescriptorSetLayout layout, const std::unordered_map<VkDescriptorType, uint32_t> &descriptorCount);
};

#endif // VULKANDESCRIPTORPOOL_HPP
