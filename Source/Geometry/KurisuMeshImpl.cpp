#include "KurisuMeshImpl.hpp"
#include "KurisuRootImpl.hpp"
#include <algorithm>

KurisuMeshImpl::KurisuMeshImpl(KawaiiMesh3D *model):
  KawaiiMeshImpl(model)
{
}

KurisuMeshImpl::~KurisuMeshImpl()
{
  preDestruct();
}

void KurisuMeshImpl::drawIndirect(std::vector<VulkanBuffer> &indirectDrawBuf)
{
  auto &cmd = root()->getActiveCommands();
  const size_t device_index = root()->getActiveDevice();

  cmd.bindVbos(0, {&static_cast<KurisuBufferHandle*>(getBaseAttrBuf())->buffer(device_index)}, {0});
  cmd.bindIndexBuffer(static_cast<KurisuBufferHandle*>(getIndicesBuffer())->buffer(device_index));

  cmd.drawIndexedIndirect(indirectDrawBuf[device_index], 0);
}

void KurisuMeshImpl::draw() const
{
  drawInstanced(1);
}

void KurisuMeshImpl::drawInstanced(size_t instances) const
{
  if(getModel()->getRawTriangles().empty()) return;

  auto &cmd = root()->getActiveCommands();
  const size_t device_index = root()->getActiveDevice();

  cmd.bindVbos(0, {&static_cast<KurisuBufferHandle*>(getBaseAttrBuf())->buffer(device_index)}, {0});
  cmd.bindIndexBuffer(static_cast<KurisuBufferHandle*>(getIndicesBuffer())->buffer(device_index));

  cmd.drawIndexed(3 * getModel()->trianglesCount(), instances, 0, 0);
}

void KurisuMeshImpl::createVertexArray()
{
}

void KurisuMeshImpl::deleteVertexArray()
{
}

void KurisuMeshImpl::bindBaseAttr()
{
}

void KurisuMeshImpl::bindIndices()
{
}

KurisuRootImpl *KurisuMeshImpl::root() const
{
  return static_cast<KurisuRootImpl*>(getRoot());
}
