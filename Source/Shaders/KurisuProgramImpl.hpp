#ifndef KURISUPROGRAMIMPL_HPP
#define KURISUPROGRAMIMPL_HPP

#include "../KurisuRenderer_global.hpp"
#include "Vulkan/VulkanPipeline.hpp"

#include <KawaiiRenderer/Shaders/KawaiiProgramImpl.hpp>
#include <glslang/Public/ShaderLang.h>
#include <unordered_map>
#include <vector>

class KurisuRootImpl;
class KurisuShaderImpl;
class KURISURENDERER_SHARED_EXPORT KurisuProgramImpl : public KawaiiProgramImpl
{
public:
  KurisuProgramImpl(KawaiiProgram *model);
  ~KurisuProgramImpl();

  // KawaiiProgramImpl interface
public:
  bool use() const override final;

private:
  void createProgram() override final;
  void deleteProgram() override final;
  void reAddShader(KawaiiShaderImpl *shader) override final;



  //IMPLEMENT
private:
  void linkStage(VkShaderStageFlagBits stage, EShLanguage kind);
  void linkProg();

  struct PipelineInfo {
    VulkanPipeline pipeline;
    std::unordered_map<VkShaderStageFlagBits, std::unique_ptr<VulkanShader>> stages;

    PipelineInfo(const VulkanDevice &dev, VulkanPipelineLayout &layout);

    void setStageCode(VkShaderStageFlagBits stage, const std::vector<uint32_t> &code);
    void removeStage(VkShaderStageFlagBits stage);
  };

  std::vector<std::unique_ptr<PipelineInfo>> pipelines;

  KurisuRootImpl* rootImpl() const;

  QString getName() const;

  void dumpShaders() const;

  void invalidatePipelines();

  std::vector<uint32_t> compileStage(const std::vector<KurisuShaderImpl*> &shaders, EShLanguage kind);

  void onModelCullModeChanged(KawaiiProgram::CullMode cullMode);
};

#endif // KURISUPROGRAMIMPL_HPP
