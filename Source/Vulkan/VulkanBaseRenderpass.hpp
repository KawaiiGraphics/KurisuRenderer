#ifndef VULKANBASERENDERPASS_HPP
#define VULKANBASERENDERPASS_HPP

#include "VulkanTexture.hpp"
#include <QColor>
#include <vector>
#include <QRect>

class VulkanBaseRenderpass
{
  VulkanBaseRenderpass(const VulkanBaseRenderpass &orig) = delete;
  VulkanBaseRenderpass& operator= (const VulkanBaseRenderpass &orig) = delete;

public:
  VulkanBaseRenderpass(VulkanDevice &dev);
  VulkanBaseRenderpass(VulkanDevice &dev, VkFormat colorFmt);
  VulkanBaseRenderpass(VulkanDevice &dev, const std::vector<std::pair<VulkanTexture*, int>> &renderBufs);
  VulkanBaseRenderpass(VulkanBaseRenderpass &&orig);
  VulkanBaseRenderpass& operator=(VulkanBaseRenderpass &&orig);
  virtual ~VulkanBaseRenderpass();

  VulkanDevice &getDevice() const;

  VkRenderPass getRenderpass();

  uint32_t addAttachement(const VkAttachmentDescription &att, const VkClearValue &clearValue);
  void addInputAttachment(uint32_t attachmentIndex, VkImageLayout imgLayout, size_t subpass);
  void addColorAttachment(uint32_t attachmentIndex, VkImageLayout imgLayout, size_t subpass);
  void addPreserveAttachment(uint32_t attachmentIndex, size_t subpass);

  size_t appendSubpass(const VkAttachmentReference &colorAttachment, const VkAttachmentReference &depthAttachment);
  void prependSubpass(const VkAttachmentReference &colorAttachment, const VkAttachmentReference &depthAttachment);
  void addSubpassDependency(const VkSubpassDependency &dependency);

  VkAttachmentDescription& getAttachment(size_t i);
  const VkAttachmentDescription& getAttachment(size_t i) const;

  const VkAttachmentReference& getInputAttachmentRef(uint32_t subpass, uint32_t inputAttachmentIndex);

  const std::vector<VkClearValue> &getClearValues() const;

protected:
  VulkanDevice *dev;
  VkRenderPass renderPass;

  std::vector<VkSubpassDependency> dependencies;
  std::vector<VkAttachmentDescription> attachments;
  std::vector<VkClearValue> clearValues;

  std::vector<std::vector<VkAttachmentReference>> inputAttachments;
  std::vector<std::vector<VkAttachmentReference>> colorAttachments;
  std::vector<std::vector<uint32_t>> preserveAttachments;
  std::vector<VkAttachmentReference> depthAttachments;

  std::vector<VkSubpassDescription> subpasses;

  VkRenderPassCreateInfo createInfo;

  bool dirty;

  void onSubpassesRelocated();
  void onAttachementsRelocated();
  void onDependenciesRelocated();

  void destroyResources();
};

#endif // VULKANBASERENDERPASS_HPP
