#ifndef KAWAIIKURISUFACTORY_HPP
#define KAWAIIKURISUFACTORY_HPP

#include <Kawaii3D/KawaiiImplFactory.hpp>
#include "KurisuRenderer_global.hpp"

class KURISURENDERER_SHARED_EXPORT KawaiiKurisuFactory : public KawaiiImplFactory
{
  KawaiiKurisuFactory();
  ~KawaiiKurisuFactory() = default;

public:
  static KawaiiImplFactory* getInstance();
  static void deleteInstance();



  //IMPLEMENT
private:
  static KawaiiKurisuFactory *instance;
  static void createInstance();
};

#endif // KAWAIIKURISUFACTORY_HPP
