#include "KurisuShaderEffectImpl.hpp"
#include "KurisuRenderpassImpl.hpp"
#include "KurisuRootImpl.hpp"

KurisuShaderEffectImpl::KurisuShaderEffectImpl(KawaiiShaderEffect *model):
  KawaiiImageSourceImpl(model),
  KurisuImageSourceImpl(model),
  KawaiiShaderEffectImpl(model),
  root(nullptr)
{
}

KurisuShaderEffectImpl::~KurisuShaderEffectImpl()
{
  preDestruct();
}

void KurisuShaderEffectImpl::drawQuad()
{
  auto effect = static_cast<KawaiiShaderEffect*>(getModel());
  if(!effect->getEffect()->getVertexShaders().empty())
    {
      // todo: bind quad VBO
    }
  if(Q_LIKELY(root))
    root->getActiveCommands().draw(6, 1, 0, 0);
}

void KurisuShaderEffectImpl::writeCmdBuf(RenderingCommands &cmdBuf, size_t dev_index)
{
  if(Q_LIKELY(root))
    {
      root->activateCommands(&cmdBuf, dev_index);
      KawaiiShaderEffectImpl::directDraw();
      root->activateCommands(nullptr, dev_index);
    }
}

void KurisuShaderEffectImpl::initConnections()
{
  auto modelRoot = KawaiiRoot::getRoot(getModel());
  if(Q_UNLIKELY(!modelRoot)) return;

  auto newRoot = static_cast<KurisuRootImpl*>(modelRoot->getRendererImpl());
  if(newRoot != root)
    {
      disconnect(onRootBufferBindingChanged);
      onRootBufferBindingChanged = connect(newRoot, &KurisuRootImpl::bufferBindingChanged, this, [this] {
          refreshNeeded = true;
        });
      root = newRoot;
    }
}
