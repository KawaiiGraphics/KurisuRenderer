#ifndef VULKANGBUF_HPP
#define VULKANGBUF_HPP

#include "VulkanDevice.hpp"
#include <Kawaii3D/Renderpass/KawaiiRenderpass.hpp>
#include <variant>
#include <cstring>
#include <QRect>

class VulkanGBuf
{
  VulkanGBuf(const VulkanGBuf &orig) = delete;
  VulkanGBuf& operator=(const VulkanGBuf &orig) = delete;

public:
  VulkanGBuf(VulkanDevice *dev);
  VulkanGBuf(VulkanGBuf &&orig);
  VulkanGBuf& operator=(VulkanGBuf &&orig);
  ~VulkanGBuf();

  void init(const KawaiiRenderpass::GBuffer &gbuf, uint32_t w, uint32_t h, VkImageUsageFlags usage, bool exportMemoryHandle);
  void allocateMem();
  void importMem(VulkanDevice::Handle handle, VkExternalMemoryHandleTypeFlagBits type);
  void importMem(const VulkanGBuf &orig);
  void pointToMem(VkDeviceMemory memory, VkDeviceSize offset);
  VulkanDevice::Handle getHandle(VkExternalMemoryHandleTypeFlagBits type) const;
  VkMemoryRequirements getMemoryRequirements() const;

  VkImage getImg() const;
  VkImageView getImgView();

  VkDeviceMemory getMem() const;

  bool isLinear() const;
  void setLinear(bool newLinear);

  VkImageSubresourceRange getImageSubresourceRange() const;

  VulkanDevice *getDevice() const;

  std::vector<float> readF (const QRect &reg);
  std::vector<int32_t> readI (const QRect &reg);
  std::vector<uint32_t> readU (const QRect &reg);

  static VkFormat trFormat(KawaiiRenderpass::InternalImgFormat fmt);



  //IMPLEMENT
private:
  VulkanDevice *dev;

  VkImage img;
  VkDeviceMemory mem;
  VkDeviceSize memOffset;
  VkImageView imgView;

  VkFormat fmt;
  VkImageAspectFlags imgAspect;

  struct StagingImg
  {
    VkImage img;
    VkExtent2D extent;
    VkDeviceSize memOffset;
    VkSubresourceLayout subresLayout;

    StagingImg();
    bool requireExtent(VulkanDevice *dev, const VkExtent2D &minExtent, VkFormat fmt);
    void increaseMemReq(VulkanDevice *dev, VkMemoryRequirements &req);
    void bindMemory(VkDevice vkDev, VkDeviceMemory mem);
    void prepareReadImg(VulkanDevice *dev, VkImage fromImg, const QRect &reg);

    template<typename T>
    std::vector<T> readImg(VulkanDevice *dev, VkImage fromImg, const QRect &reg, std::byte *stagingPtr)
    {
      prepareReadImg(dev, fromImg, reg);
      std::byte *p = stagingPtr + memOffset + subresLayout.offset;
      const size_t lineSz = 4ul*reg.width();
      std::vector<T> result(lineSz*reg.height());
      for(int y = 0; y < reg.height(); ++y)
        {
          std::memcpy(result.data() + lineSz*y, p, lineSz*sizeof(T));
          p += subresLayout.rowPitch;
        }
      return result;
    }
  };
  StagingImg stagingImgF;
  StagingImg stagingImgU;
  StagingImg stagingImgI;

  struct StagingDepth
  {
    VkBuffer buf;
    VkDeviceSize sz;
    VkDeviceSize memOffset;

    StagingDepth();
    bool requireSize(VulkanDevice *dev, VkDeviceSize minSize);
    void increaseMemReq(VulkanDevice *dev, VkMemoryRequirements &req);
    void bindMemory(VkDevice vkDev, VkDeviceMemory mem);
    void readImg(VulkanDevice *dev, VkImage fromImg, const QRect &reg);
  };
  StagingDepth stagingDepth;

  VkDeviceMemory stagingMem;
  std::byte *stagingPtr;

  bool linear;
  bool ownsMem;

  void bindMem();
  void destroy();

  void recreateStagingMem();

  std::variant<std::vector<float>, std::vector<uint32_t>, std::vector<int32_t>>
  readRegion(const QRect &reg);

  std::variant<std::vector<float>, std::vector<uint32_t>, std::vector<int32_t>>
  readColorRegion(const QRect &reg);

  std::vector<float> readDepthRegion(const QRect &reg);
};

#endif // VULKANGBUF_HPP
