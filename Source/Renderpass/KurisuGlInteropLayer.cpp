#include "KurisuGlInteropLayer.hpp"
#include "KurisuRenderpassImpl.hpp"
#include "KurisuRootImpl.hpp"

KurisuGlInteropLayer::KurisuGlInteropLayer(KawaiiImageSource *model):
  KawaiiImageSourceImpl(model),
  KurisuImageSourceImpl(model)
{
  setPixmapsCacheble(true);
  needsGBufs = false;
}

KurisuGlInteropLayer::~KurisuGlInteropLayer()
{
  preDestruct();
  overlayImg.clear();
  pipeline.clear();
}

VulkanOverlayImg &KurisuGlInteropLayer::getCurrentOverlayImg() const
{
  auto *rp = getModel()->getOwner();
  auto *rp_impl = static_cast<KurisuRenderpassImpl*>(rp->getRendererImpl());
  return *overlayImg[rp_impl->getActiveDevice()];
}

void KurisuGlInteropLayer::drawQuad()
{ }

bool KurisuGlInteropLayer::loadPixmaps()
{
  auto *rp = getModel()->getOwner();
  auto *rp_impl = static_cast<KurisuRenderpassImpl*>(rp->getRendererImpl());
  const size_t activeDevice = rp_impl->getActiveDevice();
  return painted[activeDevice];
}

void KurisuGlInteropLayer::onRpDraw(size_t activeDevice)
{
  Q_ASSERT(!overlayImg.empty());
  directDraw();
  overlayImg[activeDevice]->flush();
  painted[activeDevice] = true;
}

void KurisuGlInteropLayer::writeCmdBuf(RenderingCommands &cmdBuf, size_t dev_index)
{
  auto *rp = getModel()->getOwner();
  auto *rp_impl = static_cast<KurisuRenderpassImpl*>(rp->getRendererImpl());
  auto *root = rp_impl->getRoot();
  const auto sz = QSize(getModel()->getRenderableSize().x,
                        getModel()->getRenderableSize().y);

  painted = std::vector<bool>(root->deviceCount(), false);

  if(overlayImg.empty())
    {
      root->forallDevices([this, &sz] (VulkanDevice &dev) {
          overlayImg.push_back(std::make_unique<VulkanOverlayImg>(dev, sz, VulkanOverlayCtx::globalOverlayCtx()));
          overlayImg.back()->init();
        });
    } else
    overlayImg[dev_index]->resize(sz);

  if(waitSemaphores.empty() && VulkanOverlayCtx::globalOverlayCtx().vulkanInteropSupported())
    {
      root->forallDevices([this, rp_impl] (VulkanDevice&, size_t dev_i) {
          waitSemaphores.emplace_back(rp_impl->getAsyncFrames());
          for(size_t i = 0; i < rp_impl->getAsyncFrames(); ++i)
            waitSemaphores.back()[i] = {overlayImg[dev_i]->getCompleteSemaphore()};
        });
      waitStages = { VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT };
    }

  if(pipeline.empty())
    {
      static const constexpr VkPipelineVertexInputStateCreateInfo vertexInputState = {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .vertexBindingDescriptionCount = 0,
        .pVertexBindingDescriptions = nullptr,
        .vertexAttributeDescriptionCount = 0,
        .pVertexAttributeDescriptions = nullptr
      };

      root->forallDevices([this, root] (VulkanDevice &dev, size_t dev_index) {
        pipeline.push_back(std::make_unique<VulkanPipeline>(dev, root->getOverlayPipelineLayout(dev_index)));
        pipeline.back()->setShaderStages(root->getFlippedOverlayShaders(dev_index));
        pipeline.back()->setVertexInputState(vertexInputState);
        pipeline.back()->setCullMode(VK_CULL_MODE_BACK_BIT);
      });
    }
  VkPipeline vkPipeline;
  vkPipeline = pipeline[dev_index]->getPipeline(&cmdBuf,
                                                rp_impl->getRenderpass(dev_index).getRenderpass(),
                                                getModel()->getSubpass(),
                                                root->getColorBlendAttachmentState(),
                                                root->getDepthTest(),
                                                root->getDepthWrite(),
                                                root->getDepthCompareOp(), {});

  auto &pipelineLayout = root->getOverlayPipelineLayout(dev_index);
  VkDescriptorSetLayout descrSetLayout = pipelineLayout.getDescriptorSetLayouts()[0];

  const uint64_t descrSetName = static_cast<uint64_t>(reinterpret_cast<size_t>(this));
  cmdBuf.createDescriptorSet(descrSetName,
                             {}, { VulkanDescriptorPool::ImgBinding {
                                     .descrImg = *overlayImg[dev_index]->descriptorInfo(),
                                     .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                                     .bindingPoint = 0
                                   } }, descrSetLayout, pipelineLayout.getDescriptorCount(0));
  cmdBuf.bindDescriptorSet(descrSetName, 0, pipelineLayout.getPipelineLayout());
  cmdBuf.bindGraphicsPipeline(vkPipeline);
  cmdBuf.draw(6, 1, 0, 0);
}

void KurisuGlInteropLayer::refreshPixmaps()
{
  for(auto i = painted.begin(); i != painted.end(); ++i)
    *i = false;
}
