#ifndef KURISUTEXLAYERIMPL_HPP
#define KURISUTEXLAYERIMPL_HPP

#include "KurisuImageSourceImpl.hpp"
#include <Kawaii3D/Renderpass/KawaiiTexLayer.hpp>

#include "Vulkan/VulkanPipeline.hpp"

class KURISURENDERER_SHARED_EXPORT KurisuTexLayerImpl: public KurisuImageSourceImpl
{
public:
  KurisuTexLayerImpl(KawaiiTexLayer *model);
  ~KurisuTexLayerImpl();

  // KawaiiImageSourceImpl interface
public:
  void directDraw() override final;

  // KawaiiImageSourceImpl interface
protected:
  void drawQuad() override final;

  // KurisuImageSourceImpl interface
private:
  void writeCmdBuf(RenderingCommands &cmdBuf, size_t dev_index) override final;



  // IMPLEMENT
private:
  std::vector<std::unique_ptr<VulkanPipeline>> pipeline;
  std::vector<VulkanTexture::ViewArray2D> views;
  QMetaObject::Connection onTextureHandleChanged;
  uint32_t layers;
};

#endif // KURISUTEXLAYERIMPL_HPP
