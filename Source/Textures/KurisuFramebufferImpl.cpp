#include "KurisuFramebufferImpl.hpp"
#include "KurisuRootImpl.hpp"
#include "Renderpass/KurisuRenderpassImpl.hpp"
#include "Surfaces/KurisuSurfaceImpl.hpp"
#include "KurisuTextureHandle.hpp"

KurisuFramebufferImpl::KurisuFramebufferImpl(KawaiiFramebuffer *model):
  KawaiiFramebufferImpl(model),
  oldRp(nullptr)
{
  connect(getModel(), &KawaiiFramebuffer::renderpassChanged, this, &KurisuFramebufferImpl::onRenderpassChanged);
}

KurisuFramebufferImpl::~KurisuFramebufferImpl()
{
  preDestruct();
}

VulkanBaseRenderpass &KurisuFramebufferImpl::getRp(size_t device_index) const
{
  return static_cast<KurisuRenderpassImpl*>(getModel()->getRenderpass()->getRendererImpl())->getRenderpass(device_index);
}

namespace {
  bool isCompatible(const std::array<uint64_t, 4> &a, const glm::uvec2 &b)
  {
    return a[1] == b.y
        && a[0] == b.x;
  }
}

void KurisuFramebufferImpl::attachTexture(KawaiiTextureImpl *tex, quint32 gbuf_index, int layer)
{
  auto texHandle = static_cast<KurisuTextureHandle*>(tex->getHandle());
  if(!texHandle->isAttachable())
    {
      tex->getData()->setProperty("UsedInFbo", true);
      texHandle->askRecreate();
      return;
    }

  auto el = cachedTexLayers.find(gbuf_index);
  auto indirectGbufEl = indirectGbufTargets.find(gbuf_index);
  auto *rp_impl = static_cast<KurisuRenderpassImpl*>(getModel()->getRenderpass()->getRendererImpl());
  if(isCompatible(tex->getResolution(), getModel()->getSize()))
    {
      if(indirectGbufEl != indirectGbufTargets.end())
        {
          indirectGbufTargets.erase(indirectGbufEl);
          for(auto &cmd: copyToTex)
            cmd->reset();
        }

      if(el == cachedTexLayers.end())
        el = cachedTexLayers.insert({gbuf_index, std::vector<VulkanTexture::View2D>()}).first;

      el->second.resize(root()->deviceCount());
      root()->forallDevices([&el, texHandle, layer, rp_impl, gbuf_index] (VulkanDevice&, size_t dev_index) {
          auto &tex = texHandle->getFboTexture(dev_index);
          if(layer >= 0)
            {
              el->second[dev_index] = VulkanTexture::View2D(tex, layer);
              rp_impl->setExternalGbuf(dev_index, gbuf_index, el->second[dev_index].getImgView(), tex.getVkImage(), tex.getVkImageLayout(), tex.getImageSubresourceRange(), tex.getVkFormat());
            } else
            rp_impl->setExternalGbuf(dev_index, gbuf_index, tex.getVkImageView(), tex.getVkImage(), tex.getVkImageLayout(), tex.getImageSubresourceRange(), tex.getVkFormat());
        });
    } else
    {
      if(el != cachedTexLayers.end())
        cachedTexLayers.erase(el);

      if(indirectGbufEl == indirectGbufTargets.end())
        indirectGbufEl = indirectGbufTargets.insert({gbuf_index, std::pair<std::vector<VulkanTexture*>, int>({root()->deviceCount(), nullptr}, -1)}).first;

      indirectGbufEl->second.second = layer;
      root()->forallDevices([&indirectGbufEl, texHandle, rp_impl, gbuf_index] (VulkanDevice&, size_t dev_index) {
          indirectGbufEl->second.first[dev_index] = &texHandle->getFboTexture(dev_index);
          rp_impl->setExternalGbuf(dev_index, gbuf_index, VK_NULL_HANDLE, VK_NULL_HANDLE, VK_IMAGE_LAYOUT_UNDEFINED, {}, VK_FORMAT_UNDEFINED);
        });

      for(auto &cmd: copyToTex)
        cmd->reset();
    }
}

void KurisuFramebufferImpl::detachTexture(quint32 gbuf_index)
{
  auto indirectGbufEl = indirectGbufTargets.find(gbuf_index);
  if(indirectGbufEl != indirectGbufTargets.end())
    {
      indirectGbufTargets.erase(indirectGbufEl);
      for(auto &cmd: copyToTex)
        cmd->reset();
    }

  auto el = cachedTexLayers.find(gbuf_index);
  if(el != cachedTexLayers.end())
    cachedTexLayers.erase(el);

  auto *rp_impl = static_cast<KurisuRenderpassImpl*>(getModel()->getRenderpass()->getRendererImpl());
  root()->forallDevices([rp_impl, gbuf_index] (VulkanDevice&, size_t dev_index) {
      rp_impl->setExternalGbuf(dev_index, gbuf_index, VK_NULL_HANDLE, VK_NULL_HANDLE, VK_IMAGE_LAYOUT_UNDEFINED, {}, VK_FORMAT_UNDEFINED);
    });
}

void KurisuFramebufferImpl::destroy()
{
  cachedTexLayers.clear();
  indirectGbufTargets.clear();
  copyToTex.clear();
}

namespace {
  VkImageAspectFlags trImgAspect(KawaiiRenderpass::InternalImgFormat fmt)
  {
    switch(fmt)
      {
      case KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm:
      case KawaiiRenderpass::InternalImgFormat::RGBA8_Snorm:
      case KawaiiRenderpass::InternalImgFormat::RGBA16_F:
      case KawaiiRenderpass::InternalImgFormat::RGBA16_UInt:
      case KawaiiRenderpass::InternalImgFormat::RGBA16_SInt:
      case KawaiiRenderpass::InternalImgFormat::RGBA32_F:
      case KawaiiRenderpass::InternalImgFormat::RGBA32_UInt:
      case KawaiiRenderpass::InternalImgFormat::RGBA32_SInt:

      case KawaiiRenderpass::InternalImgFormat::RG8_Unorm:
      case KawaiiRenderpass::InternalImgFormat::RG8_Snorm:
      case KawaiiRenderpass::InternalImgFormat::RG16_F:
      case KawaiiRenderpass::InternalImgFormat::RG16_UInt:
      case KawaiiRenderpass::InternalImgFormat::RG16_SInt:
      case KawaiiRenderpass::InternalImgFormat::RG32_F:
      case KawaiiRenderpass::InternalImgFormat::RG32_UInt:
      case KawaiiRenderpass::InternalImgFormat::RG32_SInt:

      case KawaiiRenderpass::InternalImgFormat::R8_Unorm:
      case KawaiiRenderpass::InternalImgFormat::R8_Snorm:
      case KawaiiRenderpass::InternalImgFormat::R16_F:
      case KawaiiRenderpass::InternalImgFormat::R16_UInt:
      case KawaiiRenderpass::InternalImgFormat::R16_SInt:
      case KawaiiRenderpass::InternalImgFormat::R32_F:
      case KawaiiRenderpass::InternalImgFormat::R32_UInt:
      case KawaiiRenderpass::InternalImgFormat::R32_SInt:
        return VK_IMAGE_ASPECT_COLOR_BIT;

      case KawaiiRenderpass::InternalImgFormat::Depth16:
      case KawaiiRenderpass::InternalImgFormat::Depth24:
      case KawaiiRenderpass::InternalImgFormat::Depth32:
      case KawaiiRenderpass::InternalImgFormat::Depth16Stencil8:
      case KawaiiRenderpass::InternalImgFormat::Depth24Stencil8:
      case KawaiiRenderpass::InternalImgFormat::Depth32Stencil8:
        return VK_IMAGE_ASPECT_DEPTH_BIT;

      case KawaiiRenderpass::InternalImgFormat::Stencil8:
        return VK_IMAGE_ASPECT_STENCIL_BIT;
      }
    Q_UNREACHABLE();
  }
}

void KurisuFramebufferImpl::activate()
{
  if(!indirectGbufTargets.empty())
    {
      if(copyToTex.empty())
        {
          copyToTex.reserve(root()->deviceCount());
          root()->forallDevices([this] (VulkanDevice &dev) {
            copyToTex.push_back(std::make_unique<RenderingCommands>(dev, 1, 1, true));
          });
        }
      auto *rp = getModel()->getRenderpass();
      auto *rp_impl = static_cast<KurisuRenderpassImpl*>(rp->getRendererImpl());
      for(size_t dev_i = 0; dev_i < copyToTex.size(); ++dev_i)
        if(copyToTex[dev_i]->isEmpty())
          {
            copyToTex[dev_i]->startRecording(VK_NULL_HANDLE, {}, {}, VK_SUBPASS_CONTENTS_INLINE, 0);
            for(auto &i: indirectGbufTargets)
              {
                const VkImageSubresourceLayers subresLayers = {
                  .aspectMask = trImgAspect(rp->getGBuf(i.first).format),
                  .baseArrayLayer = 0,
                  .layerCount = 1
                };
                VkImageSubresourceLayers dstSubresLayers = {
                  .aspectMask = subresLayers.aspectMask,
                  .baseArrayLayer = 0,
                  .layerCount = i.second.first[dev_i]->getArrayLayers()
                };
                if(i.second.second >= 0)
                  {
                    dstSubresLayers.baseArrayLayer = static_cast<uint32_t>(i.second.second);
                    dstSubresLayers.layerCount = 1;
                  }
                copyToTex[dev_i]->blitImage(rp_impl->getGbuf(dev_i, i.first, 0).getImg(), VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                                           i.second.first[dev_i]->getVkImage(), i.second.first[dev_i]->getVkImageLayout(),
                                           { VkImageBlit {
                                               .srcSubresource = subresLayers,
                                               .srcOffsets = { VkOffset3D {0,0,0},
                                                               VkOffset3D {
                                                                 static_cast<int32_t>(rp->getRenderableSize().x),
                                                                 static_cast<int32_t>(rp->getRenderableSize().y),
                                                                 1 } },
                                               .dstSubresource = dstSubresLayers,
                                               .dstOffsets = { VkOffset3D {0,0,0}, VkOffset3D {
                                                                 static_cast<int32_t>(i.second.first[dev_i]->getExtent().width),
                                                                 static_cast<int32_t>(i.second.first[dev_i]->getExtent().height),
                                                                 1 } }
                                             }}, VK_FILTER_NEAREST);
              }
            copyToTex[dev_i]->finishRecording();
          }
    }
  root()->setOffscreenTarget(this);
  auto *rp_impl = static_cast<KurisuRenderpassImpl*>(getModel()->getRenderpass()->getRendererImpl());
  if(!oldRp)
    onRenderpassChanged(getModel()->getRenderpass());
  rp_impl->prepareRendering(root()->getActiveSurface()->getActiveGpu(), 0);
}

void KurisuFramebufferImpl::deactivate()
{
  if(!indirectGbufTargets.empty())
    copyToTex[root()->getActiveDevice()]->exec(0,0, {}, {}, {}, VK_NULL_HANDLE);
  root()->setOffscreenTarget(nullptr);
}

KurisuRootImpl *KurisuFramebufferImpl::root()
{
  return static_cast<KurisuRootImpl*>(getRoot());
}

void KurisuFramebufferImpl::onRenderpassChanged(KawaiiRenderpass *newRenderpass)
{
  if(oldRp == newRenderpass)
    return;

  if(oldRp && oldRp->getRendererImpl())
    static_cast<KurisuRenderpassImpl*>(oldRp->getRendererImpl())->unuseExternalGbufs();

  auto *rp_impl = static_cast<KurisuRenderpassImpl*>(newRenderpass->getRendererImpl());
  getModel()->forallAtachments([this, rp_impl] (const KawaiiFramebuffer::Attachment &att) {
      if(att.layer >= 0)
        {
          auto el = cachedTexLayers.find(att.useGbuf);
          if(el != cachedTexLayers.end())
            root()->forallDevices([&att, &el, rp_impl] (VulkanDevice&, size_t dev_index) {
                rp_impl->setExternalGbuf(dev_index, att.useGbuf,
                                         el->second[dev_index].getImgView(),
                                         el->second[dev_index].getTexture()->getVkImage(),
                                         el->second[dev_index].getTexture()->getVkImageLayout(),
                                         el->second[dev_index].getTexture()->getImageSubresourceRange(),
                                         el->second[dev_index].getTexture()->getVkFormat());
              });
          else
            root()->forallDevices([&att, rp_impl] (VulkanDevice&, size_t dev_index) {
                rp_impl->setExternalGbuf(dev_index, att.useGbuf, VK_NULL_HANDLE, VK_NULL_HANDLE, VK_IMAGE_LAYOUT_UNDEFINED, {}, VK_FORMAT_UNDEFINED);
              });
        } else
        {
          auto texHandle = static_cast<KurisuTextureHandle*>(static_cast<KawaiiTextureImpl*>(att.target->getRendererImpl())->getHandle());
          root()->forallDevices([&att, rp_impl, texHandle] (VulkanDevice&, size_t dev_index) {
              rp_impl->setExternalGbuf(dev_index, att.useGbuf,
                                       texHandle->getFboTexture(dev_index).getVkImageView(),
                                       texHandle->getFboTexture(dev_index).getVkImage(),
                                       texHandle->getFboTexture(dev_index).getVkImageLayout(),
                                       texHandle->getFboTexture(dev_index).getImageSubresourceRange(),
                                       texHandle->getFboTexture(dev_index).getVkFormat());
            });
        }
    });
  for(auto &cmd: copyToTex)
    cmd->reset();

  oldRp = newRenderpass;
  rp_impl->setSharedGbufs({});
  rp_impl->setAsyncFrames(1);
}
