#include "VulkanPipeline.hpp"
#include "RenderingCommands.hpp"

#include <Kawaii3D/Shaders/KawaiiShader.hpp>
#include <QVector4D>
#include <QVector3D>
#include <cstring>
#include <stdexcept>
#include <array>

namespace {
  template<typename T>
  bool isArrEqual(size_t aSize, size_t bSize, const T *aArr, const T *bArr)
  {
    if(static_cast<bool>(aArr) != static_cast<bool>(bArr))
      return false;

    if(aSize != bSize)
      return false;

    if(static_cast<bool>(aArr))
      for(size_t i = 0; i < aSize; ++i)
        if(aArr[i] != bArr[i])
          return false;

    return true;
  }

  template<>
  bool isArrEqual(size_t aSize, size_t bSize, const void *aArr, const void *bArr)
  {
    if(static_cast<bool>(aArr) != static_cast<bool>(bArr))
      return false;

    if(aSize != bSize)
      return false;

    return !static_cast<bool>(aArr) || (std::memcmp(aArr, bArr, aSize) == 0);
  }
}

bool operator==(const VkPipelineColorBlendAttachmentState &a, const VkPipelineColorBlendAttachmentState &b)
{
  if(a.blendEnable != b.blendEnable)
    return false;

  if(!a.blendEnable)
    return a.colorWriteMask == b.colorWriteMask;

  return a.alphaBlendOp == b.alphaBlendOp
      && a.colorBlendOp == b.colorBlendOp
      && a.colorWriteMask == b.colorWriteMask
      && a.dstAlphaBlendFactor == b.dstAlphaBlendFactor
      && a.dstColorBlendFactor == b.dstColorBlendFactor
      && a.srcAlphaBlendFactor == b.srcAlphaBlendFactor
      && a.srcColorBlendFactor == b.srcColorBlendFactor;
}

bool operator==(const VkVertexInputBindingDescription &a, const VkVertexInputBindingDescription &b)
{
  return (a.binding == b.binding
     && a.inputRate == b.inputRate
     && a.stride == b.stride);
}

bool operator==(const VkVertexInputAttributeDescription &a, const VkVertexInputAttributeDescription &b)
{
  return (a.binding == b.binding
          && a.format == b.format
          && a.location == b.location
          && a.offset == b.offset);
}

bool operator==(const VkPipelineVertexInputStateCreateInfo &a, const VkPipelineVertexInputStateCreateInfo &b)
{
  if(!isArrEqual(a.vertexAttributeDescriptionCount, b.vertexAttributeDescriptionCount,
                 a.pVertexAttributeDescriptions, b.pVertexAttributeDescriptions))
    return false;

  if(!isArrEqual(a.vertexBindingDescriptionCount, b.vertexBindingDescriptionCount,
                 a.pVertexBindingDescriptions, b.pVertexBindingDescriptions))
    return false;

  return (a.flags == b.flags
          && a.pNext == b.pNext);
}

bool operator==(const VkSpecializationMapEntry &a, const VkSpecializationMapEntry &b)
{
  return a.constantID == b.constantID
      && a.offset == b.offset
      && a.size == b.size;
}

bool operator==(const VkSpecializationInfo &a, const VkSpecializationInfo &b)
{
  return a.dataSize == b.dataSize
      && isArrEqual(a.mapEntryCount, b.mapEntryCount, a.pMapEntries, b.pMapEntries)
      && isArrEqual(a.dataSize, b.dataSize, a.pData, b.pData);
}

VulkanPipeline::VulkanPipeline(const VulkanDevice &dev, VulkanPipelineLayout &layout):
  devInfo(dev),

  vertices {
    VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO, // sType
    nullptr, // pNext
    0, // flags
    1, // vertexBindingDescriptionCount
    &vertexBindingDescription, // pVertexBindingDescriptions
    baseAtributes.size(), // vertexAttributeDescriptionCount
    baseAtributes.data() // pVertexAttributeDescriptions
    },

  inputAssembly {
    VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO, // sType
    nullptr, // pNext
    0, // flags
    VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST, // topology
    VK_FALSE // primitiveRestartEnable
    },

  rasterizer {
    VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO, // sType
    nullptr, // pNext
    0, // flags
    VK_FALSE, // depthClampEnable
    VK_FALSE, // rasterizerDiscardEnable
    VK_POLYGON_MODE_FILL, // polygonMode
    VK_CULL_MODE_NONE, // cullMode
    VK_FRONT_FACE_COUNTER_CLOCKWISE, // frontFace
    VK_FALSE, // depthBiasEnable
    0, // depthBiasConstantFactor
    0, // depthBiasClamp
    0, // depthBiasSlopeFactor
    1 // lineWidth
    },

  multisampling {
    VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO, // sType
    nullptr, // pNext
    0, // flags
    VK_SAMPLE_COUNT_1_BIT, // rasterizationSamples
    VK_FALSE, // sampleShadingEnable
    1, // minSampleShading
    nullptr, // pSampleMask
    VK_FALSE, // alphaToCoverageEnable
    VK_FALSE // alphaToOneEnable
    },

  colorBlending {
    VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO, // sType
    nullptr, // pNext
    0, // flags
    VK_FALSE, // logicOpEnable
    VK_LOGIC_OP_COPY, // logicOp
    0, // attachmentCount
    nullptr, // pAttachments
    {0, 0, 0, 0}// blendConstants[4]
    },

  layout(layout)
{
  onLayoutInvalidate = layout.addOnInvalidate([this] { markDirty(); });
}

VulkanPipeline::~VulkanPipeline()
{
  layout.removeOnInvalidate(onLayoutInvalidate);

  devInfo.waitIdle();
  for(const auto &i: pipelines)
    {
      QObject::disconnect(i.second.onCmdbufDestroyed);
      vkDestroyPipeline(devInfo.vkDev, i.second.vkPipeline, nullptr);
    }
}

VulkanPipeline::Topology VulkanPipeline::getTopology() const
{
  switch(inputAssembly.topology) {
    case VK_PRIMITIVE_TOPOLOGY_POINT_LIST:
      return Topology::Points;

    case VK_PRIMITIVE_TOPOLOGY_LINE_LIST:
      return Topology::Lines;

    case VK_PRIMITIVE_TOPOLOGY_LINE_STRIP:
      return Topology::LineStrip;

    case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST:
      return Topology::Triangles;

    case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP:
      return Topology::TriangleStrip;

    case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN:
      return Topology::TriangleFan;

    case VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY:
      return Topology::LinesWithAdjacency;

    case VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY:
      return Topology::LineStripWithAdjacency;

    case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY:
      return Topology::TrianglesWithAdjacency;

    case VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY:
      return Topology::TriangleStripWithAdjacency;

    case VK_PRIMITIVE_TOPOLOGY_PATCH_LIST:
      return Topology::Patches;

    default:
      throw std::runtime_error("VulkanPipeline::getTopology: illegal state");
    }
}

void VulkanPipeline::setTopology(VulkanPipeline::Topology topology)
{
  switch(topology)
    {
    case Topology::Points:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
      break;
    case Topology::Lines:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
      break;
    case Topology::LineStrip:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_LINE_STRIP;
      break;
    case Topology::Triangles:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
      break;
    case Topology::TriangleStrip:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP;
      break;
    case Topology::TriangleFan:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN;
      break;
    case Topology::LinesWithAdjacency:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST_WITH_ADJACENCY;
      break;
    case Topology::LineStripWithAdjacency:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_LINE_STRIP_WITH_ADJACENCY;
      break;
    case Topology::TrianglesWithAdjacency:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST_WITH_ADJACENCY;
      break;
    case Topology::TriangleStripWithAdjacency:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP_WITH_ADJACENCY;
      break;
    case Topology::Patches:
      inputAssembly.topology = VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
      break;
    }
  markDirty();
}

void VulkanPipeline::setShaderStages(const std::vector<const VulkanShader *> &stages)
{
  if(shaderStagesSrc != stages)
    {
      shaderStagesSrc = stages;
      shaderStagesInfo.resize(stages.size());
      for(size_t i = 0; i < stages.size(); ++i)
        shaderStagesInfo[i] = stages[i]->stageCreateInfo();

      markDirty();
    }
}

void VulkanPipeline::addShaderStage(const VulkanShader *shader)
{
  shaderStagesSrc.push_back(shader);
  shaderStagesInfo.push_back(shader->stageCreateInfo());
  markDirty();
}

void VulkanPipeline::updateShaderStage(const VulkanShader *shader)
{
  auto el = std::find(shaderStagesSrc.cbegin(), shaderStagesSrc.cend(), shader);
  if(el != shaderStagesSrc.end()) {
      auto el1 = shaderStagesInfo.begin() + (el - shaderStagesSrc.cbegin());
      *el1 = shader->stageCreateInfo();
      markDirty();
    } else
    addShaderStage(shader);
}

void VulkanPipeline::removeShaderStage(const VulkanShader *shader)
{
  auto el = std::find(shaderStagesSrc.cbegin(), shaderStagesSrc.cend(), shader);
  if(el != shaderStagesSrc.end()) {
      shaderStagesInfo.erase(shaderStagesInfo.cbegin() + (el - shaderStagesSrc.cbegin()));
      shaderStagesSrc.erase(el);
      markDirty();
    }
}

void VulkanPipeline::setVertexInputState(const VkPipelineVertexInputStateCreateInfo &state)
{
  if(vertices != state)
    {
      vertices = state;
      markDirty();
    }
}

void VulkanPipeline::setCullMode(VkCullModeFlagBits cullMode)
{
  if(rasterizer.cullMode != cullMode)
    {
      rasterizer.cullMode = cullMode;
      markDirty();
    }
}

VkPipeline VulkanPipeline::getPipeline(RenderingCommands *commands, VkRenderPass renderpass, uint32_t subpass,
                                       const std::vector<VkPipelineColorBlendAttachmentState> &blendState,
                                       bool depthTest, bool depthWrite, VkCompareOp depthFunc,
                                       const std::vector<VkSpecializationInfo> &shaderSpecializationInfo)
{
  const QRectF viewport = commands->getViewport();

  auto el = pipelines.find(commands);
  if(el == pipelines.end())
    {
      el = pipelines.insert(std::pair(commands, SpecializedPipeline {.dirty = true})).first;
      el->second.onCmdbufDestroyed = QObject::connect(commands, &QObject::destroyed,
                                                      [this] (QObject *obj) { remove(obj); });
    }

  if(el->second.dirty
     || el->second.renderpass != renderpass
     || el->second.subpass != subpass
     || el->second.viewport != viewport
     || el->second.colorBlendAttachment != blendState
     || el->second.depthFunc != depthFunc
     || el->second.depthTest != depthTest
     || el->second.depthWrite != depthWrite
     || el->second.shaderSpecializationInfo != shaderSpecializationInfo)
    {
      el->second.renderpass = renderpass;
      el->second.subpass = subpass;
      el->second.viewport = viewport;
      el->second.colorBlendAttachment = blendState;
      el->second.depthFunc = depthFunc;
      el->second.depthTest = depthTest;
      el->second.depthWrite = depthWrite;
      el->second.shaderSpecializationInfo = shaderSpecializationInfo;
      recreate(el->second);
    }

  return el->second.vkPipeline;
}

const VulkanDevice &VulkanPipeline::getDevice() const
{
  return devInfo;
}

namespace {
  VkBool32 vkBool(bool b)
  { return b? VK_TRUE: VK_FALSE; }
}

void VulkanPipeline::recreate(SpecializedPipeline &pipeline)
{
  if(pipeline.vkPipeline) {
      devInfo.waitIdle();
      vkDestroyPipeline(devInfo.vkDev, pipeline.vkPipeline, nullptr);
      pipeline.vkPipeline = VK_NULL_HANDLE;
    }

  const VkViewport vkViewport = {
    .x        = static_cast<float>(pipeline.viewport.x()),
    .y        = static_cast<float>(pipeline.viewport.y()),
    .width    = static_cast<float>(pipeline.viewport.width()),
    .height   = static_cast<float>(pipeline.viewport.height()),
    .minDepth = 0,
    .maxDepth = 1.0f
  };

  const VkRect2D scissor = {
    .offset = VkOffset2D {
      .x = static_cast<int32_t>(vkViewport.x),
      .y = static_cast<int32_t>(vkViewport.y)
    },
    .extent = VkExtent2D {
      .width = static_cast<uint32_t>(vkViewport.width),
      .height = static_cast<uint32_t>(vkViewport.height)
    }
  };

  const VkPipelineViewportStateCreateInfo viewportState = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO, // sType
    .viewportCount = 1, // viewportCount
    .pViewports = &vkViewport, // pViewports
    .scissorCount = 1, // scissorCount
    .pScissors = &scissor // pScissors
  };

  colorBlending.attachmentCount = pipeline.colorBlendAttachment.size();
  colorBlending.pAttachments = pipeline.colorBlendAttachment.data();

  VkPipelineDepthStencilStateCreateInfo depthStencil = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
    .depthTestEnable = vkBool(pipeline.depthTest),
    .depthWriteEnable = vkBool(pipeline.depthWrite),
    .depthCompareOp = pipeline.depthFunc,
    .depthBoundsTestEnable = VK_FALSE,
    .stencilTestEnable = VK_FALSE,
    .minDepthBounds = 0,
    .maxDepthBounds = 1
  };

  if(pipeline.shaderSpecializationInfo.size() == shaderStagesInfo.size())
    for(size_t i = 0; i < shaderStagesInfo.size(); ++i)
      shaderStagesInfo[i].pSpecializationInfo = &pipeline.shaderSpecializationInfo[i];

  const VkGraphicsPipelineCreateInfo createInfo = {
    .sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
    .stageCount = static_cast<uint32_t>(shaderStagesInfo.size()),
    .pStages = shaderStagesInfo.data(),
    .pVertexInputState = &vertices,
    .pInputAssemblyState = &inputAssembly,
    .pTessellationState = nullptr,
    .pViewportState = &viewportState,
    .pRasterizationState = &rasterizer,
    .pMultisampleState = &multisampling,
    .pDepthStencilState = &depthStencil,
    .pColorBlendState = &colorBlending,
    .layout = layout.getPipelineLayout(),
    .renderPass = pipeline.renderpass,
    .subpass = pipeline.subpass,
  };

  VkResult result = vkCreateGraphicsPipelines(devInfo.vkDev, devInfo.pipelineCache, 1, &createInfo, nullptr, &pipeline.vkPipeline);
  if(Q_UNLIKELY(result != VK_SUCCESS))
    throw std::runtime_error("failed to create pipeline!");

  if(pipeline.shaderSpecializationInfo.size() == shaderStagesInfo.size())
    for(size_t i = 0; i < shaderStagesInfo.size(); ++i)
      shaderStagesInfo[i].pSpecializationInfo = nullptr;

  colorBlending.attachmentCount = 0;
  colorBlending.pAttachments = nullptr;
  pipeline.dirty = false;
}

void VulkanPipeline::remove(QObject *owner)
{
  auto el = pipelines.find(static_cast<RenderingCommands*>(owner));
  if(el != pipelines.end())
    {
      if(el->second.vkPipeline)
        {
          devInfo.waitIdle();
          vkDestroyPipeline(devInfo.vkDev, el->second.vkPipeline, nullptr);
        }
      QObject::disconnect(el->second.onCmdbufDestroyed);
      pipelines.erase(el);
    }
}

void VulkanPipeline::markDirty()
{
  for(auto &i: pipelines)
    i.second.dirty = true;
}

