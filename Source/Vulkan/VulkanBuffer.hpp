#ifndef VULKANBUFFER_HPP
#define VULKANBUFFER_HPP

#include "VulkanCommandPool.hpp"
#include "VulkanDevice.hpp"
#include <QObject>
#include <sib_utils/PairHash.hpp>
#include <QReadWriteLock>

class VulkanBuffer
{
  VulkanBuffer(const VulkanBuffer &) = delete;
  VulkanBuffer& operator=(const VulkanBuffer &) = delete;

public:
  class Instance;

  VulkanBuffer(VulkanDevice &dev, VkBufferUsageFlags usage);
  ~VulkanBuffer();

  VulkanBuffer(VulkanBuffer &&orig);
  VulkanBuffer& operator=(VulkanBuffer &&orig);

  bool setData(const void *ptr, size_t length);

  static bool mergeRg(const VkBufferCopy &rgA, VkBufferCopy &rgB);

  void updateSubData(size_t bytes, size_t offset);

  Instance &getDetached(QObject *owner, size_t frame_count);

  VkBuffer getDetachedBuffer(QObject *owner, size_t frameIndex, size_t frame_count);

  void setUsedInGpu(QObject *owner, size_t frame_index);
  void setUnusedInGpu(QObject *owner, size_t frame_index);

  const VkDescriptorBufferInfo* descrInfo(QObject *owner, size_t frameIndex, size_t frame_count);

  void syncDetachedBuffer(QObject *owner, size_t frameIndex);

  VulkanDevice& getDevice() const;

  bool empty() const;

  static QThreadPool &memcpyToGpuBufThreadPool();



  //IMPLEMENT
private:
  VulkanDevice *dev;
  const void *data;
  size_t length;

  std::unordered_map<QObject*, std::unique_ptr<Instance>> detached;
  std::unique_ptr<QReadWriteLock> detachedLock;
  VkBufferUsageFlags usage;
};

class VulkanBuffer::Instance
{
  Instance(const Instance &) = delete;
  Instance& operator=(const Instance &) = delete;

public:
  QMetaObject::Connection onDestroyed;

  Instance(VulkanDevice &dev, size_t frameCount, VkBufferUsageFlags usage, bool useStaging);
  ~Instance();

  Instance(Instance &&orig);
  Instance& operator=(Instance &&orig);

  void setHostData(const void *hostData, size_t length);

  void markDirty(const VkBufferCopy &rg);
  void sendToDevice(size_t frame_index);

  VkBuffer getBuffer(size_t frame_index) const;
  const VkDescriptorBufferInfo* getDescriptor(size_t frame_index) const;

  void setUsedInGpu(size_t frame_index);
  void setUnusedInGpu(size_t frame_index);

  void waitForGpu();

private:
  VulkanDevice *dev;

  VkDeviceMemory stagingMem;
  VkDeviceMemory mem;
  void *mappedMem;

  std::vector<VkDeviceSize> bufOffset;
  std::vector<VkBuffer> stagingBuf;
  std::vector<VkBuffer> buf;
  std::vector<VkDescriptorBufferInfo> descr;

  std::vector<std::vector<VkBufferCopy>> dirtyRg;
  std::unique_ptr<QMutex> dirtyRgLock;

  const void *hostData;
  size_t length;

  QVector<bool> isInGpuUse;

  VkBufferUsageFlags usage;

  void createBuffers(std::vector<VkBuffer> &buf, VkDeviceMemory &mem, VkMemoryPropertyFlags memProperties, VkBufferUsageFlags usage, bool &deviceLocal);
  void init();
  void destroy();
};

#endif // VULKANBUFFER_HPP
