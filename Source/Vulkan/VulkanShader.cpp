#include "VulkanShader.hpp"
#include <QDebug>

VulkanShader::VulkanShader(VkDevice dev, VkShaderStageFlagBits stage):
  dev(dev),
  shader(VK_NULL_HANDLE),
  stage(stage)
{
}

VulkanShader::VulkanShader(VulkanShader &&orig):
  dev(orig.dev),
  shader(orig.shader),
  stage(orig.stage)
{
  orig.shader = VK_NULL_HANDLE;
}

VulkanShader &VulkanShader::operator=(VulkanShader &&orig)
{
  destroy();
  dev = orig.dev;
  shader = orig.shader;
  stage = orig.stage;
  orig.shader = VK_NULL_HANDLE;
  return *this;
}

VulkanShader::~VulkanShader()
{
  destroy();
}

void VulkanShader::setCode(const std::vector<uint32_t> &spirv_bytes)
{
  if(shader)
    {
      vkDeviceWaitIdle(dev);
      vkDestroyShaderModule(dev, shader, nullptr);
    }

  if(!spirv_bytes.empty())
    {
      const VkShaderModuleCreateInfo createInfo = {
        .sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
        .codeSize = 4 * spirv_bytes.size(),
        .pCode = spirv_bytes.data()
      };

      if (vkCreateShaderModule(dev, &createInfo, nullptr, &shader) != VK_SUCCESS)
        throw std::runtime_error("failed to create shader module!");
    } else
    shader = VK_NULL_HANDLE;
}

VulkanShader::operator bool() const
{
  return shader;
}

VkPipelineShaderStageCreateInfo VulkanShader::stageCreateInfo() const
{
  Q_ASSERT(shader);

  return VkPipelineShaderStageCreateInfo {
      VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO, // sType
          nullptr, // pNext
          0, // flags
          stage, // stage
          shader,  // module
          "main", // pName
          nullptr // pSpecializationInfo
    };
}

void VulkanShader::destroy()
{
  if(shader)
    {
      vkDeviceWaitIdle(dev);
      vkDestroyShaderModule(dev, shader, nullptr);
    }
}
