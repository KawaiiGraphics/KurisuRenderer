#version 450 core

const vec2 vertices[6] = vec2[6] (
    vec2(1,1), vec2(1,-1), vec2(-1,-1),
    vec2(-1,-1), vec2(-1,1), vec2(1,1)
);

const vec2 coords[6] = vec2[6] (
    vec2(1,1), vec2(1,0), vec2(0,0),
    vec2(0,0), vec2(0,1), vec2(1,1)
);

layout (constant_id = 0) const float aspectRatio = 1.0; // (tex.height * screen.width) / (tex.width * screen.height)

layout (location = 0) out vec2 texcoord;

void main(void)
{
    texcoord = coords[gl_VertexIndex];
    gl_Position = vec4(vertices[gl_VertexIndex], 0.0, 1.0);
    if(aspectRatio > 1)
        gl_Position.x /= aspectRatio;
    else if(aspectRatio < 1)
        gl_Position.y *= aspectRatio;
}
