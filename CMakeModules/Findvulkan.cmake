if (DEFINED ENV{VULKAN_SDK})
    if(WIN32)
        set(ADDITIONAL_PATHS_INCLUDE "$ENV{VULKAN_SDK}/Include")
        if(CMAKE_SIZEOF_VOID_P EQUAL 8)
            set(ADDITIONAL_PATHS_LIBS
                "$ENV{VULKAN_SDK}/Lib"
                "$ENV{VULKAN_SDK}/Bin"
            )
        elseif(CMAKE_SIZEOF_VOID_P EQUAL 4)
            set(ADDITIONAL_PATHS_LIBS
                "$ENV{VULKAN_SDK}/Lib32"
                "$ENV{VULKAN_SDK}/Bin32"
            )
        endif()
    else()
        set(ADDITIONAL_PATHS_INCLUDE "$ENV{VULKAN_SDK}/include")
        set(ADDITIONAL_PATHS_LIBS "$ENV{VULKAN_SDK}/lib")
    endif()
endif()

find_path(vulkan_INCLUDE_DIR
    NAMES vulkan/vulkan.h
    PATHS ${ADDITIONAL_PATHS_INCLUDE}
)

find_library(vulkan_LIBRARY
    NAMES vulkan vulkan-1
    PATHS ${ADDITIONAL_PATHS_LIBS}
)

if(vulkan_LIBRARY AND vulkan_INCLUDE_DIR)
    set(vulkan_FOUND "YES")
    message(STATUS "Found vulkan: ${vulkan_LIBRARY}; headers: ${vulkan_INCLUDE_DIR}")
else()
    set(vulkan_FOUND "NO")
    message(STATUS "Failed to find vulkan")
endif()

if(vulkan_FOUND AND NOT TARGET vulkan::vulkan)
    add_library(vulkan::vulkan UNKNOWN IMPORTED)
    set_target_properties(vulkan::vulkan PROPERTIES IMPORTED_LOCATION "${vulkan_LIBRARY}" INTERFACE_INCLUDE_DIRECTORIES "${vulkan_INCLUDE_DIR}")
endif()
