#ifndef VULKANPIPELINE_HPP
#define VULKANPIPELINE_HPP

#include "VulkanDevice.hpp"
#include "VulkanShader.hpp"
#include "VulkanPipelineLayout.hpp"

#include <array>
#include <QRectF>
#include <QVector3D>
#include <QVector4D>
#include <QSurfaceFormat>

#include <sib_utils/TupleHash.hpp>
#include <sib_utils/QSizeHash.hpp>

bool operator==(const VkPipelineColorBlendAttachmentState &a, const VkPipelineColorBlendAttachmentState &b);
inline bool operator!=(const VkPipelineColorBlendAttachmentState &a, const VkPipelineColorBlendAttachmentState &b)
{ return !(a==b); }

bool operator==(const VkVertexInputBindingDescription &a, const VkVertexInputBindingDescription &b);
inline bool operator!=(const VkVertexInputBindingDescription &a, const VkVertexInputBindingDescription &b)
{ return !(a==b); }

bool operator==(const VkPipelineVertexInputStateCreateInfo &a, const VkPipelineVertexInputStateCreateInfo &b);
inline bool operator!=(const VkPipelineVertexInputStateCreateInfo &a, const VkPipelineVertexInputStateCreateInfo &b)
{ return !(a==b); }

bool operator==(const VkVertexInputAttributeDescription &a, const VkVertexInputAttributeDescription &b);
inline bool operator!=(const VkVertexInputAttributeDescription &a, const VkVertexInputAttributeDescription &b)
{ return !(a==b); }

bool operator==(const VkSpecializationMapEntry &a, const VkSpecializationMapEntry &b);
inline bool operator!=(const VkSpecializationMapEntry &a, const VkSpecializationMapEntry &b)
{ return !(a==b); }

bool operator==(const VkSpecializationInfo &a, const VkSpecializationInfo &b);
inline bool operator!=(const VkSpecializationInfo &a, const VkSpecializationInfo &b)
{ return !(a==b); }

class RenderingCommands;
class VulkanPipeline
{
  VulkanPipeline(const VulkanPipeline&) = delete;
  VulkanPipeline& operator=(const VulkanPipeline&) = delete;

public:
  enum class Topology {
    Points,
    Lines,
    LineStrip,
    Triangles,
    TriangleStrip,
    TriangleFan,
    LinesWithAdjacency,
    LineStripWithAdjacency,
    TrianglesWithAdjacency,
    TriangleStripWithAdjacency,
    Patches
  };

  inline static constexpr VkVertexInputBindingDescription vertexBindingDescription = {
    .binding = 0,
    .stride = sizeof(QVector4D) + sizeof(QVector3D) + sizeof(QVector3D),
    .inputRate = VK_VERTEX_INPUT_RATE_VERTEX
  };

  inline static constexpr std::array<VkVertexInputAttributeDescription, 3> baseAtributes = {
    VkVertexInputAttributeDescription {
      .location = 0,
      .binding = 0,
      .format = VK_FORMAT_R32G32B32A32_SFLOAT,
      .offset = 0
    },
    VkVertexInputAttributeDescription {
      .location = 1,
      .binding = 0,
      .format = VK_FORMAT_R32G32B32_SFLOAT,
      .offset = sizeof(QVector4D)
    },
    VkVertexInputAttributeDescription {
      .location = 2,
      .binding = 0,
      .format = VK_FORMAT_R32G32B32_SFLOAT,
      .offset = sizeof(QVector4D) + sizeof(QVector3D)
    }
  };

  VulkanPipeline(const VulkanDevice &dev, VulkanPipelineLayout &layout);
  ~VulkanPipeline();

  Topology getTopology() const;
  void setTopology(Topology topology);

  void setShaderStages(const std::vector<const VulkanShader*> &stages);
  void addShaderStage(const VulkanShader *shader);
  void updateShaderStage(const VulkanShader *shader);
  void removeShaderStage(const VulkanShader *shader);

  void setVertexInputState(const VkPipelineVertexInputStateCreateInfo &state);

  void setCullMode(VkCullModeFlagBits cullMode);

  VkPipeline getPipeline(RenderingCommands *commands, VkRenderPass renderpass, uint32_t subpass,
                         const std::vector<VkPipelineColorBlendAttachmentState> &blendState,
                         bool depthTest, bool depthWrite, VkCompareOp depthFunc,
                         const std::vector<VkSpecializationInfo> &shaderSpecializationInfo);

  const VulkanDevice &getDevice() const;

private:
  const VulkanDevice &devInfo;

  VkPipelineVertexInputStateCreateInfo vertices;

  VkPipelineInputAssemblyStateCreateInfo inputAssembly;

  VkPipelineRasterizationStateCreateInfo rasterizer;

  VkPipelineMultisampleStateCreateInfo multisampling;

  VkPipelineColorBlendStateCreateInfo colorBlending;

  struct SpecializedPipeline
  {
    std::vector<VkPipelineColorBlendAttachmentState> colorBlendAttachment;
    std::vector<VkSpecializationInfo> shaderSpecializationInfo;
    QRectF viewport;
    QMetaObject::Connection onCmdbufDestroyed;
    VkPipeline vkPipeline;
    VkRenderPass renderpass;
    uint32_t subpass;
    VkCompareOp depthFunc;
    bool depthTest;
    bool depthWrite;
    bool dirty;
  };
  std::unordered_map<RenderingCommands*, SpecializedPipeline> pipelines;

  std::vector<VkPipelineShaderStageCreateInfo> shaderStagesInfo;
  std::vector<const VulkanShader*> shaderStagesSrc;

  VulkanPipelineLayout &layout;
  VulkanPipelineLayout::OnInvalidateFuncs::const_iterator onLayoutInvalidate;

  void recreate(SpecializedPipeline &pipeline);
  void remove(QObject *owner);

  void markDirty();
};

#endif // VULKANPIPELINE_HPP
