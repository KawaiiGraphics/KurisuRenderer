#ifndef KURISUEXTERNALQRHIRENDER_HPP
#define KURISUEXTERNALQRHIRENDER_HPP

#include <Kawaii3D/KawaiiRendererImpl.hpp>
#include <Kawaii3D/Textures/KawaiiExternalQRhiRender.hpp>

#include "../KurisuRenderer_global.hpp"
#include "KurisuTextureHandle.hpp"

class KurisuRootImpl;

class KURISURENDERER_SHARED_EXPORT KurisuExternalQRhiRender: public KawaiiRendererImpl
{
public:
  KurisuExternalQRhiRender(KawaiiExternalQRhiRender *model);
  ~KurisuExternalQRhiRender();



  //IMPLEMENT
private:
  KurisuRootImpl *root;
  KurisuTextureHandle *tex;

  QMetaObject::Connection onTexHandleChanged;

  std::unique_ptr<QRhiTexture> rhiTex;

  std::vector<VulkanTexture> transferTargets;

  std::unique_ptr<QMutexLocker<QMutex>> graphicsQueueLock;

  void setTexture(KawaiiTexture *newTex);
  void updateRhiTex();

  void trySetRhi();

  void updateRoot();

  void lockGraphicsQueue();
  void unlockGraphicsQueue();
  void sendPostRenderCmds();

  // KawaiiRendererImpl interface
private:
  void initConnections() override final;
};

#endif // KURISUEXTERNALQRHIRENDER_HPP
