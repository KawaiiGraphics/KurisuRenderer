#ifndef KURISUENVMAPIMPL_HPP
#define KURISUENVMAPIMPL_HPP

#include <KawaiiRenderer/Textures/KawaiiEnvMapImpl.hpp>

#include "../Vulkan/VulkanBaseRenderpass.hpp"
#include "../Vulkan/RenderingCommands.hpp"
#include "../Vulkan/VulkanFbo.hpp"
#include "KurisuOffscrRenderer.hpp"
#include "KurisuTextureHandle.hpp"

#include <vector>
#include <memory>

class KURISURENDERER_SHARED_EXPORT KurisuEnvMapImpl : public KawaiiEnvMapImpl,
    public KurisuOffscrRenderer
{
public:
  KurisuEnvMapImpl(KawaiiEnvMap *model);
  ~KurisuEnvMapImpl();

  std::vector<VkFramebuffer> getFbos(size_t device_index) const;

  // KurisuOffscrRenderer interface
public:
  VulkanBaseRenderpass &getRp(size_t device_index) const override final;

  // KawaiiEnvMapImpl interface
protected:
  void updateCache(DrawCache &cache, const QRect &viewport) override final;
  DrawCache *createDrawCache(const QRect &viewport) override final;
  void onRootChanged() override final;

private:
  void attachTexture(KawaiiTextureImpl *tex, KawaiiEnvMap::AttachmentMode mode, int layer) override final;
  void attachDepthRenderbuffer(const QSize &) override final;
  void detachTexture(KawaiiEnvMap::AttachmentMode mode) override final;
  void deleteRenderbuffers() override final;
  void setRenderLayer(size_t layer) override final;



  // IMPLEMENT
private:
  struct CachedRenderpass: KawaiiEnvMapImpl::DrawCache {
    std::vector<std::unique_ptr<RenderingCommands>> cmd;

    CachedRenderpass(KurisuRootImpl *root);
    ~CachedRenderpass();

    void reset(KurisuEnvMapImpl *envmap);
    void draw(KurisuEnvMapImpl *envmap, const QRect &viewport);

    void exec(KurisuEnvMapImpl *envmap);
  };

  struct SceneRenderer {
    VulkanBaseRenderpass rp;

    const QSize extent;
    std::array<VulkanFbo, 6> fbo_layers;

    SceneRenderer(VulkanDevice &dev, std::vector<std::pair<VulkanTexture *, int> > &&textures);
    SceneRenderer(const SceneRenderer&) = delete;
    SceneRenderer& operator=(const SceneRenderer&) = delete;
  };

  QMetaObject::Connection onRootBufferBindingChanged;

  std::vector<std::unique_ptr<SceneRenderer>> sceneRenderers; //one per device

  std::unordered_map<KawaiiEnvMap::AttachmentMode, std::pair<KurisuTextureHandle*, int>> activeAttachements;
  std::vector<std::pair<KurisuTextureHandle*, int>> cachedAttachements;
  size_t layer;

  KurisuRootImpl *root();
  void checkSceneRenderers();
};

#endif // KURISUENVMAPIMPL_HPP
