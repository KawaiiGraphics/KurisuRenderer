#include "KurisuRootImpl.hpp"
#include "Surfaces/KurisuSurfaceImpl.hpp"
#include "Vulkan/GlslCompiler.hpp"
#include "KurisuBufferHandle.hpp"
#include "Textures/KurisuTextureHandle.hpp"

#include <Kawaii3D/KawaiiConfig.hpp>
#include <sib_utils/ioReadAll.hpp>
#include <sib_utils/Strings.hpp>
#include <QLoggingCategory>
#include <QMetaMethod>

namespace {
  const auto deviceExtensions = std::vector<std::string>
      ({"VK_KHR_swapchain"
        , "VK_KHR_get_memory_requirements2" // needed by VK_KHR_dedicated_allocation
        , "VK_KHR_dedicated_allocation"
        , VK_EXT_ROBUSTNESS_2_EXTENSION_NAME
       });

  const auto optionalDeviceExtensions = std::vector<std::string>
      ({"VK_EXT_nested_command_buffer"
        , "VK_NV_linear_color_attachment"
        , "VK_KHR_external_semaphore"
        , "VK_KHR_external_memory"
        //, "VK_EXT_external_memory_host"
      #ifdef Q_OS_UNIX
        , "VK_KHR_external_semaphore_fd"
        , "VK_KHR_external_memory_fd"
        , "VK_EXT_external_memory_dma_buf"
      #elif defined(Q_OS_WINDOWS)
        , "VK_KHR_external_semaphore_win32"
        , "VK_KHR_external_memory_win32"
      #endif
       });

  static const std::initializer_list<std::vector<VkDescriptorSetLayoutBinding>> overlayLayoutBindings = {
    { VkDescriptorSetLayoutBinding {
        0, // binding
        VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, // descriptorType
        1, // descriptorCount
        VK_SHADER_STAGE_FRAGMENT_BIT, // stageFlags
        nullptr // pImmutableSamplers
      }
    }
  };

  const bool allowedMultithreadingRequestUpdate = !qgetenv("WAYLAND_DISPLAY").isEmpty();
}

KurisuRootImpl::KurisuRootImpl(KawaiiRoot *model):
  KawaiiRootImpl(model),
  currentSfc(nullptr),
  currentCmd(nullptr),
  current_device(0),
  surfacesLock(std::make_unique<QReadWriteLock>()),
  activeRp(nullptr),
  activeSubpass(0),
  depthCompareOp(VK_COMPARE_OP_LESS),
  depthTest(true),
  depthWrite(true),
  offscreen(false)
{
  VulkanInstanceFactory instFactory;

  instFactory.extensions = { VK_EXT_DEBUG_UTILS_EXTENSION_NAME };
  instFactory.layers = {"VK_LAYER_KHRONOS_validation", "VK_LAYER_LUNARG_standard_validation"};
  if(KawaiiConfig::getInstance().getDebugLevel() > 1)
    {
      QLoggingCategory::setFilterRules(QStringLiteral("qt.vulkan=true"));
      if(KawaiiConfig::getInstance().getDebugLevel() > 2)
        instFactory.layers.push_back("VK_LAYER_MESA_overlay");
    } else
    QLoggingCategory::setFilterRules(QStringLiteral("qt.vulkan=false"));
  instFactory.extensions << QByteArrayList ({"VK_KHR_external_memory_capabilities", "VK_KHR_external_semaphore_capabilities", "VK_KHR_get_physical_device_properties2"});

  inst = instFactory.create();

  if(!inst)
    throw std::runtime_error("KurisuRootImpl::KurisuRootImpl could not initialize vulkan instance");

  devices = std::make_unique<VulkanDeviceManager>(inst->getVkInstance(), deviceExtensions, optionalDeviceExtensions);
  if(devices->allDevices().empty())
    throw std::runtime_error("KurisuRootImpl::KurisuRootImpl could not initialize any vulkan device");

  config = std::make_unique<KurisuConfig>(*devices);

  connect(&bufBindings, &KurisuBufferBindings::changedBlockBinding, this, &KurisuRootImpl::onChangedBlockBufBinding);

  QByteArray usingDevicesStr;
  for(const auto &dev: devices->allDevices())
    {
      usingDevicesStr.append("; \'");
      usingDevicesStr.append(dev.properties.deviceName);
      usingDevicesStr.append("\'");
    }
  usingDevicesStr.remove(0, 2);
  qDebug().noquote() << "KurisuRenderer:info: Using Vulkan devices: [" << usingDevicesStr << "]";
}

KurisuRootImpl::~KurisuRootImpl()
{
  waitForSurfacesRenderFinished();
  std::vector<std::unique_ptr<QWriteLocker>> l;
  forallDevices([&l] (VulkanDevice &dev) {
    l.push_back(std::make_unique<QWriteLocker>(&dev.resLock));
  });
  scenePipelineLayouts.clear();
  quadVertShaders.clear();
  quadFlippedVertShaders.clear();
  quadFragShaders.clear();
  quadArrayedFragShaders.clear();
  overlayPipelineLayouts.clear();
  for(size_t i = 0; i < commonSampler.size(); ++i)
    vkDestroySampler(devices->device(i).vkDev, commonSampler[i], nullptr);
  l.clear();

  qrhi.reset();
  devices.reset();
  inst.reset();
}

VkSurfaceKHR KurisuRootImpl::getSurface(QWindow *wnd)
{
  auto sfc = inst->getSurfaceForWindow(wnd);
  if(sfc == VK_NULL_HANDLE)
    throw std::runtime_error("Can not get VkSurface");
  return sfc;
}

void KurisuRootImpl::forallDevices(const std::function<void(VulkanDevice &)> &func)
{
  for(size_t i = 0; i < devices->allDevices().size(); ++i)
    func(devices->device(i));
}

void KurisuRootImpl::forallDevices(const std::function<void(VulkanDevice &, size_t)> &func)
{
  for(size_t i = 0; i < devices->allDevices().size(); ++i)
    func(devices->device(i), i);
}

VulkanDevice &KurisuRootImpl::getDevice(size_t device_index) const
{
  return devices->device(device_index);
}

size_t KurisuRootImpl::getComposerDeviceIndex() const
{
  return getConfig().getComposerGpu();
}

VulkanDevice &KurisuRootImpl::getComposerDevice() const
{
  return getDevice(getComposerDeviceIndex());
}

size_t KurisuRootImpl::getDeviceIndex(VulkanDevice *dev) const
{
  for(size_t i = 0; i < deviceCount(); ++i)
    if(&getDevice(i) == dev)
      return i;
# undef max
  return std::numeric_limits<size_t>::max();
}

uint32_t KurisuRootImpl::deviceCount() const
{
  return devices->allDevices().size();
}

VulkanPipelineLayout& KurisuRootImpl::getScenePipelineLayout(size_t device_index)
{
  if(scenePipelineLayouts.empty())
    {
      scenePipelineLayouts.reserve(deviceCount());
      forallDevices([this] (VulkanDevice &i) {
        if(!i.queueFamilies.transferFamily.has_value())
          throw std::runtime_error("Can not create transfer command pool");

        scenePipelineLayouts.emplace_back(i.vkDev);
        scenePipelineLayouts.back().addOnInvalidate([this] {
            if(hasActiveCommands() && (getActiveCommands().hasBindedPipeline() || getActiveCommands().getState() == RenderingCommands::State::Compleate))
              throw std::runtime_error("Pipeline layout invalidated");
          });
      });
    }
  return scenePipelineLayouts[device_index];
}

VulkanPipelineLayout& KurisuRootImpl::getOverlayPipelineLayout(size_t device_index)
{
  if(overlayPipelineLayouts.empty())
    {
      overlayPipelineLayouts.reserve(deviceCount());
      forallDevices([this] (VulkanDevice &i) {
        if(!i.queueFamilies.transferFamily.has_value())
          throw std::runtime_error("Can not create transfer command pool");
        overlayPipelineLayouts.emplace_back(i.vkDev, overlayLayoutBindings);
      });
    }
  return overlayPipelineLayouts[device_index];
}

namespace {
  const std::vector<uint32_t>& getFlippedQuadVsSpirv()
  {
    static std::vector<uint32_t> code;
    if(code.empty())
      code = GlslCompiler().glslToSpirv(sib_utils::ioReadAll(QFile(":/kurisu-glsl/quadFlipped.vs.glsl")).data(), EShLangVertex, ":/kurisu-glsl/quadFlipped.vs.glsl");
    return code;
  }

  const std::vector<uint32_t>& getQuadVsSpirv()
  {
    static std::vector<uint32_t> code;
    if(code.empty())
      code = GlslCompiler().glslToSpirv(sib_utils::ioReadAll(QFile(":/kurisu-glsl/quad.vs.glsl")).data(), EShLangVertex, ":/kurisu-glsl/quad.vs.glsl");
    return code;
  }

  const std::vector<uint32_t>& getQuadFsSpirv()
  {
    static std::vector<uint32_t> code;
    if(code.empty())
      code = GlslCompiler().glslToSpirv(sib_utils::ioReadAll(QFile(":/kurisu-glsl/quad.fs.glsl")).data(), EShLangFragment, ":/kurisu-glsl/quad.fs.glsl");
    return code;
  }

  const std::vector<uint32_t>& getArrayedQuadFsSpirv(uint32_t layersCount)
  {
    static std::vector<uint32_t> code;
    if(code.empty())
      {
        QByteArray glsl = "#version 450 core\n"
                          "layout (set = 0, binding = 0) uniform sampler2DArray inputTex;\n"
                          "layout (location = 0) in vec2 texcoord;\n";

        for(uint32_t i = 0; i < layersCount; ++i)
          glsl += QStringLiteral("layout (location = %1) out vec4 outColor_%1;\n").arg(i).toLatin1();

        glsl += "void main (void) {\n";
        for(uint32_t i = 0; i < layersCount; ++i)
          glsl += QStringLiteral("    outColor_%1 = texture(inputTex, vec3(texcoord, %1));\n").arg(i).toLatin1();
        glsl += "}\n";

        code = GlslCompiler().glslToSpirv(glsl.data(), EShLangFragment, QStringLiteral(":/kurisu-glsl/arrayedQuad_%1.fs.glsl").arg(layersCount).toStdString());
      }
    return code;
  }
}

std::vector<const VulkanShader *> KurisuRootImpl::getOverlayShaders(size_t device_index)
{
  if(quadVertShaders.empty())
    {
      quadVertShaders.reserve(deviceCount());
      forallDevices([this] (VulkanDevice &i) {
        quadVertShaders.emplace_back(i.vkDev, VK_SHADER_STAGE_VERTEX_BIT);
        quadVertShaders.back().setCode(getQuadVsSpirv());
      });
    }
  if(quadFragShaders.empty())
    {
      quadFragShaders.reserve(deviceCount());
      forallDevices([this] (VulkanDevice &i) {
        quadFragShaders.emplace_back(i.vkDev, VK_SHADER_STAGE_FRAGMENT_BIT);
        quadFragShaders.back().setCode(getQuadFsSpirv());
      });
    }

  return { &quadVertShaders[device_index], &quadFragShaders[device_index] };
}

std::vector<const VulkanShader *> KurisuRootImpl::getArrayedOverlayShaders(size_t device_index, uint32_t layersCount)
{
  if(layersCount == 1)
    return getOverlayShaders(device_index);

  if(quadVertShaders.empty())
    {
      quadVertShaders.reserve(deviceCount());
      forallDevices([this] (VulkanDevice &i) {
        quadVertShaders.emplace_back(i.vkDev, VK_SHADER_STAGE_VERTEX_BIT);
        quadVertShaders.back().setCode(getQuadVsSpirv());
      });
    }

  auto el = quadArrayedFragShaders.find(layersCount);
  if(el == quadArrayedFragShaders.end())
    el = quadArrayedFragShaders.insert({layersCount, std::vector<VulkanShader>()}).first;

  if(el->second.empty())
    {
      el->second.reserve(deviceCount());
      forallDevices([&el, layersCount] (VulkanDevice &i) {
        el->second.emplace_back(i.vkDev, VK_SHADER_STAGE_FRAGMENT_BIT);
        el->second.back().setCode(getArrayedQuadFsSpirv(layersCount));
      });
    }

  return { &quadVertShaders[device_index], &el->second[device_index] };
}

std::vector<const VulkanShader *> KurisuRootImpl::getFlippedOverlayShaders(size_t device_index)
{
  if(quadFlippedVertShaders.empty())
    {
      quadFlippedVertShaders.reserve(deviceCount());
      forallDevices([this] (VulkanDevice &i) {
        quadFlippedVertShaders.emplace_back(i.vkDev, VK_SHADER_STAGE_VERTEX_BIT);
        quadFlippedVertShaders.back().setCode(getFlippedQuadVsSpirv());
      });
    }
  if(quadFragShaders.empty())
    {
      quadFragShaders.reserve(deviceCount());
      forallDevices([this] (VulkanDevice &i) {
        quadFragShaders.emplace_back(i.vkDev, VK_SHADER_STAGE_FRAGMENT_BIT);
        quadFragShaders.back().setCode(getQuadFsSpirv());
      });
    }
  return { &quadFlippedVertShaders[device_index], &quadFragShaders[device_index] };
}

void KurisuRootImpl::activateCommands(RenderingCommands *cmd, size_t device_index)
{
  currentCmd = cmd;
  current_device = device_index;

  if(!currentCmd || currentCmd->getState() == RenderingCommands::State::Compleate) return;

  const auto bindedUserBlocks = bufBindings.bindedBufferBlocksUser();
  std::vector<BufferBinding> bindings;
  bindings.reserve(bindedUserBlocks.size());

  for(const auto &i: bindedUserBlocks)
    {
      const uint32_t realBlock = getGlobBufLocation(std::get<uint32_t>(i), std::get<VkDescriptorType>(i)).binding;
      bindings.push_back(BufferBinding {std::get<VkDescriptorType>(i), realBlock, std::get<KurisuBufferHandle*>(i)});
    }

  bindBuffersToDescriptor(std::move(bindings), *currentCmd, device_index);
}

RenderingCommands &KurisuRootImpl::getActiveCommands()
{
  Q_ASSERT(currentCmd);
  return *currentCmd;
}

bool KurisuRootImpl::hasActiveCommands() const
{
  return (currentCmd != nullptr);
}

void KurisuRootImpl::releaseCommands()
{
  currentCmd = nullptr;
}

void KurisuRootImpl::activateSurface(KurisuSurfaceImpl *surface)
{
  currentSfc = surface;
}

KurisuSurfaceImpl *KurisuRootImpl::getActiveSurface()
{
  return currentSfc;
}

void KurisuRootImpl::releaseSurface()
{
  currentSfc = nullptr;
}

void KurisuRootImpl::requestWindowUpdate(QWindow &wnd)
{
  if(allowedMultithreadingRequestUpdate)
    wnd.requestUpdate();
  else
    {
      static const auto qwndRequestUpdateMethodName = QMetaObject::normalizedSignature("requestUpdate()");
      static const auto qwndRequestUpdateMethodIndex = QWindow::staticMetaObject.indexOfMethod(qwndRequestUpdateMethodName.data());
      static const auto qwndRequestUpdateMethod = QWindow::staticMetaObject.method(qwndRequestUpdateMethodIndex);
      Q_ASSERT(qwndRequestUpdateMethod.invoke(&wnd));
    }
}

void KurisuRootImpl::activateRenderpass(KurisuRenderpassImpl *rp, quint32 subpass)
{
  activeRp = rp;
  activeSubpass = subpass;
}

VulkanBaseRenderpass &KurisuRootImpl::getActiveRenderpass(size_t device_index)
{
  Q_ASSERT(currentSfc);
  if(activeRp)
    return activeRp->getRenderpass(device_index);
  else if(offscreen && currentOffscr)
    return currentOffscr->getRp(device_index);
  else
    throw std::runtime_error("No active renderpass!");
}

quint32 KurisuRootImpl::getActiveSubpass() const
{
  return activeSubpass;
}

bool KurisuRootImpl::isNowOffscreen() const
{
  return offscreen;
}

void KurisuRootImpl::setOffscreenTarget(KurisuOffscrRenderer *offscrRender)
{
  Q_ASSERT(offscreen);
  currentOffscr = offscrRender;
}

size_t KurisuRootImpl::getActiveDevice() const
{
  return current_device;
}

KurisuConfig &KurisuRootImpl::getConfig() const
{
  return *config;
}

KurisuRootImpl::Binding KurisuRootImpl::getTextureLocation(const std::string &textureName, uint32_t blockBinding)
{
  if(blockBinding > KawaiiShader::getUboCount() + 1)
    blockBinding = KawaiiShader::getUboCount() + 1;
  auto el = textureLocations.find({textureName, blockBinding});
  if(el == textureLocations.end())
    {
      uint32_t binding = std::numeric_limits<uint32_t>::quiet_NaN();
      uint32_t set = std::numeric_limits<uint32_t>::quiet_NaN();
      for(size_t i = 0; i < deviceCount(); ++i)
        {
          if(blockBinding == KawaiiShader::getUboCount() + 1)
            set = KawaiiShader::getUboCount();
          else
            set = getScenePipelineLayout(i).getDescriptorSetLayout(blockBinding).second;
          binding = getScenePipelineLayout(i).addDescriptorBinding(set, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_ALL_GRAPHICS);
        }
      if(deviceCount() > 0)
        el = textureLocations.insert({{textureName, blockBinding}, Binding { .binding = binding, .set = set }}).first;
      else
        return { .binding = binding, .set = set };
    }

  return el->second;
}

KurisuRootImpl::Binding KurisuRootImpl::getGlobBufLocation(uint32_t number, VkDescriptorType descriptorType)
{
  auto el = globBufLocations.find({number, descriptorType});
  if(el == globBufLocations.end())
    {
      uint32_t binding = std::numeric_limits<uint32_t>::quiet_NaN();
      uint32_t set = KawaiiShader::getUboCount();
      for(size_t i = 0; i < deviceCount(); ++i)
        binding = getScenePipelineLayout(i).addDescriptorBinding(set, descriptorType, VK_SHADER_STAGE_ALL_GRAPHICS);
      if(deviceCount() > 0)
        el = globBufLocations.insert({{number, descriptorType}, Binding { .binding = binding, .set = set } }).first;
      else
        return { .binding = binding, .set = binding }; // return <NaN, NaN>
    }

  return el->second;
}

KurisuRootImpl::Binding KurisuRootImpl::getInputGbufLocation(uint32_t number)
{
  auto el = inputGbufLocations.find(number);
  if(el == inputGbufLocations.end())
    {
      uint32_t binding = std::numeric_limits<uint32_t>::quiet_NaN();
      uint32_t set = KawaiiShader::getUboCount() + 1;
      for(size_t i = 0; i < deviceCount(); ++i)
        binding = getScenePipelineLayout(i).addDescriptorBinding(set, VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT, VK_SHADER_STAGE_FRAGMENT_BIT);
      if(deviceCount() > 0)
        el = inputGbufLocations.insert({number, Binding { .binding = binding, .set = set } }).first;
      else
        return { .binding = binding, .set = binding }; // return <NaN, NaN>
    }

  return el->second;
}

KurisuRootImpl::Binding KurisuRootImpl::getStorageGbufLocation(uint32_t number)
{
  auto el = storageGbufLocations.find(number);
  if(el == storageGbufLocations.end())
    {
      uint32_t binding = std::numeric_limits<uint32_t>::quiet_NaN();
      uint32_t set = KawaiiShader::getUboCount() + 1;
      for(size_t i = 0; i < deviceCount(); ++i)
        binding = getScenePipelineLayout(i).addDescriptorBinding(set, VK_DESCRIPTOR_TYPE_STORAGE_IMAGE, VK_SHADER_STAGE_ALL_GRAPHICS);
      if(deviceCount() > 0)
        el = storageGbufLocations.insert({number, Binding { .binding = binding, .set = set } }).first;
      else
        return { .binding = binding, .set = binding }; // return <NaN, NaN>
    }

  return el->second;
}

KurisuRootImpl::Binding KurisuRootImpl::getSampledGbufLocation(uint32_t number)
{
  auto el = sampledGbufLocations.find(number);
  if(el == sampledGbufLocations.end())
    {
      uint32_t binding = std::numeric_limits<uint32_t>::quiet_NaN();
      uint32_t set = KawaiiShader::getUboCount() + 1;
      for(size_t i = 0; i < deviceCount(); ++i)
        binding = getScenePipelineLayout(i).addDescriptorBinding(set, VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER, VK_SHADER_STAGE_ALL_GRAPHICS);
      if(deviceCount() > 0)
        el = sampledGbufLocations.insert({number, Binding { .binding = binding, .set = set } }).first;
      else
        return { .binding = binding, .set = binding }; // return <NaN, NaN>
    }

  return el->second;
}

uint32_t KurisuRootImpl::getDescrSetIndex(uint32_t binding)
{
  return getScenePipelineLayout(0).getDescriptorSetLayout(binding).second;
}

void KurisuRootImpl::registerSurface(KurisuSurfaceImpl *sfc)
{
  QWriteLocker l(surfacesLock.get());
  surfaces.insert(sfc);
}

void KurisuRootImpl::unregisterSurface(KurisuSurfaceImpl *sfc)
{
  QWriteLocker l(surfacesLock.get());
  if(auto el = surfaces.find(sfc); el != surfaces.end())
    surfaces.erase(el);
}

void KurisuRootImpl::waitForSurfacesRenderFinished()
{
  QReadLocker l(surfacesLock.get());
  for(auto *i: surfaces)
    i->waitForRenderFinished();
}

QReadLocker &KurisuRootImpl::getGpuOffscrResLocked(size_t dev_index)
{
  Q_ASSERT(dev_index < offscrGpuResLock.size());
  return *offscrGpuResLock[dev_index];
}

bool KurisuRootImpl::checkSystem()
{
  const bool dbg_enabled = KawaiiConfig::getInstance().getDebugLevel() > 0;
  auto dbg_prnt = [&dbg_enabled] (const auto &str) {
      if(dbg_enabled)
        qCritical().noquote() << sib_utils::strings::getQLatin1Str(str);
    };

  VulkanInstanceFactory instFactory;

  if(KawaiiConfig::getInstance().getDebugLevel() > 1)
    {
      instFactory.extensions = { VK_EXT_DEBUG_UTILS_EXTENSION_NAME };
      instFactory.layers = {"VK_LAYER_KHRONOS_validation", "VK_LAYER_LUNARG_standard_validation"};
      QLoggingCategory::setFilterRules(QStringLiteral("qt.vulkan=true"));
      if(KawaiiConfig::getInstance().getDebugLevel() > 2)
        instFactory.layers.push_back("VK_LAYER_MESA_overlay");
    } else
    QLoggingCategory::setFilterRules(QStringLiteral("qt.vulkan=false"));
  instFactory.extensions << QByteArrayList ({"VK_KHR_external_memory_capabilities", "VK_KHR_external_semaphore_capabilities", "VK_KHR_get_physical_device_properties2"});

  std::unique_ptr<VulkanInstance> inst = instFactory.create();

  if(!inst)
    {
      dbg_prnt("KurisuRenderer failed to initialize vulkan instance");
      return false;
    }

  auto devices = std::make_unique<VulkanDeviceManager>(inst->getVkInstance(), deviceExtensions, optionalDeviceExtensions);
  if(devices->allDevices().empty())
    {
      dbg_prnt("KurisuRenderer failed to initialize any vulkan device");
      return false;
    }
  for(const auto &i: devices->allDevices())
    if(!i.queueFamilies.transferFamily.has_value())
      {
        dbg_prnt("KurisuRenderer failed to create transfer command pool");
        return false;
      }
  return true;
}

KawaiiBufferHandle *KurisuRootImpl::createBuffer(const void *ptr, size_t n, const std::initializer_list<KawaiiBufferTarget> &availableTargets)
{
  return new KurisuBufferHandle(ptr, n, *this, availableTargets);
}

KawaiiTextureHandle *KurisuRootImpl::createTexture(KawaiiTexture *model)
{
  return new KurisuTextureHandle(*this, model);
}

void KurisuRootImpl::beginOffscreen()
{
  if(!offscreen)
    {
      offscreen = true;
      offscrGpuResLock.reserve(devices->allDevices().size());
      forallDevices([this] (VulkanDevice &dev) {
        offscrGpuResLock.push_back(std::make_unique<QReadLocker>(&dev.resLock));
      });
    }
}

void KurisuRootImpl::endOffscreen()
{
  if(offscreen)
    {
      offscreen = false;
      currentOffscr = nullptr;
      offscrGpuResLock.clear();
    }
}

const std::vector<VkPipelineColorBlendAttachmentState> &KurisuRootImpl::getColorBlendAttachmentState() const
{
  return colorBlendAttachmentState;
}

void KurisuRootImpl::setColorBlendAttachmentState(const std::vector<VkPipelineColorBlendAttachmentState> &newColorBlendAttachmentState)
{
  colorBlendAttachmentState = newColorBlendAttachmentState;
}

void KurisuRootImpl::setDepthState(VkCompareOp depthCompareOp, bool depthTest, bool depthWrite)
{
  this->depthCompareOp = depthCompareOp;
  this->depthTest = depthTest;
  this->depthWrite = depthWrite;
}

bool KurisuRootImpl::getDepthTest() const
{
  return depthTest;
}

bool KurisuRootImpl::getDepthWrite() const
{
  return depthWrite;
}

VkCompareOp KurisuRootImpl::getDepthCompareOp() const
{
  return depthCompareOp;
}

VkSampler KurisuRootImpl::getCommonSampler(size_t dev_i)
{
  if(commonSampler.empty())
    {
      static const constexpr VkSamplerCreateInfo samplerInfo = {
        .sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
        .magFilter = VK_FILTER_NEAREST,
        .minFilter = VK_FILTER_NEAREST,
        .mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST,
        .addressModeU = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeV = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .addressModeW = VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE,
        .mipLodBias = 0,
        .anisotropyEnable = VK_FALSE,
        .maxAnisotropy = 1,
        .compareEnable = VK_FALSE,
        .compareOp = VK_COMPARE_OP_ALWAYS,
        .minLod = 0,
        .maxLod = 1,
        .unnormalizedCoordinates = VK_FALSE
      };
      commonSampler.resize(devices->allDevices().size(), VK_NULL_HANDLE);
      for(size_t i = 0; i < commonSampler.size(); ++i)
        if (Q_UNLIKELY(vkCreateSampler(devices->device(i).vkDev, &samplerInfo, nullptr, &commonSampler[i]) != VK_SUCCESS))
          {
            for(size_t j = 0; j < i; ++j)
              vkDestroySampler(devices->device(j).vkDev, commonSampler[j], nullptr);
            commonSampler.clear();
            throw std::runtime_error("failed to create common sampler!");
          }
    }
  return commonSampler[dev_i];
}

QRhi &KurisuRootImpl::getQRhi()
{
  if(!qrhi && inst && !devices->allDevices().empty())
    {
      auto &composerDevice = getComposerDevice();
      QRhiVulkanNativeHandles handlers = {
        .physDev = composerDevice.phDev,
        .dev = composerDevice.vkDev,
        .gfxQueueFamilyIdx = *composerDevice.queueFamilies.graphicsFamily,
        .gfxQueueIdx = 0,
        .vmemAllocator = nullptr,
        .gfxQueue = composerDevice.graphicsQueue,
        .inst = &inst->getQVulkanInstance()
      };
      QRhiVulkanInitParams rhiInit = {
        .inst = &inst->getQVulkanInstance(),
        .window = nullptr,
        .deviceExtensions = {}
      };

      qrhi.reset(QRhi::create(QRhi::Vulkan, &rhiInit, {}, &handlers));
    }
  return *qrhi;
}

void KurisuRootImpl::bindBuffersToDescriptor(std::vector<KurisuRootImpl::BufferBinding> &&buffers, RenderingCommands &cmd, size_t device_index)
{
  if(buffers.empty()) return;

  auto &pipelineLayout = getScenePipelineLayout(device_index);

  std::vector<VulkanDescriptorPool::BufBinding> bufBindings;
  std::vector<VulkanDescriptorPool::ImgBinding> imgBindings;

  std::pair<VkDescriptorSetLayout, uint32_t> descrSetLayout = {VK_NULL_HANDLE, 0};

  uint64_t name = 0;
  // Multiple buffer bindings are only allowed in global binding (not camera, material, model or surface)
  // In global binding there will be one set of buffers for all command buffers
  if(buffers.size() == 1)
    name = static_cast<uint64_t>(reinterpret_cast<size_t>(buffers.front().buf));

  for(const auto &i: buffers)
    {
      bufBindings.push_back(VulkanDescriptorPool::BufBinding {
                              .buf = &i.buf->buffer(device_index),
                              .descriptorType = i.target,
                              .bindingPoint = i.block
                            });

      auto currentDescrSetLayout = pipelineLayout.getDescriptorSetLayout(i.block);
      Q_ASSERT(!descrSetLayout.first || descrSetLayout == currentDescrSetLayout);
      descrSetLayout = currentDescrSetLayout;

      for(const auto &tex: i.buf->getBindedTextures())
        {
          auto &vkTex = tex.second->getTexture(device_index);

          imgBindings.push_back(VulkanDescriptorPool::ImgBinding {
                                  .descrImg = *vkTex.descriptorInfo(),
                                  .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                                  .bindingPoint = getTextureLocation(tex.first, bufBindings.back().bindingPoint).binding
                                });

          Q_ASSERT(descrSetLayout == pipelineLayout.getDescriptorSetLayout(imgBindings.back().bindingPoint));
        }
    }

  cmd.createDescriptorSet(name, bufBindings, imgBindings, descrSetLayout.first, pipelineLayout.getDescriptorCount(descrSetLayout.second));
  cmd.bindDescriptorSet(name, descrSetLayout.second, pipelineLayout.getPipelineLayout());
}

void KurisuRootImpl::onChangedBlockBufBinding(KawaiiBufferTarget target, uint32_t block, KurisuBufferHandle *buf)
{
  if(!buf) return;

  VkDescriptorType descrType;
  switch(target)
    {
    case KawaiiBufferTarget::UBO:
      descrType = VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
      break;

    case KawaiiBufferTarget::SSBO:
      descrType = VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
      break;

    default: return;
    }

  if(hasActiveCommands())
    bindBuffersToDescriptor({BufferBinding{descrType, block, buf}}, getActiveCommands(), getActiveDevice());
  else
    emit bufferBindingChanged();
}
