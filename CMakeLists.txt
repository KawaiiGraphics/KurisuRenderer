cmake_minimum_required (VERSION 3.10)
project (KurisuRenderer)

cmake_policy(SET CMP0069 NEW)

#________________________________________________________________________________________
#Variables:
set(LIBNAME "${PROJECT_NAME}")

set(${LIBNAME}_MAJOR_VERSION 0)
set(${LIBNAME}_MINOR_VERSION 0)
set(${LIBNAME}_PATCH_VERSION 9)
set(${LIBNAME}_VERSION
  ${${LIBNAME}_MAJOR_VERSION}.${${LIBNAME}_MINOR_VERSION}.${${LIBNAME}_PATCH_VERSION})

string(TOUPPER ${PROJECT_NAME} PROJECT_NAME_UPPER)

# Offer the user the choice of overriding the installation directories
set(INSTALL_LIB_DIR lib CACHE PATH "Installation directory for libraries")
set(INSTALL_BIN_DIR bin CACHE PATH "Installation directory for executables")
set(INSTALL_INCLUDE_DIR include CACHE PATH "Installation directory for header files")
set(DEF_INSTALL_CMAKE_DIR lib/cmake/${LIBNAME})
set(INSTALL_CMAKE_DIR ${DEF_INSTALL_CMAKE_DIR} CACHE PATH "Installation directory for CMake files")

# Make relative paths absolute
foreach(p LIB BIN INCLUDE CMAKE)
  set(var INSTALL_${p}_DIR)
  if(NOT IS_ABSOLUTE "${${var}}")
    set(${var} "${CMAKE_INSTALL_PREFIX}/${${var}}")
  endif()
endforeach()

option(USE_LINKABLE_SPIRV "Use linkable SPIR-V for better performance (requires support in glslang)" OFF)

#________________________________________________________________________________________
#Config compiler:
if(MSVC)
    set (CMAKE_CXX_FLAGS "/std:c++latest")
else()
    set (CMAKE_CXX_STANDARD 17)
    set (CMAKE_CXX_STANDARD_REQUIRED ON)
    set (CMAKE_CXX_EXTENSIONS ON)
endif()

if(NOT MSVC)
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wno-missing-field-initializers")
    set (CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g -O0 -Wp,-U_FORTIFY_SOURCE")
    set (CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O3")
    set (CMAKE_CXX_FLAGS_RELWITHDEBINFO "${CMAKE_CXX_FLAGS_RELWITHDEBINFO} -g -O3")
    set (CMAKE_CXX_FLAGS_MINSIZEREL "${CMAKE_CXX_FLAGS_MINSIZEREL} -Os")
endif()

if(NOT CMAKE_DEBUG_POSTFIX)
  set(CMAKE_DEBUG_POSTFIX d)
endif()

# Tell CMake to run moc when necessary:
set (CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)
# As moc files are generated in the binary dir, tell CMake to always look for includes there:
set (CMAKE_INCLUDE_CURRENT_DIR ON)


#________________________________________________________________________________________
#Dependencies:
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${CMAKE_CURRENT_SOURCE_DIR}/CMakeModules)
find_package (OpenMP)
find_package (Qt6 COMPONENTS Core Gui Concurrent OpenGL REQUIRED)
find_package (glm REQUIRED)
find_package (glslang REQUIRED)
find_package (vulkan REQUIRED)
find_package (sib_utils REQUIRED)
find_package (Kawaii3D ${${LIBNAME}_VERSION} REQUIRED)
find_package (KawaiiRenderer ${${LIBNAME}_VERSION} REQUIRED)
if (OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set (CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif()

#________________________________________________________________________________________
#Set targets:
file(GLOB_RECURSE ${LIBNAME}_SRC Source/*.cpp Source/*.hpp Source/*.qrc)
add_library(${LIBNAME} SHARED ${${LIBNAME}_SRC})

target_include_directories(${LIBNAME} PRIVATE Source)

target_compile_definitions(${LIBNAME}
    PRIVATE ${PROJECT_NAME_UPPER}_LIBRARY
    $<$<CONFIG:Debug>:${PROJECT_NAME_UPPER}_DEBUG>
    $<$<NOT:$<CONFIG:Debug>>:QT_NO_DEBUG>
    )

target_link_libraries(${LIBNAME} PUBLIC Kawaii3D::KawaiiRenderer vulkan::vulkan ${glslang_LIBRARIES} PRIVATE Qt6::OpenGL)

if(USE_LINKABLE_SPIRV)
    target_link_libraries(${LIBNAME} PUBLIC SPIRV-Tools-link)
    target_compile_definitions(${LIBNAME} PRIVATE SPIRV_LINKER)
endif()

target_include_directories(${LIBNAME} INTERFACE
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
    $<INSTALL_INTERFACE:include>)

if(UNIX)
    target_include_directories(${LIBNAME} PUBLIC
        ${Qt6Gui_PRIVATE_INCLUDE_DIRS})
endif()

option(FORBID_LTO "Force disable LTO for ${LIBNAME}" OFF)
if(NOT FORBID_LTO)
    set_property(TARGET ${LIBNAME} PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
    message(STATUS "${LIBNAME}: Enabled LTO")
else()
    set_property(TARGET ${LIBNAME} PROPERTY INTERPROCEDURAL_OPTIMIZATION FALSE)
    message(STATUS "${LIBNAME}: Disabled LTO")
endif()

install(TARGETS ${LIBNAME}
    EXPORT ${LIBNAME}Targets
    ARCHIVE DESTINATION lib
    LIBRARY DESTINATION lib
    RUNTIME DESTINATION bin)

set(_src_root_path ${PROJECT_SOURCE_DIR}/Source)
set(_source_list ${${LIBNAME}_SRC})
foreach(_source IN ITEMS ${_source_list})
    get_filename_component(_source_path "${_source}" PATH)
    file(RELATIVE_PATH _source_path_rel "${_src_root_path}" "${_source_path}")
#    string(REPLACE "/" "\\" _group_path "${_source_path_rel}")
    source_group("${_source_path_rel}" FILES "${_source}")
endforeach()

#add_subdirectory(Tests)
include("Common/CPackConfig.cmake")

