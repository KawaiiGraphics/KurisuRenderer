# KurisuRenderer
Vulkan render for Kawaii3D. Based on KawaiiRenderer.

The project aims to provide maximum performance for Kawaii3D on latest desktop systems.

Caching quasistatic scenes, using optimal memory and queue types and even using multiple GPUs to render a single frame! But it's not guaranteed that every Kawaii3D feature will be supported. Also on some systems the renderer can cause GPU resets or even hangs (especially on wrong Kawaii3D trees)! So BEWARE: It's bleeding edge, it's Vulkan, it's Kurisu :) !

Requires Vulkan 1.0 and extension 'VK\_KHR\_device\_group\_creation'.

