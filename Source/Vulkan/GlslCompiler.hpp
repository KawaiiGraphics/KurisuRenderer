#ifndef GLSLCOMPILER_HPP
#define GLSLCOMPILER_HPP

#include <string>
#include <vector>
#include <memory>
#include <cinttypes>
#include <vulkan/vulkan.h>
#include <spirv-tools/optimizer.hpp>
#include <glslang/Public/ShaderLang.h>
#include <Kawaii3D/Shaders/KawaiiShaderType.hpp>

class GlslCompiler
{
  spvtools::Optimizer *spvOptimizer;
  spvtools::Context *spvContext;
  spvtools::SpirvTools *spvTools;

public:
  GlslCompiler();

  std::unique_ptr<glslang::TShader> compileGlsl(const std::string &glsl_code, EShLanguage kind, const std::string &fName);
  static EShLanguage kind(KawaiiShaderType type);
  static EShLanguage kind(VkShaderStageFlagBits type);

  void recompileGlsl(glslang::TShader &shader, const std::string &glsl_code, EShLanguage kind, const std::string &fName);

  std::vector<uint32_t> glslToSpirv(const std::string &glsl_code, EShLanguage kind, const std::string &fName);
  std::vector<uint32_t> glslToSpirv(const std::vector<std::unique_ptr<glslang::TShader>> &glsl_code, EShLanguage kind);
  std::vector<uint32_t> buildSpirV(glslang::TIntermediate *code);

  std::vector<uint32_t> glslModuleToSpirv(const std::string &glsl_code, EShLanguage kind, const std::string &fName);
  std::vector<uint32_t> linkSpirv(const std::vector<std::vector<uint32_t>> &modules);

#ifdef SPIRV_LINKER
  inline static constexpr bool linkable_spirv = true;
#else
  inline static constexpr bool linkable_spirv = false;
#endif
};

#endif // GLSLCOMPILER_HPP
