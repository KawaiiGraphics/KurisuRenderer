#include "KurisuSurfaceImpl.hpp"

#include "Renderpass/KurisuRenderpassImpl.hpp"
#include "KurisuRootImpl.hpp"
#include "KurisuConfig.hpp"

#include <Kawaii3D/KawaiiConfig.hpp>
#include <sib_utils/ioReadAll.hpp>
#include <QtConcurrent>

namespace {
  bool vsync = true;

  class Initializer {
  public:

    Initializer()
    {
      qAddPreRoutine([] {
          vsync = (KawaiiConfig::getInstance().getPreferredSfcFormat().swapInterval()>0);
        });
    }
  };

  const Initializer init;
}

KurisuSurfaceImpl::KurisuSurfaceImpl(KawaiiSurface *model):
  KawaiiSurfaceImpl(model),
  composeFrameIndex(0),
  frameIndex(),
  currentGpu(),
  previousGpu(),
  pendingUpdates(0)
{
  const auto wndStates = getModel()->getWindow().windowStates();
  bool wasVisible = getModel()->getWindow().isVisible();

  getModel()->getWindow().setSurfaceType(QSurface::VulkanSurface);
  getModel()->getWindow().destroy();
  getModel()->getWindow().create();
  getModel()->getWindow().setWindowStates(wndStates);
  getModel()->getWindow().setVisible(wasVisible);

  getRoot()->registerSurface(this);

  connect(model, &KawaiiSurface::renderpassChanged, this, &KurisuSurfaceImpl::setTileRenderpass);
}

KurisuSurfaceImpl::~KurisuSurfaceImpl()
{
  invalidate();
  getRoot()->unregisterSurface(this);
}

void KurisuSurfaceImpl::waitForRenderFinished()
{
  for(auto &task: finishRenderingTask)
    if(task.isRunning())
      task.waitForFinished();

  getRoot()->forallDevices([] (VulkanDevice &dev) {
    dev.waitIdle();
  });
}

uint32_t KurisuSurfaceImpl::getImgCount() const
{
  return swapchain? swapchain->getImageCount(): 0;
}

size_t KurisuSurfaceImpl::getAsyncFrames() const
{
  return swapchain? swapchain->getAsyncFrames(): 0;
}

size_t KurisuSurfaceImpl::getFrameIndex() const
{
  return frameIndex[*currentGpu];
}

size_t KurisuSurfaceImpl::getActiveGpu() const
{
  Q_ASSERT(currentGpu != decltype(currentGpu)());
  return *currentGpu;
}

void KurisuSurfaceImpl::initConnections()
{
  currentGpu = getRoot()->getConfig().getUsingGpus().cbegin();
  previousGpu = currentGpu;
  for(size_t i = 0; i < getModel()->tileCount(); ++i)
    {
      auto &tile = getModel()->getTile(i);
      auto renderpass = tile.getRenderpass()?
            static_cast<KurisuRenderpassImpl*>(tile.getRenderpass()->getRendererImpl()):
            nullptr;
      if(renderpass)
        renderpass->setAsyncFrames(getAsyncFrames());
    }
}

KurisuRootImpl *KurisuSurfaceImpl::getRoot() const
{
  return static_cast<KurisuRootImpl*>(getModel()->getRoot()->getRendererImpl());
}

void KurisuSurfaceImpl::resize(const QSize &sz)
{
  static_cast<void>(sz);
  forceResize();
}

bool KurisuSurfaceImpl::startRendering()
{
  if(!swapchain)
    initSwapchain();

  if(vsync)
    --pendingUpdates;

  if(!swapchain->checkExtent())
    forceResize();

  getRoot()->activateSurface(this);

# ifndef Q_OS_WINDOWS
  if(vsync && pendingUpdates > 0)
    return false;
# endif

  const size_t frameI = composeFrameIndex;

  if(finishRenderingTask[frameI].isRunning())
    finishRenderingTask[frameI].waitForFinished();

  for(size_t i = 0; i < getModel()->tileCount(); ++i)
    {
      auto rp = getModel()->getTile(i).getRenderpass();
      if(rp)
        static_cast<KurisuRenderpassImpl*>(rp->getRendererImpl())->prepareRendering(*currentGpu, frameIndex[*currentGpu]);
    }
  QMutexLocker l(frameRenderedFenceM[frameI].get());
  vkWaitForFences(getRoot()->getComposerDevice().vkDev, 1, &frameRenderedFence[frameI], VK_TRUE, std::numeric_limits<uint64_t>::max());
  l.unlock();
  return true;
}

void KurisuSurfaceImpl::finishRendering()
{
  getRoot()->releaseSurface();

  const auto gpu = currentGpu;
  const size_t frameI = frameIndex[*gpu];
  const size_t composeFrameI = composeFrameIndex;

  if(copyToSwapchainCmd->isEmpty())
    writeCopyToSwapchainCmd();

  finishRenderingTask[composeFrameI] = QtConcurrent::run([this, gpu, frameI, composeFrameI] {
      bool gbufsWasUpdated = false;
      uint32_t imgI = static_cast<uint32_t>(swapchain->getImageCount());
      for(size_t i = 0; i < getModel()->tileCount(); ++i)
        {
          auto rp = getModel()->getTile(i).getRenderpass();
          if(Q_LIKELY(rp) && static_cast<KurisuRenderpassImpl*>(rp->getRendererImpl())->getWasUpdated())
            {
              static_cast<KurisuRenderpassImpl*>(rp->getRendererImpl())->waitForRendered(*gpu, frameI);
              if(!gbufsWasUpdated)
                {
                  bool outdated;
                  imgI = swapchain->acquireNextImage(aquireImageSemaphore[composeFrameI].getSemaphore(), &outdated);
                }
              gbufsWasUpdated = true;
            }
        }
      if(gbufsWasUpdated)
        {
          Q_ASSERT(Q_LIKELY(imgI < swapchain->getImageCount()));

          QReadLocker resL(&getRoot()->getDevice(*gpu).resLock);
          const VkSemaphore frameRenderedSem = frameRenderedSemaphore[composeFrameI].getSemaphore();
          QMutexLocker frameRenderedFenceL(frameRenderedFenceM[composeFrameI].get());
          QMutexLocker queueL(&getRoot()->getComposerDevice().graphicsQueueM);
          vkResetFences(getRoot()->getComposerDevice().vkDev, 1, &frameRenderedFence[composeFrameI]);
          copyToSwapchainCmd->exec(imgI, frameI,
                                   {aquireImageSemaphore[composeFrameI].getSemaphore()}, {VK_PIPELINE_STAGE_TRANSFER_BIT},
                                   {frameRenderedSem}, frameRenderedFence[composeFrameI]);
          frameRenderedFenceL.unlock();
          queueL.unlock();
          swapchain->present(frameRenderedSem, imgI);
        }

#     ifndef Q_OS_WINDOWS
      if(vsync)
        {
          getRoot()->requestWindowUpdate(getModel()->getWindow());
          ++pendingUpdates;
        }
#     endif
    });

  frameIndex[*gpu] = (frameI+1) % swapchain->getAsyncFrames();
  composeFrameIndex = (composeFrameI+1) % swapchain->getAsyncFrames();
  previousGpu = currentGpu;
  selectNextGpu();
# ifdef Q_OS_WINDOWS
  if(vsync)
    getModel()->getWindow().requestUpdate();
# endif
}

void KurisuSurfaceImpl::invalidate()
{
  waitForRenderFinished();
  copyToSwapchainCmd.reset();
  swapchain.reset();
  aquireImageSemaphore.clear();
  frameRenderedSemaphore.clear();

  auto &dev = getRoot()->getComposerDevice();
  for(auto fence: frameRenderedFence)
    vkDestroyFence(dev.vkDev, fence, nullptr);
  frameRenderedFence.clear();
  frameRenderedFenceM.clear();
}

void KurisuSurfaceImpl::initSwapchain()
{
  auto &dev = getRoot()->getComposerDevice();

  swapchain = std::make_unique<VulkanSwapchain>(&getModel()->getWindow(),
                                                getRoot()->getSurface(&getModel()->getWindow()),
                                                dev);

  frameRenderedFence.resize(swapchain->getAsyncFrames());
  frameRenderedFenceM.reserve(swapchain->getAsyncFrames());
  for(size_t i = 0; i < swapchain->getAsyncFrames(); ++i)
    {
      aquireImageSemaphore.emplace_back(dev);
      frameRenderedSemaphore.emplace_back(dev);
      frameRenderedFenceM.push_back(std::make_unique<QMutex>());
      VkFence fence = VK_NULL_HANDLE;
      VkResult result = vkCreateFence(dev.vkDev, &VulkanDevice::fenceCreateInfo, nullptr, &fence);
      if(Q_UNLIKELY(result != VK_SUCCESS))
        throw std::runtime_error("Can not create fence!");
      frameRenderedFence[i] = fence;
    }

  finishRenderingTask.resize(swapchain->getAsyncFrames());
  initConnections();
  for(size_t i = 0; i < getModel()->tileCount(); ++i)
    setTileRenderpass(i, getModel()->getTile(i).getRenderpass());
  copyToSwapchainCmd = std::make_unique<RenderingCommands>(dev, getImgCount(), getAsyncFrames(), true);
  frameIndex.resize(getRoot()->deviceCount(), 0);
  for(auto &frame: frameIndex)
    frame = 0;
  composeFrameIndex = 0;
}

void KurisuSurfaceImpl::forceResize()
{
  if(!swapchain) return;

  waitForRenderFinished();
  swapchain->recreate();

  copyToSwapchainCmd->reset();
}

void KurisuSurfaceImpl::setTileRenderpass(size_t/* tileIndex*/, KawaiiRenderpass *rp)
{
  if(!rp || !rp->getRendererImpl()) return;
  static_cast<KurisuRenderpassImpl*>(rp->getRendererImpl())->setAsyncFrames(getAsyncFrames());
  if(getRoot()->deviceCount() > 1)
    static_cast<KurisuRenderpassImpl*>(rp->getRendererImpl())->setSharedGbufs({0});
  else
    static_cast<KurisuRenderpassImpl*>(rp->getRendererImpl())->setSharedGbufs({});
}

void KurisuSurfaceImpl::writeCopyToSwapchainCmd()
{
  copyToSwapchainCmd->startRecording(VK_NULL_HANDLE, {}, {}, VK_SUBPASS_CONTENTS_INLINE, 0);
  copyToSwapchainCmd->clearImage(swapchain->getImages());
  const size_t composerGpuIndex = getRoot()->getComposerDeviceIndex();
  for(size_t i = 0; i < getModel()->tileCount(); ++i)
    {
      static const constexpr VkImageSubresourceLayers subresLayers = {
        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .mipLevel = 0,
        .baseArrayLayer = 0,
        .layerCount = 1,
      };
      const auto &tileRg = getModel()->getTile(i).getResultRect();
      const VkImageBlit rg = {
        .srcSubresource = subresLayers,
        .srcOffsets = {
          VkOffset3D {
            .x = 0,
            .y = 0,
            .z = 0
          },
          VkOffset3D {
            .x = tileRg.width(),
            .y = tileRg.height(),
            .z = 1
          }
        },
        .dstSubresource = subresLayers,
        .dstOffsets = {
          VkOffset3D {
            .x = tileRg.x(),
            .y = tileRg.y(),
            .z = 0
          },
          VkOffset3D {
            .x = tileRg.width(),
            .y = tileRg.height(),
            .z = 1
          }
        }
      };
      KawaiiRenderpass *rp = getModel()->getTile(i).getRenderpass();
      auto *rp_impl = static_cast<KurisuRenderpassImpl*>(rp->getRendererImpl());

      std::vector<std::vector<VkImage>> gBufImg(getImgCount(), { getAsyncFrames(), VK_NULL_HANDLE });
      std::vector<std::vector<VkImage>> swapchainImg(getImgCount(), { getAsyncFrames(), VK_NULL_HANDLE });
      for(size_t frameI = 0; frameI < getAsyncFrames(); ++frameI)
        {
          VkImage vkImg = rp_impl->getGbuf(composerGpuIndex, 0, frameI).getImg();
          for(size_t imgIndex = 0; imgIndex < getImgCount(); ++imgIndex)
            {
              gBufImg[imgIndex][frameI] = vkImg;
              swapchainImg[imgIndex][frameI] = swapchain->getImages()[imgIndex];
            }
        }
      copyToSwapchainCmd->blitImage(gBufImg, getRoot()->deviceCount()>1 ? VK_IMAGE_LAYOUT_UNDEFINED : VK_IMAGE_LAYOUT_GENERAL,
          swapchainImg, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
      {rg}, VK_FILTER_NEAREST);
    }
  copyToSwapchainCmd->finishRecording();
}

void KurisuSurfaceImpl::selectNextGpu()
{
  if(getRoot()->getConfig().getUsingGpus().size() > 1)
    {
      ++currentGpu;
      if(currentGpu == getRoot()->getConfig().getUsingGpus().cend())
        currentGpu = getRoot()->getConfig().getUsingGpus().cbegin();
    }
}
