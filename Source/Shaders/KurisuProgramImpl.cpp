#include "KurisuProgramImpl.hpp"
#include "KurisuShaderImpl.hpp"
#include "KurisuRootImpl.hpp"
#include "Vulkan/GlslCompiler.hpp"

#include <Kawaii3D/KawaiiConfig.hpp>
#include <sib_utils/ioReadAll.hpp>
#include <array>

namespace {
  const bool dumpingShaders = !qgetenv("SIB3D_DUMP_SHADERS").isEmpty()
      && qgetenv("SIB3D_DUMP_SHADERS").trimmed() != "0";
}

KurisuProgramImpl::KurisuProgramImpl(KawaiiProgram *model):
  KawaiiProgramImpl(model)
{
  connect(model, &KawaiiProgram::cullModeChanged, this, &KurisuProgramImpl::onModelCullModeChanged);
}

KurisuProgramImpl::~KurisuProgramImpl()
{
  preDestruct();
}

bool KurisuProgramImpl::use() const
{
  const size_t gpu_id = rootImpl()->getActiveDevice();

  if(pipelines.empty() || pipelines[gpu_id]->stages.empty())
    {
      qCritical().noquote() << QStringLiteral("Program \"%1\" was not succesfully linked!").arg(getName());
      return false;
    }

  const VkRenderPass rp = rootImpl()->getActiveRenderpass(gpu_id).getRenderpass();
  RenderingCommands *cmd = &rootImpl()->getActiveCommands();

  if(dumpingShaders)
    dumpShaders();

  std::array<bool, 3> enabledVertexAttributes = { false, false, false };
  forallShaders([&enabledVertexAttributes] (KawaiiShaderImpl *shader) {
    auto *sh = static_cast<KurisuShaderImpl*>(shader);
    for(size_t i = 0; i < enabledVertexAttributes.size(); ++i)
      if(sh->isAttributeEnabled(i))
        enabledVertexAttributes[i] = true;
  });

  VkPipelineVertexInputStateCreateInfo inputState = {
    .sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
    .vertexBindingDescriptionCount = 1,
    .pVertexBindingDescriptions = &VulkanPipeline::vertexBindingDescription,
  };
  std::vector<VkVertexInputAttributeDescription> vertAttributes;
  vertAttributes.reserve(3);
  for(size_t i = 0; i < enabledVertexAttributes.size(); ++i)
    if(enabledVertexAttributes[i])
      vertAttributes.push_back(VulkanPipeline::baseAtributes[i]);
  inputState.vertexAttributeDescriptionCount = vertAttributes.size();
  if(vertAttributes.empty())
    {
      inputState.pVertexAttributeDescriptions = nullptr;
      inputState.vertexBindingDescriptionCount = 0;
      inputState.pVertexBindingDescriptions = nullptr;
    } else
    inputState.pVertexAttributeDescriptions = vertAttributes.data();
  pipelines[gpu_id]->pipeline.setVertexInputState(inputState);
  pipelines[gpu_id]->pipeline.setCullMode(static_cast<VkCullModeFlagBits>(static_cast<KawaiiProgram*>(getData())->getCullMode()));

  VkPipeline pipeline;
  try {
    pipeline = pipelines[gpu_id]->pipeline.getPipeline (
          cmd,
          rp,
          rootImpl()->getActiveSubpass(),
          rootImpl()->getColorBlendAttachmentState(),
          rootImpl()->getDepthTest(),
          rootImpl()->getDepthWrite(),
          rootImpl()->getDepthCompareOp(), {});
  } catch(const std::runtime_error&)
  {
    qCritical().noquote() << QStringLiteral("Can not create pipeline for program \"%1\"!").arg(getName());
    return false;
  }
  cmd->bindGraphicsPipeline(pipeline);
  return true;
}

void KurisuProgramImpl::createProgram()
{
  pipelines.reserve(rootImpl()->deviceCount());
  rootImpl()->forallDevices([this] (const VulkanDevice &dev, size_t i) {
      auto pipeline = std::make_unique<PipelineInfo>(dev, rootImpl()->getScenePipelineLayout(i));
//      pipeline->pipeline.setSurfaceFormat(KawaiiConfig::getInstance().getPreferredSfcFormat());
      pipelines.push_back(std::move(pipeline));
    });

  linkProg();
}

void KurisuProgramImpl::deleteProgram()
{
  pipelines.clear();
}

void KurisuProgramImpl::linkStage(VkShaderStageFlagBits stage, EShLanguage kind)
{
  Q_ASSERT(GlslCompiler::kind(stage) == kind);

  std::vector<KurisuShaderImpl*> shaders;
  forallShaders([&shaders, stage] (KawaiiShaderImpl *shader) {
      auto *sh = static_cast<KurisuShaderImpl*>(shader);
      if(sh->getStage() == stage)
        shaders.push_back(sh);
    });

  const std::vector<uint32_t> spirvCode = compileStage(shaders, kind);
  for(const auto &i: pipelines)
    i->setStageCode(stage, spirvCode);
}

namespace {
  const std::vector<uint32_t>& getQuadVsCode()
  {
    static std::vector<uint32_t> code;
    if(code.empty())
      code = GlslCompiler().glslToSpirv(sib_utils::ioReadAll(QFile(":/kurisu-glsl/quadNoTexcoord.vs.glsl")).data(), EShLangVertex, ":/kurisu-glsl/quadNoTexcoord.vs.glsl");
    return code;
  }
}

void KurisuProgramImpl::linkProg()
{
  std::unordered_map<VkShaderStageFlagBits, std::vector<KurisuShaderImpl*>> shaders;

  forallShaders([&shaders] (KawaiiShaderImpl *shader) {
      auto *sh = static_cast<KurisuShaderImpl*>(shader);
      shaders[sh->getStage()].push_back(sh);
    });

  std::vector<std::pair<VkShaderStageFlagBits, std::vector<uint32_t>>> spirvCode;
  for(const auto &i: shaders)
    spirvCode.push_back(std::pair(i.first, compileStage(i.second, GlslCompiler::kind(i.first))));

  if(shaders.count(VK_SHADER_STAGE_VERTEX_BIT) == 0)
    spirvCode.push_back({ VK_SHADER_STAGE_VERTEX_BIT, getQuadVsCode() });

  for(const auto &i: pipelines)
    for(auto &stage: spirvCode)
      i->setStageCode(stage.first, stage.second);
}

void KurisuProgramImpl::reAddShader([[maybe_unused]] KawaiiShaderImpl *shader)
{
  if(pipelines.empty())
    return createProgram();

  auto *sh = static_cast<KurisuShaderImpl*>(shader);
  linkStage(sh->getStage(), sh->getKind());
}

KurisuRootImpl *KurisuProgramImpl::rootImpl() const
{
  return static_cast<KurisuRootImpl*>(getRoot());
}

QString KurisuProgramImpl::getName() const
{
  const QString dataObjName = getData()->objectName();
  if(!dataObjName.isNull() && !dataObjName.isEmpty())
    return dataObjName;
  else
    return QStringLiteral("0x%1").arg(reinterpret_cast<qulonglong>(getData()), 0, 16);
}

void KurisuProgramImpl::dumpShaders() const
{
  QDir d = QDir::temp();
  const auto path = QStringLiteral("KurisuRenderer/%1_shaders/").arg(getName());
  d.mkpath(path);
  d.cd(path);
  forallShaders([&d](KawaiiShaderImpl *shader) {
    auto *sh = static_cast<KurisuShaderImpl*>(shader);
    QFile f(d.absoluteFilePath(sh->getModuleFName()));
    f.open(QFile::WriteOnly | QFile::Text);
    f.write(QByteArray::fromRawData(sh->getGlslCode().data(), sh->getGlslCode().size()));
    f.close();
  });
}

void KurisuProgramImpl::invalidatePipelines()
{
  for(const auto &i: pipelines)
    i->stages.clear();
}

std::vector<uint32_t> KurisuProgramImpl::compileStage(const std::vector<KurisuShaderImpl *> &shaders, EShLanguage kind)
{
  if(shaders.empty()) return {};

  if constexpr(GlslCompiler::linkable_spirv)
  {
    std::vector<std::vector<uint32_t>> modules;
    for(auto *sh: shaders)
      {
        const std::vector<uint32_t> *module_spirv;
        try {
          module_spirv = &sh->getLinkableSpirv();
        } catch(const std::runtime_error &)
        {
          return {};
        }
        modules.push_back(*module_spirv);
      }
    return GlslCompiler().linkSpirv(modules);
  } else
  {
    std::vector<std::unique_ptr<glslang::TShader>> modules;
    for(auto *sh: shaders)
      {
        std::unique_ptr<glslang::TShader> compiled;
        try {
          compiled = sh->compile();
        } catch(const std::runtime_error &)
        {
          return {};
        }
        modules.push_back(std::move(compiled));
      }
    std::vector<uint32_t> spirvCode;
    try {
      spirvCode = GlslCompiler().glslToSpirv(modules, kind);
    } catch(const std::runtime_error &)
    {
      spirvCode.clear();
    }
    return spirvCode;
  }
}

void KurisuProgramImpl::onModelCullModeChanged(KawaiiProgram::CullMode cullMode)
{
  for(const auto &i: pipelines)
    i->pipeline.setCullMode(static_cast<VkCullModeFlagBits>(cullMode));
}

KurisuProgramImpl::PipelineInfo::PipelineInfo(const VulkanDevice &dev, VulkanPipelineLayout &layout):
  pipeline(dev, layout)
{
}

void KurisuProgramImpl::PipelineInfo::setStageCode(VkShaderStageFlagBits stage, const std::vector<uint32_t> &code)
{
  if(code.empty())
    return removeStage(stage);

  auto &vulkanStage = stages[stage];
  if(!vulkanStage)
    vulkanStage = std::make_unique<VulkanShader>(pipeline.getDevice().vkDev, stage);

  vulkanStage->setCode(code);
  pipeline.updateShaderStage(vulkanStage.get());
}

void KurisuProgramImpl::PipelineInfo::removeStage(VkShaderStageFlagBits stage)
{
  if(auto el = stages.find(stage); el != stages.end())
    {
      pipeline.removeShaderStage(el->second.get());
      stages.erase(el);
    }
}
