#include "VulkanDevice.hpp"
#include "VulkanStagingBuffer.hpp"
#include "KurisuConfig.hpp"
#include <QCoreApplication>
#include <cstring>
#include <QFile>
#include <QDir>

namespace {
  struct VkPipelineCacheHeaderOne
  {
    uint32_t length; // == sizeof(VkPipelineCacheHeaderOne)
    uint32_t version; // == VK_PIPELINE_CACHE_HEADER_VERSION_ONE
    uint32_t vendorID;
    uint32_t deviceID;
    uint8_t uuid[VK_UUID_SIZE];
  };
}

VulkanDevice::VulkanDevice(const VulkanDevice::QueueFamilyIndices &queueFamilies, const VkDevice vkDev, const VkPhysicalDevice phDev):
  queueFamilies(queueFamilies),

  vkDev(vkDev),
  phDev(phDev),
  properties(([phDev] {
    VkPhysicalDeviceProperties devProps;
    vkGetPhysicalDeviceProperties(phDev, &devProps);
    return devProps;
  })()),

  graphicsQueue(getQueue(queueFamilies.graphicsFamily, 0)),
  transferQueue(getQueue(queueFamilies.transferFamily, (queueFamilies.graphicsFamily==queueFamilies.transferFamily && queueFamilies.transferQCount>1))),

  memProperties(([phDev] {
    VkPhysicalDeviceMemoryProperties memProperties;
    vkGetPhysicalDeviceMemoryProperties(phDev, &memProperties);
    return memProperties;
  })()),

  depthFormat(getSupportedDepthStencilFormat({VK_FORMAT_D24_UNORM_S8_UINT, VK_FORMAT_D32_SFLOAT})),
  depthStencilFormat(getSupportedDepthStencilFormat({VK_FORMAT_D24_UNORM_S8_UINT, VK_FORMAT_D32_SFLOAT_S8_UINT})),

  pipelineCache(([vkDev, this] {
    static const bool ignoreDiskCache = (qgetenv("SIB3D_CLEAR_PIPELINE_CACHE").toInt() > 0);

    QByteArray initialData;
    QDir d = QDir::home();
    if(!ignoreDiskCache && d.cd(".cache") && d.cd("KurisuRenderer"))
      {
        const auto fName =
            QStringLiteral("%1-%2-%3-%4-pipeline_cache").
            arg(QCoreApplication::organizationName(),
                QCoreApplication::applicationName()).
            arg(properties.vendorID).arg(properties.deviceID);

//        QFile f(d.absoluteFilePath(fName));
//        if(f.open(QFile::ReadOnly))
//          {
//            initialData = f.readAll();
//            f.close();

//            const VkPipelineCacheHeaderOne *header = reinterpret_cast<const VkPipelineCacheHeaderOne*>(initialData.constData());

//            if(static_cast<size_t>(initialData.size()) < sizeof(VkPipelineCacheHeaderOne)
//               || header->length != sizeof(VkPipelineCacheHeaderOne)
//               || header->version != VK_PIPELINE_CACHE_HEADER_VERSION_ONE
//               || header->vendorID != properties.vendorID
//               || header->deviceID != properties.deviceID
//               || std::memcmp(header->uuid, properties.pipelineCacheUUID, VK_UUID_SIZE) != 0)
//              {
//                initialData.clear();
//              }
//          }
      }

    const VkPipelineCacheCreateInfo createInfo = {
      .sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO,
//      .flags = VK_PIPELINE_CACHE_CREATE_EXTERNALLY_SYNCHRONIZED_BIT_EXT,
      .initialDataSize = static_cast<size_t>(initialData.size()),
      .pInitialData = initialData.isEmpty()? nullptr: initialData.constData(),
    };

    VkPipelineCache result = VK_NULL_HANDLE;
    if(vkCreatePipelineCache(vkDev, &createInfo, nullptr, &result) != VK_SUCCESS)
      {
        result = VK_NULL_HANDLE;
        qWarning("KurisuRenderer: performance tip: can not enable pipeline cache!");
      }
    return result;
  })()),

  #ifdef Q_OS_UNIX
  fnGetSemaphoreFd(nullptr),
  fnGetMemoryFd(nullptr),
  fnGetMemoryFdProperties(nullptr),
  #elif defined(Q_OS_WINDOWS)
  fnGetSemaphoreHandle(nullptr),
  fnGetMemoryHandle(nullptr),
  #endif

  cachedStagingBuffers(KurisuConfig::getMaxCachedStagingBuffers(), nullptr),
  cachedStagingBuffer_i(cachedStagingBuffers.begin()),

  transferCmdPool(std::make_unique<VulkanCommandPool>(vkDev, queueFamilies.transferFamily.value())),

  owner(true)
{
}

VulkanDevice::~VulkanDevice()
{
  if(owner)
    {
      vkDeviceWaitIdle(vkDev);

      for(auto *i: cachedStagingBuffers)
        if(i)
          i->setAsyncTask({}, VK_NULL_HANDLE);

      transferCmdPool.reset();

      for(auto *i: cachedStagingBuffers)
        if(i)
          delete i;

      if(pipelineCache)
        {
//          if(QDir::home().mkpath(".cache/KurisuRenderer"))
//            {
//              size_t sz;
//              vkGetPipelineCacheData(vkDev, pipelineCache, &sz, nullptr);
//              if(sz > 0)
//                {
//                  std::vector<std::byte> data(sz);
//                  vkGetPipelineCacheData(vkDev, pipelineCache, &sz, data.data());

//                  const VkPipelineCacheHeaderOne *header = reinterpret_cast<const VkPipelineCacheHeaderOne*>(data.data());
//                  Q_ASSERT(header->length == sizeof(VkPipelineCacheHeaderOne));
//                  Q_ASSERT(header->version == VK_PIPELINE_CACHE_HEADER_VERSION_ONE);
//                  Q_ASSERT(header->vendorID == properties.vendorID);
//                  Q_ASSERT(header->deviceID == properties.deviceID);
//                  Q_ASSERT(std::memcmp(header->uuid, properties.pipelineCacheUUID, VK_UUID_SIZE) == 0);

//                  const auto fName =
//                      QStringLiteral("%1-%2-%3-%4-pipeline_cache").
//                      arg(QCoreApplication::organizationName(),
//                          QCoreApplication::applicationName()).
//                      arg(properties.vendorID).arg(properties.deviceID);

//                  QDir d = QDir::home();
//                  d.cd(".cache");
//                  d.cd("KurisuRenderer");

//                  QFile f(d.absoluteFilePath(fName));
//                  f.open(QFile::WriteOnly);
//                  f.write(reinterpret_cast<const char*>(data.data()), data.size());
//                  f.close();
//                }
//            }

          vkDestroyPipelineCache(vkDev, pipelineCache, nullptr);
        }
      vkDestroyDevice(vkDev, nullptr);
    }
}

VulkanDevice::VulkanDevice(VulkanDevice &&orig):
  queueFamilies(orig.queueFamilies),
  vkDev(orig.vkDev),
  phDev(orig.phDev),
  properties(orig.properties),
  graphicsQueue(orig.graphicsQueue),
  transferQueue(orig.transferQueue),
  memProperties(orig.memProperties),
  depthFormat(orig.depthFormat),
  depthStencilFormat(orig.depthStencilFormat),
  pipelineCache(orig.pipelineCache),

  #ifdef Q_OS_UNIX
  fnGetSemaphoreFd(orig.fnGetSemaphoreFd),
  fnGetMemoryFd(orig.fnGetMemoryFd),
  fnGetMemoryFdProperties(orig.fnGetMemoryFdProperties),
  #elif defined(Q_OS_WINDOWS)
  fnGetSemaphoreHandle(orig.fnGetSemaphoreHandle),
  fnGetMemoryHandle(orig.fnGetMemoryHandle),
  #endif

  cachedStagingBuffers(std::move(orig.cachedStagingBuffers)),
  cachedStagingBuffer_i(cachedStagingBuffers.begin()),

  transferCmdPool(std::move(orig.transferCmdPool)),

  owner(true)
{
  orig.owner = false;
}

std::optional<uint32_t> VulkanDevice::getPresentQueueFamily(VkSurfaceKHR sfc) const
{
  auto checkCandidate = [this, sfc] (uint32_t candidate) -> std::optional<uint32_t> {
      VkBool32 presentSupport = false;

      vkGetPhysicalDeviceSurfaceSupportKHR(phDev, candidate, sfc, &presentSupport);
      if(presentSupport)
        return candidate;
      else
        return {};
    };

  std::optional<uint32_t> result;
  if(queueFamilies.graphicsFamily.has_value())
    result = checkCandidate(queueFamilies.graphicsFamily.value());

  uint32_t candidate = 0;
  while(candidate < queueFamilies.familiesCount && !result.has_value())
    {
      result = checkCandidate(candidate);
      ++candidate;
    }

  return result;
}

std::optional<VkQueue> VulkanDevice::getPresentQueue(VkSurfaceKHR sfc) const
{
  const auto family = getPresentQueueFamily(sfc);
  if(family.has_value()) {
      if(Q_LIKELY(*family == *queueFamilies.graphicsFamily))
        return graphicsQueue;

      VkQueue result;
      vkGetDeviceQueue(vkDev, family.value(), 0, &result);
      return result;
    } else
    return {};
}

uint32_t VulkanDevice::findMemoryType(uint32_t typeFilter, VkMemoryPropertyFlags properties) const
{
  for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++)
    if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
      return i;

  throw std::runtime_error("failed to find suitable memory type!");
}

std::pair<VkDeviceMemory, bool> VulkanDevice::allocateMemory(const VkMemoryRequirements &memReq, VkMemoryPropertyFlags properties, VkExternalMemoryHandleTypeFlags exportTypes, std::optional<VkMemoryDedicatedAllocateInfo> &&dedicatedAllocateInfo) const
{
  if(Q_UNLIKELY(memReq.size == 0))
    return {VK_NULL_HANDLE, false};

  void *pDedicatedAllocateInfo = dedicatedAllocateInfo.has_value()? &dedicatedAllocateInfo.value(): nullptr;

  const VkExportMemoryAllocateInfo exportInfo = {
    .sType = VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO,
    .pNext = pDedicatedAllocateInfo,
    .handleTypes = exportTypes,
  };

  VkMemoryAllocateInfo allocateInfo = {
    .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
    .pNext = exportTypes? &exportInfo: pDedicatedAllocateInfo,
    .allocationSize = memReq.size,
    .memoryTypeIndex = findMemoryType(memReq.memoryTypeBits, properties | VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT)
  };
  VkDeviceMemory mem;
  if(vkAllocateMemory(vkDev, &allocateInfo, nullptr, &mem) != VK_SUCCESS)
    {
      allocateInfo.memoryTypeIndex = findMemoryType(memReq.memoryTypeBits, properties);
      if(vkAllocateMemory(vkDev, &allocateInfo, nullptr, &mem) != VK_SUCCESS)
        throw std::bad_alloc();
    }
  return {mem, (memProperties.memoryTypes[allocateInfo.memoryTypeIndex].propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) == VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT };
}

VkDeviceMemory VulkanDevice::importMemory(const VkMemoryRequirements &memReq, VkMemoryPropertyFlags properties, Handle handle, VkExternalMemoryHandleTypeFlagBits type, VkExternalMemoryHandleTypeFlags exportTypes, std::optional<VkMemoryDedicatedAllocateInfo> &&dedicatedAllocateInfo)
{
  uint32_t handleTypeBits = memReq.memoryTypeBits;
# ifdef Q_OS_UNIX
  VkMemoryFdPropertiesKHR fdMemProperties { .sType = VK_STRUCTURE_TYPE_MEMORY_FD_PROPERTIES_KHR };
  if(type != VK_EXTERNAL_MEMORY_HANDLE_TYPE_OPAQUE_FD_BIT)
    {
      if(!fnGetMemoryFdProperties)
        {
          fnGetMemoryFdProperties = reinterpret_cast<PFN_vkGetMemoryFdPropertiesKHR>(vkGetDeviceProcAddr(vkDev, "vkGetMemoryFdPropertiesKHR"));
          if(!fnGetMemoryFdProperties)
            fnGetMemoryFdProperties = [] (VkDevice, VkExternalMemoryHandleTypeFlagBits, int, VkMemoryFdPropertiesKHR*) -> VkResult { return VK_ERROR_EXTENSION_NOT_PRESENT; };
        }

      if(fnGetMemoryFdProperties(vkDev, type, handle, &fdMemProperties) != VK_SUCCESS)
        throw std::runtime_error("failed to detect suitable memory type for fd!");
      handleTypeBits = fdMemProperties.memoryTypeBits;
    }
# endif

  const uint32_t memoryTypeBits = memReq.memoryTypeBits & handleTypeBits;
  if(memoryTypeBits == 0)
    throw std::runtime_error("incompatible handle!");

  const VkExportMemoryAllocateInfo exportInfo {
    .sType = VK_STRUCTURE_TYPE_EXPORT_MEMORY_ALLOCATE_INFO,
    .pNext = dedicatedAllocateInfo.has_value()? &dedicatedAllocateInfo.value(): nullptr,
    .handleTypes = exportTypes,
  };

#ifdef Q_OS_UNIX
  const VkImportMemoryFdInfoKHR importInfo = {
    .sType = VK_STRUCTURE_TYPE_IMPORT_MEMORY_FD_INFO_KHR,
    .pNext = &exportInfo,
    .handleType = type,
    .fd = handle
  };
#elif defined(Q_OS_WINDOWS)
  const VkImportMemoryWin32HandleInfoKHR importInfo = {
    .sType = VK_STRUCTURE_TYPE_IMPORT_MEMORY_WIN32_HANDLE_INFO_KHR,
    .pNext = &exportInfo,
    .handleType = type,
    .handle = handle,
    .name = nullptr
  };
#else
  const auto &importInfo = exportInfo;
  throw std::runtime_error("importing device memory is not implemented for your os!");
#endif

  const VkMemoryAllocateInfo allocateInfo {
    .sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO,
    .pNext = &importInfo,
    .allocationSize = memReq.size,
    .memoryTypeIndex = findMemoryType(memoryTypeBits, properties)
  };

  VkDeviceMemory mem;
  if(vkAllocateMemory(vkDev, &allocateInfo, nullptr, &mem) != VK_SUCCESS)
    throw std::bad_alloc();
  return mem;
}

std::optional<VulkanDevice::Handle> VulkanDevice::getMemoryHandle(VkDeviceMemory mem, VkExternalMemoryHandleTypeFlagBits handleType)
{
# ifdef Q_OS_UNIX
  const VkMemoryGetFdInfoKHR getHandleInfo = {
    .sType = VK_STRUCTURE_TYPE_MEMORY_GET_FD_INFO_KHR,
    .memory = mem,
    .handleType = handleType
  };
  if(!fnGetMemoryFd)
    {
      fnGetMemoryFd = reinterpret_cast<PFN_vkGetMemoryFdKHR>(vkGetDeviceProcAddr(vkDev, "vkGetMemoryFdKHR"));
      if(!fnGetMemoryFd)
        return std::nullopt;
    }
  const auto &vkGetMemHandle = fnGetMemoryFd;
# elif defined(Q_OS_WINDOWS)
  const VkMemoryGetWin32HandleInfoKHR getHandleInfo = {
    .sType = VK_STRUCTURE_TYPE_MEMORY_GET_WIN32_HANDLE_INFO_KHR,
    .memory = mem,
    .handleType = handleType
  };
  if(!fnGetMemoryHandle)
    {
      fnGetMemoryHandle = reinterpret_cast<PFN_vkGetMemoryWin32HandleKHR>(vkGetDeviceProcAddr(vkDev, "vkGetMemoryWin32HandleKHR"));
      if(!fnGetMemoryHandle)
        return std::nullopt;
    }
  const auto &vkGetMemHandle = fnGetMemoryHandle;
# else
  const int getHandleInfo = 0;
  const auto &vkGetMemHandle = [] (VkDevice device, const void* pGetInfo, Handle* pFd) -> VkResult {return VK_ERROR_EXTENSION_NOT_PRESENT;}
  return std::nullopt;
# endif

  Handle result;
  if(vkGetMemHandle(vkDev, &getHandleInfo, &result) == VK_SUCCESS)
    return result;
  else
    return std::nullopt;
}

VulkanStagingBuffer &VulkanDevice::getStagingBuffer(const void *data, size_t dataSize)
{
  bool bufReady = true;
  auto i = cachedStagingBuffer_i;
  while(*i && !(*i)->isReady())
    {
      ++i;
      if(i == cachedStagingBuffers.end())
        i = cachedStagingBuffers.begin();
      if(i == cachedStagingBuffer_i)
        {
          bufReady = false;
          break;
        }
    }

  cachedStagingBuffer_i = i;
  if(!bufReady)
    {
      for(auto *buf: cachedStagingBuffers)
        buf->waitForTask();
      submitTransferCommands();
      for(auto *buf: cachedStagingBuffers)
        buf->setAsyncTask({}, VK_NULL_HANDLE);
    }

  if(!*cachedStagingBuffer_i)
    *cachedStagingBuffer_i = new VulkanStagingBuffer(*this, data, dataSize);
  else if(!(*cachedStagingBuffer_i)->setData(data, dataSize))
    {
      delete *cachedStagingBuffer_i;
      *cachedStagingBuffer_i = new VulkanStagingBuffer(*this, data, dataSize);
    }
  return **cachedStagingBuffer_i;
}

VulkanStagingBuffer &VulkanDevice::getStagingBuffer(const std::function<void (void *)> &dataSrc, size_t dataSize)
{
  bool bufReady = true;
  auto i = cachedStagingBuffer_i;
  while(*i && !(*i)->isReady())
    {
      ++i;
      if(i == cachedStagingBuffers.end())
        i = cachedStagingBuffers.begin();
      if(i == cachedStagingBuffer_i)
        {
          bufReady = false;
          break;
        }
    }

  cachedStagingBuffer_i = i;
  if(!bufReady)
    {
      for(auto *buf: cachedStagingBuffers)
        buf->waitForTask();
      submitTransferCommands();
      for(auto *buf: cachedStagingBuffers)
        buf->setAsyncTask({}, VK_NULL_HANDLE);
    }

  if(!*cachedStagingBuffer_i)
    *cachedStagingBuffer_i = new VulkanStagingBuffer(*this, dataSrc, dataSize);
  else if(!(*cachedStagingBuffer_i)->setData(dataSrc, dataSize))
    {
      delete *cachedStagingBuffer_i;
      *cachedStagingBuffer_i = new VulkanStagingBuffer(*this, dataSrc, dataSize);
    }
  return **cachedStagingBuffer_i;
}

VulkanCommandPool &VulkanDevice::getTransferCmdPool() const
{
  return *transferCmdPool;
}

bool VulkanDevice::submitTransferCommands()
{
  QMutexLocker l(&transferQueueM);
  const bool result = transferCmdPool->submit(transferQueue);
  return result;
}

bool VulkanDevice::submitTransferCommandsAsync(VkSemaphore finished)
{
  QMutexLocker l(&transferQueueM);
  const bool result = transferCmdPool->submitAsync(transferQueue, finished);
  return result;
}

std::optional<VulkanDevice::Handle> VulkanDevice::getSemHandle(VkSemaphore sem, VkExternalSemaphoreHandleTypeFlagBits handleType)
{
#ifdef Q_OS_UNIX
  if(!fnGetSemaphoreFd)
    {
      fnGetSemaphoreFd = reinterpret_cast<PFN_vkGetSemaphoreFdKHR>(vkGetDeviceProcAddr(vkDev, "vkGetSemaphoreFdKHR"));
      if(!fnGetSemaphoreFd)
        return std::nullopt;
    }

  const VkSemaphoreGetFdInfoKHR getHandleInfo = {
    .sType = VK_STRUCTURE_TYPE_SEMAPHORE_GET_FD_INFO_KHR,
    .semaphore = sem,
    .handleType = handleType
  };
  const auto &vkGetSemHandle = fnGetSemaphoreFd;
#elif defined(Q_OS_WINDOWS)
  if(!fnGetSemaphoreHandle)
    {
      fnGetSemaphoreHandle = reinterpret_cast<PFN_vkGetSemaphoreWin32HandleKHR>(vkGetDeviceProcAddr(vkDev, "vkGetSemaphoreWin32HandleKHR"));
      if(!fnGetSemaphoreHandle)
        return std::nullopt;
    }

  const VkSemaphoreGetWin32HandleInfoKHR getHandleInfo = {
    .sType = VK_STRUCTURE_TYPE_SEMAPHORE_GET_WIN32_HANDLE_INFO_KHR,
    .semaphore = sem,
    .handleType = handleType
  };
  const auto &vkGetSemHandle = fnGetSemaphoreHandle;
#else
  const int getHandleInfo = 0;
  const auto &vkGetSemHandle = [] (VkDevice device, const void* pGetInfo, Handle* pFd) -> VkResult {return VK_ERROR_EXTENSION_NOT_PRESENT;}
  return std::nullopt;
#endif

  Handle handle;
  if(vkGetSemHandle(vkDev, &getHandleInfo, &handle) != VK_SUCCESS)
    return std::nullopt;
  else
    return handle;
}

void VulkanDevice::waitIdle() const
{
  QMutexLocker t_l(const_cast<QMutex*>(&transferQueueM));
  transferCmdPool->waitAsyncTask();
  QMutexLocker g_l(const_cast<QMutex*>(&graphicsQueueM));
  vkDeviceWaitIdle(vkDev);
}

VkQueue VulkanDevice::getQueue(const std::optional<uint32_t> &family, uint32_t index) const
{
  VkQueue result = VK_NULL_HANDLE;
  if(family.has_value())
    vkGetDeviceQueue(vkDev, family.value(), index, &result);
  return result;
}

VkFormat VulkanDevice::getSupportedDepthStencilFormat(const std::initializer_list<VkFormat> &formats)
{
  for (VkFormat format: formats)
    {
      VkFormatProperties props;
      vkGetPhysicalDeviceFormatProperties(phDev, format, &props);
      if(props.optimalTilingFeatures & VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT)
        return format;
    }

  throw std::runtime_error("DEPTH_STENCIL_ATTACHMENT is not supported");
}
