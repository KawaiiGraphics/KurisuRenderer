#include "KurisuBufferBindings.hpp"
#include "KurisuBufferHandle.hpp"
#include "KurisuRootImpl.hpp"
#include <cstring>

KurisuBufferHandle::KurisuBufferHandle(const void *ptr, size_t n, KurisuRootImpl &root, const std::initializer_list<KawaiiBufferTarget> &availableTargets):
  r(root),
  data(ptr),
  length(n),
  availableTargets(trBufferTarget(availableTargets))
{
  buffers.reserve(r.deviceCount());
  r.forallDevices([this] (VulkanDevice &dev) {
    buffers.emplace_back(dev, this->availableTargets);
    buffers.back().setData(data, length);
  });
}

KurisuBufferHandle::~KurisuBufferHandle()
{
  r.waitForSurfacesRenderFinished();
  r.bufBindings.unbindBuffer(this);
  buffers.clear();
}

VkBufferUsageFlagBits KurisuBufferHandle::trBufferTarget(KawaiiBufferTarget target)
{
  switch(target)
    {
    case KawaiiBufferTarget::UBO:
      return VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;

    case KawaiiBufferTarget::VBO:
      return VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;

    case KawaiiBufferTarget::SSBO:
      return VK_BUFFER_USAGE_STORAGE_BUFFER_BIT;

    case KawaiiBufferTarget::CopyRead:
      return VK_BUFFER_USAGE_TRANSFER_SRC_BIT;

    case KawaiiBufferTarget::CopyWrite:
      return VK_BUFFER_USAGE_TRANSFER_DST_BIT;

    case KawaiiBufferTarget::DrawIndirect:
      return VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT;

    case KawaiiBufferTarget::ElementArray:
      return VK_BUFFER_USAGE_INDEX_BUFFER_BIT;

    case KawaiiBufferTarget::DispatchIndirect:
      return VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT;

    case KawaiiBufferTarget::TextureBuffer:
      return VK_BUFFER_USAGE_UNIFORM_TEXEL_BUFFER_BIT;

    case KawaiiBufferTarget::TransformFeedback:
    case KawaiiBufferTarget::PixelPack:
    case KawaiiBufferTarget::PixelUnpack:
    case KawaiiBufferTarget::QueryBuffer:
    case KawaiiBufferTarget::AtomicCounter:
      throw std::invalid_argument("unsupported buffer type");
    }
  Q_UNREACHABLE();
}

VkBufferUsageFlags KurisuBufferHandle::trBufferTarget(const std::initializer_list<KawaiiBufferTarget> &availableTargets)
{
  VkBufferUsageFlags flags = 0;
  for(const auto &i: availableTargets)
    flags |= trBufferTarget(i);
  return flags;
}

VulkanBuffer &KurisuBufferHandle::buffer(size_t device_index)
{
  return buffers[device_index];
}

void KurisuBufferHandle::bindTexture(const std::string &fieldName, KurisuTextureHandle *tex)
{
  if(Q_LIKELY(tex))
    textures[fieldName] = tex;
  else
    unbindTexture(fieldName);
}

void KurisuBufferHandle::unbindTexture(const std::string &fieldName)
{
  if(auto el = textures.find(fieldName); el != textures.end())
    textures.erase(el);
}

void KurisuBufferHandle::unbindAllTextures()
{
  textures.clear();
}

const std::unordered_map<std::string, KurisuTextureHandle *> &KurisuBufferHandle::getBindedTextures() const
{
  return textures;
}

void KurisuBufferHandle::setBufferData(const void *ptr, size_t n)
{
  if(length && length != n)
    invalidate();
  else {
      data = ptr;
      length = n;
      for(auto &buf: buffers)
        buf.setData(data, length);
    }
}

void KurisuBufferHandle::sendChunkToGpu(size_t offset, size_t n)
{
  for(auto &buf: buffers)
    buf.updateSubData(n, offset);
}

size_t KurisuBufferHandle::getBufferSize() const
{
  return length;
}

void KurisuBufferHandle::bind(KawaiiBufferTarget target)
{
  r.bufBindings.bindBuffer(target, this);
}

void KurisuBufferHandle::unbind(KawaiiBufferTarget target)
{
  r.bufBindings.unbindBuffer(target, this);
}

void KurisuBufferHandle::bindToBlock(KawaiiBufferTarget target, uint32_t bindingPoint)
{
  r.bufBindings.bindBufferBlock(target, bindingPoint, this);
}

void KurisuBufferHandle::userBinding(KawaiiBufferTarget target, uint32_t bindingPoint)
{
  r.bufBindings.bindBufferBlockUser(target, bindingPoint, this);
}

void KurisuBufferHandle::unbindFromBlock(KawaiiBufferTarget target, uint32_t bindingPoint)
{
  r.bufBindings.unbindBufferBlock(target, bindingPoint, this);
}
