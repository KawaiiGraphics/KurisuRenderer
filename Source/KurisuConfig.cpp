#include "KurisuConfig.hpp"
#include <QProcessEnvironment>
#include <sib_utils/Strings.hpp>

namespace {
  const QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
}

KurisuConfig::StaticConfig KurisuConfig::sharedCfg(env.value("SIB3D_GPUS", "").split(',', Qt::SkipEmptyParts),
                                                   env.value("SIB3D_GPU", "0").toULongLong(),
                                                   std::max<uint64_t>(env.value("SIB3D_CACHED_STAGING_BUFS", "8").toULongLong(), 1),
                                                   env.value("SIB3D_VULKAN_GL_INTEROP", "1").toUShort()
                                                   );

KurisuConfig::KurisuConfig(const VulkanDeviceManager &devices)
{
  const size_t gpuCount = devices.allDevices().size();
  composerGpu = std::min<uint64_t>(sharedCfg.preferredComposerGpu, gpuCount-1);
  if(sharedCfg.usingGpus.empty())
    {
      usingGpus.resize(gpuCount);
      for(uint64_t i = 0; i < gpuCount; ++i)
        usingGpus[i] = i;
    } else
    {
      usingGpus.reserve(sharedCfg.usingGpus.size());
      for(uint64_t el: sharedCfg.usingGpus)
        usingGpus.push_back(std::min<uint64_t>(el, gpuCount-1));
    }
}

KurisuConfig::~KurisuConfig()
{
}

uint64_t KurisuConfig::getComposerGpu() const
{
  return composerGpu;
}

const std::vector<uint64_t> &KurisuConfig::getUsingGpus() const
{
  return usingGpus;
}

uint64_t KurisuConfig::getMaxCachedStagingBuffers()
{
  return sharedCfg.maxCachedStagingBuffers;
}

bool KurisuConfig::isGlInteropAllowed()
{
  return sharedCfg.glInterop;
}

KurisuConfig::StaticConfig::StaticConfig(const QStringList &usingGpus, uint64_t composerGpu, uint64_t maxCachedStagingBuffers, bool glInterop):
  preferredComposerGpu(composerGpu),
  maxCachedStagingBuffers(maxCachedStagingBuffers),
  glInterop(glInterop)
{
  this->usingGpus.reserve(usingGpus.size());
  for(const auto &i: usingGpus)
    this->usingGpus.push_back(i.toULongLong());
}
