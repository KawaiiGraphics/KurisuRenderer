#include "SingleTimeCommands.hpp"
#include "VulkanTexture.hpp"

#include <QtConcurrent>
#include <array>

SingleTimeCommands::SingleTimeCommands(const VulkanDevice &dev_info):
  dev(dev_info.vkDev),
  pool(dev_info.getTransferCmdPool()),
  commited(false)
{
}

SingleTimeCommands::~SingleTimeCommands()
{
  if(!commited)
    write();
}

void SingleTimeCommands::copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, std::vector<VkBufferCopy> &regions)
{
  commands.push_back([srcBuffer, dstBuffer, &regions] (VkCommandBuffer buf) {
      vkCmdCopyBuffer(buf, srcBuffer, dstBuffer, regions.size(), regions.data());
    });
}

void SingleTimeCommands::transitionImageLayout(VkImage image, VkImageSubresourceRange subresourceRange, VkImageLayout oldLayout, VkImageLayout newLayout, uint32_t srcQFamily, uint32_t dstQFamily)
{
  if(oldLayout == newLayout) return;

  VkPipelineStageFlags sourceStage = 0;
  VkPipelineStageFlags destinationStage = 0;

  VkImageMemoryBarrier barrier = {
    .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
    .oldLayout = oldLayout,
    .newLayout = newLayout,
    .srcQueueFamilyIndex = srcQFamily,
    .dstQueueFamilyIndex = dstQFamily,
    .image = image,
    .subresourceRange = subresourceRange
  };

  if (oldLayout == VK_IMAGE_LAYOUT_UNDEFINED)
    {
      barrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
      sourceStage = VK_PIPELINE_STAGE_HOST_BIT;
    } else if (oldLayout == VK_IMAGE_LAYOUT_GENERAL)
    {
      barrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT;
      sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    } else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
      barrier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
      sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if (oldLayout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
    {
      barrier.srcAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
      sourceStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if(oldLayout == VK_IMAGE_LAYOUT_PREINITIALIZED)
    {
      barrier.srcAccessMask = VK_ACCESS_HOST_WRITE_BIT;
      sourceStage = VK_PIPELINE_STAGE_HOST_BIT;
    } else if (oldLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)
    {
      barrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT | VK_ACCESS_MEMORY_WRITE_BIT;
      sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    } else if (oldLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
      barrier.srcAccessMask = VK_ACCESS_MEMORY_READ_BIT;
      sourceStage = VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT;
    } else if(oldLayout != newLayout)
    throw std::invalid_argument("unsupported layout transition!");

  if (newLayout == VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL)
    {
      barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
      destinationStage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;

#     ifndef Q_OS_ANDROID
    } else if (newLayout == VK_IMAGE_LAYOUT_DEPTH_READ_ONLY_OPTIMAL)
    {
      barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
      destinationStage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
#     endif

    } else if (newLayout == VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL) {
      barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
      destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if (newLayout == VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL) {
      barrier.dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT;
      destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;
    } else if (newLayout == VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)
    {
      barrier.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;
      destinationStage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
    } else if (newLayout == VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL) {
      barrier.dstAccessMask = VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;
      destinationStage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
    } else if (newLayout == VK_IMAGE_LAYOUT_GENERAL)
    {
      barrier.dstAccessMask = VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_SHADER_WRITE_BIT;
      destinationStage = VK_PIPELINE_STAGE_ALL_COMMANDS_BIT;
    } else if(oldLayout != newLayout)
    throw std::invalid_argument("unsupported layout transition!");

  commands.push_back([sourceStage, destinationStage, barrier] (VkCommandBuffer buf) {
      vkCmdPipelineBarrier (
            buf,
            sourceStage, destinationStage,
            0,
            0, nullptr,
            0, nullptr,
            1, &barrier );
    });
}

void SingleTimeCommands::copyBufferToImage(VkBuffer buffer, VkImage image, VkDeviceSize bufOffset, VkOffset3D offset, VkExtent3D size, VkImageSubresourceLayers imgSubresourceLayers)
{
  commands.push_back([offset, bufOffset, size, imgSubresourceLayers, buffer, image] (VkCommandBuffer buf) {
      const VkBufferImageCopy region {
        .bufferOffset = bufOffset,
        .imageSubresource = imgSubresourceLayers,
        .imageOffset = offset,
        .imageExtent = size
      };

      vkCmdCopyBufferToImage(buf, buffer, image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);
    });
}

void SingleTimeCommands::copyImageToBuffer(VkImage image, VkBuffer buffer, VkOffset3D offset, VkExtent3D size, VkDeviceSize bufOffset, VkImageSubresourceLayers imgSubresourceLayers)
{
  commands.push_back([offset, bufOffset, size, imgSubresourceLayers, buffer, image] (VkCommandBuffer buf) {
      const VkBufferImageCopy region {
        .bufferOffset = bufOffset,
        .imageSubresource = imgSubresourceLayers,
        .imageOffset = offset,
        .imageExtent = size
      };
      vkCmdCopyImageToBuffer(buf, image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, buffer, 1, &region);
    });
}

void SingleTimeCommands::copyImage(const VulkanTexture &from, const VulkanTexture &to, const std::vector<VkImageCopy> &regions)
{
  commands.push_back([&from, &to, regions] (VkCommandBuffer cmdBuf) {
      SingleTimeCommands::copyImageFunc(cmdBuf, from, to, regions, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT);
    });
}

void SingleTimeCommands::copyImage(VkImage from, VkImageLayout fromInitLayout, const VulkanTexture &to, const std::vector<VkImageCopy> &regions)
{
  commands.push_back([from, fromInitLayout, &to, regions] (VkCommandBuffer cmdBuf) {
      SingleTimeCommands::copyImageFunc(cmdBuf, from, fromInitLayout, to.getVkImage(), to.getVkImageLayout(), regions, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT);
    });
}

QFuture<void> SingleTimeCommands::write()
{
  struct CmdBufOrphan {
    VulkanCommandPool *pool;
    std::vector<std::function<void(VkCommandBuffer)>> commands;
  };
  CmdBufOrphan *cmdBufOrphan = new CmdBufOrphan {&pool, std::move(commands)};

  commited = true;

  return QtConcurrent::run([cmdBufOrphan] {
      cmdBufOrphan->pool->writeCommands(cmdBufOrphan->commands);
      delete cmdBufOrphan;
    });
}

void SingleTimeCommands::copyImageFunc(VkCommandBuffer cmdBuf, VkImage from, VkImageLayout fromInitLayout, VkImage to, VkImageLayout toInitLayout, const std::vector<VkImageCopy> &regions, VkPipelineStageFlags sourceStage)
{
  VkPipelineStageFlags destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;

  std::array<VkImageMemoryBarrier, 2> barriers = {
    VkImageMemoryBarrier {
      .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
      .srcAccessMask = VK_ACCESS_MEMORY_WRITE_BIT,
      .dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
      .oldLayout = fromInitLayout,
      .newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
      .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
      .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
      .image = from,
      .subresourceRange = VkImageSubresourceRange {
        .aspectMask = regions[0].srcSubresource.aspectMask,
        .levelCount = 1,
        .layerCount = 1,
      }
    },
    VkImageMemoryBarrier {
      .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
      .srcAccessMask = VK_ACCESS_MEMORY_WRITE_BIT | VK_ACCESS_MEMORY_READ_BIT,
      .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
      .oldLayout = toInitLayout,
      .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
      .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
      .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
      .image = to,
      .subresourceRange = VkImageSubresourceRange {
        .aspectMask = regions[0].dstSubresource.aspectMask,
        .levelCount = 1,
        .layerCount = 1,
      }
    }
  };

  vkCmdPipelineBarrier ( cmdBuf,
                         sourceStage, destinationStage,
                         0,
                         0, nullptr,
                         0, nullptr,
                         2, barriers.data() );

  vkCmdCopyImage(cmdBuf, from, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, to, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, regions.size(), regions.data());

  std::swap(barriers[0].oldLayout, barriers[0].newLayout);
  std::swap(barriers[0].srcAccessMask, barriers[0].dstAccessMask);
  std::swap(barriers[1].oldLayout, barriers[1].newLayout);
  std::swap(barriers[1].srcAccessMask, barriers[1].dstAccessMask);

  if(barriers[0].newLayout == VK_IMAGE_LAYOUT_UNDEFINED)
    std::swap(barriers[0], barriers[1]);

  if(barriers[1].newLayout != VK_IMAGE_LAYOUT_UNDEFINED)
    vkCmdPipelineBarrier ( cmdBuf,
                           VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                           0,
                           0, nullptr,
                           0, nullptr,
                           2, barriers.data() );
  else if(barriers[0].newLayout != VK_IMAGE_LAYOUT_UNDEFINED)
    vkCmdPipelineBarrier ( cmdBuf,
                           VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                           0,
                           0, nullptr,
                           0, nullptr,
                           1, barriers.data() );
}

void SingleTimeCommands::copyImageFunc(VkCommandBuffer cmdBuf, const VulkanTexture &from, const VulkanTexture &to, const std::vector<VkImageCopy> &regions, VkPipelineStageFlags sourceStage)
{
  copyImageFunc(cmdBuf,
                from.getVkImage(), from.getVkImageLayout(),
                to.getVkImage(), to.getVkImageLayout(),
                regions, sourceStage);
}
