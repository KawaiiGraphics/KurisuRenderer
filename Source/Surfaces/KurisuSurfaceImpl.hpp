#ifndef KURISUSURFACEIMPL_HPP
#define KURISUSURFACEIMPL_HPP

#include <KawaiiRenderer/Surfaces/KawaiiSurfaceImpl.hpp>
#include "../KurisuRenderer_global.hpp"
#include "../Vulkan/VulkanSwapchain.hpp"
#include "../Vulkan/VulkanRenderTarget.hpp"
#include "../Vulkan/RenderingCommands.hpp"
#include "../Vulkan/VulkanSemaphore.hpp"
#include "../Vulkan/VulkanGBuf.hpp"

#include <unordered_map>
#include <sib_utils/PairHash.hpp>

class KurisuRootImpl;
class KURISURENDERER_SHARED_EXPORT KurisuSurfaceImpl : public KawaiiSurfaceImpl
{
public:
  KurisuSurfaceImpl(KawaiiSurface *model);
  ~KurisuSurfaceImpl();

  void waitForRenderFinished();

  uint32_t getImgCount() const;
  size_t getAsyncFrames() const;

  size_t getFrameIndex() const;

  size_t getActiveGpu() const;


  // KawaiiRendererImpl interface
private:
  void initConnections() override final;

  // KawaiiSurfaceImpl interface
private:
  KurisuRootImpl* getRoot() const;

  void resize(const QSize &sz) override final;

  bool startRendering() override final;
  void finishRendering() override final;

  void invalidate() override final;



  //IMPLEMENT
private:
  std::unique_ptr<VulkanSwapchain> swapchain;
  std::unique_ptr<RenderingCommands> copyToSwapchainCmd;

  size_t composeFrameIndex;
  std::vector<size_t> frameIndex;
  std::vector<VulkanSemaphore> aquireImageSemaphore;
  std::vector<VulkanSemaphore> frameRenderedSemaphore;
  std::vector<VkFence> frameRenderedFence;
  std::vector<std::unique_ptr<QMutex>> frameRenderedFenceM;
  std::vector<QFuture<void>> finishRenderingTask;

  std::vector<uint64_t>::const_iterator currentGpu;
  std::vector<uint64_t>::const_iterator previousGpu;

  std::atomic_int32_t pendingUpdates;

  void initSwapchain();
  void forceResize();

  void setTileRenderpass(size_t tileIndex, KawaiiRenderpass *rp);
  void writeCopyToSwapchainCmd();
  void selectNextGpu();
};

#endif // KURISUSURFACEIMPL_HPP
