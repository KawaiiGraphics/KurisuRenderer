#ifndef VULKANOVERLAYCTX_HPP
#define VULKANOVERLAYCTX_HPP

#include <QOpenGLPaintDevice>
#include <QOffscreenSurface>
#include <QOpenGLContext>
#include <qopenglext.h>
//#include <GL/glext.h>
//#include <GLES2/gl2ext.h>
#include <memory>

#include "VulkanDevice.hpp"

class VulkanTexture;

struct GlExternalTexture {
  GLuint tex;
  GLuint mem;
};

class VulkanOverlayCtx
{
public:
  VulkanOverlayCtx(bool allowVulkanInterop);
  ~VulkanOverlayCtx();

  void init();
  void makeCurrent();

  GLuint importVulkanSemaphore(VulkanDevice::Handle fd);
  GlExternalTexture importVulkanTexture(const VulkanTexture &tex);

  void signalGlSemaphore(GLuint sem, const GlExternalTexture &tex, GLenum dstLayout);

  void free(GlExternalTexture &tex);
  void deleteGlSemaphore(GLuint sem);

  QOpenGLFunctions *gl();

  bool vulkanInteropSupported() const;

  static VulkanOverlayCtx& globalOverlayCtx();



  // IMPLEMENT
private:
#ifdef Q_OS_UNIX
  inline static constexpr GLenum opaqueHandleType = GL_HANDLE_TYPE_OPAQUE_FD_EXT;
#elif defined(Q_OS_WINDOWS)
  inline static constexpr GLenum opaqueHandleType = GL_HANDLE_TYPE_OPAQUE_WIN32_KMT_EXT;
#endif

  QOpenGLContext ctx;
  QOffscreenSurface sfc;

  void (*glGenSemaphoresEXT) (GLsizei n, GLuint* semaphores);
  void (*glSignalSemaphoreEXT) (GLuint semaphore, GLuint numBufferBarriers, const GLuint *buffers, GLuint numTextureBarriers, const GLuint *textures, const GLenum *dstLayouts);
  void (*glDeleteSemaphoresEXT) (GLsizei n, const GLuint *semaphores);

  void (*glCreateMemoryObjectsEXT) (GLsizei n, GLuint *memoryObjects);
  void (*glDeleteMemoryObjectsEXT) (GLsizei n, const GLuint *memoryObjects);

  void (*glImportSemHandleEXT) (GLuint semaphore, GLenum handleType, VulkanDevice::Handle fd);
  void (*glImportMemHandleEXT) (GLuint memory, GLuint64 size, GLenum handleType, VulkanDevice::Handle fd);

  void (*glTexStorageMem2DEXT) (GLenum target, GLsizei levels, GLenum internalFormat, GLsizei width, GLsizei height, GLuint memory, GLuint64 offset);

  bool vulkanInterop;
};

#endif // VULKANOVERLAYCTX_HPP
