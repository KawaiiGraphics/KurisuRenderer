#include <KawaiiRenderer/Textures/KawaiiTextureImpl.hpp>
#include "Surfaces/KurisuSurfaceImpl.hpp"
#include "KurisuRootImpl.hpp"

#include "KurisuEnvMapImpl.hpp"

namespace {
  inline const glm::mat4 vulkanToGlClipCorrectionEnvmap(1.0f, 0.0f, 0.0f, 0.0f,
                                                        0.0f, 1.0f, 0.0f, 0.0f,
                                                        0.0f, 0.0f, 0.5f, 0.0f,
                                                        0.0f, 0.0f, 0.5f, 1.0f);
}

KurisuEnvMapImpl::KurisuEnvMapImpl(KawaiiEnvMap *model):
  KawaiiEnvMapImpl(model),
  layer(0)
{
  model->setClipCorrectionMatrix(vulkanToGlClipCorrectionEnvmap);
  onRootChanged();
}

KurisuEnvMapImpl::~KurisuEnvMapImpl()
{
  preDestruct();
}

VulkanBaseRenderpass &KurisuEnvMapImpl::getRp(size_t device_index) const
{
  return sceneRenderers[device_index]->rp;
}

std::vector<VkFramebuffer> KurisuEnvMapImpl::getFbos(size_t device_index) const
{
  return { sceneRenderers[device_index]->fbo_layers[layer].getFbo() };
}

void KurisuEnvMapImpl::updateCache(KawaiiEnvMapImpl::DrawCache &c, const QRect &viewport)
{
  auto *cache = static_cast<CachedRenderpass*>(&c);
  cache->reset(this);
  cache->draw(this, viewport);
}

KawaiiEnvMapImpl::DrawCache *KurisuEnvMapImpl::createDrawCache(const QRect &viewport)
{
  CachedRenderpass *cache = new CachedRenderpass(root());
  cache->drawFunc = std::bind(&CachedRenderpass::exec, cache, this);
  cache->draw(this, viewport);

  return cache;
}

void KurisuEnvMapImpl::onRootChanged()
{
  disconnect(onRootBufferBindingChanged);
  if(root())
    onRootBufferBindingChanged = connect(root(), &KurisuRootImpl::bufferBindingChanged, this, [this] {
        markCacheDirty();
      });
}

void KurisuEnvMapImpl::attachTexture(KawaiiTextureImpl *tex, KawaiiEnvMap::AttachmentMode mode, int layer)
{
  auto texHandle = static_cast<KurisuTextureHandle*>(tex->getHandle());
  if(!texHandle->isAttachable())
    {
      tex->getData()->setProperty("UsedInFbo", true);
      texHandle->askRecreate();
      return;
    }

  auto el = activeAttachements.find(mode);
  if(el == activeAttachements.end())
    {
      cachedAttachements.clear();
      sceneRenderers.clear();
      el = activeAttachements.insert({mode, {texHandle, layer}}).first;
    } else if(el->second.first != texHandle || el->second.second != layer)
    {
      cachedAttachements.clear();
      sceneRenderers.clear();
      el->second = {texHandle, layer};
    }
}

void KurisuEnvMapImpl::attachDepthRenderbuffer(const QSize &)
{
}

void KurisuEnvMapImpl::detachTexture(KawaiiEnvMap::AttachmentMode mode)
{
  auto el = activeAttachements.find(mode);
  if(el != activeAttachements.end())
    {
      cachedAttachements.clear();
      sceneRenderers.clear();
      activeAttachements.erase(el);
    }
}

void KurisuEnvMapImpl::deleteRenderbuffers()
{
}

void KurisuEnvMapImpl::setRenderLayer(size_t layer)
{
  checkSceneRenderers();
  this->layer = layer;
  root()->setOffscreenTarget(this);

  auto &cmd = root()->getActiveCommands();
  if(layer>0 && cmd.isPrimary())
    {
      cmd.endRenderpass();

      size_t device_index = root()->getActiveDevice();
      auto &rp = getRp(device_index);
      cmd.beginRenderpass(rp.getRenderpass(), getFbos(device_index), rp.getClearValues(), VK_SUBPASS_CONTENTS_INLINE);
    }
}

KurisuRootImpl *KurisuEnvMapImpl::root()
{
  return static_cast<KurisuRootImpl*>(getRoot());
}

void KurisuEnvMapImpl::checkSceneRenderers()
{
  if(cachedAttachements.empty())
    {
      cachedAttachements.resize(2);

      cachedAttachements[0] = ([this] () -> std::pair<KurisuTextureHandle*, int> {
                                 auto el = activeAttachements.find(KawaiiEnvMap::AttachmentMode::Color);
                                 if(el == activeAttachements.end())
                                   return {nullptr, -1};
                                 else
                                   return el->second;
                               })();

      cachedAttachements[1] = ([this] () -> std::pair<KurisuTextureHandle*, int> {
                                 auto el = activeAttachements.find(KawaiiEnvMap::AttachmentMode::Depth);
                                 if(el == activeAttachements.end())
                                   el = activeAttachements.find(KawaiiEnvMap::AttachmentMode::DepthStencil);
                                 if(el == activeAttachements.end())
                                   return {nullptr, -1};
                                 return el->second;
                               })();

      sceneRenderers.reserve(root()->deviceCount());
      root()->forallDevices([this](VulkanDevice &dev, size_t device_index) {
        std::vector<std::pair<VulkanTexture*, int>> attachements(cachedAttachements.size(), {nullptr, -1});
        for(size_t i = 0; i < attachements.size(); ++i)
          if(cachedAttachements[i].first)
            attachements[i] = { &cachedAttachements[i].first->getTexture(device_index), cachedAttachements[i].second };

        sceneRenderers.emplace_back(std::make_unique<SceneRenderer>(dev, std::move(attachements)));
      });
    }
}

KurisuEnvMapImpl::CachedRenderpass::CachedRenderpass(KurisuRootImpl *root):
  cmd(root->deviceCount())
{
  for(size_t i = 0; i < cmd.size(); ++i)
    cmd[i] = std::make_unique<RenderingCommands>(root->getDevice(i), 1, root->getActiveSurface()->getAsyncFrames(), true);
}

KurisuEnvMapImpl::CachedRenderpass::~CachedRenderpass()
{
  cmd.clear();
}

void KurisuEnvMapImpl::CachedRenderpass::reset(KurisuEnvMapImpl *envmap)
{
  for(size_t i = 0; i < cmd.size(); ++i)
    {
      envmap->root()->getGpuOffscrResLocked(i).unlock();
      cmd[i]->reset();
      envmap->root()->getGpuOffscrResLocked(i).relock();
    }
}

void KurisuEnvMapImpl::CachedRenderpass::draw(KurisuEnvMapImpl *envmap, const QRect &viewport)
{
  envmap->checkSceneRenderers();
  envmap->root()->setColorBlendAttachmentState(
        { VkPipelineColorBlendAttachmentState {
            .blendEnable = VK_FALSE,
            .srcColorBlendFactor = VK_BLEND_FACTOR_ONE,
            .dstColorBlendFactor = VK_BLEND_FACTOR_ZERO,
            .colorBlendOp = VK_BLEND_OP_ADD,
            .srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE,
            .dstAlphaBlendFactor = VK_BLEND_FACTOR_ZERO,
            .alphaBlendOp = VK_BLEND_OP_ADD,
            .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT
          } });

  envmap->root()->setDepthState(
        VK_COMPARE_OP_LESS,
        true,
        true);

  for(size_t i = 0; i < cmd.size(); ++i)
    {
      const auto framebuffers = envmap->getFbos(i);
      auto &rp = envmap->getRp(i);

      cmd[i]->setViewport(viewport);
      cmd[i]->startRecording(rp.getRenderpass(), framebuffers, rp.getClearValues(), VK_SUBPASS_CONTENTS_INLINE, 0);
      envmap->root()->activateCommands(cmd[i].get(), i);
      try {
        envmap->drawScene();
        dirty = false;
      } catch(...)
      { dirty = true; }

      if(cmd[i]->isPrimary())
        cmd[i]->endRenderpass();
      cmd[i]->finishRecording();
    }
  envmap->root()->releaseCommands();

  this->viewport = viewport;
}

void KurisuEnvMapImpl::CachedRenderpass::exec(KurisuEnvMapImpl *envmap)
{
  const size_t i = envmap->root()->getActiveSurface()->getActiveGpu();
  if(cmd[i]->isPrimary())
    {
      QReadLocker l(&envmap->root()->getDevice(i).resLock);
      cmd[i]->render(0, envmap->root()->getActiveSurface()->getFrameIndex());
    }
}

namespace {
  std::vector<std::pair<VulkanTexture *, int>> prepareTextures(const std::vector<std::pair<VulkanTexture *, int>> &tex, int face)
  {
    std::vector<std::pair<VulkanTexture *, int>> textures(tex);
    for(auto &i: textures)
      {
        if(i.second < 0)
          i.second = 0;
        i.second = 6 * i.second + face;
      }
    return textures;
  }
}

namespace {
  QSize getFirstExtent(const std::vector<std::pair<VulkanTexture *, int>> &textures)
  {
    for(const auto &i: textures)
      if(i.first)
        return QSize(i.first->getExtent().width, i.first->getExtent().height);
    return QSize(1,1);
  }
}

KurisuEnvMapImpl::SceneRenderer::SceneRenderer(VulkanDevice &dev, std::vector<std::pair<VulkanTexture *, int> > &&textures):
  rp(dev, textures),
  extent(getFirstExtent(textures)),
  fbo_layers({
             VulkanFbo { rp, extent, prepareTextures(textures, 0) },
             VulkanFbo { rp, extent, prepareTextures(textures, 1) },
             VulkanFbo { rp, extent, prepareTextures(textures, 2) },
             VulkanFbo { rp, extent, prepareTextures(textures, 3) },
             VulkanFbo { rp, extent, prepareTextures(textures, 4) },
             VulkanFbo { rp, extent, prepareTextures(textures, 5) }
             })
{
}
