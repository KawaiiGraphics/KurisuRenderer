#ifndef KURISURENDERPASSIMPL_HPP
#define KURISURENDERPASSIMPL_HPP

#include <KawaiiRenderer/Renderpass/KawaiiRenderpassImpl.hpp>
#include "../KurisuRenderer_global.hpp"
#include "../Vulkan/VulkanBaseRenderpass.hpp"
#include "../Vulkan/VulkanRenderTarget.hpp"
#include "../Vulkan/RenderingCommands.hpp"
#include "../Vulkan/VulkanGBuf.hpp"
#include <unordered_set>
#include <optional>
#include <QFuture>
#include <vector>

class KurisuRootImpl;
class KurisuGlInteropLayer;
class KURISURENDERER_SHARED_EXPORT KurisuRenderpassImpl: public KawaiiRenderpassImpl
{
  Q_OBJECT

public:
  struct ExternalGBufDescr
  {
    VkImageView imgView;
    VkImage img;
    VkImageLayout layout;
    VkImageSubresourceRange subresourceRange;
    VkFormat fmt;
  };

  KurisuRenderpassImpl(KawaiiRenderpass *renderpass);
  ~KurisuRenderpassImpl();

  size_t getAsyncFrames() const;
  void setAsyncFrames(size_t newAsyncFrames);

  VulkanBaseRenderpass &getRenderpass(size_t device_index);

  void prepareRendering(size_t device, size_t frame_index);

  VulkanGBuf& getGbuf(size_t dev_i, size_t gBufIndex, size_t frame_index);

  void setExternalGbuf(size_t dev_i, size_t index, VkImageView imgView, VkImage img, VkImageLayout layout, VkImageSubresourceRange subresourceRange, VkFormat fmt);
  ExternalGBufDescr getExternalGbuf(size_t dev_i, size_t index) const;
  void unuseExternalGbufs();
  void forallExternalGbufs(size_t dev_i, const std::function<void(size_t, const ExternalGBufDescr&)> &func);
  void setSharedGbufs(const std::initializer_list<size_t> &newSharedGbufs);

  VkImage getGBufImage(size_t dev_i, size_t gbuf_index, size_t frame_index);
  VkImageView getGBufImageView(size_t dev_i, size_t gbuf_index, size_t frame_index);

  VkImageSubresourceRange getGBufSubresourceRange(size_t dev_i, size_t gbuf_index);

  std::vector<VkFramebuffer> getFbo(size_t dev_i);

  KurisuRootImpl *getRoot() const;

  static VkClearValue defaultClearValue(KawaiiRenderpass::InternalImgFormat fmt);
  VkAttachmentDescription attDescription(quint32 gbuf_index);
  VkImageLayout getInputGbufImgLayout(uint32_t subpass, uint32_t inputAttachmentIndex);

  RenderingCommands& getCmdbuf(size_t dev_index) const;

  size_t getActiveDevice() const;
  size_t getFrameIndex() const;

  void waitForRendered(size_t dev_i, size_t frame_i) const;

  bool getWasUpdated() const;

  // KawaiiRenderpassImpl interface
private:
  void create() override final;
  void destroy() override final;
  bool start() override final;
  void nextSubpass() override final;
  void finish() override final;
  void drawCache() override final;



  // IMPLEMENT
private:
  std::vector<VulkanBaseRenderpass> renderpass; // [dev_index]
  std::vector<std::unique_ptr<RenderingCommands>> cmd; // [dev_index]
  std::vector<std::vector<VulkanRenderTarget>> renderTargets; // [dev_index][frameIndex]
  std::vector<std::vector<VulkanGBuf>> gBufs; // [dev_index][gBufIndex]
  std::vector<std::vector<std::vector<VulkanGBuf>>> gBufsShared; // [dev_index][frameIndex][gBufIndex]
  std::unordered_set<quint32> nonlocalGBufs;
  std::vector<std::vector<ExternalGBufDescr>> externalGBufs; // [dev_index][gBufIndex]
  std::unordered_set<size_t> sharedGbufs;
  std::vector<VkDeviceMemory> gBufMemory; // [dev_index]

  std::vector<std::vector<std::vector<VkSemaphore>>> waitSemaphores; // [dev_index][frameIndex][semaphoreIndex]
  std::vector<VkPipelineStageFlags> waitStages; // [semaphoreIndex]

  std::vector<KurisuGlInteropLayer*> interopLayers;
  std::vector<std::vector<VkFence>> renderedFence; // [dev_index][frameIndex]

  std::vector<bool> needRedraw; // [dev_index]

  KurisuRootImpl *root;
  size_t asyncFrames;

  size_t activeDevice;
  size_t frameIndex;
  bool wasUpdated;

  void setRoot(KurisuRootImpl *newRoot);
  void initVulkanRenderpass(VulkanBaseRenderpass &rp);

  void createCmd();
  void initCmd(size_t i);

  void onModelParentChanged();

  VkImageLayout checkGBufLayout(quint32 gbuf_index, VkImageLayout layout);

  std::vector<float> readGbufF (uint32_t gbuf, const QRect &reg);
  std::vector<int32_t> readGbufI (uint32_t gbuf, const QRect &reg);
  std::vector<uint32_t> readGbufU (uint32_t gbuf, const QRect &reg);
};

#endif // KURISURENDERPASSIMPL_HPP
