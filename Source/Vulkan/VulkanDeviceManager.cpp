#include "VulkanDeviceManager.hpp"

#include <QProcessEnvironment>
#include <QDebug>

#include <variant>
#include <vector>
#include <array>

namespace {
  std::vector<const char*> get_cstrs(const std::vector<std::string> &strs)
  {
    std::vector<const char*> result;
    result.reserve(strs.size());
    for(const auto &i: strs)
      result.push_back(i.c_str());
    return result;
  }
}

VulkanDeviceManager::VulkanDeviceManager(VkInstance instance, const std::vector<std::string> &device_extensions,
                                         const std::vector<std::string> &optional_device_extensions):
  inst(instance),
  extensions(device_extensions),
  extensions_cstr(get_cstrs(this->extensions)),
  optional_extensions(optional_device_extensions.cbegin(), optional_device_extensions.cend())
{
  createDevices();
}

VulkanDeviceManager::~VulkanDeviceManager()
{
  devices.clear();
}

VulkanDevice &VulkanDeviceManager::device(size_t i)
{
  return devices[i];
}

const VulkanDevice &VulkanDeviceManager::device(size_t i) const
{
  return devices[i];
}

const std::vector<VulkanDevice> &VulkanDeviceManager::allDevices() const
{
  return devices;
}

VulkanDevice::QueueFamilyIndices VulkanDeviceManager::findQueueFamilies(VkPhysicalDevice device)
{
  VulkanDevice::QueueFamilyIndices indices;

  vkGetPhysicalDeviceQueueFamilyProperties(device, &indices.familiesCount, nullptr);

  std::vector<VkQueueFamilyProperties> queueFamilies(indices.familiesCount);
  vkGetPhysicalDeviceQueueFamilyProperties(device, &indices.familiesCount, queueFamilies.data());

  indices.transferQCount = 0;

  for(uint32_t i = 0; i < queueFamilies.size(); ++i)
    if (queueFamilies[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
      {
        indices.graphicsFamily = i;
        break;
      }

  for(uint32_t i = 0; i < queueFamilies.size(); ++i)
    if (queueFamilies[i].queueFlags & VK_QUEUE_TRANSFER_BIT && queueFamilies[i].queueCount > indices.transferQCount)
      {
        indices.transferFamily = i;
        indices.transferQCount = queueFamilies[i].queueCount;
      }
  return indices;
}

void VulkanDeviceManager::createDevices()
{
  static const std::vector<std::variant<size_t, std::string>> deviceFilters = ([] {
      const QStringList lst = QProcessEnvironment::systemEnvironment().value("SIB3D_GPU_FILTER", "0").split(',');

      std::vector<std::variant<size_t, std::string>> result;
      for(const auto &i: lst)
      {
        bool isNumber;
        const auto num = i.toULong(&isNumber);
        if(isNumber)
          result.push_back(num);
        else
          result.push_back(i.toStdString());
      }
      return result;
    })();

  uint32_t deviceCount = 0;

  // Query the number of devices
  if(vkEnumeratePhysicalDevices(inst, &deviceCount, nullptr) != VK_SUCCESS)
    return;

  std::vector<VkPhysicalDevice> phDevs(deviceCount);

  if(vkEnumeratePhysicalDevices(inst, &deviceCount, phDevs.data()) != VK_SUCCESS)
    return;

  for(size_t p = 0; p < phDevs.size(); ++p)
    {
      const auto &phDev = phDevs[p];

      const VulkanDevice::QueueFamilyIndices indices = findQueueFamilies(phDev);

      if(!indices.graphicsFamily.has_value())
        continue;

      VkPhysicalDeviceProperties props;
      vkGetPhysicalDeviceProperties(phDev, &props);

      if (!deviceFilters.empty())
        {
          bool match = false;
          for (const auto &i: deviceFilters)
            {
              if (std::holds_alternative<size_t>(i))
                {
                  if(p == std::get<size_t>(i))
                    match = true;
                } else if (std::holds_alternative<std::string>(i))
                {
                  if (std::string(props.deviceName).find(std::get<std::string>(i)) != std::string::npos)
                    {
                      match = true;
                      break;
                    }
                }
            }
          if(!match) continue;
        }

      const std::array<float, 2> queuePriorities = {1, 1};

      std::array<VkDeviceQueueCreateInfo, 2> queueCreateInfo = {
        VkDeviceQueueCreateInfo {
          .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
          .queueFamilyIndex = indices.graphicsFamily.value(),
          .queueCount = 1,
          .pQueuePriorities = queuePriorities.data()
        },
        VkDeviceQueueCreateInfo {
          .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
          .queueFamilyIndex = indices.transferFamily.value(),
          .queueCount = 1,
          .pQueuePriorities = queuePriorities.data()
        }
      };
      bool singleQueue = (indices.graphicsFamily == indices.transferFamily);
      if(singleQueue && indices.transferQCount > 1)
        ++queueCreateInfo[0].queueCount;

#     if defined (VK_NV_linear_color_attachment)
      VkPhysicalDeviceLinearColorAttachmentFeaturesNV linearColorAttachmentFeatures = {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_LINEAR_COLOR_ATTACHMENT_FEATURES_NV,
        .linearColorAttachment = VK_TRUE
      };
      VkPhysicalDeviceLinearColorAttachmentFeaturesNV *devFeaturesNext =
              (props.vendorID == 0x10DE)? &linearColorAttachmentFeatures: nullptr;
#     else
      auto devFeaturesNext = nullptr;
#     endif

#ifdef VK_EXT_nested_command_buffer
      VkPhysicalDeviceNestedCommandBufferFeaturesEXT deviceNestedCommandBufferFeatures = {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_NESTED_COMMAND_BUFFER_FEATURES_EXT,
        .pNext = devFeaturesNext,
        .nestedCommandBufferSimultaneousUse = VK_TRUE
      };
      auto devFeaturesNext2 = &deviceNestedCommandBufferFeatures;
#else
      auto devFeaturesNext2 = devFeaturesNext;
#endif

      VkPhysicalDeviceRobustness2FeaturesEXT robustness2Features = {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ROBUSTNESS_2_FEATURES_EXT,
        .pNext = devFeaturesNext2,
        .nullDescriptor = VK_TRUE
      };

      VkPhysicalDeviceFeatures2 deviceFeatures = {
        .sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2,
        .pNext = &robustness2Features,
        .features = VkPhysicalDeviceFeatures {
          .imageCubeArray = VK_TRUE,
          .samplerAnisotropy = VK_TRUE,
          .fragmentStoresAndAtomics = VK_TRUE,
        }
      };

      std::vector<const char*> enabledExtensionNames;
      if(!optional_extensions.empty())
        {
          enabledExtensionNames.insert(enabledExtensionNames.end(), extensions_cstr.cbegin(), extensions_cstr.cend());
          uint32_t supportedExtensionCount = 0;
          vkEnumerateDeviceExtensionProperties(phDev, nullptr, &supportedExtensionCount, nullptr);
          std::vector<VkExtensionProperties> supportedExtensions(supportedExtensionCount);
          vkEnumerateDeviceExtensionProperties(phDev, nullptr, &supportedExtensionCount, supportedExtensions.data());
          for(const auto &extension: supportedExtensions)
            if(auto el = optional_extensions.find(extension.extensionName); el != optional_extensions.cend())
              enabledExtensionNames.push_back(el->c_str());
        }

      const VkDeviceCreateInfo devCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
        .pNext = &deviceFeatures,
        .queueCreateInfoCount = static_cast<uint32_t>(singleQueue? 1: queueCreateInfo.size()),
        .pQueueCreateInfos = queueCreateInfo.data(),
        .enabledLayerCount = 0,
        .ppEnabledLayerNames = nullptr,
        .enabledExtensionCount = static_cast<uint32_t>(enabledExtensionNames.empty()? extensions_cstr.size(): enabledExtensionNames.size()),
        .ppEnabledExtensionNames = enabledExtensionNames.empty()? extensions_cstr.data(): enabledExtensionNames.data(),
      };

      VkDevice dev;
      VkResult devCreateResult = vkCreateDevice(phDev, &devCreateInfo, nullptr, &dev);
      if(devCreateResult == VK_SUCCESS)
        {
          devices.emplace_back(indices, dev, phDev);
        } else
        qCritical().noquote() << QLatin1String("KurisuRenderer:warning: Vulkan device \'") << QLatin1String(props.deviceName) << QLatin1String("\': initialization failed: ") << devCreateResult;
    }
}
