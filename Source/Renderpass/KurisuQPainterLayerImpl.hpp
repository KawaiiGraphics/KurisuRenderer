#ifndef KURISUQPAINTERLAYERIMPL_HPP
#define KURISUQPAINTERLAYERIMPL_HPP

#include "KurisuGlInteropLayer.hpp"
#include <KawaiiRenderer/Renderpass/KawaiiQPainterLayerImpl.hpp>

class KURISURENDERER_SHARED_EXPORT KurisuQPainterLayerImpl: public KurisuGlInteropLayer, public KawaiiQPainterLayerImpl
{
public:
  KurisuQPainterLayerImpl(KawaiiQPainterLayer *model);
  ~KurisuQPainterLayerImpl();

  // KawaiiGlPaintedLayerImpl interface
protected:
  void refreshPixmaps() override final;



  // IMPLEMENT
private:
  std::unique_ptr<QPainter> qpainter;

  QPainter *getQPainter();
};

#endif // KURISUQPAINTERLAYERIMPL_HPP
