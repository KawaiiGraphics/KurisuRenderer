#ifndef VULKANFBO_HPP
#define VULKANFBO_HPP

#include "VulkanBaseRenderpass.hpp"
#include "VulkanTexture.hpp"
#include <optional>
#include <vector>

class VulkanFbo
{
  VulkanFbo(const VulkanFbo &orig) = delete;
  VulkanFbo& operator=(const VulkanFbo &orig) = delete;

public:
  VulkanFbo(VulkanBaseRenderpass &renderpass, const QSize &sz, KawaiiTextureFormat colorFormat, const VulkanTexture::UsageFlags &attachmentsUsage);

  // externalAttachements[0] is assumed to be color attachement 0; externalAttachements[1] is assumed to be depth stencil attachement;
  VulkanFbo(VulkanBaseRenderpass &renderpass, const QSize &sz, const std::vector</*<texture, layer>*/std::pair<VulkanTexture*, int>> &externalAttachements);
  ~VulkanFbo();

  VulkanFbo(VulkanFbo &&orig);
  VulkanFbo& operator=(VulkanFbo &&orig);

  const VulkanDevice& getDevice() const;

  VkFramebuffer getFbo() const;

  const VkDescriptorImageInfo* getColorImgDescriptor();
  const VkDescriptorImageInfo* getDepthImgDescriptor();

  VkFormat getColorVkFormat() const;
  VkFormat getDepthVkFormat() const;

  const VulkanTexture &getColorTexture() const;
  const VulkanTexture &getDepthTexture() const;



  //IMPLEMENT
private:
  std::vector<VulkanTexture> attachedTextures;
  std::vector<VulkanTexture::View2D> attachedLayers;

  VkFramebuffer fbo;
  VulkanDevice *dev;

  VulkanTexture &getColorTex();
  VulkanTexture &getDepthTex();

  void destroy();
};

#endif // VULKANFBO_HPP
