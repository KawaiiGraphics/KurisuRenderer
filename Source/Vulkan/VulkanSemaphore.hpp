#ifndef VULKANSEMAPHORE_HPP
#define VULKANSEMAPHORE_HPP

#include "VulkanDevice.hpp"
#include <QtGlobal>
#include <optional>

class VulkanSemaphore
{
  VulkanSemaphore(const VulkanSemaphore&) = delete;
  VulkanSemaphore& operator= (const VulkanSemaphore&) = delete;
public:
  VulkanSemaphore(VulkanDevice &dev, bool allowOpaqueHandle = false);
  VulkanSemaphore(VulkanSemaphore &&orig);
  VulkanSemaphore& operator=(VulkanSemaphore &&orig);

  ~VulkanSemaphore();

  VkSemaphore getSemaphore() const;

  std::optional<VulkanDevice::Handle> getOpaqueHandle() const;

  void reset();



  //IMPLEMENT
private:
  VulkanDevice *dev;
  VkSemaphore sem;

  void destroy();
};

#endif // VULKANSEMAPHORE_HPP
