#include "KurisuImageSourceImpl.hpp"
#include "KurisuRenderpassImpl.hpp"
#include "KurisuRootImpl.hpp"

namespace {
  VkBlendFactor trBlendFactor(KawaiiImageSource::BlendFactor factor)
  {
    switch (factor)
      {
      case KawaiiImageSource::BlendFactor::FactorZero:
        return VK_BLEND_FACTOR_ZERO;
      case KawaiiImageSource::BlendFactor::FactorOne:
        return VK_BLEND_FACTOR_ONE;
      case KawaiiImageSource::BlendFactor::FactorSrcAlpha:
        return VK_BLEND_FACTOR_SRC_ALPHA;
      case KawaiiImageSource::BlendFactor::FactorOneMinusSrcAlpha:
        return VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
      case KawaiiImageSource::BlendFactor::FactorDstAlpha:
        return VK_BLEND_FACTOR_DST_ALPHA;
      case KawaiiImageSource::BlendFactor::FactorOneMinusDstAlpha:
        return VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA;
      }
    Q_UNREACHABLE();
  }

  VkCompareOp trCompareOp(KawaiiImageSource::DepthFunc op)
  {
    switch(op)
      {
      case KawaiiImageSource::DepthFunc::Never:
        return VK_COMPARE_OP_NEVER;
      case KawaiiImageSource::DepthFunc::Less:
        return VK_COMPARE_OP_LESS;
      case KawaiiImageSource::DepthFunc::LessEqual:
        return VK_COMPARE_OP_LESS_OR_EQUAL;
      case KawaiiImageSource::DepthFunc::Greater:
        return VK_COMPARE_OP_GREATER;
      case KawaiiImageSource::DepthFunc::GreaterEqual:
        return VK_COMPARE_OP_GREATER_OR_EQUAL;
      case KawaiiImageSource::DepthFunc::Equal:
        return VK_COMPARE_OP_EQUAL;
      case KawaiiImageSource::DepthFunc::Always:
        return VK_COMPARE_OP_ALWAYS;
      }
    Q_UNREACHABLE();
  }
}

KurisuImageSourceImpl::KurisuImageSourceImpl(KawaiiImageSource *model):
  KawaiiImageSourceImpl(model),
  needsGBufs(true)
{
}

KurisuImageSourceImpl::~KurisuImageSourceImpl()
{
}

const std::vector<VkSemaphore>& KurisuImageSourceImpl::getWaitSemaphores(size_t dev_index, size_t frameIndex) const
{
  static const std::vector<VkSemaphore> empty;
  if(waitSemaphores.empty())
    return empty;
  else
    return waitSemaphores[dev_index][frameIndex];
}

const std::vector<VkPipelineStageFlags> &KurisuImageSourceImpl::getWaitStages() const
{
  return waitStages;
}

void KurisuImageSourceImpl::createCache()
{
  auto *rp = getModel()->getOwner();
  auto *rp_impl = static_cast<KurisuRenderpassImpl*>(rp->getRendererImpl());
  auto *root = rp_impl->getRoot();
  cmd.reserve(root->deviceCount());
  root->forallDevices([this, rp_impl] (VulkanDevice &dev) {
    cmd.push_back(std::make_unique<RenderingCommands>(dev, rp_impl->getAsyncFrames(), rp_impl->getAsyncFrames(), false));
  });
}

void KurisuImageSourceImpl::drawCache()
{
  auto *rp = getModel()->getOwner();
  auto *rp_impl = static_cast<KurisuRenderpassImpl*>(rp->getRendererImpl());
  for(size_t dev_i = 0; dev_i < cmd.size(); ++dev_i)
    rp_impl->getCmdbuf(dev_i).execute(*cmd[dev_i]);
}

void KurisuImageSourceImpl::updateCache()
{
  auto *rp = getModel()->getOwner();
  auto *rp_impl = static_cast<KurisuRenderpassImpl*>(rp->getRendererImpl());
  auto *root = rp_impl->getRoot();
  root->activateRenderpass(rp_impl, getModel()->getSubpass());

  root->setColorBlendAttachmentState(
        std::vector<VkPipelineColorBlendAttachmentState>(
          rp->getSubpass(getModel()->getSubpass()).outputGBufs.size(),
          getColorBlendAttachmentState()));

  root->setDepthState(
        trCompareOp(getModel()->getDepthTestFunc()),
        getModel()->getDepthTest(),
        getModel()->getDepthWrite());

  for(size_t dev_i = 0; dev_i < cmd.size(); ++dev_i)
    {
      if(root->isNowOffscreen())
        root->getGpuOffscrResLocked(dev_i).unlock();
      cmd[dev_i]->reset();
      if(root->isNowOffscreen())
        root->getGpuOffscrResLocked(dev_i).relock();
      const auto &res = getModel()->getRenderableSize();
      cmd[dev_i]->setViewport(QRect(0,0, res.x, res.y));
      auto &vulkanRenderpass = rp_impl->getRenderpass(dev_i);
      cmd[dev_i]->startRecording(vulkanRenderpass.getRenderpass(),
                                 rp_impl->getFbo(dev_i),
                                 vulkanRenderpass.getClearValues(),
                                 VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS,
                                 getModel()->getSubpass());

      if(needsGBufs)
        bindGbufsDescriptor(dev_i);
      writeCmdBuf(*cmd[dev_i], dev_i);
      cmd[dev_i]->finishRecording();
    }
}

void KurisuImageSourceImpl::deleteCache()
{
  cmd.clear();
}

VkPipelineColorBlendAttachmentState KurisuImageSourceImpl::getColorBlendAttachmentState() const
{
  return VkPipelineColorBlendAttachmentState {
      .blendEnable = getModel()->getBlendEnabled()? VK_TRUE: VK_FALSE,
          .srcColorBlendFactor = trBlendFactor(getModel()->getSrcColorFactor()),
          .dstColorBlendFactor = trBlendFactor(getModel()->getDstColorFactor()),
          .colorBlendOp = VK_BLEND_OP_ADD,
          .srcAlphaBlendFactor = trBlendFactor(getModel()->getSrcAlphaFactor()),
          .dstAlphaBlendFactor = trBlendFactor(getModel()->getDstAlphaFactor()),
          .alphaBlendOp = VK_BLEND_OP_ADD,
          .colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT
    };
}

void KurisuImageSourceImpl::bindGbufsDescriptor(size_t dev_i)
{
  auto *rp = getModel()->getOwner();
  auto *rp_impl = static_cast<KurisuRenderpassImpl*>(rp->getRendererImpl());
  const auto &subpass = rp->getSubpass(getModel()->getSubpass());

  if(subpass.inputGBufs.empty()
     && subpass.storageGBufs.empty()
     && subpass.sampledGBufs.empty())
    return;

  auto *root = rp_impl->getRoot();
  const uint64_t descriptorName = static_cast<uint64_t>(reinterpret_cast<size_t>(this));

  std::vector<std::vector<VulkanDescriptorPool::ImgBinding>> imgBindings(rp_impl->getAsyncFrames());
  for(auto &bindings: imgBindings)
    bindings.reserve(subpass.inputGBufs.size() + subpass.storageGBufs.size() + subpass.sampledGBufs.size());

  uint32_t set = KawaiiShader::getUboCount() + 1;

  for(size_t frameI = 0; frameI < imgBindings.size(); ++frameI)
    {
      for(uint32_t i = 0; i < subpass.inputGBufs.size(); ++i)
        {
          imgBindings[frameI].push_back(VulkanDescriptorPool::ImgBinding {
                                          .descrImg = VkDescriptorImageInfo {
                                            .sampler = VK_NULL_HANDLE,
                                            .imageView = rp_impl->getGBufImageView(dev_i, subpass.inputGBufs[i], frameI),
                                            .imageLayout = rp_impl->getInputGbufImgLayout(getModel()->getSubpass(), i)
                                          },
                                          .descriptorType = VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,
                                          .bindingPoint = root->getInputGbufLocation(i).binding
                                        });
        }
      for(uint32_t i = 0; i < subpass.storageGBufs.size(); ++i)
        {
          imgBindings[frameI].push_back(VulkanDescriptorPool::ImgBinding {
                                          .descrImg = VkDescriptorImageInfo {
                                            .sampler = VK_NULL_HANDLE,
                                            .imageView = rp_impl->getGBufImageView(dev_i, subpass.storageGBufs[i], frameI),
                                            .imageLayout = VK_IMAGE_LAYOUT_GENERAL
                                          },
                                          .descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE,
                                          .bindingPoint = root->getStorageGbufLocation(i).binding
                                        });
        }
      for(uint32_t i = 0; i < subpass.sampledGBufs.size(); ++i)
        {
          imgBindings[frameI].push_back(VulkanDescriptorPool::ImgBinding {
                                          .descrImg = VkDescriptorImageInfo {
                                            .sampler = root->getCommonSampler(dev_i),
                                            .imageView = rp_impl->getGBufImageView(dev_i, subpass.sampledGBufs[i], frameI),
                                            .imageLayout = VK_IMAGE_LAYOUT_GENERAL
                                          },
                                          .descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                                          .bindingPoint = root->getSampledGbufLocation(i).binding
                                        });
        }
    }
  auto &pipelineLayout = root->getScenePipelineLayout(dev_i);
  const auto &descrSetLayouts = pipelineLayout.getDescriptorSetLayouts();
  if(descrSetLayouts.size() <= set)
    {
      qWarning("KurisuRenderer: KurisuImageSourceImpl::bindGbufsDescriptor ignored input and/or storage GBufs since no shader ever used them!");
      return;
    }
  VkDescriptorSetLayout descrSetLayout = descrSetLayouts[set];

  cmd[dev_i]->createDescriptorSet(descriptorName, {}, imgBindings, descrSetLayout, pipelineLayout.getDescriptorCount(set));
  cmd[dev_i]->bindDescriptorSet(descriptorName, set, pipelineLayout.getPipelineLayout());
}
