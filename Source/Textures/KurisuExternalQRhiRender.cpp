#include "KurisuExternalQRhiRender.hpp"
#include "KurisuRootImpl.hpp"
#include "Vulkan/SingleTimeCommands.hpp"

#include <Kawaii3D/KawaiiRoot.hpp>
#include <KawaiiRenderer/Textures/KawaiiTextureImpl.hpp>

KurisuExternalQRhiRender::KurisuExternalQRhiRender(KawaiiExternalQRhiRender *model):
  KawaiiRendererImpl(model),
  root(nullptr),
  tex(nullptr)
{
  connect(model, &KawaiiExternalQRhiRender::renderTargetTextureChanged, this, &KurisuExternalQRhiRender::setTexture);
  connect(model, &KawaiiExternalQRhiRender::renderTargetTextureResized, this, &KurisuExternalQRhiRender::updateRhiTex);
  connect(model, &KawaiiExternalQRhiRender::aboutToBeRendered, this, &KurisuExternalQRhiRender::lockGraphicsQueue, Qt::DirectConnection);
  connect(model, &KawaiiExternalQRhiRender::rendered, this, &KurisuExternalQRhiRender::unlockGraphicsQueue, Qt::DirectConnection);
  connect(model, &KawaiiExternalQRhiRender::rendered, this, &KurisuExternalQRhiRender::sendPostRenderCmds);
  connect(model, &sib_utils::TreeNode::parentUpdated, this, &KurisuExternalQRhiRender::updateRoot);
}

KurisuExternalQRhiRender::~KurisuExternalQRhiRender()
{
  if(getData())
    {
      auto rhiRenderTarget = static_cast<KawaiiExternalQRhiRender*>(getData())->getRenderTargetRhi();
      auto rhiRenderTargetRpDescr = rhiRenderTarget? rhiRenderTarget->renderPassDescriptor(): nullptr;

      if(rhiRenderTargetRpDescr)
        delete rhiRenderTargetRpDescr;
    }
}

void KurisuExternalQRhiRender::setTexture(KawaiiTexture *newTex)
{
  disconnect(onTexHandleChanged);

  auto newTexHandle = newTex && newTex->getRendererImpl()?
        static_cast<KurisuTextureHandle*>(
        static_cast<KawaiiTextureImpl*>(newTex->getRendererImpl())->getHandle())
      : nullptr;

  if(newTex && newTex->getRendererImpl())
    onTexHandleChanged = connect(static_cast<KawaiiTextureImpl*>(newTex->getRendererImpl()), &KawaiiTextureImpl::handleChanged,
                                 this, [this, newTex] { setTexture(newTex); });

  if(newTexHandle && !newTexHandle->isAttachable())
    {
      newTex->setProperty("UsedInFbo", true);
      newTexHandle->askRecreate();
      return;
    }

  if(newTexHandle != tex)
    {
      tex = newTexHandle;
      transferTargets.clear();
      updateRhiTex();
    }
}

namespace {
  QRhiTexture::Format trVkFmt(VkFormat fmt)
  {
    switch(fmt)
      {
      case VK_FORMAT_R8_UNORM:
        return QRhiTexture::R8;

      case VK_FORMAT_R8G8_UNORM:
        return QRhiTexture::RG8;

      case VK_FORMAT_R8G8B8_UNORM:
      case VK_FORMAT_R8G8B8A8_UNORM:
        return QRhiTexture::RGBA8;

      case VK_FORMAT_B8G8R8_UNORM:
      case VK_FORMAT_B8G8R8A8_UNORM:
        return QRhiTexture::BGRA8;

      case VK_FORMAT_D16_UNORM:
        return QRhiTexture::D16;
      case VK_FORMAT_D24_UNORM_S8_UINT:
        return QRhiTexture::D24S8;
      case VK_FORMAT_D32_SFLOAT:
        return QRhiTexture::D32F;

      case VK_FORMAT_R32_SFLOAT:
        return QRhiTexture::R32F;

      case VK_FORMAT_R32G32B32_SFLOAT:
      case VK_FORMAT_R32G32B32A32_SFLOAT:
        return QRhiTexture::RGBA32F;

      default:
        return QRhiTexture::BGRA8;
      }
  }
}

void KurisuExternalQRhiRender::updateRhiTex()
{
  if(!tex) return;

  auto &vulkanTex = tex->getTexture(root->getComposerDeviceIndex());

  const auto sz = QSize(tex->getWidth(), tex->getHeight());
  if(!rhiTex || rhiTex->pixelSize() != sz)
    {
      transferTargets.clear();
      if(sz.width() && sz.height())
        {
          rhiTex.reset(root->getQRhi().newTexture(trVkFmt(vulkanTex.getVkFormat()), sz, 1,
                                                  { QRhiTexture::RenderTarget, QRhiTexture::UsedAsTransferSource }));
          rhiTex->create();
          trySetRhi();
        } else
        rhiTex.reset();
    }
}

void KurisuExternalQRhiRender::trySetRhi()
{
  if(root && rhiTex)
    {
      auto oldRhiRenderTarget = static_cast<KawaiiExternalQRhiRender*>(getData())->getRenderTargetRhi();
      auto oldRhiRenderTargetRpDescr = oldRhiRenderTarget? oldRhiRenderTarget->renderPassDescriptor(): nullptr;

      auto rhiRenderTarget = root->getQRhi().newTextureRenderTarget({QRhiColorAttachment(rhiTex.get())});
      rhiRenderTarget->setRenderPassDescriptor(rhiRenderTarget->newCompatibleRenderPassDescriptor());

      static_cast<KawaiiExternalQRhiRender*>(getData())->
          setRhi(&root->getQRhi(), rhiRenderTarget);

      if(oldRhiRenderTargetRpDescr)
        delete oldRhiRenderTargetRpDescr;
    }
}

void KurisuExternalQRhiRender::updateRoot()
{
  KawaiiRoot *kawaiiRoot = KawaiiRoot::getRoot(getData());
  auto newRoot = static_cast<KurisuRootImpl*>(kawaiiRoot->getRendererImpl());
  if(newRoot != root)
    {
      root = newRoot;
      trySetRhi();
    }
}

void KurisuExternalQRhiRender::lockGraphicsQueue()
{
  graphicsQueueLock = std::make_unique<QMutexLocker<QMutex>>(&root->getComposerDevice().graphicsQueueM);
}

void KurisuExternalQRhiRender::unlockGraphicsQueue()
{
  graphicsQueueLock.reset();
}

void KurisuExternalQRhiRender::sendPostRenderCmds()
{
  if(Q_LIKELY(root))
    {
      auto &vulkanTex = tex->getTexture(root->getComposerDeviceIndex());
      if(transferTargets.empty() && root->deviceCount() > 1)
        {
          root->forallDevices([this](VulkanDevice &dev) {
            transferTargets.emplace_back(dev, VulkanTexture::UsageSampler);
          });
          auto &transferTarget = transferTargets[root->getComposerDeviceIndex()];
          transferTarget.setCubemapCompatible(vulkanTex.isCubemapCompatible());
          transferTarget.setExtent(vulkanTex.getExtent(), vulkanTex.getImageType());
          if(vulkanTex.getAspectFlagsFull() == VK_IMAGE_ASPECT_COLOR_BIT)
            transferTarget.setFormat(VK_FORMAT_R8G8B8A8_UNORM, vulkanTex.getAspectFlagsFull(), vulkanTex.getAspectFlagsUsage());
          else
            transferTarget.setFormat(vulkanTex.getVkFormat(), vulkanTex.getAspectFlagsFull(), vulkanTex.getAspectFlagsUsage());
          transferTarget.setLayers(vulkanTex.getMipMapLayers(), vulkanTex.getArrayLayers());
          transferTarget.setLinear(true);
          transferTarget.setBits(nullptr,
                                 root->deviceCount() > 1? VulkanTexture::imgMultiGpuMemoryHandleType : 0);

          root->forallDevices([this](VulkanDevice&, size_t i) {
            if(i != root->getComposerDeviceIndex())
              transferTargets[i].importImage(transferTargets[root->getComposerDeviceIndex()]);
          });
        }

      VkImageSubresourceLayers subresLayers = {
        .aspectMask = vulkanTex.getImageSubresourceRange().aspectMask,
        .mipLevel = 0,
        .baseArrayLayer = vulkanTex.getImageSubresourceRange().baseArrayLayer,
        .layerCount = vulkanTex.getImageSubresourceRange().layerCount
      };

      VkImageCopy rg = {
        .srcSubresource = subresLayers,
        .srcOffset = {0, 0, 0},
        .dstSubresource = subresLayers,
        .dstOffset = {0, 0, 0},
        .extent = vulkanTex.getExtent()
      };
      const auto rhiVulkanTex = rhiTex->nativeTexture();
      SingleTimeCommands cmd(root->getComposerDevice());
      if(root->deviceCount() > 1)
        {
          const size_t i = root->getComposerDeviceIndex();
          cmd.copyImage(reinterpret_cast<VkImage>(rhiVulkanTex.object), static_cast<VkImageLayout>(rhiVulkanTex.layout),
                        transferTargets[i], {rg});
          cmd.copyImage(transferTargets[i], tex->getTexture(i), {rg});
        } else {
          cmd.copyImage(reinterpret_cast<VkImage>(rhiVulkanTex.object), static_cast<VkImageLayout>(rhiVulkanTex.layout),
                        tex->getTexture(root->getComposerDeviceIndex()), {rg});
        }
      cmd.write().waitForFinished();
      root->getComposerDevice().submitTransferCommands();

      root->forallDevices([this, &rg](VulkanDevice &dev, size_t i) {
          if(i == root->getComposerDeviceIndex()) return;

          SingleTimeCommands cmd(dev);
          cmd.copyImage(transferTargets[i], tex->getTexture(i), {rg});
          cmd.write().waitForFinished();
          dev.submitTransferCommands();
        });
    }
}

void KurisuExternalQRhiRender::initConnections()
{
  updateRoot();
  setTexture(static_cast<KawaiiExternalQRhiRender*>(getData())->getRenderTargetTexture());
}
