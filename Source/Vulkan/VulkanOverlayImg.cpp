#include "VulkanOverlayImg.hpp"

VulkanOverlayImg::VulkanOverlayImg(VulkanDevice &dev, const QSize &sz, VulkanOverlayCtx &ctx):
  dev(dev),
  ctx(ctx),
  imagePrepared(dev, true),
  completeRendering(dev, true),
  texture(dev, VulkanTexture::UsageFlags(VulkanTexture::UsageSampler | VulkanTexture::UsageAttachment)),
  size(sz),
  fbo(0),
  renderbuff(0)
{
}

VulkanOverlayImg::~VulkanOverlayImg()
{
  destroy();
}

void VulkanOverlayImg::init()
{
  ctx.makeCurrent();
  paintDev = std::make_unique<QOpenGLPaintDevice>(size);

  if(ctx.vulkanInteropSupported())
    {
      glPrepared = ctx.importVulkanSemaphore(*imagePrepared.getOpaqueHandle());
      glComplete = ctx.importVulkanSemaphore(*completeRendering.getOpaqueHandle());
    }
  initTexture(size, texture);
  initFbo(size);
}

void VulkanOverlayImg::destroy()
{
  ctx.makeCurrent();
  destroyGlFbo();
  if(ctx.vulkanInteropSupported())
    {
      ctx.deleteGlSemaphore(glPrepared);
      ctx.deleteGlSemaphore(glComplete);
      glPrepared = glComplete = 0;
    }

  paintDev.reset();
}

void VulkanOverlayImg::resize(const QSize &sz)
{
  if(size != sz)
    {
      initTexture(sz, texture);
      initFbo(sz);
      paintDev->setSize(sz);
      ctx.gl()->glFlush();
      ctx.gl()->glFinish();

      size = sz;
    }
}

QPaintDevice &VulkanOverlayImg::qPaintDev()
{
  return *paintDev;
}

const VkDescriptorImageInfo *VulkanOverlayImg::descriptorInfo() const
{
  return texture.descriptorInfo();
}

VkSemaphore VulkanOverlayImg::getCompleteSemaphore() const
{
  return completeRendering.getSemaphore();
}

void VulkanOverlayImg::flush()
{
  if(isZeroCopy())
    {
      ctx.signalGlSemaphore(glComplete, glTex, GL_LAYOUT_SHADER_READ_ONLY_EXT);
      ctx.gl()->glFlush();
    } else
    texture.copyBitsFromGl(*ctx.gl());
}

void VulkanOverlayImg::prepare()
{
  ctx.makeCurrent();
  ctx.gl()->glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  ctx.gl()->glViewport(0,0, size.width(), size.height());
}

void VulkanOverlayImg::clear()
{
  ctx.gl()->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

bool VulkanOverlayImg::isZeroCopy() const
{
  return ctx.vulkanInteropSupported();
}

void VulkanOverlayImg::destroyGlFbo()
{
  if(fbo)
    {
      ctx.gl()->glBindFramebuffer(GL_FRAMEBUFFER, 0);
      ctx.gl()->glBindRenderbuffer(GL_RENDERBUFFER, 0);

      ctx.gl()->glDeleteFramebuffers(1, &fbo);
      fbo = 0;
      ctx.gl()->glDeleteRenderbuffers(1, &renderbuff);
      renderbuff = 0;

      ctx.free(glTex);
    }
}

void VulkanOverlayImg::initTexture(const QSize &sz, VulkanTexture &tex)
{
  tex.setExtent(sz.width(), sz.height());
  tex.setFormat_8bpc(KawaiiTextureFormat::RedGreenBlueAlpha);
  tex.setLinear(isZeroCopy());

  VkExternalMemoryHandleTypeFlags memoryHandleType = isZeroCopy() ? VulkanTexture::imgSameGpuMemoryHandleType : 0;
  try {
    tex.createImage(nullptr, memoryHandleType);
  } catch (const std::runtime_error&) {
    tex.setLinear(false);
    tex.createImage(nullptr, memoryHandleType);
    qWarning("KurisuRenderer: VulkanOverlayImg: using non linear overlay texture!");
  }
  tex.setMinFilter(KawaiiTextureFilter::Nearest);
  tex.setMagFilter(KawaiiTextureFilter::Nearest);
  tex.setWrapModeS(KawaiiTextureWrapMode::ClampToEdge);
  tex.setWrapModeT(KawaiiTextureWrapMode::ClampToEdge);
  tex.setWrapModeR(KawaiiTextureWrapMode::ClampToEdge);
  tex.createSampler();
  tex.createImageView(VkImageViewType::VK_IMAGE_VIEW_TYPE_2D);
}

void VulkanOverlayImg::initFbo(const QSize &sz)
{
  destroyGlFbo();

  ctx.gl()->glGenRenderbuffers(1, &renderbuff);
  ctx.gl()->glBindRenderbuffer(GL_RENDERBUFFER, renderbuff);
  ctx.gl()->glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, sz.width(), sz.height());
  Q_ASSERT(ctx.gl()->glIsRenderbuffer(renderbuff));

  ctx.gl()->glViewport(0,0, sz.width(), sz.height());
  ctx.gl()->glGenFramebuffers(1, &fbo);
  ctx.gl()->glBindFramebuffer(GL_FRAMEBUFFER, fbo);
  ctx.gl()->glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, renderbuff);

  if(isZeroCopy())
    {
      glTex = ctx.importVulkanTexture(texture);
      ctx.gl()->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, glTex.tex, 0);
    } else
    {
      glTex.mem = 0;
      ctx.gl()->glGenTextures(1, &glTex.tex);
      ctx.gl()->glBindTexture(GL_TEXTURE_2D, glTex.tex);
      ctx.gl()->glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, sz.width(), sz.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
      ctx.gl()->glBindTexture(GL_TEXTURE_2D, 0);
      Q_ASSERT(ctx.gl()->glIsTexture(glTex.tex));
      ctx.gl()->glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, glTex.tex, 0);
    }

  Q_ASSERT(ctx.gl()->glIsFramebuffer(fbo));
//  Q_ASSERT(ctx.gl()->glCheckFramebufferStatus(fbo) == GL_FRAMEBUFFER_COMPLETE);
}
