#include "RenderingCommands.hpp"

#include <Kawaii3D/KawaiiConfig.hpp>

#include <unordered_map>
#include <QtConcurrent>
#include <array>

namespace {
  template<size_t N>
  void printTraceCommand(const char (&str) [N])
  {
    if(KawaiiConfig::getInstance().getDebugLevel() > 2)
      qDebug().noquote() << QLatin1String(str, N);
  }
}

RenderingCommands::RenderingCommands(VulkanDevice &dev, size_t image_count, size_t asyncFrames, bool primary):
  dev(dev),
  commandPool(VK_NULL_HANDLE),
  commandBuffers(image_count, std::vector<VkCommandBuffer>(asyncFrames)),
  descriptorPools(asyncFrames),
  waitingForResources(asyncFrames, false),
  state(State::Empty),
  bindedPipeline(VK_NULL_HANDLE),
  isUsedOnGpu(asyncFrames, false),
  primary(primary)
{
  const VkCommandPoolCreateInfo poolInfo = {
    .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
    .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
    .queueFamilyIndex = *dev.queueFamilies.graphicsFamily
  };

  if (vkCreateCommandPool(dev.vkDev, &poolInfo, nullptr, &commandPool) != VK_SUCCESS)
    throw std::runtime_error("failed to create command pool!");

  const VkCommandBufferAllocateInfo allocInfo = {
    .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
    .commandPool = commandPool,
    .level = primary? VK_COMMAND_BUFFER_LEVEL_PRIMARY: VK_COMMAND_BUFFER_LEVEL_SECONDARY,
    .commandBufferCount = static_cast<uint32_t>(commandBuffers.front().size())
  };

  for(auto &cmdBufs: commandBuffers)
    if (vkAllocateCommandBuffers(dev.vkDev, &allocInfo, cmdBufs.data()) != VK_SUCCESS)
      throw std::runtime_error("failed to allocate command buffers!");

  resourcesReady.reserve(asyncFrames);
  for(size_t i = 0; i < asyncFrames; ++i)
    {
      resourcesReady.emplace_back(dev);
      descriptorPools[i] = std::make_unique<VulkanDescriptorPool>(dev, i, asyncFrames);
      descriptorPools[i]->setParent(this);

      connect(descriptorPools[i].get(), &VulkanDescriptorPool::recreated, this, &RenderingCommands::rewrite, Qt::DirectConnection);
      connect(descriptorPools[i].get(), &VulkanDescriptorPool::descriptorSetUpdated, this, &RenderingCommands::onDescriptorSetUpdated);
    }
}

RenderingCommands::RenderingCommands(const VulkanSwapchain &swapchain):
  RenderingCommands(swapchain.getDevice(), swapchain.getImageCount(), swapchain.getAsyncFrames(), true)
{
}

RenderingCommands::~RenderingCommands()
{
  waitForCmdbuf();
  // dev.waitIdle();
  for(bool &i: isUsedOnGpu)
    while(i)
      QThread::msleep(5);
  vkDestroyCommandPool(dev.vkDev, commandPool, nullptr);
  resourcesReady.clear();
  descriptorPools.clear();
}

void RenderingCommands::reset()
{
  if(!isEmpty())
    {
      waitForCmdbuf();
      // dev.waitIdle();
      for(bool &i: isUsedOnGpu)
        while(i)
          QThread::msleep(5);
      emit aboutToReset();

      QWriteLocker l(&dev.resLock);

      delayedCommands.clear();
      usedBufers.clear();
      for(auto *cmd: usedCommands)
        disconnect(cmd, &RenderingCommands::aboutToReset, this, &RenderingCommands::rewrite);
      usedCommands.clear();

      for(const auto &cmdBufs: commandBuffers)
        for(auto &i: cmdBufs)
          vkResetCommandBuffer(i, 0);
      state = State::Empty;
      bindedPipeline = VK_NULL_HANDLE;

      for(const auto &pool: descriptorPools)
        {
          for(const auto i: usedDescriptorSetNames)
            pool->releaseDescriptorSet(i);
          pool->forgetUsedBuffers();
        }
      usedDescriptorSetNames.clear();
    }
}

void RenderingCommands::startRecording(VkRenderPass renderPass, const std::vector<VkFramebuffer> &fbo, const std::vector<VkClearValue> &clearValues, VkSubpassContents subpassContents, uint32_t subpass)
{
  waitForCmdbuf();
  delayedCommands.push_back([this, renderPass, fbo, subpass] (VkCommandBuffer cmdBuf, size_t imgIndex, [[maybe_unused]] size_t frameIndex) {
      const VkCommandBufferInheritanceInfo inheritanceInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO,
        .renderPass = renderPass,
        .subpass = subpass,
        .framebuffer = !fbo.empty()? fbo[imgIndex % fbo.size()]: VK_NULL_HANDLE,
      };

      const VkCommandBufferBeginInfo beginInfo = {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
        .flags = static_cast<VkCommandBufferUsageFlags>(isPrimary()? VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT: VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT | VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT),
        .pInheritanceInfo = !renderPass || isPrimary()? nullptr: &inheritanceInfo,
      };

      if (vkBeginCommandBuffer(cmdBuf, &beginInfo) != VK_SUCCESS)
        throw std::runtime_error("failed to begin recording command buffer!");
    });

  state = State::WritingCommands;
  if(primary && renderPass)
    beginRenderpass(renderPass, fbo, clearValues, subpassContents);

  printTraceCommand("=======");
}

void RenderingCommands::beginRenderpass(VkRenderPass renderPass, const std::vector<VkFramebuffer> &fbo, const std::vector<VkClearValue> &clearValues, VkSubpassContents subpassContents)
{
  waitForCmdbuf();
  delayedCommands.push_back([this, renderPass, fbo, clearValues, subpassContents] (VkCommandBuffer cmd, size_t imgIndex, [[maybe_unused]] size_t frameIndex) {
      const VkRenderPassBeginInfo renderPassInfo = {
        .sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
        .renderPass = renderPass,
        .framebuffer = fbo[imgIndex % fbo.size()],

        .renderArea = VkRect2D {
          .offset = VkOffset2D {
            .x = viewport.x(),
            .y = viewport.y()
          },
          .extent = VkExtent2D {
            .width = static_cast<uint32_t>(viewport.width()),
            .height = static_cast<uint32_t>(viewport.height())}
        },

        .clearValueCount = static_cast<uint32_t>(clearValues.size()),
        .pClearValues = clearValues.empty()? nullptr: clearValues.data()
      };
      vkCmdBeginRenderPass(cmd, &renderPassInfo, subpassContents);
    });
}

void RenderingCommands::setViewport(const QRect &vp)
{
  if(viewport == vp) return;
  viewport = vp;
  rewrite();
}

void RenderingCommands::bindGraphicsPipeline(VkPipeline pipeline)
{
  if(bindedPipeline != pipeline)
    {
      bindedPipeline = pipeline;

      waitForCmdbuf();
      delayedCommands.push_back([pipeline] (VkCommandBuffer i, size_t, size_t) {
        vkCmdBindPipeline(i, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);
      });
    printTraceCommand("vkCmdBindPipeline");
    }
}

void RenderingCommands::bindVbos(uint32_t firstBinding, const std::vector<VulkanBuffer*> &buffers, const std::vector<VkDeviceSize> &offsets)
{
  Q_ASSERT(buffers.size() == offsets.size());

  usedBufers.insert(usedBufers.end(), buffers.cbegin(), buffers.cend());

  waitForCmdbuf();
  delayedCommands.push_back([this, buffers, offsets, firstBinding] (VkCommandBuffer i, [[maybe_unused]] size_t imgIndex, size_t frameIndex) {
      std::vector<VkBuffer> v;
      v.reserve(buffers.size());
      for(auto *i: buffers)
        v.push_back(i->getDetachedBuffer(this, frameIndex, commandBuffers.front().size()));

      vkCmdBindVertexBuffers(i, firstBinding, v.size(), v.data(), offsets.data());
    });
  printTraceCommand("vkCmdBindVertexBuffers");
}

void RenderingCommands::bindIndexBuffer(VulkanBuffer &buffer, VkIndexType indexType, VkDeviceSize offset)
{
  usedBufers.push_back(&buffer);

  waitForCmdbuf();
  delayedCommands.push_back([this, &buffer, indexType, offset] (VkCommandBuffer i, [[maybe_unused]] size_t imgIndex, size_t frameIndex) {
      vkCmdBindIndexBuffer(i, buffer.getDetachedBuffer(this, frameIndex, commandBuffers.front().size()), offset, indexType);
    });
  printTraceCommand("vkCmdBindIndexBuffer");
}

void RenderingCommands::createDescriptorSet(uint64_t name, const std::vector<VulkanDescriptorPool::BufBinding> &bufBindings, const std::vector<VulkanDescriptorPool::ImgBinding> &imgBindings, VkDescriptorSetLayout layout, const std::unordered_map<VkDescriptorType, uint32_t> &descriptorCount)
{
  usedDescriptorSetNames.insert(name);
  for(const auto &pool: descriptorPools)
    pool->createDescriptorSet(name, bufBindings, imgBindings, layout, descriptorCount);
}

void RenderingCommands::createDescriptorSet(uint64_t name, const std::vector<VulkanDescriptorPool::BufBinding> &bufBindings, const std::vector<std::vector<VulkanDescriptorPool::ImgBinding> > &imgBindings, VkDescriptorSetLayout layout, const std::unordered_map<VkDescriptorType, uint32_t> &descriptorCount)
{
  usedDescriptorSetNames.insert(name);
  for(size_t frameI = 0; frameI < descriptorPools.size(); ++frameI)
    descriptorPools[frameI]->createDescriptorSet(name, bufBindings, imgBindings[frameI], layout, descriptorCount);
}

void RenderingCommands::bindDescriptorSet(uint64_t descrSetName, uint32_t descrSetBinding, VkPipelineLayout pipelineLayout)
{
  waitForCmdbuf();
  delayedCommands.push_back([this, descrSetName, descrSetBinding, pipelineLayout] (VkCommandBuffer i, [[maybe_unused]] size_t imgIndex, size_t frameIndex) {
      VkDescriptorSet descrSet = descriptorPools[frameIndex]->getDescriptorSet(descrSetName);
      vkCmdBindDescriptorSets(i, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineLayout, descrSetBinding, 1, &descrSet, 0, nullptr);
  });
  printTraceCommand("vkCmdBindDescriptorSets");
}

void RenderingCommands::draw(uint32_t vertexCount, uint32_t instanceCount, uint32_t vertex_i, uint32_t instance_i)
{
  waitForCmdbuf();
  delayedCommands.push_back([vertexCount, instanceCount, vertex_i, instance_i] (VkCommandBuffer i, size_t, size_t) {
      vkCmdDraw(i, vertexCount,instanceCount, vertex_i,instance_i);
    });
  printTraceCommand("vkCmdDraw");
}

void RenderingCommands::drawIndexed(uint32_t vertexCount, uint32_t instanceCount, uint32_t vertex_i, uint32_t instance_i)
{
  waitForCmdbuf();
  delayedCommands.push_back([vertexCount, instanceCount, vertex_i, instance_i] (VkCommandBuffer i, size_t, size_t) {
      vkCmdDrawIndexed(i, vertexCount,instanceCount, vertex_i,0,instance_i);
    });
  printTraceCommand("vkCmdDrawIndexed");
}

void RenderingCommands::drawIndexedIndirect(VulkanBuffer &buffer, VkDeviceSize offset)
{
  usedBufers.push_back(&buffer);

  waitForCmdbuf();
  delayedCommands.push_back([this, &buffer, offset] (VkCommandBuffer i, [[maybe_unused]] size_t imgIndex, size_t frameIndex) {
      vkCmdDrawIndexedIndirect(i, buffer.getDetachedBuffer(this, frameIndex, commandBuffers.front().size()), offset, 1, 0);
    });
  printTraceCommand("vkCmdDrawIndexedIndirect");
}

void RenderingCommands::execute(RenderingCommands &commands)
{
  Q_ASSERT(primary);
  Q_ASSERT(!commands.isPrimary());

  usedCommands.push_back(&commands);
  connect(&commands, &RenderingCommands::aboutToReset, this, &RenderingCommands::rewrite, Qt::DirectConnection);

  waitForCmdbuf();
  delayedCommands.push_back([&commands] (VkCommandBuffer i, size_t imageIndex, size_t frameIndex) {
    commands.check();
    const VkCommandBuffer cmdBuf = commands.getCommandBuffer(imageIndex, frameIndex);
    vkCmdExecuteCommands(i, 1, &cmdBuf);
  });
}

void RenderingCommands::nextSubpass(VkSubpassContents subpassContents)
{
  waitForCmdbuf();
  delayedCommands.push_back([subpassContents](VkCommandBuffer cmdBuf, size_t, size_t) {
    vkCmdNextSubpass(cmdBuf, subpassContents);
  });
}

void RenderingCommands::endRenderpass()
{
  if(primary)
    {
      waitForCmdbuf();
      delayedCommands.push_back([] (VkCommandBuffer cmdBuf, size_t, size_t) { vkCmdEndRenderPass(cmdBuf); });
    }
}

void RenderingCommands::finishRecording()
{
  waitForCmdbuf();
  Q_ASSERT(state == State::WritingCommands || state == State::Dirty);
  state = State::WritingCommands;

  QtConcurrent::map(descriptorPools, [](const std::unique_ptr<VulkanDescriptorPool> &i) { i->collectGarbage(); }).waitForFinished();

  writeCmdbufTask = QtConcurrent::run([this] {
      for(size_t i = 0; i < commandBuffers.size(); ++i)
        for(size_t j = 0; j < commandBuffers[i].size(); ++j)
          {
            VkCommandBuffer cmdBuf = commandBuffers[i][j];
            for(const auto &func: delayedCommands)
              func(cmdBuf, i, j);

            if (vkEndCommandBuffer(cmdBuf) != VK_SUCCESS)
              throw std::runtime_error("failed to record command buffer!");
          }

      if(KawaiiConfig::getInstance().getDebugLevel() > 2)
        qDebug("KurisuRenderer: backed command buffer(s): %lu commands", delayedCommands.size());

      state = State::Compleate;
    });
}

void RenderingCommands::clearImage(const std::vector<VkImage> &img)
{
  waitForCmdbuf();
  delayedCommands.push_back([&img] (VkCommandBuffer cmdBuf, size_t imgIndex, size_t) {
    static const constexpr VkClearColorValue clearVal = {
      .float32 = { 0, 0, 0, 0 }
    };

    static const constexpr VkImageSubresourceRange range = {
      .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
      .levelCount = 1,
      .layerCount = 1
    };

    VkImageMemoryBarrier barier = {
      .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
      .srcAccessMask = VK_ACCESS_NONE_KHR,
      .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
      .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
      .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
      .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
      .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
      .image = img[imgIndex],
      .subresourceRange = range,
    };
    vkCmdPipelineBarrier(cmdBuf,
                         VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT,
                         0,
                         0, nullptr,
                         0, nullptr,
                         1, &barier);
    vkCmdClearColorImage(cmdBuf, img[imgIndex], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &clearVal, 1, &range);
    barier.srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
    barier.dstAccessMask = VK_ACCESS_NONE_KHR;
    barier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
    barier.newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;
    vkCmdPipelineBarrier(cmdBuf,
                         VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT,
                         0,
                         0, nullptr,
                         0, nullptr,
                         1, &barier);
  });
}

void RenderingCommands::blitImage(const std::vector<std::vector<VkImage> > &from, VkImageLayout fromImgLayout, const std::vector<std::vector<VkImage> > &to, VkImageLayout toImgLayout, const std::vector<VkImageBlit> &regions, VkFilter filter)
{
  waitForCmdbuf();
  delayedCommands.push_back([from, fromImgLayout, to, toImgLayout, regions, filter] (VkCommandBuffer cmdBuf, size_t imgIndex, size_t frameIndex) {
      VkPipelineStageFlags destinationStage = VK_PIPELINE_STAGE_TRANSFER_BIT;

      std::array<VkImageMemoryBarrier, 2> barriers = {
        VkImageMemoryBarrier {
          .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
          .srcAccessMask = VK_ACCESS_MEMORY_WRITE_BIT,
          .dstAccessMask = VK_ACCESS_TRANSFER_READ_BIT,
          .oldLayout = fromImgLayout,
          .newLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
          .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
          .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
          .image = from[imgIndex][frameIndex],
          .subresourceRange = VkImageSubresourceRange {
            .aspectMask = regions[0].srcSubresource.aspectMask,
            .levelCount = 1,
            .layerCount = 1,
          }
        },
        VkImageMemoryBarrier {
          .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
          .srcAccessMask = VK_ACCESS_MEMORY_WRITE_BIT | VK_ACCESS_MEMORY_READ_BIT,
          .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
          .oldLayout = toImgLayout,
          .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
          .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
          .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
          .image = to[imgIndex][frameIndex],
          .subresourceRange = VkImageSubresourceRange {
            .aspectMask = regions[0].dstSubresource.aspectMask,
            .levelCount = 1,
            .layerCount = 1,
          }
        }
      };
      if(fromImgLayout == VK_IMAGE_LAYOUT_GENERAL)
        barriers[0].srcAccessMask |= VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT;

      vkCmdPipelineBarrier ( cmdBuf,
                             VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, destinationStage,
                             0,
                             0, nullptr,
                             0, nullptr,
                             2, barriers.data() );

      vkCmdBlitImage(cmdBuf, from[imgIndex][frameIndex], VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, to[imgIndex][frameIndex], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, regions.size(), regions.data(), filter);

      std::swap(barriers[0].oldLayout, barriers[0].newLayout);
      barriers[0].srcAccessMask = VK_ACCESS_MEMORY_WRITE_BIT | VK_ACCESS_MEMORY_READ_BIT;
      std::swap(barriers[0].srcAccessMask, barriers[0].dstAccessMask);
      std::swap(barriers[1].oldLayout, barriers[1].newLayout);
      std::swap(barriers[1].srcAccessMask, barriers[1].dstAccessMask);

      if(fromImgLayout != VK_IMAGE_LAYOUT_UNDEFINED && toImgLayout != VK_IMAGE_LAYOUT_UNDEFINED)
        vkCmdPipelineBarrier ( cmdBuf,
                               VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                               0,
                               0, nullptr,
                               0, nullptr,
                               2, barriers.data() );
      else if(fromImgLayout != VK_IMAGE_LAYOUT_UNDEFINED || toImgLayout != VK_IMAGE_LAYOUT_UNDEFINED)
        {
          if(fromImgLayout == VK_IMAGE_LAYOUT_UNDEFINED)
            vkCmdPipelineBarrier ( cmdBuf,
                                   VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                                   0,
                                   0, nullptr,
                                   0, nullptr,
                                   1, barriers.data()+1 );
          else
            vkCmdPipelineBarrier ( cmdBuf,
                                   VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                                   0,
                                   0, nullptr,
                                   0, nullptr,
                                   1, barriers.data() );
        }
    });
}

void RenderingCommands::blitImage(VkImage from, VkImageLayout fromImgLayout, VkImage to, VkImageLayout toImgLayout, const std::vector<VkImageBlit> &regions, VkFilter filter)
{
  std::vector<std::vector<VkImage>> fromVec(commandBuffers.size(), std::vector<VkImage>(descriptorPools.size(), from)),
      toVec(commandBuffers.size(), std::vector<VkImage>(descriptorPools.size(), to));
  blitImage(fromVec, fromImgLayout, toVec, toImgLayout, regions, filter);
}

void RenderingCommands::imageMemBarier(VkPipelineStageFlags srcStage, VkPipelineStageFlags dstStage, const std::vector<std::vector<std::vector<VkImageMemoryBarrier>>> &imgBarriers)
{
  if(Q_LIKELY(!imgBarriers.empty()))
    {
      waitForCmdbuf();
      delayedCommands.push_back([srcStage, dstStage, imgBarriers] (VkCommandBuffer cmdBuf, size_t imgIndex, size_t frameIndex) {
          const auto &barriers = imgBarriers[imgIndex % imgBarriers.size()][frameIndex];
          vkCmdPipelineBarrier ( cmdBuf,
                                 srcStage, dstStage,
                                 0,
                                 0, nullptr,
                                 0, nullptr,
                                 barriers.size(), barriers.data() );
        });
    }
}

void RenderingCommands::prepare(size_t frameIndex)
{
  isUsedOnGpu[frameIndex] = true;

  if(primary)
    for(auto *i: usedCommands)
      i->prepare(frameIndex);

  waitForCmdbuf();
  for(auto *i: usedBufers)
    {
      i->syncDetachedBuffer(this, frameIndex);
      i->setUsedInGpu(this, frameIndex);
    }

  descriptorPools[frameIndex]->prepareResources();
  descriptorPools[frameIndex]->setUsedInGpu();
  check();

  if(primary)
    waitingForResources[frameIndex] = dev.submitTransferCommandsAsync(resourcesReady[frameIndex].getSemaphore());
}

bool RenderingCommands::isPrimary() const
{
  return primary;
}

RenderingCommands::State RenderingCommands::getState() const
{
  return state;
}

bool RenderingCommands::isEmpty() const
{
  return (state == State::Empty);
}

bool RenderingCommands::hasBindedPipeline()
{
  return bindedPipeline != VK_NULL_HANDLE;
}

void RenderingCommands::exec(uint32_t imageIndex, size_t frameIndex, const std::vector<VkSemaphore> &waitSemaphores, const std::vector<VkPipelineStageFlags> &waitStages, const std::vector<VkSemaphore> &signalSemaphores, VkFence signalFence)
{
  waitForCmdbuf();
  check();
  waitForCmdbuf();
  Q_ASSERT(getState() == State::Compleate);

  Q_ASSERT(waitStages.size() == waitSemaphores.size());

  std::vector<VkSemaphore> wait_sem;
  std::vector<VkPipelineStageFlags> wait_stages;

  VkCommandBuffer cmdBuf = getCommandBuffer(imageIndex, frameIndex);
  VkSubmitInfo submitInfo = {
    .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
    .waitSemaphoreCount = static_cast<uint32_t>(waitSemaphores.size()),
    .pWaitSemaphores = waitSemaphores.empty()? nullptr: waitSemaphores.data(),
    .pWaitDstStageMask = waitStages.empty()? nullptr: waitStages.data(),
    .commandBufferCount = 1,
    .pCommandBuffers = &cmdBuf,
    .signalSemaphoreCount = static_cast<uint32_t>(signalSemaphores.size()),
    .pSignalSemaphores = signalSemaphores.empty()? nullptr: signalSemaphores.data()
  };

  if(waitingForResources[frameIndex])
    {
      wait_sem = waitSemaphores;
      wait_stages = waitStages;
      wait_sem.push_back(resourcesReady[frameIndex].getSemaphore());
      wait_stages.push_back(VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT);

      submitInfo.waitSemaphoreCount = static_cast<uint32_t>(wait_sem.size());
      submitInfo.pWaitSemaphores = wait_sem.data();
      submitInfo.pWaitDstStageMask = wait_stages.data();

      waitingForResources[frameIndex] = false;
    }

  for(auto *i: usedCommands)
    i->waitForCmdbuf();

  if (vkQueueSubmit(dev.graphicsQueue, 1, &submitInfo, signalFence) != VK_SUCCESS)
    throw std::runtime_error("failed to submit draw command buffer!");
}

void RenderingCommands::finalize(size_t frameIndex)
{
  isUsedOnGpu[frameIndex] = false;

  for(auto *i: usedCommands)
    i->finalize(frameIndex);

  for(auto *i: usedBufers)
    i->setUnusedInGpu(this, frameIndex);
  descriptorPools[frameIndex]->setUnusedInGpu();
}

void RenderingCommands::render(uint32_t imageIndex, size_t frameIndex)
{
  static const std::vector<VkSemaphore> waitSemaphores = {};
  static const std::vector<VkPipelineStageFlags> waitStages = {};

  static const std::vector<VkSemaphore> signalSemaphores = {};
  render(imageIndex, frameIndex, waitSemaphores, waitStages, signalSemaphores, VK_NULL_HANDLE);
}

void RenderingCommands::render(uint32_t imageIndex, size_t frameIndex, const std::vector<VkSemaphore> &waitSemaphores, const std::vector<VkPipelineStageFlags> &waitStages, const std::vector<VkSemaphore> &signalSemaphores, VkFence signalFence)
{
  prepare(frameIndex);
  {
    QMutexLocker l(&dev.graphicsQueueM);
    exec(imageIndex, frameIndex, waitSemaphores, waitStages, signalSemaphores, signalFence);
  }

  if(signalFence)
    {
      auto vkDev = dev.vkDev;
      QPointer<RenderingCommands> cmdbuf = this;
      VulkanBuffer::memcpyToGpuBufThreadPool().start([vkDev, signalFence, cmdbuf, frameIndex] {
          vkWaitForFences(vkDev, 1, &signalFence, VK_TRUE, std::numeric_limits<uint64_t>::max());
          if(Q_LIKELY(cmdbuf))
            cmdbuf->finalize(frameIndex);
          else
            {
              qCritical("KurisuRenderer: unreachable reached - a cmdbuf destroyed before finished!");
              Q_UNREACHABLE();
            }
        });
    } else
    finalize(frameIndex);
}

void RenderingCommands::waitForCmdbuf()
{
  if(writeCmdbufTask.isRunning())
    writeCmdbufTask.waitForFinished();
}

VkCommandBuffer RenderingCommands::getCommandBuffer(size_t imageIndex, size_t frameIndex)
{
  waitForCmdbuf();
  return commandBuffers[imageIndex][frameIndex];
}

void RenderingCommands::rewrite()
{
  if(getState() == State::Compleate)
    {
      waitForCmdbuf();
      dev.waitIdle();
      emit aboutToReset();
      QWriteLocker l(&dev.resLock);
      for(const auto &cmdBufs: commandBuffers)
        for(VkCommandBuffer cmdBuf: cmdBufs)
          vkResetCommandBuffer(cmdBuf, 0);
      state = State::Dirty;
    }
}

void RenderingCommands::onDescriptorSetUpdated(uint64_t name)
{
  if(usedDescriptorSetNames.count(name) > 0)
    rewrite();
}

void RenderingCommands::check()
{
  if(getState() == State::Dirty)
    {
      dev.waitIdle();
      for(auto i = waitingForResources.begin(); i != waitingForResources.end(); ++i)
        if(*i)
          *i = false;
      finishRecording();
    }
}
