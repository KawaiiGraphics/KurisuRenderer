#ifndef KURISUSHADEREFFECTIMPL_HPP
#define KURISUSHADEREFFECTIMPL_HPP

#include "KurisuImageSourceImpl.hpp"
#include <KawaiiRenderer/Renderpass/KawaiiShaderEffectImpl.hpp>

class KurisuRootImpl;

class KURISURENDERER_SHARED_EXPORT KurisuShaderEffectImpl: public KurisuImageSourceImpl, public KawaiiShaderEffectImpl
{
public:
  KurisuShaderEffectImpl(KawaiiShaderEffect *model);
  ~KurisuShaderEffectImpl();

  // KawaiiImageSourceImpl interface
protected:
  void drawQuad() override final;

  // KurisuImageSourceImpl interface
private:
  void writeCmdBuf(RenderingCommands &cmdBuf, size_t dev_index) override final;

  // KawaiiRendererImpl interface
private:
  void initConnections() override final;



  // IMPLEMENT
private:
  KurisuRootImpl *root;
  QMetaObject::Connection onRootBufferBindingChanged;
};

#endif // KURISUSHADEREFFECTIMPL_HPP
