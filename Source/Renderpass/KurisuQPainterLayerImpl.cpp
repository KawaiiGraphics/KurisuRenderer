#include "KurisuQPainterLayerImpl.hpp"

KurisuQPainterLayerImpl::KurisuQPainterLayerImpl(KawaiiQPainterLayer *model):
  KawaiiImageSourceImpl(model),
  KurisuGlInteropLayer(model),
  KawaiiQPainterLayerImpl(model)
{
  painterCreater = std::bind(&KurisuQPainterLayerImpl::getQPainter, this);
}

KurisuQPainterLayerImpl::~KurisuQPainterLayerImpl()
{
  qpainter.reset();
}

void KurisuQPainterLayerImpl::refreshPixmaps()
{
  KurisuGlInteropLayer::refreshPixmaps();
}

QPainter *KurisuQPainterLayerImpl::getQPainter()
{
  getCurrentOverlayImg().prepare();
  getCurrentOverlayImg().clear();

  if(!qpainter)
    qpainter = std::make_unique<QPainter>(&getCurrentOverlayImg().qPaintDev());
  else
    qpainter->begin(&getCurrentOverlayImg().qPaintDev());
  return qpainter.get();
}
