#include "VulkanStagingBuffer.hpp"
#include <cstring>

VulkanStagingBuffer::VulkanStagingBuffer(const VulkanDevice &dev, const void *data, size_t dataSize):
  gpuTask(VK_NULL_HANDLE),
  dev(dev.vkDev),
  allocatedSize(dataSize)
{
  const uint32_t transferFamily = *dev.queueFamilies.transferFamily;

  const VkBufferCreateInfo createInfo = {
    .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
    .size = dataSize,
    .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
    .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    .queueFamilyIndexCount = 1,
    .pQueueFamilyIndices = &transferFamily
  };

  vkCreateBuffer(dev.vkDev, &createInfo, nullptr, &buf);

  VkMemoryRequirements memRequirements;
  vkGetBufferMemoryRequirements(dev.vkDev, buf, &memRequirements);

  const VkMemoryDedicatedAllocateInfo dedicatedAllocateInfo {
    .sType = VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO,
    .buffer = buf
  };

  mem = dev.allocateMemory(memRequirements, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 0, dedicatedAllocateInfo).first;
  vkBindBufferMemory(dev.vkDev, buf, mem, 0);

  vkMapMemory(dev.vkDev, mem, 0, VK_WHOLE_SIZE, 0, &mappedMemory);
  std::memcpy(mappedMemory, data, dataSize);
}

VulkanStagingBuffer::VulkanStagingBuffer(const VulkanDevice &dev, const std::function<void (void *)> &dataSrc, size_t dataSize):
  gpuTask(VK_NULL_HANDLE),
  dev(dev.vkDev),
  allocatedSize(dataSize)
{
  const uint32_t transferFamily = *dev.queueFamilies.transferFamily;

  const VkBufferCreateInfo createInfo = {
    .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
    .size = dataSize,
    .usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
    .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    .queueFamilyIndexCount = 1,
    .pQueueFamilyIndices = &transferFamily
  };

  vkCreateBuffer(dev.vkDev, &createInfo, nullptr, &buf);

  VkMemoryRequirements memRequirements;
  vkGetBufferMemoryRequirements(dev.vkDev, buf, &memRequirements);

  const VkMemoryDedicatedAllocateInfo dedicatedAllocateInfo {
    .sType = VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO,
    .buffer = buf
  };

  mem = dev.allocateMemory(memRequirements, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_COHERENT_BIT, 0, dedicatedAllocateInfo).first;
  vkBindBufferMemory(dev.vkDev, buf, mem, 0);

  vkMapMemory(dev.vkDev, mem, 0, VK_WHOLE_SIZE, 0, &mappedMemory);
  dataSrc(mappedMemory);
}

VulkanStagingBuffer::~VulkanStagingBuffer()
{
  vkUnmapMemory(dev, mem);
  vkDeviceWaitIdle(dev);
  vkDestroyBuffer(dev, buf, nullptr);
  vkFreeMemory(dev, mem, nullptr);
}

VkBuffer VulkanStagingBuffer::getBuf() const
{
  return buf;
}

size_t VulkanStagingBuffer::getAllocatedSize() const
{
  return allocatedSize;
}

bool VulkanStagingBuffer::setData(const void *data, size_t dataSize)
{
  if(dataSize > allocatedSize)
    return false;

  std::memcpy(mappedMemory, data, dataSize);
  return true;
}

bool VulkanStagingBuffer::setData(const std::function<void (void *)> &dataSrc, size_t dataSize)
{
  if(dataSize > allocatedSize)
    return false;

  dataSrc(mappedMemory);
  return true;
}

bool VulkanStagingBuffer::isReady() const
{
  return !cpuTask.isRunning() && (!gpuTask || vkWaitForFences(dev, 1, &gpuTask, VK_TRUE, 0) == VK_SUCCESS);
}

void VulkanStagingBuffer::waitForTask()
{
  if(cpuTask.isRunning())
    cpuTask.waitForFinished();
}

void VulkanStagingBuffer::setAsyncTask(const QFuture<void> &future, VkFence fence)
{
  cpuTask = future;
  gpuTask = fence;
}
