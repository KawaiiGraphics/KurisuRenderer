#include "GlslCompiler.hpp"
#include <Kawaii3D/KawaiiConfig.hpp>
#include <glslang/SPIRV/GlslangToSpv.h>
#include <spirv-tools/libspirv.hpp>
#include <QDebug>
#include <memory>

namespace {
  template<size_t N>
  constexpr QLatin1String toLatin1(const char (&str)[N])
  { return QLatin1String(str, N); }

  void spirv_logger(spv_message_level_t level, const char* /* source */,
                    const spv_position_t& /* position */, const char* message)
  {
    static const std::unordered_map<spv_message_level_t, QLatin1String> map = {
      {SPV_MSG_FATAL,          toLatin1("SPV_MSG_FATAL  ") },
      {SPV_MSG_INTERNAL_ERROR, toLatin1("SPV_MSG_INTERNAL_ERROR  ")},
      {SPV_MSG_ERROR,          toLatin1("SPV_MSG_ERROR  ")},
      {SPV_MSG_WARNING,        toLatin1("SPV_MSG_WARNING  ")},
      {SPV_MSG_INFO,           toLatin1("SPV_MSG_INFO  ")},
      {SPV_MSG_DEBUG,          toLatin1("SPV_MSG_DEBUG  ")}
    };

    if(auto el = map.find(level); el != map.end())
      qCritical().noquote() << el->second << QLatin1String(message);
    else
      Q_UNREACHABLE();
  }

  class GlslangInitializer {
    spvtools::Optimizer spirvOptimizer;
    spvtools::Context spirvContext;
    spvtools::SpirvTools spirvTools;

  public:
    GlslangInitializer():
      spirvOptimizer(SPV_ENV_VULKAN_1_0),
      spirvContext(SPV_ENV_VULKAN_1_0),
      spirvTools(SPV_ENV_VULKAN_1_0)
    {
      glslang::InitializeProcess();
      spirvOptimizer.RegisterPerformancePasses();
      spirvOptimizer.SetMessageConsumer(spirv_logger);
      spirvContext.SetMessageConsumer(spirv_logger);
      spirvTools.SetMessageConsumer(spirv_logger);
    }

    ~GlslangInitializer()
    {
      glslang::FinalizeProcess();
    }

    spvtools::Optimizer &optimizer()
    {
      return spirvOptimizer;
    }

    spvtools::Context &context()
    {
      return spirvContext;
    }

    spvtools::SpirvTools &tools()
    {
      return spirvTools;
    }
  };
}

GlslCompiler::GlslCompiler()
{
  static std::unique_ptr<GlslangInitializer> initializer;

  if(!initializer)
    initializer = std::make_unique<GlslangInitializer>();

  spvOptimizer = &initializer->optimizer();
  spvContext = &initializer->context();
  spvTools = &initializer->tools();
}

namespace {
  constexpr TBuiltInResource defaultResLimits =
  {
    .maxLights                                 = 32,
    .maxClipPlanes                             = 6,
    .maxTextureUnits                           = 32,
    .maxTextureCoords                          = 32,
    .maxVertexAttribs                          = 64,
    .maxVertexUniformComponents                = 4096,
    .maxVaryingFloats                          = 64,
    .maxVertexTextureImageUnits                = 32,
    .maxCombinedTextureImageUnits              = 80,
    .maxTextureImageUnits                      = 32,
    .maxFragmentUniformComponents              = 4096,
    .maxDrawBuffers                            = 32,
    .maxVertexUniformVectors                   = 128,
    .maxVaryingVectors                         = 8,
    .maxFragmentUniformVectors                 = 16,
    .maxVertexOutputVectors                    = 16,
    .maxFragmentInputVectors                   = 15,
    .minProgramTexelOffset                     = -8,
    .maxProgramTexelOffset                     = 7,
    .maxClipDistances                          = 8,
    .maxComputeWorkGroupCountX                 = 65535,
    .maxComputeWorkGroupCountY                 = 65535,
    .maxComputeWorkGroupCountZ                 = 65535,
    .maxComputeWorkGroupSizeX                  = 1024,
    .maxComputeWorkGroupSizeY                  = 1024,
    .maxComputeWorkGroupSizeZ                  = 64,
    .maxComputeUniformComponents               = 1024,
    .maxComputeTextureImageUnits               = 16,
    .maxComputeImageUniforms                   = 8,
    .maxComputeAtomicCounters                  = 8,
    .maxComputeAtomicCounterBuffers            = 1,
    .maxVaryingComponents                      = 60,
    .maxVertexOutputComponents                 = 64,
    .maxGeometryInputComponents                = 64,
    .maxGeometryOutputComponents               = 128,
    .maxFragmentInputComponents                = 128,
    .maxImageUnits                             = 8,
    .maxCombinedImageUnitsAndFragmentOutputs   = 8,
    .maxCombinedShaderOutputResources          = 8,
    .maxImageSamples                           = 0,
    .maxVertexImageUniforms                    = 0,
    .maxTessControlImageUniforms               = 0,
    .maxTessEvaluationImageUniforms            = 0,
    .maxGeometryImageUniforms                  = 0,
    .maxFragmentImageUniforms                  = 8,
    .maxCombinedImageUniforms                  = 8,
    .maxGeometryTextureImageUnits              = 16,
    .maxGeometryOutputVertices                 = 256,
    .maxGeometryTotalOutputComponents          = 1024,
    .maxGeometryUniformComponents              = 1024,
    .maxGeometryVaryingComponents              = 64,
    .maxTessControlInputComponents             = 128,
    .maxTessControlOutputComponents            = 128,
    .maxTessControlTextureImageUnits           = 16,
    .maxTessControlUniformComponents           = 1024,
    .maxTessControlTotalOutputComponents       = 4096,
    .maxTessEvaluationInputComponents          = 128,
    .maxTessEvaluationOutputComponents         = 128,
    .maxTessEvaluationTextureImageUnits        = 16,
    .maxTessEvaluationUniformComponents        = 1024,
    .maxTessPatchComponents                    = 120,
    .maxPatchVertices                          = 32,
    .maxTessGenLevel                           = 64,
    .maxViewports                              = 16,
    .maxVertexAtomicCounters                   = 0,
    .maxTessControlAtomicCounters              = 0,
    .maxTessEvaluationAtomicCounters           = 0,
    .maxGeometryAtomicCounters                 = 0,
    .maxFragmentAtomicCounters                 = 8,
    .maxCombinedAtomicCounters                 = 8,
    .maxAtomicCounterBindings                  = 1,
    .maxVertexAtomicCounterBuffers             = 0,
    .maxTessControlAtomicCounterBuffers        = 0,
    .maxTessEvaluationAtomicCounterBuffers     = 0,
    .maxGeometryAtomicCounterBuffers           = 0,
    .maxFragmentAtomicCounterBuffers           = 1,
    .maxCombinedAtomicCounterBuffers           = 1,
    .maxAtomicCounterBufferSize                = 16384,
    .maxTransformFeedbackBuffers               = 4,
    .maxTransformFeedbackInterleavedComponents = 64,
    .maxCullDistances                          = 8,
    .maxCombinedClipAndCullDistances           = 8,
    .maxSamples                                = 4,
    .maxMeshOutputVerticesNV                   = 256,
    .maxMeshOutputPrimitivesNV                 = 512,
    .maxMeshWorkGroupSizeX_NV                  = 32,
    .maxMeshWorkGroupSizeY_NV                  = 1,
    .maxMeshWorkGroupSizeZ_NV                  = 1,
    .maxTaskWorkGroupSizeX_NV                  = 32,
    .maxTaskWorkGroupSizeY_NV                  = 1,
    .maxTaskWorkGroupSizeZ_NV                  = 1,
    .maxMeshViewCountNV                        = 4,

    .limits =
    {
      .nonInductiveForLoops                 = true,
      .whileLoops                           = true,
      .doWhileLoops                         = true,
      .generalUniformIndexing               = true,
      .generalAttributeMatrixVectorIndexing = true,
      .generalVaryingIndexing               = true,
      .generalSamplerIndexing               = true,
      .generalVariableIndexing              = true,
      .generalConstantMatrixVectorIndexing  = true,
    }
  };

  constexpr const int defaultGlslVersion = 450;
  constexpr const EShMessages vulkanMessages = static_cast<EShMessages> (EShMsgSpvRules | EShMsgVulkanRules | EShMsgDebugInfo);
}

std::unique_ptr<glslang::TShader> GlslCompiler::compileGlsl(const std::string &glsl_code, EShLanguage kind, const std::string &fName)
{
  std::unique_ptr<glslang::TShader> shader(std::make_unique<glslang::TShader>(kind));
  recompileGlsl(*shader, glsl_code, kind, fName);
  return shader;
}

EShLanguage GlslCompiler::kind(KawaiiShaderType type)
{
  switch(type) {
    case KawaiiShaderType::Fragment:
      return EShLangFragment;

    case KawaiiShaderType::Vertex:
      return EShLangVertex;

    case KawaiiShaderType::Geometry:
      return EShLangGeometry;

    case KawaiiShaderType::TesselationControll:
      return EShLangTessControl;

    case KawaiiShaderType::TesselationEvaluion:
      return EShLangTessEvaluation;

    case KawaiiShaderType::Compute:
      return EShLangCompute;
    }
  Q_UNREACHABLE();
}

EShLanguage GlslCompiler::kind(VkShaderStageFlagBits type)
{
  switch(type) {
    case VK_SHADER_STAGE_FRAGMENT_BIT:
      return EShLangFragment;

    case VK_SHADER_STAGE_VERTEX_BIT:
      return EShLangVertex;

    case VK_SHADER_STAGE_GEOMETRY_BIT:
      return EShLangGeometry;

    case VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT:
      return EShLangTessControl;

    case VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT:
      return EShLangTessEvaluation;

    case VK_SHADER_STAGE_COMPUTE_BIT:
      return EShLangCompute;

    default: break;
    }
  Q_UNREACHABLE();
}

void GlslCompiler::recompileGlsl(glslang::TShader &shader, const std::string &glsl_code, EShLanguage kind, const std::string &fName)
{
  const char *glsl_code_c = glsl_code.c_str();
  int glsl_length = glsl_code.size();

  shader.setStringsWithLengths(&glsl_code_c, &glsl_length, 1);
  shader.setEnvInput(glslang::EShSourceGlsl, kind, glslang::EShClientVulkan, 100);
  shader.setEnvClient(glslang::EShClientVulkan, glslang::EShTargetVulkan_1_0);
  shader.setEnvTarget(glslang::EShTargetSpv, glslang::EShTargetSpv_1_0);

  static glslang::TShader::ForbidIncluder includer;

  std::string preprocessed_glsl;
  bool preprocessed = shader.preprocess(&defaultResLimits, defaultGlslVersion, EProfile::ECoreProfile, false, false, vulkanMessages, &preprocessed_glsl, includer);
  QByteArray infoLog = shader.getInfoLog(),
      infoDebugLog = shader.getInfoDebugLog();

  if (!preprocessed)
    {
      qCritical().noquote() << "GLSL preprocessing failed for: " << QString::fromStdString(fName);
      qCritical().noquote() << infoLog;
      qCritical().noquote() << infoDebugLog;
      qCritical().noquote() << "Shader code:\n" << QString::fromStdString(glsl_code);

      throw std::runtime_error("GLSL preprocessing failed");
    } else if(!infoLog.isEmpty() || !infoDebugLog.isEmpty())
    {
      qWarning().noquote() << "GLSL parsing log for: " << QString::fromStdString(fName);
      qWarning().noquote() << infoLog;
      qWarning().noquote() << infoDebugLog;
    }

  glsl_code_c = preprocessed_glsl.c_str();
  glsl_length = preprocessed_glsl.size();
  shader.setStringsWithLengths(&glsl_code_c, &glsl_length, 1);

  bool parsed = shader.parse(&defaultResLimits, defaultGlslVersion, false, vulkanMessages);
  infoLog = shader.getInfoLog();
  infoDebugLog = shader.getInfoDebugLog();

  if (!parsed)
  {
      qCritical().noquote() << "GLSL parsing failed for: " << QString::fromStdString(fName);
      qCritical().noquote() << infoLog;
      qCritical().noquote() << infoDebugLog;
      qCritical().noquote() << "Shader code:\n" << QString::fromStdString(glsl_code);

      throw std::runtime_error("GLSL parsing failed");
  } else if(!infoLog.isEmpty() || !infoDebugLog.isEmpty())
    {
      qWarning().noquote() << "GLSL parsing log for: " << QString::fromStdString(fName);
      qWarning().noquote() << infoLog;
      qWarning().noquote() << infoDebugLog;
    }
}

std::vector<uint32_t> GlslCompiler::glslToSpirv(const std::string &glsl_code, EShLanguage kind, const std::string &fName)
{
  std::vector<std::unique_ptr<glslang::TShader>> shaders;
  shaders.push_back(compileGlsl(glsl_code, kind, fName));
  return glslToSpirv(shaders, kind);
}

std::vector<uint32_t> GlslCompiler::glslToSpirv(const std::vector<std::unique_ptr<glslang::TShader>> &glsl_code, EShLanguage kind)
{
  glslang::TProgram prog;
  for(const auto &i: glsl_code)
    prog.addShader(i.get());

  bool linked = prog.link(vulkanMessages);

  const QByteArray infoLog = prog.getInfoLog(),
      infoDebugLog = prog.getInfoDebugLog();

  if (!linked)
    {
      qCritical().noquote() << "GLSL linking failed!";
      qCritical().noquote() << infoLog;
      qCritical().noquote() << infoDebugLog;

      throw std::runtime_error("GLSL parsing failed");
    } else if(!infoLog.isEmpty() || !infoDebugLog.isEmpty())
    {
      qWarning().noquote() << "GLSL linking log:";
      qWarning().noquote() << infoLog;
      qWarning().noquote() << infoDebugLog;
    }

  return buildSpirV(prog.getIntermediate(kind));
}

std::vector<uint32_t> GlslCompiler::buildSpirV(glslang::TIntermediate *code)
{
  std::vector<uint32_t> spirV;

  if(code)
    {
      spv::SpvBuildLogger logger;
      glslang::SpvOptions spvOptions;
      spvOptions.disableOptimizer = false;

      glslang::GlslangToSpv(*code, spirV, &logger, &spvOptions);

      if(auto log_str = logger.getAllMessages(); !log_str.empty())
        qWarning() << QString::fromStdString(log_str);

      if(!spvTools->Validate(spirV))
        {
          qCritical("\n\nINVALID SPIRV\n");
          return {};
        }

      std::vector<uint32_t> spirV_optimized;
      if(spvOptimizer->Run(spirV.data(), spirV.size(), &spirV_optimized))
        if(spvTools->Validate(spirV_optimized))
          spirV = std::move(spirV_optimized);
    }
  return spirV;
}

#ifdef SPIRV_LINKER
#include <spirv-tools/linker.hpp>

std::vector<uint32_t> GlslCompiler::glslModuleToSpirv(const std::string &glsl_code, EShLanguage kind, const std::string &fName)
{
  const char *glsl_code_c = glsl_code.c_str();
  int glsl_length = glsl_code.size();

  glslang::TShader shader(kind);
  shader.setStringsWithLengths(&glsl_code_c, &glsl_length, 1);
  shader.setAutoMapLocations(true);
  bool parsed = shader.parse(&defaultResLimits, defaultGlslVersion, false, vulkanMessages);
  const QByteArray infoLog = shader.getInfoLog(),
      infoDebugLog = shader.getInfoDebugLog();

  if (!parsed)
  {
      qCritical().noquote() << "GLSL parsing failed for: " << QString::fromStdString(fName);
      qCritical().noquote() << infoLog;
      qCritical().noquote() << infoDebugLog;

      throw std::runtime_error("GLSL parsing failed");
  } else if(!infoLog.isEmpty() || !infoDebugLog.isEmpty())
    {
      qWarning().noquote() << "GLSL parsing log for: " << QString::fromStdString(fName);
      qWarning().noquote() << infoLog;
      qWarning().noquote() << infoDebugLog;
    }

  const std::vector<uint32_t> code = buildSpirV(shader.getIntermediate(), false);
  return code;
}

std::vector<uint32_t> GlslCompiler::linkSpirv(const std::vector<std::vector<uint32_t> > &modules)
{
  std::vector<uint32_t> spirV;
  std::vector<uint32_t> spirV_optimized;

  spv_result_t result = spvtools::Link(*spvContext, modules, &spirV);
  static_cast<void>(result);

  if(!spvTools->Validate(spirV))
    {
      qCritical("\n\nINVALID SPIRV\n");
      return {};
    }

  if(spvOptimizer->Run(spirV.data(), spirV.size(), &spirV_optimized))
    if(spvTools->Validate(spirV_optimized))
      spirV = std::move(spirV_optimized);

  return spirV;
}
#else
std::vector<uint32_t> GlslCompiler::glslModuleToSpirv(const std::string &/*glsl_code*/, EShLanguage /*kind*/, const std::string &/*fName*/)
{
  return {};
}

std::vector<uint32_t> GlslCompiler::linkSpirv(const std::vector<std::vector<uint32_t> > &/*modules*/)
{
  return {};
}
#endif
