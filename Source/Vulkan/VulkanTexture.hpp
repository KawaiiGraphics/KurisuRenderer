#ifndef VULKANTEXTURE_HPP
#define VULKANTEXTURE_HPP

#include <KawaiiRenderer/Textures/KawaiiTextureFormat.hpp>
#include <Kawaii3D/Textures/KawaiiTextureFilter.hpp>
#include <Kawaii3D/Textures/KawaiiTextureWrapMode.hpp>
#include <Kawaii3D/Textures/KawaiiDepthCompareOperation.hpp>

#include "VulkanDevice.hpp"
#include "VulkanCommandPool.hpp"

#include <QOpenGLFunctions>
#include <QtGlobal>
#include <array>

class VulkanTexture
{
  bool isInExclusiveMode() const;

  VulkanTexture(const VulkanTexture &) = delete;
  VulkanTexture& operator=(const VulkanTexture &) = delete;

public:
  enum Usage {
    UsageNone = 0x0,
    UsageSampler = 0x1,
    UsageAttachment = 0x2
  };
  Q_DECLARE_FLAGS(UsageFlags, Usage)

  class View2D {
    friend class  ::VulkanTexture;

    View2D(const View2D &orig) = delete;
    View2D& operator=(const View2D &orig) = delete;
  public:
    View2D();
    View2D(VulkanTexture &tex, int layer, VkImageAspectFlags usage);
    View2D(VulkanTexture &tex, int layer);
    ~View2D();

    View2D(View2D &&orig);
    View2D& operator=(View2D &&orig);

    inline VkImageView getImgView() const
    { return imgView; }

    inline VulkanTexture *getTexture() const
    { return tex; }


  private:
    VulkanTexture *tex;
    VkImageView imgView;

    void destroy();
  };

  class ViewArray2D {
    friend class  ::VulkanTexture;

    ViewArray2D(const ViewArray2D &orig) = delete;
    ViewArray2D& operator=(const ViewArray2D &orig) = delete;
  public:
    ViewArray2D();
    ViewArray2D(VulkanTexture &tex, VkImageAspectFlags usage);
    ViewArray2D(VulkanTexture &tex);
    ~ViewArray2D();

    ViewArray2D(ViewArray2D &&orig);
    ViewArray2D& operator=(ViewArray2D &&orig);

    inline VkImageView getImgView() const
    { return imgView; }

    inline VulkanTexture *getTexture() const
    { return tex; }


  private:
    VulkanTexture *tex;
    VkImageView imgView;

    void destroy();
  };

  inline static constexpr VkExternalMemoryHandleTypeFlagBits imgMultiGpuMemoryHandleType = VulkanDevice::imgMultiGpuMemoryHandleType;
  inline static constexpr VkExternalMemoryHandleTypeFlagBits imgSameGpuMemoryHandleType = VulkanDevice::imgSameGpuMemoryHandleType;

  VulkanTexture(VulkanDevice &dev, const UsageFlags &allowedUsage);
  ~VulkanTexture();

  VulkanTexture(VulkanTexture &&orig);
  VulkanTexture& operator=(VulkanTexture &&orig);

  void setFormat_8bpc(KawaiiTextureFormat fmt);
  void setFormat_32bpc(KawaiiTextureFormat fmt);
  void setFormat(VkFormat fmt, VkImageAspectFlags aspectFlagsFull, VkImageAspectFlags aspectFlagsUsage);

  void setExtent(uint32_t width);
  void setExtent(uint32_t width, uint32_t height);
  void setExtent(uint32_t width, uint32_t height, uint32_t depth);
  void setExtent(const VkExtent3D &extent, VkImageType imgType);

  void setLayers(uint32_t mipMapLayers, uint32_t arrayLayers);
  uint32_t getMipMapLayers() const;
  uint32_t getArrayLayers() const;
  void setCompareOp(KawaiiDepthCompareOperation op);
  void setCubemapCompatible(bool compatible);
  bool isCubemapCompatible() const;

  void setLinear(bool linear);
  bool isLinear() const;

  void importImage(const VulkanTexture &texture);
  void importImage(VulkanDevice::Handle handle, VkExternalMemoryHandleTypeFlagBits type);
  void pointToImage(const VulkanTexture &texture);

  void createImage(const void *imageData, VkExternalMemoryHandleTypeFlags exportMemory=0);
  void setBits(const void *imageData, VkExternalMemoryHandleTypeFlags exportMemory);
  void copyBitsFromGl(QOpenGLFunctions &gl);
  void createImageView(VkImageViewType type);

  void setMinFilter(KawaiiTextureFilter filter);
  void setMagFilter(KawaiiTextureFilter filter);

  void setWrapModeS(KawaiiTextureWrapMode mode);
  void setWrapModeT(KawaiiTextureWrapMode mode);
  void setWrapModeR(KawaiiTextureWrapMode mode);

  void createSampler();

  VkDescriptorImageInfo *descriptorInfo();
  const VkDescriptorImageInfo *descriptorInfo() const;

  VkImageLayout getVkImageLayout() const;

  VkImage getVkImage() const;
  VkImageView getVkImageView() const;

  size_t getAllocatedSize() const;
  uint32_t bytesPerPixel() const;

  const VulkanDevice& getDevice() const;

  VkFormat getVkFormat() const;
  KawaiiTextureFormat getKawaiiFormat() const;

  const VkExtent3D& getExtent() const;

  VkImageType getImageType() const;

  VkImageAspectFlags getAspectFlagsUsage() const;
  VkImageAspectFlags getAspectFlagsFull() const;

  std::optional<VulkanDevice::Handle> getMemHandle(VkExternalMemoryHandleTypeFlagBits type) const;

  VkImageSubresourceRange getImageSubresourceRange() const;

private:
  std::vector<View2D*> views2d;
  std::vector<ViewArray2D*> viewsArray2d;

  std::array<uint32_t, 2> queueFalilyIndices;

  VkImageCreateInfo imageInfo;

  KawaiiTextureFilter minFilter;
  KawaiiTextureFilter magFilter;

  KawaiiTextureWrapMode uWrap;
  KawaiiTextureWrapMode vWrap;
  KawaiiTextureWrapMode wWrap;

  KawaiiDepthCompareOperation compareOp;

  VulkanDevice *dev;
  VulkanCommandPool::OnSubmitElement onTransferCmdSubmit;

  VkImage img;
  VkDeviceMemory mem;

  VkImageView imgView;
  VkImageViewType imgViewType;
  VkSampler sampler;

  VkDeviceSize memorySize;
  VkDeviceSize allocatedSize;
  VkDeviceSize memoryAlignment;

  QFuture<void> writeTransferCmdTask;

  VkDescriptorImageInfo descrInfo;

  UsageFlags allowedUsage;

  VkImageAspectFlags imgAspectFlagsFull;
  VkImageAspectFlags imgAspectFlagsUsage;

  VkImageLayout finalLayout;

  VkExternalMemoryHandleTypeFlags exportMemory;

  bool dirty;
  bool dirty_sampler;

  bool waitsForTransfer;
  bool ownsMem;

  void prepareCreateImage(VkMemoryRequirements &memReq, VkExternalMemoryHandleTypeFlags exportTypes);

  void destroyImgView();
  void destroyImg();
};

#endif // VULKANTEXTURE_HPP
