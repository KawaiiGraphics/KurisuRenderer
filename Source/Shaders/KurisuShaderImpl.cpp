#include "KurisuShaderImpl.hpp"
#include "KurisuRootImpl.hpp"
#include "Vulkan/GlslCompiler.hpp"

#include <Kawaii3D/KawaiiConfig.hpp>
#include <QRegularExpression>

KurisuShaderImpl::KurisuShaderImpl(KawaiiShader *model):
  KawaiiShaderImpl(model),
  modelObj(model),
  enabledVertexAttributes( { false, false, false } ),
  kind(GlslCompiler::kind(model->getType())),
  spirv_dirty(true)
{
  switch(model->getType())
    {
    case KawaiiShaderType::Fragment:
      stage = VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT;
      break;
    case KawaiiShaderType::TesselationControll:
      stage = VkShaderStageFlagBits::VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT;
      break;
    case KawaiiShaderType::TesselationEvaluion:
      stage = VkShaderStageFlagBits::VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
      break;
    case KawaiiShaderType::Vertex:
      stage = VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT;
      break;
    case KawaiiShaderType::Geometry:
      stage = VkShaderStageFlagBits::VK_SHADER_STAGE_GEOMETRY_BIT;
      break;
    case KawaiiShaderType::Compute:
      stage = VkShaderStageFlagBits::VK_SHADER_STAGE_COMPUTE_BIT;
      break;
    default: Q_UNREACHABLE();
    }

  initResource();
}

KurisuShaderImpl::~KurisuShaderImpl()
{
  preDestruct();
}

KawaiiShaderType KurisuShaderImpl::getShaderType() const
{
  return modelObj->getType();
}

QString KurisuShaderImpl::getModuleFName() const
{
  auto result = modelObj->getModuleName();
  switch(modelObj->getType())
    {
    case KawaiiShaderType::Fragment:
      result += QStringLiteral(".frag.glsl");
      break;
    case KawaiiShaderType::TesselationControll:
      result += QStringLiteral(".tesc.glsl");
      break;
    case KawaiiShaderType::TesselationEvaluion:
      result += QStringLiteral(".tese.glsl");
      break;
    case KawaiiShaderType::Vertex:
      result += QStringLiteral(".vert.glsl");
      break;
    case KawaiiShaderType::Geometry:
      result += QStringLiteral(".geom.glsl");
      break;
    case KawaiiShaderType::Compute:
      result += QStringLiteral(".comp.glsl");
      break;
    default: Q_UNREACHABLE();
    }
  return result;
}

std::unique_ptr<glslang::TShader> KurisuShaderImpl::compile() const
{
  return GlslCompiler().compileGlsl(glsl, kind, modelObj->getModuleName().toStdString());
}

const std::vector<uint32_t> &KurisuShaderImpl::getLinkableSpirv()
{
  if(spirv_dirty)
    {
      spirv = GlslCompiler().glslModuleToSpirv(glsl, kind, modelObj->getModuleName().toStdString());
      spirv_dirty = false;
    }

  return spirv;
}

bool KurisuShaderImpl::isAttributeEnabled(size_t attribute) const
{
  return enabledVertexAttributes[attribute];
}

void KurisuShaderImpl::createShader()
{
  recompile();
}

void KurisuShaderImpl::deleteShader()
{
  glsl.clear();
}

void KurisuShaderImpl::recompile()
{
  enabledVertexAttributes = { false, false, false };
  glsl = preprocessGlsl(modelObj->getCode()).toStdString();
  spirv_dirty = true;
}

namespace {
  static const QString storageImgGlslPattern = QStringLiteral("layout (binding = %1, set = %2, %3) uniform image2D STORAGE_GBUF_%4;");
  static const QString readonlyImgGlslPattern = QStringLiteral("layout (binding = %1, set = %2, %3) readonly uniform image2D STORAGE_GBUF_%4;");
  static const QString sampledImgGlslPattern = QStringLiteral("layout (binding = %1, set = %2) uniform sampler2D SAMPLED_GBUF_%3;");
}

QString KurisuShaderImpl::preprocessGlsl(const QString &code)
{
  QString glsl = code;
  if(stage == VK_SHADER_STAGE_VERTEX_BIT)
    {
      static QRegularExpression regVertAttribute(QStringLiteral("layout\\s*\\(\\s*location\\s*=\\s*(\\d+)\\s*\\)\\s*in\\s*"));
      auto vertAttMatch = regVertAttribute.globalMatch(glsl);
      while(vertAttMatch.hasNext())
        {
          auto i = vertAttMatch.next();
          if(modelObj->globalScopeAt(i.capturedStart()))
            {
              bool isAttIndexValid = false;
              size_t attIndex = i.captured(1).toULongLong(&isAttIndexValid);
              if(isAttIndexValid && attIndex < enabledVertexAttributes.size())
                enabledVertexAttributes[attIndex] = true;
            }
        }
    }

  std::array<int, 4> blockEnds = {
    modelObj->getCameraBlockEnd(),
    modelObj->getSurfaceBlockEnd(),
    modelObj->getMaterialBlockEnd(),
    modelObj->getModelBlockEnd()
  };
  struct ReplacementDefinition {
    QString after;
    int b;
    int sz;
  };

  std::map<int, ReplacementDefinition> replacements;
  modelObj->forallInputGbuf([&replacements, this] (uint32_t gbufIndex, const KawaiiShader::GBufDefinition &gbuf) {
      const auto gbufBinding = rootImpl()->getInputGbufLocation(gbufIndex);
      QString after = QStringLiteral("layout (input_attachment_index = %1, binding = %2, set = %3) uniform subpassInput INPUT_GBUF_%1;")
          .arg(gbufIndex)
          .arg(gbufBinding.binding)
          .arg(gbufBinding.set);

      replacements[gbuf.i0] = ReplacementDefinition { .after = after, .b = gbuf.i0, .sz = gbuf.sz };
    });
  modelObj->forallStorageGbuf([&replacements, this] (uint32_t gbufIndex, const KawaiiShader::GBufDefinition &gbuf) {
      const auto gbufBinding = rootImpl()->getStorageGbufLocation(gbufIndex);
      QString after = (gbuf.readonly? readonlyImgGlslPattern: storageImgGlslPattern)
          .arg(gbufBinding.binding)
          .arg(gbufBinding.set)
          .arg(gbuf.format)
          .arg(gbufIndex);

      replacements[gbuf.i0] = ReplacementDefinition { .after = after, .b = gbuf.i0, .sz = gbuf.sz };
    });
  modelObj->forallSampledGbuf([&replacements, this] (uint32_t gbufIndex, const KawaiiShader::GBufDefinition &gbuf) {
      const auto gbufBinding = rootImpl()->getSampledGbufLocation(gbufIndex);
      QString after = sampledImgGlslPattern
          .arg(gbufBinding.binding)
          .arg(gbufBinding.set)
          .arg(gbufIndex);

      replacements[gbuf.i0] = ReplacementDefinition { .after = after, .b = gbuf.i0, .sz = gbuf.sz };
    });
  for(auto i = replacements.rbegin(); i != replacements.rend(); ++i)
    {
      glsl.replace(i->second.b,i->second.sz, i->second.after);
      int delta = i->second.after.size() - (i->second.sz);
      for(int &el: blockEnds)
        if(el > i->second.b)
          el += delta;
    }
  std::map<int, QString> injections;
  auto injectTextureHandles = [this, &injections](int blockEnd, const QHash<QString, QString> &textures, uint32_t blockBinding) {
      if(textures.isEmpty())
        return;

      QString global;

      const QString globTerm = QStringLiteral("\nlayout(binding = %3, set = %4) uniform %2 %1;\n");
      for(auto i = textures.begin(); i != textures.end(); ++i)
        {
          auto components = i->split(' ');
          auto texLocation = rootImpl()->getTextureLocation(components.last().toStdString(), blockBinding);
          global += globTerm.arg(components.last(), components.first(), QString::number(texLocation.binding), QString::number(texLocation.set));
        }
      injections.insert({blockEnd+1, global});
    };

  injectTextureHandles(blockEnds[0], modelObj->getCameraTextures(), KawaiiShader::getCameraUboLocation());
  injectTextureHandles(blockEnds[1], modelObj->getSurfaceTextures(), KawaiiShader::getSurfaceUboLocation());
  injectTextureHandles(blockEnds[2], modelObj->getMaterialTextures(), KawaiiShader::getMaterialUboLocation());
  injectTextureHandles(blockEnds[3], modelObj->getModelTextures(), KawaiiShader::getModelUboLocation());
  injectTextureHandles(-1, modelObj->getGlobalTextures(), KawaiiShader::getUboCount() + 1);

  for(auto i = injections.rbegin(); i != injections.rend(); ++i)
    glsl.insert(i->first, i->second);

  static QRegularExpression regBinding(QStringLiteral("binding = (\\d+)\\)"));

  auto i = regBinding.match(glsl);
  while(i.hasMatch())
    {
      const auto captured = i.captured(1);
      const auto descrSetIndex = rootImpl()->getDescrSetIndex(captured.toUInt());

      glsl.replace(i.capturedStart(), i.capturedLength(), QStringLiteral("binding = %1, set = %2)").arg(captured).arg(descrSetIndex));
      i = regBinding.match(glsl);
    }

  static QRegularExpression regVersion(QStringLiteral("#version\\s+\\d+\\s+core\\s*"));
  auto versionMatch = regVersion.match(glsl);
  if(!versionMatch.hasMatch())
    glsl = QStringLiteral("#version 450 core\n") + glsl;

  static QRegularExpression regGlobalUbo(QStringLiteral("GLOBAL_UBO_BINDING_(\\d+)"));
  i = regGlobalUbo.match(glsl);
  while(i.hasMatch())
    {
      const auto uboLocation = rootImpl()->getGlobBufLocation(i.captured(1).toUInt(), VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER);

      glsl.replace(i.capturedStart(), i.capturedLength(), QStringLiteral("%1, set = %2").arg(uboLocation.binding).arg(uboLocation.set) );
      i = regGlobalUbo.match(glsl);
    }

  static QRegularExpression regGlobalSsbo(QStringLiteral("GLOBAL_SSBO_BINDING_(\\d+)"));
  i = regGlobalSsbo.match(glsl);
  while(i.hasMatch())
    {
      const auto ssboLocation = rootImpl()->getGlobBufLocation(i.captured(1).toUInt(), VK_DESCRIPTOR_TYPE_STORAGE_BUFFER);

      glsl.replace(i.capturedStart(), i.capturedLength(), QStringLiteral("%1, set = %2").arg(ssboLocation.binding).arg(ssboLocation.set) );
      i = regGlobalSsbo.match(glsl);
    }

  glsl.replace("gl_InstanceID", "gl_InstanceIndex");
  glsl.replace("gl_VertexID", "gl_VertexIndex");

  return glsl;
}

KurisuRootImpl *KurisuShaderImpl::rootImpl() const
{
  return static_cast<KurisuRootImpl*>(getRoot());
}
