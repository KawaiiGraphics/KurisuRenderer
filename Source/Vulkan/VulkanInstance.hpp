#ifndef VULKANINSTANCE_HPP
#define VULKANINSTANCE_HPP

#include <vulkan/vulkan.h>
#include <QByteArrayList>
#include <QWindow>
#include <memory>

class QVulkanInstance;

class VulkanInstance
{
  QVulkanInstance *qtVulkanInstance;

  VulkanInstance(const VulkanInstance &) = delete;
  VulkanInstance& operator=(const VulkanInstance &) = delete;

public:
  VulkanInstance();
  virtual ~VulkanInstance();

  virtual VkSurfaceKHR getSurfaceForWindow(QWindow *) = 0;
  virtual VkInstance getVkInstance() = 0;
  virtual const std::vector<std::string> &getDeviceExtensions() = 0;

  QVulkanInstance& getQVulkanInstance();
};

class VulkanInstanceFactory
{
public:
  QByteArrayList layers;
  QByteArrayList extensions;
  bool debugEnabled;

  VulkanInstanceFactory();

  std::unique_ptr<VulkanInstance> create();
};

#endif // VULKANINSTANCE_HPP
