#include "KawaiiKurisuFactory.hpp"

#include "KurisuRootImpl.hpp"
#include "Surfaces/KurisuSurfaceImpl.hpp"
#include "KurisuGpuBufImpl.hpp"
#include "Geometry/KurisuMeshImpl.hpp"
#include "Renderpass/KurisuRenderpassImpl.hpp"
#include "Renderpass/KurisuSceneLayerImpl.hpp"
#include "Renderpass/KurisuGlPaintedLayerImpl.hpp"
#include "Renderpass/KurisuQPainterLayerImpl.hpp"
#include "Renderpass/KurisuShaderEffectImpl.hpp"
#include "Renderpass/KurisuTexLayerImpl.hpp"
#include "Shaders/KurisuShaderImpl.hpp"
#include "Shaders/KurisuProgramImpl.hpp"
#include <KawaiiRenderer/KawaiiBufBindingImpl.hpp>
#include "Geometry/KurisuMeshInstanceImpl.hpp"
#include <KawaiiRenderer/KawaiiMaterialImpl.hpp>
#include <KawaiiRenderer/Shaders/KawaiiSceneImpl.hpp>
#include <KawaiiRenderer/KawaiiCameraImpl.hpp>
#include <KawaiiRenderer/Textures/KawaiiCubemapImpl.hpp>
#include <KawaiiRenderer/Textures/KawaiiImgImpl.hpp>
#include <KawaiiRenderer/Textures/KawaiiDepthCubemapArrayImpl.hpp>
#include <KawaiiRenderer/Textures/KawaiiDepthTex2dArrayImpl.hpp>
#include "Textures/KurisuFramebufferImpl.hpp"
#include "Textures/KurisuEnvMapImpl.hpp"
#include "Textures/KurisuExternalQRhiRender.hpp"

#include <QCoreApplication>

KawaiiKurisuFactory *KawaiiKurisuFactory::instance = nullptr;

KawaiiKurisuFactory::KawaiiKurisuFactory()
{
  registerImpl<KawaiiRoot,               KurisuRootImpl>              ();
  registerImpl<KawaiiSurface,            KurisuSurfaceImpl>           ();
  registerImpl<KawaiiGpuBuf,             KurisuGpuBufImpl>            ();
  registerImpl<KawaiiBufBinding,         KawaiiBufBindingImpl>        ();
  registerImpl<KawaiiMesh3D,             KurisuMeshImpl>              ();
  registerImpl<KawaiiRenderpass,         KurisuRenderpassImpl>        ();
  registerImpl<KawaiiSceneLayer,         KurisuSceneLayerImpl>        ();
  registerImpl<KawaiiGlPaintedLayer,     KurisuGlPaintedLayerImpl>    ();
  registerImpl<KawaiiQPainterLayer,      KurisuQPainterLayerImpl>     ();
  registerImpl<KawaiiShaderEffect,       KurisuShaderEffectImpl>      ();
  registerImpl<KawaiiTexLayer,           KurisuTexLayerImpl>          ();
  registerImpl<KawaiiProgram,            KurisuProgramImpl>           ();
  registerImpl<KawaiiShader,             KurisuShaderImpl>            ();
  registerImpl<KawaiiMeshInstance,       KurisuMeshInstanceImpl>      ();
  registerImpl<KawaiiMaterial,           KawaiiMaterialImpl>          ();
  registerImpl<KawaiiScene,              KawaiiSceneImpl>             ();
  registerImpl<KawaiiCamera,             KawaiiCameraImpl>            ();
  registerImpl<KawaiiCubemap,            KawaiiCubemapImpl>           ();
  registerImpl<KawaiiImage,              KawaiiImgImpl>               ();
  registerImpl<KawaiiDepthCubemapArray,  KawaiiDepthCubemapArrayImpl> ();
  registerImpl<KawaiiDepthTex2dArray,    KawaiiDepthTex2dArrayImpl>   ();
  registerImpl<KawaiiEnvMap,             KurisuEnvMapImpl>            ();
  registerImpl<KawaiiExternalQRhiRender, KurisuExternalQRhiRender>    ();
  registerImpl<KawaiiFramebuffer,        KurisuFramebufferImpl>       ();
}

KawaiiImplFactory *KawaiiKurisuFactory::getInstance()
{
  if(!instance)
    createInstance();
  return instance;
}

void KawaiiKurisuFactory::deleteInstance()
{
  if(instance)
    {
      instance->deleteLater();
      instance = nullptr;
    }
}

void KawaiiKurisuFactory::createInstance()
{
  instance = new KawaiiKurisuFactory;
  qAddPostRoutine(&KawaiiKurisuFactory::deleteInstance);
}
