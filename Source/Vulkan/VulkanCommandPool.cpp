#include "VulkanCommandPool.hpp"
#include <QtConcurrent>

VulkanCommandPool::VulkanCommandPool(VkDevice dev, uint32_t queueFamily):
  dev(dev),

  pool([dev, queueFamily] {
    VkCommandPool cmdPool;
    const VkCommandPoolCreateInfo poolInfo {
      .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
      .flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT,
      .queueFamilyIndex = queueFamily
    };

    if(vkCreateCommandPool(dev, &poolInfo, nullptr, &cmdPool) != VK_SUCCESS)
      throw std::runtime_error("Can not create transfer command pool");
    return cmdPool;
  }()),

  cmdBuf(([this] {
    const VkCommandBufferAllocateInfo allocInfo = {
      .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
      .commandPool = pool,
      .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
      .commandBufferCount = 1
    };

    VkCommandBuffer result;
    vkAllocateCommandBuffers(this->dev, &allocInfo, &result);
    return result;
  })()),

  fence(([this] {
    static const VkFenceCreateInfo createInfo {
      .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO
    };
    VkFence result;
    vkCreateFence(this->dev, &createInfo, nullptr, &result);
    return result;
  })()),

  mutex(std::make_unique<QRecursiveMutex>()),
  commandsCount(0)
{
}

VulkanCommandPool::~VulkanCommandPool()
{
  if(asyncSubmit.isRunning())
    asyncSubmit.waitForFinished();

  if(pool)
    {
      vkDeviceWaitIdle(dev);
      vkDestroyCommandPool(dev, pool, nullptr);
      vkDestroyFence(dev, fence, nullptr);
    }
}

VulkanCommandPool::VulkanCommandPool(VulkanCommandPool &&orig):
  dev(orig.dev),
  pool(orig.pool),
  cmdBuf(orig.cmdBuf),
  mutex(std::move(orig.mutex))
{
  if(orig.asyncSubmit.isRunning())
    orig.asyncSubmit.waitForFinished();
  orig.pool = VK_NULL_HANDLE;
}

void VulkanCommandPool::writeCommands(const std::vector<std::function<void (VkCommandBuffer)> > &commands)
{
  QMutexLocker l(mutex.get());

  if(asyncSubmit.isRunning())
    asyncSubmit.waitForFinished();

  if(commandsCount == 0 && !commands.empty())
    {
      const VkCommandBufferBeginInfo beginInfo {
        .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO
      };
      vkBeginCommandBuffer(cmdBuf, &beginInfo);
    }

  for(const auto &i: commands)
    i(cmdBuf);
  commandsCount += commands.size();
}

bool VulkanCommandPool::submit(VkQueue queue)
{
  const VkSubmitInfo submitInfo = {
    .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
    .commandBufferCount = 1,
    .pCommandBuffers = &cmdBuf
  };

  QMutexLocker l(mutex.get());

  if(asyncSubmit.isRunning())
    asyncSubmit.waitForFinished();

  if(commandsCount)
    {
      vkEndCommandBuffer(cmdBuf);
      vkResetFences(dev, 1, &fence);
      vkQueueSubmit(queue, 1, &submitInfo, fence);

      vkWaitForFences(dev, 1, &fence, VK_TRUE, std::numeric_limits<uint64_t>::max());
      vkResetCommandBuffer(cmdBuf, 0);

      for(const auto &i: onSubmit)
        i();

      commandsCount = 0;
      return true;
    } else
    return false;
}

bool VulkanCommandPool::submitAsync(VkQueue queue, VkSemaphore finished)
{
  bool hasSignalSem = (finished!=VK_NULL_HANDLE);

  const VkSubmitInfo submitInfo = {
    .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
    .commandBufferCount = 1,
    .pCommandBuffers = &cmdBuf,
    .signalSemaphoreCount = static_cast<uint32_t>(hasSignalSem? 1: 0),
    .pSignalSemaphores = hasSignalSem? &finished: nullptr
  };

  QMutexLocker l(mutex.get());

  if(asyncSubmit.isRunning())
    asyncSubmit.waitForFinished();

  if(commandsCount)
    {
      vkEndCommandBuffer(cmdBuf);
      vkResetFences(dev, 1, &fence);
      vkQueueSubmit(queue, 1, &submitInfo, fence);

      asyncSubmit = QtConcurrent::run([this] {
          vkWaitForFences(dev, 1, &fence, VK_TRUE, std::numeric_limits<uint64_t>::max());
          vkResetCommandBuffer(cmdBuf, 0);

          for(const auto &i: onSubmit)
            i();

          commandsCount = 0;
        });
      return true;
    } else
    return false;
}

void VulkanCommandPool::waitAsyncTask() const
{
  QMutexLocker l(mutex.get());

  if(asyncSubmit.isRunning())
    QFuture<void>(asyncSubmit).waitForFinished();
}

VkFence VulkanCommandPool::getFence() const
{
  waitAsyncTask();
  vkResetFences(dev, 1, &fence);
  return fence;
}

VulkanCommandPool::OnSubmitElement VulkanCommandPool::addOnSubmit(const std::function<void ()> &func)
{
  return onSubmit.insert(onSubmit.end(), func);
}

void VulkanCommandPool::removeOnSubmit(const OnSubmitElement &el)
{
  onSubmit.erase(el);
}
