#include "KurisuMeshInstanceImpl.hpp"
#include "KurisuMeshImpl.hpp"
#include "KurisuRootImpl.hpp"

KurisuMeshInstanceImpl::KurisuMeshInstanceImpl(KawaiiMeshInstance *model):
  KawaiiMeshInstanceImpl(model),
  root(nullptr)
{
  if(!model)
    deleteLater();
  else
    {
      drawIndexedIndirectCommand = {
        .indexCount = static_cast<uint32_t>(getIndexCount()),
        .instanceCount = static_cast<uint32_t>(model->getInstanceCount()),
        .firstIndex = 0,
        .vertexOffset = 0,
        .firstInstance = 0
      };

      connect(model, &KawaiiMeshInstance::instanceCountChanged, this, &KurisuMeshInstanceImpl::setInstanceCount);
    }
}

KurisuMeshInstanceImpl::~KurisuMeshInstanceImpl()
{
  if(root)
    root->waitForSurfacesRenderFinished();
  indirectDrawBuf.clear();
}

void KurisuMeshInstanceImpl::draw()
{
  if(drawIndexedIndirectCommand.indexCount != getIndexCount())
    {
      drawIndexedIndirectCommand.indexCount = getIndexCount();
      static const constexpr size_t offset = offsetof(VkDrawIndexedIndirectCommand, indexCount);
      for(auto &i: indirectDrawBuf)
        i.updateSubData(sizeof(uint32_t), offset);
    }

  if(drawIndexedIndirectCommand.indexCount == 0) return;

  bindUbo();

  static_cast<KurisuMeshImpl*>(getMesh())->drawIndirect(indirectDrawBuf);
}

void KurisuMeshInstanceImpl::setInstanceCount(uint32_t instanceCount)
{
  static const constexpr size_t offset = offsetof(VkDrawIndexedIndirectCommand, instanceCount);
  drawIndexedIndirectCommand.instanceCount = instanceCount;
  for(auto &i: indirectDrawBuf)
    i.updateSubData(sizeof(uint32_t), offset);
}

void KurisuMeshInstanceImpl::initConnections()
{
  auto r = KawaiiRoot::getRoot<KawaiiRoot>(getData());
  auto root_impl = static_cast<KurisuRootImpl*>(r->getRendererImpl());
  if(root_impl != root)
    {
      indirectDrawBuf.clear();
      indirectDrawBuf.reserve(root_impl->deviceCount());
      root_impl->forallDevices([this] (VulkanDevice &dev) {
        indirectDrawBuf.emplace_back(dev, VK_BUFFER_USAGE_INDIRECT_BUFFER_BIT);
        indirectDrawBuf.back().setData(&drawIndexedIndirectCommand, sizeof(VkDrawIndexedIndirectCommand));
      });
      root = root_impl;
    }
}
