#include "KurisuGpuBufImpl.hpp"
#include "Textures/KurisuTextureHandle.hpp"
#include <KawaiiRenderer/Textures/KawaiiTextureImpl.hpp>
#include <Kawaii3D/KawaiiConfig.hpp>
#include <cstring>

KurisuGpuBufImpl::KurisuGpuBufImpl(KawaiiGpuBuf *model):
  KawaiiGpuBufImpl(model)
{
  connect(model, &KawaiiGpuBuf::textureBinded, this, &KurisuGpuBufImpl::bindTexture);
  connect(this, &KawaiiGpuBufImpl::handleChanged, this, &KurisuGpuBufImpl::rebindTextures);
}

KurisuGpuBufImpl::~KurisuGpuBufImpl()
{
}

void KurisuGpuBufImpl::bindTexture(const QString &fieldName, KawaiiTexture *tex)
{
  const std::string fieldNameStd = fieldName.toStdString();
  auto el = onBindedTexHandleChanged.find(fieldNameStd);
  if(el == onBindedTexHandleChanged.end())
    el = onBindedTexHandleChanged.insert({fieldNameStd, {}}).first;
  else
    disconnect(el->second);

  if(tex)
    {
      auto texImpl = static_cast<KawaiiTextureImpl*>(tex->getRendererImpl());
      auto bindTexHandle = [this, fieldNameStd, texImpl] {
          handle()->bindTexture(fieldNameStd, static_cast<KurisuTextureHandle*>(texImpl->getHandle()));
        };
      el->second = connect(texImpl, &KawaiiTextureImpl::handleChanged, this, bindTexHandle);
      bindTexHandle();
    } else
    handle()->unbindTexture(fieldNameStd);
}

void KurisuGpuBufImpl::initConnections()
{
  rebindTextures();
}

void KurisuGpuBufImpl::rebindTextures()
{
  handle()->unbindAllTextures();
  getModel()->forallTextureBindings([this] (const QString &bindingName, KawaiiTexture *tex) {
      bindTexture(bindingName, tex);
    });
}

KurisuBufferHandle *KurisuGpuBufImpl::handle()
{
  return static_cast<KurisuBufferHandle*>(getHandle());
}
