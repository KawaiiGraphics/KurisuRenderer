#include "VulkanInstance.hpp"
#include <QGuiApplication>

VulkanInstanceFactory::VulkanInstanceFactory():
  debugEnabled(false)
{
}

#if defined(Q_OS_LINUX) && !defined(Q_OS_ANDROID)
#include <QWindow>
#include <Kawaii3D/KawaiiConfig.hpp>
#include <sib_utils/Strings.hpp>
#include <qpa/qplatformwindow.h>
#include <qpa/qplatformnativeinterface.h>
#include <wayland-client.h>
#include <vulkan/vulkan_wayland.h>
#include <cstring>
#include <QDebug>

namespace {
  std::vector<const char*> get_cstrs(const QByteArrayList &strs)
  {
    std::vector<const char*> result;
    result.reserve(strs.size());
    for(const auto &i: strs)
      result.push_back(i.data());
    return result;
  }

  class VulkanInstanceWayland: public VulkanInstance
  {
    QPlatformNativeInterface *native;
    wl_display *display;
    VkInstance vkInst;
    PFN_vkCreateDebugUtilsMessengerEXT createDebugUtilsMessenger;
    PFN_vkDestroyDebugUtilsMessengerEXT destroyDebugUtilsMessenger;
    VkDebugUtilsMessengerEXT debugMessenger;
    struct CreatedSfc {
      VkSurfaceKHR vkSfc;
      QMetaObject::Connection onPropertyChanged;
      QMetaObject::Connection onWndDestroyed;
    };

    std::unordered_map<QWindow*, CreatedSfc> createdSfc;

    void removeWindowSurface(QWindow *wnd)
    {
      auto el = createdSfc.find(wnd);
      if(el != createdSfc.end())
        {
          QObject::disconnect(el->second.onWndDestroyed);
          QObject::disconnect(el->second.onPropertyChanged);
          vkDestroySurfaceKHR(vkInst, el->second.vkSfc, nullptr);
          createdSfc.erase(el);
        }
    }

    // VulkanInstance interface
  public:
    VulkanInstanceWayland(QPlatformNativeInterface *native):
      native(native),
      display(static_cast<wl_display*>(native->nativeResourceForWindow("display", nullptr))),
      vkInst(VK_NULL_HANDLE),
      createDebugUtilsMessenger(nullptr),
      destroyDebugUtilsMessenger(nullptr),
      debugMessenger(VK_NULL_HANDLE)
    { }

    ~VulkanInstanceWayland()
    {
      if(debugMessenger)
        destroyDebugUtilsMessenger(vkInst, debugMessenger, nullptr);

      for(const auto &i: createdSfc)
        {
          QObject::disconnect(i.second.onWndDestroyed);
          QObject::disconnect(i.second.onPropertyChanged);
          vkDestroySurfaceKHR(vkInst, i.second.vkSfc, nullptr);
        }

      if(vkInst)
        vkDestroyInstance(vkInst, nullptr);
    }

    static VkBool32 debugCallback(
        VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
        VkDebugUtilsMessageTypeFlagsEXT messageType,
        const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
        void* pUserData)
    {
      static_cast<void>(messageType);
      static_cast<void>(pUserData);
      using namespace sib_utils::strings;

      if (messageSeverity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT)
        qCritical().noquote() << getQLatin1Str("KurisuRenderer: vulkan error: ") << QLatin1String(pCallbackData->pMessage);
      else if (messageSeverity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT)
        qWarning().noquote() << getQLatin1Str("KurisuRenderer: vulkan warning: ") << QLatin1String(pCallbackData->pMessage);
      else if(KawaiiConfig::getInstance().getDebugLevel() > 2)
        qDebug().noquote() << getQLatin1Str("KurisuRenderer: vulkan tip: ") << QLatin1String(pCallbackData->pMessage);

      return VK_FALSE;
    }

    bool create(const QByteArrayList &layers, const QByteArrayList &extensions)
    {
      if(!display) return false;

      const std::string appName = QCoreApplication::applicationName().toStdString();

      const VkApplicationInfo appInfo = {
        .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
        .pApplicationName = appName.c_str(),
        .applicationVersion = VK_MAKE_VERSION(1, 0, 0),
        .pEngineName = "Kawaii3D:Kurisu",
        .engineVersion = VK_MAKE_VERSION(0, 0, 9),
        .apiVersion = VK_API_VERSION_1_0
      };

      auto extensions_cstr = get_cstrs(extensions);
      extensions_cstr.push_back("VK_KHR_surface");
      extensions_cstr.push_back("VK_KHR_wayland_surface");

      uint32_t layerCount;
      vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

      std::vector<VkLayerProperties> availableLayers(layerCount);
      vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

      std::vector<const char*> layers_cstr;
      layers_cstr.reserve(layers.size());
      for(const auto &i: layers)
        for(const auto &layer: availableLayers)
          if(i == QByteArray(layer.layerName))
            layers_cstr.push_back(layer.layerName);

      const VkInstanceCreateInfo createInfo = {
        .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
        .pApplicationInfo = &appInfo,
        .enabledLayerCount = static_cast<uint32_t>(layers_cstr.size()),
        .ppEnabledLayerNames = !layers_cstr.empty()? layers_cstr.data(): nullptr,
        .enabledExtensionCount = static_cast<uint32_t>(extensions_cstr.size()),
        .ppEnabledExtensionNames = !extensions_cstr.empty()? extensions_cstr.data(): nullptr
      };
      VkResult res = vkCreateInstance(&createInfo, nullptr, &vkInst);
      if(res != VK_SUCCESS)
        return false;


      bool validationLayerEnabled = false;
      for(const char *layer_cstr: layers_cstr)
        if(std::strcmp("VK_LAYER_KHRONOS_validation", layer_cstr) == 0
           || std::strcmp("VK_LAYER_LUNARG_standard_validation", layer_cstr) == 0)
          {
            validationLayerEnabled = true;
            break;
          }

      if(validationLayerEnabled)
        {
          createDebugUtilsMessenger = reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(vkInst, "vkCreateDebugUtilsMessengerEXT"));
          destroyDebugUtilsMessenger = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(vkInst, "vkDestroyDebugUtilsMessengerEXT"));
          if(createDebugUtilsMessenger && destroyDebugUtilsMessenger)
            {
              const VkDebugUtilsMessengerCreateInfoEXT createInfo = {
                .sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
                .messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT,
                .messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT,
                .pfnUserCallback = debugCallback,
              };
              createDebugUtilsMessenger(vkInst, &createInfo, nullptr, &debugMessenger);
            }
        }

      return true;
    }

    VkSurfaceKHR getSurfaceForWindow(QWindow *wnd) override final
    {
      auto el = createdSfc.find(wnd);
      if(el != createdSfc.end())
        return el->second.vkSfc;

      wl_surface *surface = static_cast<wl_surface*>(native->nativeResourceForWindow("surface", wnd));
      if(!surface)
        return VK_NULL_HANDLE;

      const VkWaylandSurfaceCreateInfoKHR createInfo = {
        .sType = VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR,
        .display = display,
        .surface = surface
      };

      VkSurfaceKHR vkSfc;
      if(vkCreateWaylandSurfaceKHR(vkInst, &createInfo, nullptr, &vkSfc) != VK_SUCCESS)
        return VK_NULL_HANDLE;

      auto onPropChanged = QObject::connect(native, &QPlatformNativeInterface::windowPropertyChanged, [this, wnd] (QPlatformWindow *platformWindow, const QString &propertyName) {
          if(wnd->handle() == platformWindow && propertyName == QStringLiteral("surface"))
            removeWindowSurface(wnd);
        });

      auto onWndDestroyed = QObject::connect(wnd, &QWindow::destroyed, [this, wnd] (QObject *) {
          removeWindowSurface(wnd);
        });

      createdSfc.insert({wnd, CreatedSfc {
                           .vkSfc = vkSfc,
                           .onPropertyChanged = onPropChanged,
                           .onWndDestroyed = onWndDestroyed
                         } });

      return vkSfc;
    }

    VkInstance getVkInstance() override final
    {
      return vkInst;
    }

    const std::vector<std::string> &getDeviceExtensions() override final
    {
      static const std::vector<std::string> extensions;
      return extensions;
    }
  };
}
#endif

#include <QVulkanInstance>

namespace {
  class VulkanInstanceQt: public VulkanInstance
  {
    QVulkanInstance inst;
    std::vector<std::string> deviceExtensions;

    // VulkanInstance interface
  public:
    ~VulkanInstanceQt()
    {
      inst.destroy();
    }

    bool create(const QByteArrayList &layers, const QByteArrayList &extensions)
    {
      inst.setApiVersion(QVersionNumber(1, 0));
      inst.setLayers(layers);
      inst.setExtensions(extensions);
      return inst.create();
    }

    VkSurfaceKHR getSurfaceForWindow(QWindow *wnd) override final
    {
      if(!wnd->vulkanInstance())
        wnd->setVulkanInstance(&inst);

      Q_ASSERT(wnd->vulkanInstance() == &inst);

      return inst.surfaceForWindow(wnd);
    }

    VkInstance getVkInstance() override final
    {
      return inst.vkInstance();
    }

    const std::vector<std::string> &getDeviceExtensions() override
    {
      return deviceExtensions;
    }
  };
}

std::unique_ptr<VulkanInstance> VulkanInstanceFactory::create()
{
# ifdef Q_OS_UNIX
  if(!qgetenv("WAYLAND_DISPLAY").isEmpty())
    {
      auto *native = QGuiApplication::platformNativeInterface();
      auto instWayland = std::make_unique<VulkanInstanceWayland>(native);
      if(instWayland->create(layers, extensions))
        return instWayland;
    }
# endif

  auto instQt = std::make_unique<VulkanInstanceQt>();
  if(!instQt->create(layers, extensions))
    instQt.reset();
  return instQt;
}

VulkanInstance::VulkanInstance():
  qtVulkanInstance(nullptr)
{ }

VulkanInstance::~VulkanInstance()
{
  if(qtVulkanInstance)
    delete qtVulkanInstance;
}

QVulkanInstance &VulkanInstance::getQVulkanInstance()
{
  if(!qtVulkanInstance)
    qtVulkanInstance = new QVulkanInstance;

  if(!qtVulkanInstance->isValid())
    {
      qtVulkanInstance->setVkInstance(getVkInstance());
      qtVulkanInstance->create();
    }
  return *qtVulkanInstance;
}
