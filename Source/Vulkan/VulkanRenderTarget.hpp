#ifndef KURISURENDERTARGETIMPL_HPP
#define KURISURENDERTARGETIMPL_HPP

#include "../Vulkan/VulkanDevice.hpp"
#include <vector>

class VulkanRenderTarget
{
  VulkanRenderTarget(const VulkanRenderTarget &orig) = delete;
  VulkanRenderTarget& operator=(const VulkanRenderTarget &orig) = delete;

public:
  VulkanRenderTarget(VulkanDevice *dev);
  VulkanRenderTarget(VulkanRenderTarget &&orig);
  VulkanRenderTarget& operator=(VulkanRenderTarget &&orig);
  ~VulkanRenderTarget();

  void setImgs(const std::vector<VkImageView> &newImgs);
  const std::vector<VkImageView> &getImgs() const;

  VkFramebuffer getFbo();

  VkRenderPass getRenderpass() const;
  void setRenderpass(VkRenderPass newRenderpass);

  uint32_t getWidth() const;
  void setWidth(uint32_t newWidth);

  uint32_t getHeight() const;
  void setHeight(uint32_t newHeight);

  void replaceAttachement(VkImageView oldImg, VkImageView newImg);



  // IMPLEMENT
private:
  VulkanDevice *dev;
  VkRenderPass renderpass;

  std::vector<VkImageView> imgs;
  VkFramebuffer fbo;

  uint32_t width;
  uint32_t height;

  bool fboDirty;

  void destroyFramebuffer();
};

#endif // KURISURENDERTARGETIMPL_HPP
