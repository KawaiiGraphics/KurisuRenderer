#ifndef VULKANPIPELINELAYOUT_HPP
#define VULKANPIPELINELAYOUT_HPP

#include <vulkan/vulkan.h>
#include <functional>
#include <vector>
#include <list>

class VulkanPipelineLayout
{
public:
  using OnInvalidateFuncs = std::list<std::function<void ()>>;

  VulkanPipelineLayout(VkDevice dev, std::vector<std::vector<VkDescriptorSetLayoutBinding>> &&initialDescriptorSetLayoutBindings);
  VulkanPipelineLayout(VkDevice dev);
  ~VulkanPipelineLayout();

  uint32_t addDescriptorBinding(size_t descriptorSetIndex, VkDescriptorType type, VkShaderStageFlags shaderStage);

  VkPipelineLayout getPipelineLayout();
  std::pair<VkDescriptorSetLayout, uint32_t> getDescriptorSetLayout(uint32_t binding);
  const std::vector<VkDescriptorSetLayout> &getDescriptorSetLayouts();
  std::unordered_map<VkDescriptorType, uint32_t> getDescriptorCount(size_t descriptorSetIndex);

  OnInvalidateFuncs::const_iterator addOnInvalidate(const std::function<void ()> &func);
  void removeOnInvalidate(const OnInvalidateFuncs::const_iterator &el);

  inline VkDevice device() const { return dev; }



private:
  VkDevice dev;

  VkPipelineLayout layout;

  std::vector<std::vector<VkDescriptorSetLayoutBinding>> setLayoutBindings;
  std::vector<VkDescriptorSetLayout> descriptorSetLayouts;
  std::vector<VkPushConstantRange> pushConstantRanges;
  VkPipelineLayoutCreateInfo layoutInfo;

  OnInvalidateFuncs onInvalidate;

  bool dirty;

  void validate();
  void markDirty();

  uint32_t getFreeBindingPoint() const;

  void insertDescriptorSetLayoutBinding(size_t descriptorSetIndex, const VkDescriptorSetLayoutBinding &val);
};

#endif // VULKANPIPELINELAYOUT_HPP
