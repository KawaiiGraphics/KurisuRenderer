#include "VulkanGBuf.hpp"
#include "RenderingCommands.hpp"
#include "SingleTimeCommands.hpp"
#include <QtConcurrent>

VulkanGBuf::VulkanGBuf(VulkanDevice *dev):
  dev(dev),
  img(VK_NULL_HANDLE),
  mem(VK_NULL_HANDLE),
  memOffset(0),
  imgView(VK_NULL_HANDLE),
  fmt(),
  imgAspect(),
  stagingMem(VK_NULL_HANDLE),
  linear(false),
  ownsMem(false)
{
}

VulkanGBuf::VulkanGBuf(VulkanGBuf &&orig):
  dev(orig.dev),
  img(orig.img),
  mem(orig.mem),
  memOffset(orig.memOffset),
  imgView(orig.imgView),
  fmt(orig.fmt),
  imgAspect(orig.imgAspect),
  stagingImgF(orig.stagingImgF),
  stagingImgU(orig.stagingImgU),
  stagingImgI(orig.stagingImgI),
  stagingDepth(orig.stagingDepth),
  stagingMem(orig.stagingMem),
  linear(orig.linear),
  ownsMem(orig.ownsMem)
{
  orig.img = VK_NULL_HANDLE;
  orig.stagingImgF = orig.stagingImgU = orig.stagingImgI = {};
  orig.stagingDepth = {};
  orig.mem = orig.stagingMem = VK_NULL_HANDLE;
  orig.imgView = VK_NULL_HANDLE;
  orig.ownsMem = false;
}

VulkanGBuf &VulkanGBuf::operator=(VulkanGBuf &&orig)
{
  destroy();

  dev = orig.dev;
  img = orig.img;
  mem = orig.mem;
  memOffset = orig.memOffset;
  imgView = orig.imgView;
  fmt = orig.fmt;
  imgAspect = orig.imgAspect;
  stagingImgF = orig.stagingImgF;
  stagingImgU = orig.stagingImgU;
  stagingImgI = orig.stagingImgI;
  stagingDepth = orig.stagingDepth;
  stagingMem = orig.stagingMem;
  linear = orig.linear;
  ownsMem = orig.ownsMem;

  orig.img = VK_NULL_HANDLE;
  orig.stagingImgF = orig.stagingImgU = orig.stagingImgI = {};
  orig.stagingDepth = {};
  orig.mem = orig.stagingMem = VK_NULL_HANDLE;
  orig.imgView = VK_NULL_HANDLE;
  orig.ownsMem = false;

  return *this;
}

VulkanGBuf::~VulkanGBuf()
{
  destroy();
}

namespace {
  VkImageAspectFlags getImgAspect(KawaiiRenderpass::InternalImgFormat fmt)
  {
    switch(fmt)
      {
      case KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm:
      case KawaiiRenderpass::InternalImgFormat::RGBA8_Snorm:
      case KawaiiRenderpass::InternalImgFormat::RGBA16_F:
      case KawaiiRenderpass::InternalImgFormat::RGBA16_UInt:
      case KawaiiRenderpass::InternalImgFormat::RGBA16_SInt:
      case KawaiiRenderpass::InternalImgFormat::RGBA32_F:
      case KawaiiRenderpass::InternalImgFormat::RGBA32_UInt:
      case KawaiiRenderpass::InternalImgFormat::RGBA32_SInt:

      case KawaiiRenderpass::InternalImgFormat::RG8_Unorm:
      case KawaiiRenderpass::InternalImgFormat::RG8_Snorm:
      case KawaiiRenderpass::InternalImgFormat::RG16_F:
      case KawaiiRenderpass::InternalImgFormat::RG16_UInt:
      case KawaiiRenderpass::InternalImgFormat::RG16_SInt:
      case KawaiiRenderpass::InternalImgFormat::RG32_F:
      case KawaiiRenderpass::InternalImgFormat::RG32_UInt:
      case KawaiiRenderpass::InternalImgFormat::RG32_SInt:

      case KawaiiRenderpass::InternalImgFormat::R8_Unorm:
      case KawaiiRenderpass::InternalImgFormat::R8_Snorm:
      case KawaiiRenderpass::InternalImgFormat::R16_F:
      case KawaiiRenderpass::InternalImgFormat::R16_UInt:
      case KawaiiRenderpass::InternalImgFormat::R16_SInt:
      case KawaiiRenderpass::InternalImgFormat::R32_F:
      case KawaiiRenderpass::InternalImgFormat::R32_UInt:
      case KawaiiRenderpass::InternalImgFormat::R32_SInt:
        return VK_IMAGE_ASPECT_COLOR_BIT;

      case KawaiiRenderpass::InternalImgFormat::Depth32:
      case KawaiiRenderpass::InternalImgFormat::Depth24:
      case KawaiiRenderpass::InternalImgFormat::Depth16:
        return VK_IMAGE_ASPECT_DEPTH_BIT;

      case KawaiiRenderpass::InternalImgFormat::Depth32Stencil8:
      case KawaiiRenderpass::InternalImgFormat::Depth24Stencil8:
      case KawaiiRenderpass::InternalImgFormat::Depth16Stencil8:
        return VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;

      case KawaiiRenderpass::InternalImgFormat::Stencil8:
        return VK_IMAGE_ASPECT_STENCIL_BIT;
      }
    Q_UNREACHABLE();
  }
}

void VulkanGBuf::init(const KawaiiRenderpass::GBuffer &gbuf, uint32_t w, uint32_t h, VkImageUsageFlags usage, bool exportMemoryHandle)
{
  const uint32_t queueFalilyInd = *dev->queueFamilies.graphicsFamily;

  const VkExternalMemoryImageCreateInfo externalMemInfo = {
    .sType = VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO,
    .handleTypes = VulkanDevice::imgMultiGpuMemoryHandleType
  };

  if(usage == 0)
    usage = VK_IMAGE_USAGE_SAMPLED_BIT;

  const VkImageCreateInfo createInfo = {
    .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
    .pNext = exportMemoryHandle ? &externalMemInfo : nullptr,
    .imageType = VK_IMAGE_TYPE_2D,
    .format = trFormat(gbuf.format),
    .extent = VkExtent3D { .width = w, .height = h, .depth = 1 },
    .mipLevels = 1,
    .arrayLayers = 1,
    .samples = VK_SAMPLE_COUNT_1_BIT,
    .tiling = linear? VK_IMAGE_TILING_LINEAR: VK_IMAGE_TILING_OPTIMAL,
    .usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT | usage,
    .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    .queueFamilyIndexCount = 1,
    .pQueueFamilyIndices = &queueFalilyInd,
    .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
  };

  VkResult result = vkCreateImage(dev->vkDev, &createInfo, nullptr, &img);
  if(Q_UNLIKELY(result != VK_SUCCESS))
    {
      img = VK_NULL_HANDLE;
      throw std::runtime_error("Can not create VkImage");
    }
  fmt = createInfo.format;
  imgAspect = getImgAspect(gbuf.format);
}

void VulkanGBuf::allocateMem()
{
  VkMemoryRequirements memRequirements;
  vkGetImageMemoryRequirements(dev->vkDev, img, &memRequirements);

  mem = dev->allocateMemory(memRequirements, 0, VulkanDevice::imgMultiGpuMemoryHandleType,
                            VkMemoryDedicatedAllocateInfo { .sType = VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO, .image = img }).first;
  ownsMem = true;
  bindMem();
}

void VulkanGBuf::importMem(VulkanDevice::Handle handle, VkExternalMemoryHandleTypeFlagBits type)
{
  VkMemoryRequirements memRequirements;
  vkGetImageMemoryRequirements(dev->vkDev, img, &memRequirements);

  mem = dev->importMemory(memRequirements, 0, handle, type, 0,
                          VkMemoryDedicatedAllocateInfo { .sType = VK_STRUCTURE_TYPE_MEMORY_DEDICATED_ALLOCATE_INFO, .image = img });
  ownsMem = true;
  bindMem();
}

void VulkanGBuf::importMem(const VulkanGBuf &orig)
{
  importMem(orig.getHandle(VulkanDevice::imgMultiGpuMemoryHandleType), VulkanDevice::imgMultiGpuMemoryHandleType);
}

void VulkanGBuf::pointToMem(VkDeviceMemory memory, VkDeviceSize offset)
{
  mem = memory;
  memOffset = offset;
  ownsMem = false;
  bindMem();
}

VulkanDevice::Handle VulkanGBuf::getHandle(VkExternalMemoryHandleTypeFlagBits type) const
{
  return *dev->getMemoryHandle(mem, type);
}

VkMemoryRequirements VulkanGBuf::getMemoryRequirements() const
{
  VkMemoryRequirements result = {
    .size = 0,
    .alignment = 1,
    .memoryTypeBits = 0
  };
  if(img)
    vkGetImageMemoryRequirements(dev->vkDev, img, &result);
  return result;
}

VkImage VulkanGBuf::getImg() const
{
  return img;
}

VkImageView VulkanGBuf::getImgView()
{
  if(!imgView && img)
    {
      const VkImageViewCreateInfo viewCreateInfo = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = img,
        .viewType = VK_IMAGE_VIEW_TYPE_2D,
        .format = fmt,
        .components = {
          .r = VK_COMPONENT_SWIZZLE_IDENTITY,
          .g = VK_COMPONENT_SWIZZLE_IDENTITY,
          .b = VK_COMPONENT_SWIZZLE_IDENTITY,
          .a = VK_COMPONENT_SWIZZLE_IDENTITY
        },
        .subresourceRange = getImageSubresourceRange(),
      };
      VkResult result = vkCreateImageView(dev->vkDev, &viewCreateInfo, nullptr, &imgView);
      if(Q_UNLIKELY(result != VK_SUCCESS))
        {
          imgView = VK_NULL_HANDLE;
          destroy();
          throw std::runtime_error("Can not create VkImageView");
        }
    }
  return imgView;
}

VkDeviceMemory VulkanGBuf::getMem() const
{
  return mem;
}

bool VulkanGBuf::isLinear() const
{
  return linear;
}

void VulkanGBuf::setLinear(bool newLinear)
{
  if(linear != newLinear)
    {
      linear = newLinear;
    }
}

VkImageSubresourceRange VulkanGBuf::getImageSubresourceRange() const
{
  return VkImageSubresourceRange {
      .aspectMask = imgAspect,
          .levelCount = 1,
          .baseArrayLayer = 0,
          .layerCount = 1
    };
}

VulkanDevice *VulkanGBuf::getDevice() const
{
  return dev;
}

std::vector<float> VulkanGBuf::readF(const QRect &reg)
{
  const auto readedReg = readRegion(reg);
  std::vector<float> result;
  std::visit([&result] (auto &&vec) {
      result = std::vector<float>(vec.cbegin(), vec.cend());
    }, readedReg);
  return result;
}

std::vector<int32_t> VulkanGBuf::readI(const QRect &reg)
{
  const auto readedReg = readRegion(reg);
  std::vector<int32_t> result;
  std::visit([&result] (auto &&vec) {
      result = std::vector<int32_t>(vec.cbegin(), vec.cend());
    }, readedReg);
  return result;
}

std::vector<uint32_t> VulkanGBuf::readU(const QRect &reg)
{
  const auto readedReg = readRegion(reg);
  std::vector<uint32_t> result;
  std::visit([&result] (auto &&vec) {
      result = std::vector<uint32_t>(vec.cbegin(), vec.cend());
    }, readedReg);
  return result;
}

VkFormat VulkanGBuf::trFormat(KawaiiRenderpass::InternalImgFormat fmt)
{
  switch(fmt)
    {
    case KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm:
      return VK_FORMAT_B8G8R8A8_UNORM;
    case KawaiiRenderpass::InternalImgFormat::RGBA8_Snorm:
      return VK_FORMAT_B8G8R8A8_SNORM;
    case KawaiiRenderpass::InternalImgFormat::RGBA32_F:
      return VK_FORMAT_R32G32B32A32_SFLOAT;
    case KawaiiRenderpass::InternalImgFormat::RGBA32_UInt:
      return VK_FORMAT_R32G32B32A32_UINT;
    case KawaiiRenderpass::InternalImgFormat::RGBA32_SInt:
      return VK_FORMAT_R32G32B32A32_SINT;

    case KawaiiRenderpass::InternalImgFormat::RG8_Unorm:
      return VK_FORMAT_R8G8_UNORM;
    case KawaiiRenderpass::InternalImgFormat::RG8_Snorm:
      return VK_FORMAT_R8G8_SNORM;
    case KawaiiRenderpass::InternalImgFormat::RG32_F:
      return VK_FORMAT_R32G32_SFLOAT;
    case KawaiiRenderpass::InternalImgFormat::RG32_UInt:
      return VK_FORMAT_R32G32_UINT;
    case KawaiiRenderpass::InternalImgFormat::RG32_SInt:
      return VK_FORMAT_R32G32_SINT;

    case KawaiiRenderpass::InternalImgFormat::R8_Unorm:
      return VK_FORMAT_R8_UNORM;
    case KawaiiRenderpass::InternalImgFormat::R8_Snorm:
      return VK_FORMAT_R8_SNORM;
    case KawaiiRenderpass::InternalImgFormat::R32_F:
      return VK_FORMAT_R32_SFLOAT;
    case KawaiiRenderpass::InternalImgFormat::R32_UInt:
      return VK_FORMAT_R32_UINT;
    case KawaiiRenderpass::InternalImgFormat::R32_SInt:
      return VK_FORMAT_R32_SINT;

    case KawaiiRenderpass::InternalImgFormat::RGBA16_F:
      return VK_FORMAT_R16G16B16A16_SFLOAT;
    case KawaiiRenderpass::InternalImgFormat::RGBA16_UInt:
      return VK_FORMAT_R16G16B16A16_UINT;
    case KawaiiRenderpass::InternalImgFormat::RGBA16_SInt:
      return VK_FORMAT_R16G16B16A16_SINT;

    case KawaiiRenderpass::InternalImgFormat::RG16_F:
      return VK_FORMAT_R16G16_SFLOAT;
    case KawaiiRenderpass::InternalImgFormat::RG16_UInt:
      return VK_FORMAT_R16G16_UINT;
    case KawaiiRenderpass::InternalImgFormat::RG16_SInt:
      return VK_FORMAT_R16G16_SINT;

    case KawaiiRenderpass::InternalImgFormat::R16_F:
      return VK_FORMAT_R16_SFLOAT;
    case KawaiiRenderpass::InternalImgFormat::R16_UInt:
      return VK_FORMAT_R16_UINT;
    case KawaiiRenderpass::InternalImgFormat::R16_SInt:
      return VK_FORMAT_R16_SINT;

    case KawaiiRenderpass::InternalImgFormat::Depth32:
      return VK_FORMAT_D32_SFLOAT;

    case KawaiiRenderpass::InternalImgFormat::Depth24:
    case KawaiiRenderpass::InternalImgFormat::Depth24Stencil8:
      return VK_FORMAT_D24_UNORM_S8_UINT;

    case KawaiiRenderpass::InternalImgFormat::Depth16:
      return VK_FORMAT_D16_UNORM;

    case KawaiiRenderpass::InternalImgFormat::Depth32Stencil8:
      return VK_FORMAT_D32_SFLOAT_S8_UINT;

    case KawaiiRenderpass::InternalImgFormat::Depth16Stencil8:
      return VK_FORMAT_D16_UNORM_S8_UINT;

    case KawaiiRenderpass::InternalImgFormat::Stencil8:
      return VK_FORMAT_S8_UINT;
    }
  Q_UNREACHABLE();
}

void VulkanGBuf::bindMem()
{
  VkResult result = vkBindImageMemory(dev->vkDev, img, mem, memOffset);
  if(Q_UNLIKELY(result != VK_SUCCESS))
    {
      destroy();
      throw std::runtime_error("Can not bind VkImage to VkDeviceMemory");
    }
}

void VulkanGBuf::destroy()
{
  if(img || mem || imgView || stagingMem)
    dev->waitIdle();

  if(imgView)
    vkDestroyImageView(dev->vkDev, imgView, nullptr);

  if(img)
    vkDestroyImage(dev->vkDev, img, nullptr);

  if(mem && ownsMem)
    vkFreeMemory(dev->vkDev, mem, nullptr);

  if(stagingImgF.img)
    vkDestroyImage(dev->vkDev, stagingImgF.img, nullptr);

  if(stagingImgU.img)
    vkDestroyImage(dev->vkDev, stagingImgU.img, nullptr);

  if(stagingImgI.img)
    vkDestroyImage(dev->vkDev, stagingImgI.img, nullptr);

  if(stagingDepth.buf)
    vkDestroyBuffer(dev->vkDev, stagingDepth.buf, nullptr);

  if(stagingMem)
    {
      vkUnmapMemory(dev->vkDev, stagingMem);
      vkFreeMemory(dev->vkDev, stagingMem, nullptr);
    }
}

void VulkanGBuf::recreateStagingMem()
{
  if(stagingMem)
    {
      vkUnmapMemory(dev->vkDev, stagingMem);
      dev->waitIdle();
      vkFreeMemory(dev->vkDev, stagingMem, nullptr);
    }

  VkMemoryRequirements req = {
    .size = 0,
    .alignment = 0,
    .memoryTypeBits = 0
  };
  stagingImgF.increaseMemReq(dev, req);
  stagingImgU.increaseMemReq(dev, req);
  stagingImgI.increaseMemReq(dev, req);
  stagingDepth.increaseMemReq(dev, req);

  stagingMem = dev->allocateMemory(req, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT | VK_MEMORY_PROPERTY_HOST_CACHED_BIT).first;
  stagingImgF.bindMemory(dev->vkDev, stagingMem);
  stagingImgU.bindMemory(dev->vkDev, stagingMem);
  stagingImgI.bindMemory(dev->vkDev, stagingMem);
  stagingDepth.bindMemory(dev->vkDev, stagingMem);
  void *ptr = nullptr;
  vkMapMemory(dev->vkDev, stagingMem, 0, VK_WHOLE_SIZE, 0, &ptr);
  stagingPtr = static_cast<std::byte*>(ptr);
}

std::variant<std::vector<float>, std::vector<uint32_t>, std::vector<int32_t>>
VulkanGBuf::readRegion(const QRect &reg)
{
  switch(imgAspect)
    {
    case VK_IMAGE_ASPECT_COLOR_BIT:
      return readColorRegion(reg);

    case VK_IMAGE_ASPECT_DEPTH_BIT:
    case VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT:
      return readDepthRegion(reg);

    default: Q_UNREACHABLE();
    }
}

std::variant<std::vector<float>, std::vector<uint32_t>, std::vector<int32_t>>
VulkanGBuf::readColorRegion(const QRect &reg)
{
  switch (fmt)
    {
    case VK_FORMAT_B8G8R8A8_UNORM:
    case VK_FORMAT_B8G8R8A8_SNORM:
    case VK_FORMAT_R32G32B32A32_SFLOAT:
    case VK_FORMAT_R8G8_UNORM:
    case VK_FORMAT_R8G8_SNORM:
    case VK_FORMAT_R32G32_SFLOAT:
    case VK_FORMAT_R8_UNORM:
    case VK_FORMAT_R8_SNORM:
    case VK_FORMAT_R32_SFLOAT:
    case VK_FORMAT_R16G16B16A16_SFLOAT:
    case VK_FORMAT_R16G16_SFLOAT:
    case VK_FORMAT_R16_SFLOAT:
      {
        if(VkExtent2D extent = { .width = static_cast<uint32_t>(reg.width()), .height = static_cast<uint32_t>(reg.height()) };
           stagingImgF.requireExtent(dev, extent, VK_FORMAT_R32G32B32A32_SFLOAT))
          recreateStagingMem();
        return stagingImgF.readImg<float>(dev, img, reg, stagingPtr);
      }

    case VK_FORMAT_R32G32B32A32_UINT:
    case VK_FORMAT_R32G32_UINT:
    case VK_FORMAT_R32_UINT:
    case VK_FORMAT_R16G16B16A16_UINT:
    case VK_FORMAT_R16G16_UINT:
    case VK_FORMAT_R16_UINT:
      {
        if(VkExtent2D extent = { .width = static_cast<uint32_t>(reg.width()), .height = static_cast<uint32_t>(reg.height()) };
           stagingImgU.requireExtent(dev, extent, VK_FORMAT_R32G32B32A32_UINT))
          recreateStagingMem();
        return stagingImgU.readImg<uint32_t>(dev, img, reg, stagingPtr);
      }

    case VK_FORMAT_R32G32B32A32_SINT:
    case VK_FORMAT_R32G32_SINT:
    case VK_FORMAT_R32_SINT:
    case VK_FORMAT_R16G16B16A16_SINT:
    case VK_FORMAT_R16G16_SINT:
    case VK_FORMAT_R16_SINT:
      {
        if(VkExtent2D extent = { .width = static_cast<uint32_t>(reg.width()), .height = static_cast<uint32_t>(reg.height()) };
           stagingImgI.requireExtent(dev, extent, VK_FORMAT_R32G32B32A32_SINT))
          recreateStagingMem();
        return stagingImgI.readImg<int32_t>(dev, img, reg, stagingPtr);
      }

    default:
      Q_UNREACHABLE();
      break;
    }
}

std::vector<float> VulkanGBuf::readDepthRegion(const QRect &reg)
{
  VkDeviceSize sz = reg.width()*reg.height();
  switch (fmt)
    {
    case VK_FORMAT_D32_SFLOAT:
    case VK_FORMAT_D32_SFLOAT_S8_UINT:
      sz *= sizeof(float);
      break;

    case VK_FORMAT_D16_UNORM:
    case VK_FORMAT_D16_UNORM_S8_UINT:
      sz *= sizeof(uint16_t);
      break;

    default: Q_UNREACHABLE();
    }

  if(stagingDepth.requireSize(dev, sz))
    recreateStagingMem();
  stagingDepth.readImg(dev, img, reg);
  switch (fmt)
    {
    case VK_FORMAT_D32_SFLOAT:
    case VK_FORMAT_D32_SFLOAT_S8_UINT:
      {
        float *p0 = reinterpret_cast<float*>(stagingPtr + stagingDepth.memOffset);
        return std::vector<float>(p0, p0+reg.width()*reg.height());
      }

    case VK_FORMAT_D16_UNORM:
    case VK_FORMAT_D16_UNORM_S8_UINT:
      {
        uint16_t *p0 = reinterpret_cast<uint16_t*>(stagingPtr + stagingDepth.memOffset);
        std::vector<float> f32(p0, p0+reg.width()*reg.height());
        QtConcurrent::map(f32, [](float &x) {
            static const constexpr double k = 1.0 / static_cast<double>(std::numeric_limits<uint16_t>::max());
            x *= k;
          }).waitForFinished();
        return f32;
      }
      break;

    default: Q_UNREACHABLE();
    }
}

VulkanGBuf::StagingImg::StagingImg():
  img (VK_NULL_HANDLE),
  extent { .width = 0, .height = 0 },
  memOffset(0)
{
}

bool VulkanGBuf::StagingImg::requireExtent(VulkanDevice *dev, const VkExtent2D &minExtent, VkFormat fmt)
{
  if(extent.width >= minExtent.width
     && extent.height >= minExtent.height)
    return false;

  if(img)
    {
      dev->waitIdle();
      vkDestroyImage(dev->vkDev, img, nullptr);
    }

  const uint32_t queueFalilyInd = *dev->queueFamilies.graphicsFamily;
  const VkImageCreateInfo createInfo = {
    .sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO,
    .imageType = VK_IMAGE_TYPE_2D,
    .format = fmt,
    .extent = VkExtent3D { .width = minExtent.width, .height = minExtent.height, .depth = 1 },
    .mipLevels = 1,
    .arrayLayers = 1,
    .samples = VK_SAMPLE_COUNT_1_BIT,
    .tiling = VK_IMAGE_TILING_LINEAR,
    .usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT,
    .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    .queueFamilyIndexCount = 1,
    .pQueueFamilyIndices = &queueFalilyInd,
    .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
  };

  VkResult result = vkCreateImage(dev->vkDev, &createInfo, nullptr, &img);
  if(Q_UNLIKELY(result != VK_SUCCESS))
    {
      img = VK_NULL_HANDLE;
      extent = {0,0};
      throw std::runtime_error("Can not create VkImage");
    }
  extent = minExtent;
  return true;
}

void VulkanGBuf::StagingImg::increaseMemReq(VulkanDevice *dev, VkMemoryRequirements &req)
{
  if(!img)
    return;
  memOffset = req.size;
  VkMemoryRequirements thisReq;
  vkGetImageMemoryRequirements(dev->vkDev, img, &thisReq);
  req.size += thisReq.size;
  if(req.alignment == 0)
    req.alignment = thisReq.alignment;
  Q_ASSERT(req.alignment == thisReq.alignment);
  if(req.memoryTypeBits == 0)
    req.memoryTypeBits = thisReq.memoryTypeBits;
  Q_ASSERT(req.memoryTypeBits == thisReq.memoryTypeBits);
}

void VulkanGBuf::StagingImg::bindMemory(VkDevice vkDev, VkDeviceMemory mem)
{
  if(img)
    {
      vkBindImageMemory(vkDev, img, mem, memOffset);

      VkImageSubresource subres = {
        .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
        .mipLevel = 0,
        .arrayLayer = 0
      };
      vkGetImageSubresourceLayout(vkDev, img, &subres, &subresLayout);
    }
}

void VulkanGBuf::StagingImg::prepareReadImg(VulkanDevice *dev, VkImage fromImg, const QRect &reg)
{
  const VkImageBlit blitReg = {
    .srcSubresource = {
      .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
      .mipLevel = 0,
      .baseArrayLayer = 0,
      .layerCount = 1
    },
    .srcOffsets = {
      { .x = reg.x(), .y = reg.y(), .z = 0 },
      { .x = reg.right()+1, .y = reg.bottom()+1, .z = 1 }
    },
    .dstSubresource = {
      .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
      .mipLevel = 0,
      .baseArrayLayer = 0,
      .layerCount = 1
    },
    .dstOffsets = {
      { .x = 0, .y = 0, .z = 0 },
      { .x = reg.width(), .y = reg.height(), .z = 1 }
    }
  };

  VkFence fence = VK_NULL_HANDLE;
  static const VkFenceCreateInfo fenceCI = {
    .sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO
  };
  vkCreateFence(dev->vkDev, &fenceCI, nullptr, &fence);

  RenderingCommands cmd(*dev, 1, 1, true);
  cmd.startRecording(VK_NULL_HANDLE, {}, {}, VK_SUBPASS_CONTENTS_INLINE, 0);
  cmd.blitImage(fromImg, VK_IMAGE_LAYOUT_UNDEFINED, img, VK_IMAGE_LAYOUT_UNDEFINED, {blitReg}, VK_FILTER_NEAREST);
  cmd.finishRecording();
  cmd.exec(0, 0, {}, {}, {}, fence);
  vkWaitForFences(dev->vkDev, 1, &fence, VK_TRUE, std::numeric_limits<uint64_t>::max());
  vkDestroyFence(dev->vkDev, fence, nullptr);
}

VulkanGBuf::StagingDepth::StagingDepth():
  buf(VK_NULL_HANDLE),
  sz(0),
  memOffset(0)
{
}

bool VulkanGBuf::StagingDepth::requireSize(VulkanDevice *dev, VkDeviceSize minSize)
{
  if(sz >= minSize)
    return false;

  if(buf)
    {
      dev->waitIdle();
      vkDestroyBuffer(dev->vkDev, buf, nullptr);
    }

  const uint32_t transferFamily = *dev->queueFamilies.transferFamily;
  const VkBufferCreateInfo createInfo = {
    .sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO,
    .size = minSize,
    .usage = VK_BUFFER_USAGE_TRANSFER_DST_BIT,
    .sharingMode = VK_SHARING_MODE_EXCLUSIVE,
    .queueFamilyIndexCount = 1,
    .pQueueFamilyIndices = &transferFamily
  };
  VkResult result = vkCreateBuffer(dev->vkDev, &createInfo, nullptr, &buf);
  if(Q_UNLIKELY(result != VK_SUCCESS))
    {
      buf = VK_NULL_HANDLE;
      sz = 0;
      throw std::runtime_error("Can not create VkBuffer");
    }
  sz = minSize;
  return true;
}

void VulkanGBuf::StagingDepth::increaseMemReq(VulkanDevice *dev, VkMemoryRequirements &req)
{
  if(!buf)
    return;
  memOffset = req.size;
  VkMemoryRequirements thisReq;
  vkGetBufferMemoryRequirements(dev->vkDev, buf, &thisReq);
  req.size += thisReq.size;
  if(req.alignment == 0)
    req.alignment = thisReq.alignment;
  Q_ASSERT(req.alignment == thisReq.alignment);
  if(req.memoryTypeBits == 0)
    req.memoryTypeBits = thisReq.memoryTypeBits;
  Q_ASSERT(req.memoryTypeBits == thisReq.memoryTypeBits);
}

void VulkanGBuf::StagingDepth::bindMemory(VkDevice vkDev, VkDeviceMemory mem)
{
  if(buf)
    vkBindBufferMemory(vkDev, buf, mem, memOffset);
}

void VulkanGBuf::StagingDepth::readImg(VulkanDevice *dev, VkImage fromImg, const QRect &reg)
{
  const VkOffset3D srcOffset = { .x = reg.x(), .y = reg.y(), .z = 0 };
  const VkExtent3D extent = {
    .width = static_cast<uint32_t>(reg.width()),
    .height = static_cast<uint32_t>(reg.height()),
    .depth = 1
  };
  static const constexpr VkImageSubresourceRange subresRange = {
    .aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT,
    .baseMipLevel = 0,
    .levelCount = 1,
    .baseArrayLayer = 0,
    .layerCount = 1
  };
  static const constexpr  VkImageSubresourceLayers subresLayers = {
    .aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT,
    .mipLevel = 0,
    .baseArrayLayer = 0,
    .layerCount = 1
  };
  SingleTimeCommands cmd(*dev);
  cmd.transitionImageLayout(fromImg, subresRange, VK_IMAGE_LAYOUT_GENERAL, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL);
  cmd.copyImageToBuffer(fromImg, buf, srcOffset, extent, 0, subresLayers);
  cmd.transitionImageLayout(fromImg, subresRange, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_IMAGE_LAYOUT_GENERAL);
  cmd.write().waitForFinished();
  dev->submitTransferCommands();
}
