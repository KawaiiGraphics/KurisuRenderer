#include "VulkanTexture.hpp"
#include "VulkanStagingBuffer.hpp"
#include "SingleTimeCommands.hpp"
#include <cstring>

bool VulkanTexture::isInExclusiveMode() const
{
  return queueFalilyIndices[0] == queueFalilyIndices[1];
}

VulkanTexture::VulkanTexture(VulkanDevice &dev, const UsageFlags &allowedUsage):
  queueFalilyIndices({ dev.queueFamilies.graphicsFamily.value(), dev.queueFamilies.transferFamily.value() }),

  imageInfo {
    VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO, // sType
    nullptr, // pNext
    0, // flags
    VK_IMAGE_TYPE_2D, // imageType
    VK_FORMAT_R8G8B8A8_UNORM, // format
    VkExtent3D {}, // extent
    1, // mipLevels
    1, // arrayLayers
    VK_SAMPLE_COUNT_1_BIT, // samples
    VK_IMAGE_TILING_OPTIMAL, // tiling
    VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, // usage
    isInExclusiveMode()? VK_SHARING_MODE_EXCLUSIVE: VK_SHARING_MODE_CONCURRENT, // sharingMode
    isInExclusiveMode()? 1: static_cast<uint32_t>(queueFalilyIndices.size()), // queueFamilyIndexCount
    queueFalilyIndices.data(), // pQueueFamilyIndices
    VK_IMAGE_LAYOUT_PREINITIALIZED, // initialLayout
    },

  minFilter(KawaiiTextureFilter::Linear),
  magFilter(KawaiiTextureFilter::Linear),

  uWrap(KawaiiTextureWrapMode::Repeat),
  vWrap(KawaiiTextureWrapMode::Repeat),
  wWrap(KawaiiTextureWrapMode::Repeat),

  compareOp(KawaiiDepthCompareOperation::None),

  dev(&dev),
  img(VK_NULL_HANDLE),
  mem(VK_NULL_HANDLE),
  imgView(VK_NULL_HANDLE),
  sampler(VK_NULL_HANDLE),

  memorySize(0),
  allocatedSize(0),
  memoryAlignment(0),

  descrInfo {
    sampler, imgView, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    },

  allowedUsage(allowedUsage),

  imgAspectFlagsFull(VK_IMAGE_ASPECT_COLOR_BIT),
  imgAspectFlagsUsage(VK_IMAGE_ASPECT_COLOR_BIT),

  exportMemory(0),

  dirty(true),
  dirty_sampler(true),
  waitsForTransfer(false),
  ownsMem(true)
{
  onTransferCmdSubmit = dev.getTransferCmdPool().addOnSubmit([this] { waitsForTransfer = false; });
}

VulkanTexture::~VulkanTexture()
{
  destroyImgView();
  destroyImg();
  if(sampler)
    vkDestroySampler(dev->vkDev, sampler, nullptr);

  dev->getTransferCmdPool().removeOnSubmit(onTransferCmdSubmit);
}

VulkanTexture::VulkanTexture(VulkanTexture &&orig):
  views2d(std::move(orig.views2d)),
  viewsArray2d(std::move(orig.viewsArray2d)),
  queueFalilyIndices(orig.queueFalilyIndices),
  imageInfo(orig.imageInfo),

  minFilter(orig.minFilter),
  magFilter(orig.magFilter),

  uWrap(orig.uWrap),
  vWrap(orig.vWrap),
  wWrap(orig.wWrap),

  dev(orig.dev),
  img(orig.img),
  mem(orig.mem),
  imgView(orig.imgView),
  sampler(orig.sampler),

  memorySize(orig.memorySize),
  allocatedSize(orig.allocatedSize),
  memoryAlignment(orig.memoryAlignment),

  descrInfo(orig.descrInfo),
  allowedUsage(orig.allowedUsage),

  imgAspectFlagsFull(orig.imgAspectFlagsFull),
  imgAspectFlagsUsage(orig.imgAspectFlagsUsage),

  exportMemory(orig.exportMemory),

  dirty(orig.dirty),
  dirty_sampler(orig.dirty_sampler),
  ownsMem(orig.ownsMem)
{
  for(auto *i: views2d)
    if(i->tex == &orig)
      i->tex = this;
  for(auto *i: viewsArray2d)
    if(i->tex == &orig)
      i->tex = this;
  orig.img = VK_NULL_HANDLE;
  orig.mem = VK_NULL_HANDLE;
  orig.imgView = VK_NULL_HANDLE;
  orig.sampler = VK_NULL_HANDLE;
  orig.descrInfo = {};

  onTransferCmdSubmit = dev->getTransferCmdPool().addOnSubmit([this] { waitsForTransfer = false; });
}

VulkanTexture &VulkanTexture::operator=(VulkanTexture &&orig)
{
  destroyImgView();
  destroyImg();
  if(sampler)
    vkDestroySampler(dev->vkDev, sampler, nullptr);

  dev->getTransferCmdPool().removeOnSubmit(onTransferCmdSubmit);

  views2d = std::move(orig.views2d);
  viewsArray2d = std::move(orig.viewsArray2d);
  queueFalilyIndices = orig.queueFalilyIndices;
  imageInfo = orig.imageInfo;

  minFilter = orig.minFilter;
  magFilter = orig.magFilter;

  uWrap = orig.uWrap;
  vWrap = orig.vWrap;
  wWrap = orig.wWrap;

  dev = orig.dev;
  img = orig.img;
  mem = orig.mem;
  imgView = orig.imgView;
  sampler = orig.sampler;

  memorySize = orig.memorySize;
  allocatedSize = orig.allocatedSize;
  memoryAlignment = orig.memoryAlignment;

  descrInfo = orig.descrInfo;
  allowedUsage = orig.allowedUsage;

  imgAspectFlagsFull = orig.imgAspectFlagsFull;
  imgAspectFlagsUsage = orig.imgAspectFlagsUsage;

  exportMemory = orig.exportMemory;

  dirty = orig.dirty;
  dirty_sampler = orig.dirty_sampler;
  ownsMem = orig.ownsMem;

  for(auto *i: views2d)
    if(i->tex == &orig)
      i->tex = this;
  for(auto *i: viewsArray2d)
    if(i->tex == &orig)
      i->tex = this;
  orig.img = VK_NULL_HANDLE;
  orig.mem = VK_NULL_HANDLE;
  orig.imgView = VK_NULL_HANDLE;
  orig.sampler = VK_NULL_HANDLE;
  orig.descrInfo = {};

  onTransferCmdSubmit = dev->getTransferCmdPool().addOnSubmit([this] { waitsForTransfer = false; });

  return *this;
}

void VulkanTexture::setFormat_8bpc(KawaiiTextureFormat fmt)
{
  if(fmt == getKawaiiFormat()) return;

  switch(fmt)
    {
    case KawaiiTextureFormat::Red:
      imageInfo.format = VK_FORMAT_R8_UNORM;
      imgAspectFlagsUsage = imgAspectFlagsFull = VK_IMAGE_ASPECT_COLOR_BIT;
      break;

    case KawaiiTextureFormat::RedGreen:
      imageInfo.format = VK_FORMAT_R8G8_UNORM;
      imgAspectFlagsUsage = imgAspectFlagsFull = VK_IMAGE_ASPECT_COLOR_BIT;
      break;

    case KawaiiTextureFormat::RedGreenBlue:
      imageInfo.format = VK_FORMAT_R8G8B8_UNORM;
      imgAspectFlagsUsage = imgAspectFlagsFull = VK_IMAGE_ASPECT_COLOR_BIT;
      break;

    case KawaiiTextureFormat::RedGreenBlueAlpha:
      imageInfo.format = VK_FORMAT_R8G8B8A8_UNORM;
      imgAspectFlagsUsage = imgAspectFlagsFull = VK_IMAGE_ASPECT_COLOR_BIT;
      break;

    case KawaiiTextureFormat::BlueGreenRed:
      imageInfo.format = VK_FORMAT_B8G8R8_UNORM;
      imgAspectFlagsUsage = imgAspectFlagsFull = VK_IMAGE_ASPECT_COLOR_BIT;
      break;

    case KawaiiTextureFormat::BlueGreenRedAlpha:
      imageInfo.format = VK_FORMAT_B8G8R8A8_UNORM;
      imgAspectFlagsUsage = imgAspectFlagsFull = VK_IMAGE_ASPECT_COLOR_BIT;
      break;

    case KawaiiTextureFormat::Depth:
      imageInfo.format = dev->depthFormat;
      imgAspectFlagsFull = imageInfo.format==VK_FORMAT_D24_UNORM_S8_UINT?
            VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT:
            VK_IMAGE_ASPECT_DEPTH_BIT;
      imgAspectFlagsUsage = VK_IMAGE_ASPECT_DEPTH_BIT;
      break;

    case KawaiiTextureFormat::DepthStencil:
      imageInfo.format = dev->depthStencilFormat;
      imgAspectFlagsFull = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
      imgAspectFlagsUsage = VK_IMAGE_ASPECT_DEPTH_BIT;
      break;
    }
  dirty = true;
}

void VulkanTexture::setFormat_32bpc(KawaiiTextureFormat fmt)
{
  if(fmt == getKawaiiFormat()) return;

  switch(fmt)
    {
    case KawaiiTextureFormat::Red:
      imageInfo.format = VK_FORMAT_R32_SFLOAT;
      imgAspectFlagsUsage = imgAspectFlagsFull = VK_IMAGE_ASPECT_COLOR_BIT;
      break;

    case KawaiiTextureFormat::RedGreen:
      imageInfo.format = VK_FORMAT_R32G32_SFLOAT;
      imgAspectFlagsUsage = imgAspectFlagsFull = VK_IMAGE_ASPECT_COLOR_BIT;
      break;

    case KawaiiTextureFormat::RedGreenBlue:
    case KawaiiTextureFormat::BlueGreenRed:
      imageInfo.format = VK_FORMAT_R32G32B32_SFLOAT;
      imgAspectFlagsUsage = imgAspectFlagsFull = VK_IMAGE_ASPECT_COLOR_BIT;
      break;

    case KawaiiTextureFormat::RedGreenBlueAlpha:
    case KawaiiTextureFormat::BlueGreenRedAlpha:
      imageInfo.format = VK_FORMAT_R32G32B32A32_SFLOAT;
      imgAspectFlagsUsage = imgAspectFlagsFull = VK_IMAGE_ASPECT_COLOR_BIT;
      break;

    case KawaiiTextureFormat::Depth:
      imageInfo.format = dev->depthFormat;
      imgAspectFlagsFull = imageInfo.format==VK_FORMAT_D24_UNORM_S8_UINT?
            VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT:
            VK_IMAGE_ASPECT_DEPTH_BIT;
      imgAspectFlagsUsage = VK_IMAGE_ASPECT_DEPTH_BIT;
      break;

    case KawaiiTextureFormat::DepthStencil:
      imageInfo.format = dev->depthStencilFormat;
      imgAspectFlagsFull = VK_IMAGE_ASPECT_DEPTH_BIT | VK_IMAGE_ASPECT_STENCIL_BIT;
      imgAspectFlagsUsage = VK_IMAGE_ASPECT_DEPTH_BIT;
      break;
    }

  dirty = true;
}

void VulkanTexture::setFormat(VkFormat fmt, VkImageAspectFlags aspectFlagsFull, VkImageAspectFlags aspectFlagsUsage)
{
  if(fmt != imageInfo.format)
    {
      imageInfo.format = fmt;
      dirty = true;
    }
  imgAspectFlagsFull = aspectFlagsFull;
  imgAspectFlagsUsage = aspectFlagsUsage;
}

void VulkanTexture::setExtent(uint32_t width)
{
  if(imageInfo.imageType != VkImageType::VK_IMAGE_TYPE_1D
     || imageInfo.extent.width != width
     || imageInfo.extent.height != 1
     || imageInfo.extent.depth != 1)
    {
      imageInfo.extent = VkExtent3D {width, 1, 1};
      imageInfo.imageType = VkImageType::VK_IMAGE_TYPE_1D;
      dirty = true;
    }
}

void VulkanTexture::setExtent(uint32_t width, uint32_t height)
{
  if(imageInfo.imageType != VkImageType::VK_IMAGE_TYPE_2D
     || imageInfo.extent.width != width
     || imageInfo.extent.height != height
     || imageInfo.extent.depth != 1)
    {
      imageInfo.extent = VkExtent3D {width, height, 1};
      imageInfo.imageType = VkImageType::VK_IMAGE_TYPE_2D;
      dirty = true;
    }
}

void VulkanTexture::setExtent(uint32_t width, uint32_t height, uint32_t depth)
{
  if(imageInfo.imageType != VkImageType::VK_IMAGE_TYPE_3D
     || imageInfo.extent.width != width
     || imageInfo.extent.height != height
     || imageInfo.extent.depth != depth)
    {
      imageInfo.extent = VkExtent3D {width, height, depth};
      imageInfo.imageType = VkImageType::VK_IMAGE_TYPE_3D;
      dirty = true;
    }
}

void VulkanTexture::setExtent(const VkExtent3D &extent, VkImageType imgType)
{
  if(imageInfo.extent.width != extent.width
     || imageInfo.extent.height != extent.height
     || imageInfo.extent.depth != extent.depth)
    {
      imageInfo.extent = extent;
      dirty = true;
    }

  if(imageInfo.imageType != imgType)
    {
      imageInfo.imageType = imgType;
      dirty = true;
    }
}

void VulkanTexture::setLayers(uint32_t mipMapLayers, uint32_t arrayLayers)
{
  if(imageInfo.mipLevels != mipMapLayers ||
     imageInfo.arrayLayers != arrayLayers)
    {
      imageInfo.mipLevels = mipMapLayers;
      imageInfo.arrayLayers = arrayLayers;
      dirty = true;
    }
}

uint32_t VulkanTexture::getMipMapLayers() const
{
  return imageInfo.mipLevels;
}

uint32_t VulkanTexture::getArrayLayers() const
{
  return imageInfo.arrayLayers;
}

void VulkanTexture::setCompareOp(KawaiiDepthCompareOperation op)
{
  if(compareOp != op)
    {
      compareOp = op;
      dirty_sampler = true;
    }
}

void VulkanTexture::setCubemapCompatible(bool compatible)
{
  if(compatible == isCubemapCompatible()) return;

  if(compatible)
    imageInfo.flags |= VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
  else
    imageInfo.flags &= ~VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;

  dirty = true;
}

bool VulkanTexture::isCubemapCompatible() const
{
  return (imageInfo.flags & VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT) ==
      VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
}

void VulkanTexture::setLinear(bool linear)
{
  if(isLinear() == linear) return;

  if(linear)
    imageInfo.tiling = VK_IMAGE_TILING_LINEAR;
  else
    imageInfo.tiling = VK_IMAGE_TILING_OPTIMAL;

  dirty = true;
}

bool VulkanTexture::isLinear() const
{
  return (imageInfo.tiling == VK_IMAGE_TILING_LINEAR);
}

void VulkanTexture::importImage(const VulkanTexture &texture)
{
  imageInfo = texture.imageInfo;
  imageInfo.sharingMode = isInExclusiveMode()? VK_SHARING_MODE_EXCLUSIVE: VK_SHARING_MODE_CONCURRENT;
  imageInfo.queueFamilyIndexCount = isInExclusiveMode()? 1: static_cast<uint32_t>(queueFalilyIndices.size());
  imageInfo.pQueueFamilyIndices =  queueFalilyIndices.data();

  minFilter = texture.minFilter;
  magFilter = texture.magFilter;
  uWrap = texture.uWrap;
  vWrap = texture.vWrap;
  wWrap = texture.wWrap;
  imgAspectFlagsFull = texture.imgAspectFlagsFull;
  imgAspectFlagsUsage = texture.imgAspectFlagsUsage;

  if(dev->vkDev == texture.dev->vkDev)
    importImage(*texture.getMemHandle(imgSameGpuMemoryHandleType), imgSameGpuMemoryHandleType);
  else
    importImage(*texture.getMemHandle(imgMultiGpuMemoryHandleType), imgMultiGpuMemoryHandleType);

  if(texture.allocatedSize != allocatedSize)
    qWarning("VulkanTexture::importImage: warning - allocatedSize mismatch (%lu vs %lu) (same device = %d; format = %d)", allocatedSize, texture.allocatedSize, (dev->vkDev == texture.dev->vkDev), imageInfo.format);

  if(texture.memoryAlignment != memoryAlignment)
    qWarning("VulkanTexture::importImage: warning - memoryAlignment mismatch (%lu vs %lu) (same device = %d; format = %d)", memoryAlignment, texture.memoryAlignment, (dev->vkDev == texture.dev->vkDev), imageInfo.format);
}

void VulkanTexture::pointToImage(const VulkanTexture &texture)
{
  if(dev->vkDev == texture.dev->vkDev)
    {
      imageInfo = texture.imageInfo;

      minFilter = texture.minFilter;
      magFilter = texture.magFilter;
      uWrap = texture.uWrap;
      vWrap = texture.vWrap;
      wWrap = texture.wWrap;
      imgAspectFlagsFull = texture.imgAspectFlagsFull;
      imgAspectFlagsUsage = texture.imgAspectFlagsUsage;

      allowedUsage |= texture.allowedUsage;

      VkMemoryRequirements memRequirements;
      imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
      imageInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT;
      finalLayout = VK_IMAGE_LAYOUT_GENERAL;
      prepareCreateImage(memRequirements, texture.exportMemory);
      mem = texture.mem;
      allocatedSize = texture.allocatedSize;
      memoryAlignment = texture.memoryAlignment;
      ownsMem = false;
      if(img && mem)
        vkBindImageMemory(dev->vkDev, img, mem, 0);
      else
        {
          dirty = false;
          return;
        }

      SingleTimeCommands commands(*dev);
      commands.transitionImageLayout(img, getImageSubresourceRange(), VK_IMAGE_LAYOUT_UNDEFINED, finalLayout);
      writeTransferCmdTask = commands.write();
      waitsForTransfer = true;
      dirty = false;
      exportMemory = texture.exportMemory;
      descrInfo.imageLayout = finalLayout;

      if(texture.allocatedSize != memRequirements.size)
        qWarning("VulkanTexture::pointToImage: warning - allocatedSize mismatch (%lu vs %lu) (format = %d)", memRequirements.size, texture.allocatedSize, imageInfo.format);

      if(texture.memoryAlignment != memRequirements.alignment)
        qWarning("VulkanTexture::pointToImage: warning - memoryAlignment mismatch (%lu vs %lu) (format = %d)", memRequirements.alignment, texture.memoryAlignment, imageInfo.format);
    } else
    importImage(texture);
}

void VulkanTexture::importImage(VulkanDevice::Handle handle, VkExternalMemoryHandleTypeFlagBits type)
{
  VkMemoryRequirements memRequirements;
  imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  imageInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT;
  finalLayout = VK_IMAGE_LAYOUT_GENERAL;
  prepareCreateImage(memRequirements, type);

  mem = dev->importMemory(memRequirements, 0, handle, type, type, std::nullopt);
  allocatedSize = memRequirements.size;
  memoryAlignment = memRequirements.alignment;
  vkBindImageMemory(dev->vkDev, img, mem, 0);

  SingleTimeCommands commands(*dev);
  commands.transitionImageLayout(img, getImageSubresourceRange(), VK_IMAGE_LAYOUT_UNDEFINED, finalLayout);
  writeTransferCmdTask = commands.write();
  waitsForTransfer = true;
  dirty = false;
  exportMemory = type;
  descrInfo.imageLayout = finalLayout;
  ownsMem = true;
}

void VulkanTexture::createImage(const void *imageData, VkExternalMemoryHandleTypeFlags exportMemory)
{
  VkMemoryRequirements memRequirements;
  imageInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
  imageInfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT;
  finalLayout = VK_IMAGE_LAYOUT_GENERAL;
  prepareCreateImage(memRequirements, exportMemory);
  if(memRequirements.size == 0)
    {
      dirty = false;
      return;
    }

  mem = dev->allocateMemory(memRequirements, 0, exportMemory, std::nullopt).first;
  allocatedSize = memRequirements.size;
  memoryAlignment = memRequirements.alignment;
  vkBindImageMemory(dev->vkDev, img, mem, 0);

  SingleTimeCommands commands(*dev);
  if(imageData)
    {
      VulkanStagingBuffer& stagingBuf = dev->getStagingBuffer(imageData, memorySize);
      commands.transitionImageLayout(img, getImageSubresourceRange(), VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
      const VkDeviceSize imgSz = imageInfo.extent.width * imageInfo.extent.height * imageInfo.extent.depth * bytesPerPixel();
      for(uint32_t i = 0; i < imageInfo.arrayLayers; ++i)
        {
          commands.copyBufferToImage(stagingBuf.getBuf(), img, imgSz*i, {0,0,0}, imageInfo.extent,
                                     VkImageSubresourceLayers {
                                       .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                                       .baseArrayLayer = i,
                                       .layerCount = 1
                                     });
        }
      commands.transitionImageLayout(img, getImageSubresourceRange(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, finalLayout);
      writeTransferCmdTask = commands.write();
      stagingBuf.setAsyncTask(writeTransferCmdTask, dev->getTransferCmdPool().getFence());
    } else {
      commands.transitionImageLayout(img, getImageSubresourceRange(), VK_IMAGE_LAYOUT_UNDEFINED, finalLayout);
      writeTransferCmdTask = commands.write();
    }
  waitsForTransfer = true;
  dirty = false;
  this->exportMemory = exportMemory;
  descrInfo.imageLayout = finalLayout;
  ownsMem = true;
}

void VulkanTexture::setBits(const void *imageData, VkExternalMemoryHandleTypeFlags exportMemory)
{
  if(dirty || !img || this->exportMemory != exportMemory)
    return createImage(imageData, exportMemory);

  if(imageData)
    {
      if(waitsForTransfer)
        {
          writeTransferCmdTask.waitForFinished();
          dev->submitTransferCommands();
        }

      SingleTimeCommands commands(*dev);
      VulkanStagingBuffer& stagingBuf = dev->getStagingBuffer(imageData, memorySize);
      commands.transitionImageLayout(img, getImageSubresourceRange(), VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);

      const VkDeviceSize imgSz = imageInfo.extent.width * imageInfo.extent.height * imageInfo.extent.depth * bytesPerPixel();
      for(uint32_t i = 0; i < imageInfo.arrayLayers; ++i)
        {
          commands.copyBufferToImage(stagingBuf.getBuf(), img, imgSz*i, {0,0,0}, imageInfo.extent,
                                     VkImageSubresourceLayers {
                                       .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                                       .baseArrayLayer = i,
                                       .layerCount = 1
                                     });
        }
      commands.transitionImageLayout(img, getImageSubresourceRange(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, finalLayout);
      writeTransferCmdTask = commands.write();
      stagingBuf.setAsyncTask(writeTransferCmdTask, dev->getTransferCmdPool().getFence());
      waitsForTransfer = true;
    }
}

void VulkanTexture::copyBitsFromGl(QOpenGLFunctions &gl)
{
  SingleTimeCommands commands(*dev);
  const auto dataSrc = std::bind(&QOpenGLFunctions::glReadPixels, &gl, 0,0, imageInfo.extent.width, imageInfo.extent.height, GL_RGBA, GL_UNSIGNED_BYTE, std::placeholders::_1);
  VulkanStagingBuffer& stagingBuf = dev->getStagingBuffer(dataSrc, allocatedSize);
  commands.transitionImageLayout(img, getImageSubresourceRange(), finalLayout, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL);
  commands.copyBufferToImage(stagingBuf.getBuf(), img, 0, {0,0,0}, imageInfo.extent, VkImageSubresourceLayers {
                               .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
                               .layerCount = 1
                             });
  commands.transitionImageLayout(img, getImageSubresourceRange(), VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, finalLayout);
  writeTransferCmdTask = commands.write();
  stagingBuf.setAsyncTask(writeTransferCmdTask, dev->getTransferCmdPool().getFence());
  waitsForTransfer = true;
}

void VulkanTexture::prepareCreateImage(VkMemoryRequirements &memReq, VkExternalMemoryHandleTypeFlags exportTypes)
{
  imageInfo.usage |= VK_IMAGE_USAGE_TRANSFER_SRC_BIT;

  if(allowedUsage.testFlag(UsageSampler))
    {
      switch(imageInfo.format)
        {
        case VK_FORMAT_R8_UNORM:
        case VK_FORMAT_R8G8_UNORM:
        case VK_FORMAT_R8G8B8_UNORM:
        case VK_FORMAT_B8G8R8_UNORM:
        case VK_FORMAT_R8G8B8A8_UNORM:
        case VK_FORMAT_B8G8R8A8_UNORM:
        case VK_FORMAT_R32_SFLOAT:
        case VK_FORMAT_R32G32_SFLOAT:
        case VK_FORMAT_R32G32B32_SFLOAT:
        case VK_FORMAT_R32G32B32A32_SFLOAT:
        case VK_FORMAT_D32_SFLOAT:
        case VK_FORMAT_D32_SFLOAT_S8_UINT:
        case VK_FORMAT_D24_UNORM_S8_UINT:
          if(allowedUsage.testFlag(UsageAttachment))
            finalLayout = VK_IMAGE_LAYOUT_GENERAL;
          else
            finalLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
          break;

        default:
          throw std::invalid_argument("VulkanTexture::prepareCteateImage: invalid format");
        }

      if(allowedUsage.testFlag(UsageSampler))
        imageInfo.usage |= VK_IMAGE_USAGE_SAMPLED_BIT;
    }

  if(allowedUsage.testFlag(UsageAttachment))
    {
      switch(imageInfo.format)
        {
        case VK_FORMAT_R8_UNORM:
        case VK_FORMAT_R8G8_UNORM:
        case VK_FORMAT_R8G8B8_UNORM:
        case VK_FORMAT_B8G8R8_UNORM:
        case VK_FORMAT_R8G8B8A8_UNORM:
        case VK_FORMAT_B8G8R8A8_UNORM:
        case VK_FORMAT_R32_SFLOAT:
        case VK_FORMAT_R32G32_SFLOAT:
        case VK_FORMAT_R32G32B32_SFLOAT:
        case VK_FORMAT_R32G32B32A32_SFLOAT:
          imageInfo.usage |= VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
          if(!allowedUsage.testFlag(UsageSampler))
            finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
          break;

        case VK_FORMAT_D32_SFLOAT:
        case VK_FORMAT_D32_SFLOAT_S8_UINT:
        case VK_FORMAT_D24_UNORM_S8_UINT:
          imageInfo.usage |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
          if(!allowedUsage.testFlag(UsageSampler))
            finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
          break;

        default:
          throw std::invalid_argument("VulkanTexture::prepareCteateImage: invalid format");
        }
    }

  destroyImgView();
  destroyImg();

  if(imageInfo.extent.width == 0
     || imageInfo.extent.height == 0
     || imageInfo.extent.depth == 0)
    {
      memReq.size = 0;
      return;
    }

  const VkExternalMemoryImageCreateInfo externalMemInfo { .sType = VK_STRUCTURE_TYPE_EXTERNAL_MEMORY_IMAGE_CREATE_INFO, .handleTypes = exportTypes };

  imageInfo.pNext = exportTypes? &externalMemInfo: nullptr;
  VkResult res = vkCreateImage(dev->vkDev, &imageInfo, nullptr, &img);
  imageInfo.pNext = nullptr;

  if(res != VK_SUCCESS)
    {
      img = VK_NULL_HANDLE;
      throw std::runtime_error("Can not create VkImage");
    }

  vkGetImageMemoryRequirements(dev->vkDev, img, &memReq);

  memorySize = imageInfo.extent.width;
  memorySize *= imageInfo.extent.height;
  memorySize *= imageInfo.extent.depth;
  memorySize *= imageInfo.arrayLayers;
  memorySize *= bytesPerPixel();
}

void VulkanTexture::destroyImgView()
{
  if(imgView)
    {
      dev->waitIdle();
      vkDestroyImageView(dev->vkDev, imgView, nullptr);

      imgView = VK_NULL_HANDLE;
    }
}

void VulkanTexture::destroyImg()
{
  while (!views2d.empty())
    views2d.front()->destroy();

  while (!viewsArray2d.empty())
    viewsArray2d.front()->destroy();

  if(img || mem)
    {
      if(waitsForTransfer)
        {
          writeTransferCmdTask.waitForFinished();
          dev->submitTransferCommands();
        }
      dev->waitIdle();
    }

  if(img)
    {
      vkDestroyImage(dev->vkDev, img, nullptr);
      img = VK_NULL_HANDLE;
    }
  if(mem)
    {
      if(ownsMem)
        vkFreeMemory(dev->vkDev, mem, nullptr);
      mem = VK_NULL_HANDLE;
    }
}

void VulkanTexture::createImageView(VkImageViewType type)
{
  Q_ASSERT(!dirty);

  if(imgView && type != imgViewType)
    destroyImgView();

  if(!imgView && img)
    {
      const VkImageViewCreateInfo viewInfo = {
        .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
        .image = img,
        .viewType = type,
        .format = imageInfo.format,
        .subresourceRange = VkImageSubresourceRange {
          .aspectMask = imgAspectFlagsUsage,
          .levelCount = imageInfo.mipLevels,
          .layerCount = imageInfo.arrayLayers
        }
      };

      if (vkCreateImageView(dev->vkDev, &viewInfo, nullptr, &imgView) != VK_SUCCESS)
        throw std::runtime_error("failed to create texture image view!");

      descrInfo.imageView = imgView;
      imgViewType = type;
    }

  if(!imgView)
    descrInfo.imageView = VK_NULL_HANDLE;
}

void VulkanTexture::setMinFilter(KawaiiTextureFilter filter)
{
  if(minFilter != filter)
    {
      minFilter = filter;
      dirty_sampler = true;
    }
}

void VulkanTexture::setMagFilter(KawaiiTextureFilter filter)
{
  if(magFilter != filter)
    {
      magFilter = filter;
      dirty_sampler = true;
    }
}

void VulkanTexture::setWrapModeS(KawaiiTextureWrapMode mode)
{
  if(uWrap != mode)
    {
      uWrap = mode;
      dirty_sampler = true;
    }
}

void VulkanTexture::setWrapModeT(KawaiiTextureWrapMode mode)
{
  if(vWrap != mode)
    {
      vWrap = mode;
      dirty_sampler = true;
    }
}

void VulkanTexture::setWrapModeR(KawaiiTextureWrapMode mode)
{
  if(wWrap != mode)
    {
      wWrap = mode;
      dirty_sampler = true;
    }
}

namespace {
  std::pair<VkFilter, std::optional<VkSamplerMipmapMode>> trTextureFilter(KawaiiTextureFilter filter)
  {
    switch(filter)
      {
      case KawaiiTextureFilter::Linear:
        return {VK_FILTER_LINEAR, {}};

      case KawaiiTextureFilter::Nearest:
        return {VK_FILTER_NEAREST, {}};

      case KawaiiTextureFilter::LinearMipmapLinear:
        return {VK_FILTER_LINEAR, VK_SAMPLER_MIPMAP_MODE_LINEAR};

      case KawaiiTextureFilter::LinearMipmapNearest:
        return {VK_FILTER_LINEAR, VK_SAMPLER_MIPMAP_MODE_NEAREST};

      case KawaiiTextureFilter::NearestMipmapLinear:
        return {VK_FILTER_LINEAR, VK_SAMPLER_MIPMAP_MODE_LINEAR};

      case KawaiiTextureFilter::NearestMipmapNearest:
        return {VK_FILTER_LINEAR, VK_SAMPLER_MIPMAP_MODE_NEAREST};
      }

    Q_UNREACHABLE();
  }

  VkSamplerAddressMode trWrapMode(KawaiiTextureWrapMode mode)
  {
    switch(mode)
      {
      case KawaiiTextureWrapMode::Repeat:
        return VK_SAMPLER_ADDRESS_MODE_REPEAT;

      case KawaiiTextureWrapMode::ClampToEdge:
        return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE;

      case KawaiiTextureWrapMode::ClampToBorder:
        return VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER;

      case KawaiiTextureWrapMode::MirroredRepeat:
        return VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT;

      case KawaiiTextureWrapMode::MirroredClampToEdge:
        return VK_SAMPLER_ADDRESS_MODE_MIRROR_CLAMP_TO_EDGE;
      }

    Q_UNREACHABLE();
  }
}

void VulkanTexture::createSampler()
{
  if(dirty_sampler)
    {
      if(sampler)
        {
          dev->waitIdle();
          vkDestroySampler(dev->vkDev, sampler, nullptr);
        }

      const auto min_filter = trTextureFilter(minFilter);
      const auto mag_filter = trTextureFilter(magFilter);

      VkSamplerCreateInfo samplerInfo = {};
      samplerInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
      samplerInfo.minFilter = min_filter.first;
      samplerInfo.magFilter = mag_filter.first;
      samplerInfo.addressModeU = trWrapMode(uWrap);
      samplerInfo.addressModeV = trWrapMode(vWrap);
      samplerInfo.addressModeW = trWrapMode(wWrap);
      samplerInfo.anisotropyEnable = VK_TRUE;
      samplerInfo.maxAnisotropy = 16;
      samplerInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
      samplerInfo.unnormalizedCoordinates = VK_FALSE;

      switch(compareOp)
        {
        case KawaiiDepthCompareOperation::None:
          samplerInfo.compareEnable = VK_FALSE;
          samplerInfo.compareOp = VK_COMPARE_OP_ALWAYS;
          break;
        case KawaiiDepthCompareOperation::Less:
          samplerInfo.compareEnable = VK_TRUE;
          samplerInfo.compareOp = VK_COMPARE_OP_LESS;
          break;
        case KawaiiDepthCompareOperation::LessEqual:
          samplerInfo.compareEnable = VK_TRUE;
          samplerInfo.compareOp = VK_COMPARE_OP_LESS_OR_EQUAL;
          break;
        case KawaiiDepthCompareOperation::Greater:
          samplerInfo.compareEnable = VK_TRUE;
          samplerInfo.compareOp = VK_COMPARE_OP_GREATER;
          break;
        case KawaiiDepthCompareOperation::GreaterEqual:
          samplerInfo.compareEnable = VK_TRUE;
          samplerInfo.compareOp = VK_COMPARE_OP_GREATER_OR_EQUAL;
          break;
        }

      samplerInfo.mipLodBias = 0.0f;
      samplerInfo.minLod = 0.0f;

      if(mag_filter.second.has_value())
        {
          samplerInfo.mipmapMode = mag_filter.second.value();
          samplerInfo.maxLod = imageInfo.mipLevels;
        } else
        {
          samplerInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
          samplerInfo.maxLod = 1.0f;
        }

      if (vkCreateSampler(dev->vkDev, &samplerInfo, nullptr, &sampler) != VK_SUCCESS)
        {
          sampler = VK_NULL_HANDLE;
          throw std::runtime_error("failed to create texture sampler!");
        }
      descrInfo.sampler = sampler;
      dirty_sampler = false;
    }
}

VkDescriptorImageInfo *VulkanTexture::descriptorInfo()
{
  Q_ASSERT(!dirty);

  if(allowedUsage.testFlag(UsageSampler))
    createSampler();
  else
    descrInfo.sampler = VK_NULL_HANDLE;

  return &descrInfo;
}

const VkDescriptorImageInfo *VulkanTexture::descriptorInfo() const
{
  Q_ASSERT(!dirty);
  Q_ASSERT(!allowedUsage.testFlag(UsageSampler) || (descrInfo.sampler != VK_NULL_HANDLE));
  return &descrInfo;
}

VkImageLayout VulkanTexture::getVkImageLayout() const
{
  return finalLayout;
}

VkImage VulkanTexture::getVkImage() const
{
  Q_ASSERT(!dirty);
  return img;
}

VkImageView VulkanTexture::getVkImageView() const
{
  Q_ASSERT(!dirty && imgView);
  return imgView;
}

size_t VulkanTexture::getAllocatedSize() const
{
  return allocatedSize;
}

uint32_t VulkanTexture::bytesPerPixel() const
{
  switch(imageInfo.format)
    {
    case VK_FORMAT_R8_UNORM:
      return 1;

    case VK_FORMAT_R8G8_UNORM:
      return 2;

    case VK_FORMAT_R8G8B8_UNORM:
    case VK_FORMAT_B8G8R8_UNORM:
      return 3;

    case VK_FORMAT_R8G8B8A8_UNORM:
    case VK_FORMAT_B8G8R8A8_UNORM:
      return 4;

    case VK_FORMAT_R32_SFLOAT:
    case VK_FORMAT_D32_SFLOAT:
    case VK_FORMAT_D24_UNORM_S8_UINT:
      return 4;

    case VK_FORMAT_D32_SFLOAT_S8_UINT:
      return 5;

    case VK_FORMAT_R32G32_SFLOAT:
      return 8;

    case VK_FORMAT_R32G32B32_SFLOAT:
      return 12;

    case VK_FORMAT_R32G32B32A32_SFLOAT:
      return 16;

    default:
      throw std::invalid_argument("VulkanTexture::bytesPerPixel: invalid format");
    }
}

const VulkanDevice &VulkanTexture::getDevice() const
{
  return *dev;
}

VkFormat VulkanTexture::getVkFormat() const
{
  Q_ASSERT(!dirty);
  return imageInfo.format;
}

KawaiiTextureFormat VulkanTexture::getKawaiiFormat() const
{
  switch(imageInfo.format)
    {
    case VK_FORMAT_R8_UNORM:
    case VK_FORMAT_R32_SFLOAT:
      return KawaiiTextureFormat::Red;

    case VK_FORMAT_R8G8_UNORM:
    case VK_FORMAT_R32G32_SFLOAT:
      return KawaiiTextureFormat::RedGreen;

    case VK_FORMAT_R8G8B8_UNORM:
    case VK_FORMAT_R32G32B32_SFLOAT:
      return KawaiiTextureFormat::RedGreenBlue;

    case VK_FORMAT_B8G8R8_UNORM:
      return KawaiiTextureFormat::BlueGreenRed;

    case VK_FORMAT_R8G8B8A8_UNORM:
    case VK_FORMAT_R32G32B32A32_SFLOAT:
      return KawaiiTextureFormat::RedGreenBlueAlpha;

    case VK_FORMAT_B8G8R8A8_UNORM:
      return KawaiiTextureFormat::BlueGreenRedAlpha;

    case VK_FORMAT_D32_SFLOAT:
      return KawaiiTextureFormat::Depth;

    case VK_FORMAT_D24_UNORM_S8_UINT:
    case VK_FORMAT_D32_SFLOAT_S8_UINT:
      return (imgAspectFlagsFull & VK_IMAGE_ASPECT_STENCIL_BIT)? KawaiiTextureFormat::DepthStencil: KawaiiTextureFormat::Depth;

    default:
      throw std::invalid_argument("VulkanTexture::getKawaiiFormat: invalid format");
    }
}

const VkExtent3D &VulkanTexture::getExtent() const
{
  return imageInfo.extent;
}

VkImageType VulkanTexture::getImageType() const
{
  return imageInfo.imageType;
}

VkImageAspectFlags VulkanTexture::getAspectFlagsUsage() const
{
  return imgAspectFlagsUsage;
}

VkImageAspectFlags VulkanTexture::getAspectFlagsFull() const
{
  return imgAspectFlagsFull;
}

std::optional<VulkanDevice::Handle> VulkanTexture::getMemHandle(VkExternalMemoryHandleTypeFlagBits type) const
{
  return dev->getMemoryHandle(mem, type);
}

VkImageSubresourceRange VulkanTexture::getImageSubresourceRange() const
{
  return VkImageSubresourceRange {
      .aspectMask = imgAspectFlagsFull,
          .levelCount = imageInfo.mipLevels,
          .layerCount = imageInfo.arrayLayers,
    };
}

VulkanTexture::View2D::View2D():
  tex(nullptr),
  imgView(VK_NULL_HANDLE)
{
}

VulkanTexture::View2D::View2D(VulkanTexture &tex, int layer, VkImageAspectFlags usage):
  tex(&tex),
  imgView(VK_NULL_HANDLE)
{
  const VkImageViewCreateInfo viewInfo = {
    .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
    .image = tex.img,
    .viewType = VK_IMAGE_VIEW_TYPE_2D,
    .format = tex.imageInfo.format,
    .subresourceRange = VkImageSubresourceRange {
      .aspectMask = usage,
      .levelCount = tex.imageInfo.mipLevels,
      .baseArrayLayer = layer>=0? static_cast<uint32_t>(layer): 0,
      .layerCount = 1
    }
  };

  if(viewInfo.subresourceRange.baseArrayLayer >= tex.imageInfo.arrayLayers)
    {
      throw std::runtime_error("invalid layer!");
    }

  if (vkCreateImageView(tex.dev->vkDev, &viewInfo, nullptr, &imgView) != VK_SUCCESS)
    throw std::runtime_error("failed to create texture image view!");

  tex.views2d.push_back(this);
}

VulkanTexture::View2D::View2D(VulkanTexture &tex, int layer):
  View2D(tex, layer, tex.imgAspectFlagsUsage)
{
}

VulkanTexture::View2D::~View2D()
{
  destroy();
}

VulkanTexture::View2D::View2D(VulkanTexture::View2D &&orig):
  tex(orig.tex),
  imgView(orig.imgView)
{
  orig.imgView = VK_NULL_HANDLE;
  orig.tex = nullptr;
  auto el = std::find(tex->views2d.begin(), tex->views2d.end(), &orig);
  if(el != tex->views2d.end())
    *el = this;
}

VulkanTexture::View2D &VulkanTexture::View2D::operator=(VulkanTexture::View2D &&orig)
{
  destroy();
  tex = orig.tex;
  imgView = orig.imgView;

  orig.imgView = VK_NULL_HANDLE;
  orig.tex = nullptr;
  auto el = std::find(tex->views2d.begin(), tex->views2d.end(), &orig);
  if(el != tex->views2d.end())
    *el = this;

  return *this;
}

void VulkanTexture::View2D::destroy()
{
  if(imgView && tex)
    {
      tex->dev->waitIdle();
      vkDestroyImageView(tex->dev->vkDev, imgView, nullptr);
      imgView = VK_NULL_HANDLE;

      auto el = std::find(tex->views2d.begin(), tex->views2d.end(), this);
      if(el != tex->views2d.end())
        tex->views2d.erase(el);
    }
}

VulkanTexture::ViewArray2D::ViewArray2D():
  tex(nullptr),
  imgView(VK_NULL_HANDLE)
{
}

VulkanTexture::ViewArray2D::ViewArray2D(VulkanTexture &tex, VkImageAspectFlags usage):
  tex(&tex),
  imgView(VK_NULL_HANDLE)
{
  const VkImageViewCreateInfo viewInfo = {
    .sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,
    .image = tex.img,
    .viewType = VK_IMAGE_VIEW_TYPE_2D_ARRAY,
    .format = tex.imageInfo.format,
    .subresourceRange = VkImageSubresourceRange {
      .aspectMask = usage,
      .levelCount = tex.imageInfo.mipLevels,
      .baseArrayLayer = 0,
      .layerCount = tex.getArrayLayers()
    }
  };
  if (vkCreateImageView(tex.dev->vkDev, &viewInfo, nullptr, &imgView) != VK_SUCCESS)
    throw std::runtime_error("failed to create texture image view!");

  tex.viewsArray2d.push_back(this);
}

VulkanTexture::ViewArray2D::ViewArray2D(VulkanTexture &tex):
  ViewArray2D(tex, tex.imgAspectFlagsUsage)
{
}

VulkanTexture::ViewArray2D::~ViewArray2D()
{
  destroy();
}

VulkanTexture::ViewArray2D::ViewArray2D(ViewArray2D &&orig):
  tex(orig.tex),
  imgView(orig.imgView)
{
  orig.imgView = VK_NULL_HANDLE;
  orig.tex = nullptr;
  auto el = std::find(tex->viewsArray2d.begin(), tex->viewsArray2d.end(), &orig);
  if(el != tex->viewsArray2d.end())
    *el = this;
}

VulkanTexture::ViewArray2D &VulkanTexture::ViewArray2D::operator=(ViewArray2D &&orig)
{
  destroy();
  tex = orig.tex;
  imgView = orig.imgView;

  orig.imgView = VK_NULL_HANDLE;
  orig.tex = nullptr;
  auto el = std::find(tex->viewsArray2d.begin(), tex->viewsArray2d.end(), &orig);
  if(el != tex->viewsArray2d.end())
    *el = this;

  return *this;
}

void VulkanTexture::ViewArray2D::destroy()
{
  if(imgView && tex)
    {
      tex->dev->waitIdle();
      vkDestroyImageView(tex->dev->vkDev, imgView, nullptr);
      imgView = VK_NULL_HANDLE;

      auto el = std::find(tex->viewsArray2d.begin(), tex->viewsArray2d.end(), this);
      if(el != tex->viewsArray2d.end())
        tex->viewsArray2d.erase(el);
    }
}
