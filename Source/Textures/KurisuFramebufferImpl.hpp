#ifndef KURISUFRAMEBUFFERIMPL_HPP
#define KURISUFRAMEBUFFERIMPL_HPP

#include <KawaiiRenderer/Textures/KawaiiFramebufferImpl.hpp>
#include "Vulkan/RenderingCommands.hpp"
#include "KurisuOffscrRenderer.hpp"

class KurisuRootImpl;

class KURISURENDERER_SHARED_EXPORT KurisuFramebufferImpl : public KawaiiFramebufferImpl,
    public KurisuOffscrRenderer
{
public:
  KurisuFramebufferImpl(KawaiiFramebuffer *model);
  ~KurisuFramebufferImpl();

  // KurisuOffscrRenderer interface
public:
  VulkanBaseRenderpass &getRp(size_t device_index) const override final;

  // KawaiiFramebufferImpl interface
private:
  void attachTexture(KawaiiTextureImpl *tex, quint32 gbuf_index, int layer) override final;
  void detachTexture(quint32 gbuf_index) override final;
  void destroy() override final;
  void activate() override final;
  void deactivate() override final;



  //IMPLEMENT
private:
  KawaiiRenderpass *oldRp;
  std::unordered_map<quint32, std::vector<VulkanTexture::View2D>> cachedTexLayers;
  std::unordered_map<quint32, std::pair<std::vector<VulkanTexture*>, int>> indirectGbufTargets;
  std::vector<std::unique_ptr<RenderingCommands>> copyToTex;

  KurisuRootImpl *root();
  void onRenderpassChanged(KawaiiRenderpass *newRenderpass);
};

#endif // KURISUFRAMEBUFFERIMPL_HPP
