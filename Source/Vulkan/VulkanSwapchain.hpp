#ifndef VULKANSWAPCHAIN_HPP
#define VULKANSWAPCHAIN_HPP

#include "VulkanDevice.hpp"
#include <QWindow>

class VulkanSwapchain
{
  VulkanSwapchain(const VulkanSwapchain &orig) = delete;
  VulkanSwapchain& operator=(const VulkanSwapchain &orig) = delete;

public:
  VulkanSwapchain(QWindow *wnd, VkSurfaceKHR sfc, VulkanDevice &dev);
  ~VulkanSwapchain();

  VkFormat getImageFormat() const;
  VulkanDevice &getDevice() const;

  bool checkExtent() const;
  void recreate();

  size_t getImageCount() const;
  const std::vector<VkImage> &getImages() const;

  uint32_t width() const;
  uint32_t height() const;

  size_t getAsyncFrames() const;

  //returns the acquired image index
  uint32_t acquireNextImage(VkSemaphore semaphore, bool *outdatedSwapchain);

  void present(VkSemaphore waitSemaphore, uint32_t imageIndex);

private:
  VkSwapchainKHR swapchain;
  VkSurfaceKHR sfc;
  VkFormat imageFormat;
  VulkanDevice &dev;
  QWindow *wnd;

  std::vector<VkImage> images;

  VkQueue presentQ;

  size_t asyncFrames;

  uint32_t w;
  uint32_t h;
};

#endif // VULKANSWAPCHAIN_HPP
