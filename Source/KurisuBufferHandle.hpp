#ifndef KURISUBUFFERHANDLE_HPP
#define KURISUBUFFERHANDLE_HPP

#include <KawaiiRenderer/KawaiiBufferHandle.hpp>
#include "Vulkan/VulkanBuffer.hpp"
#include <sib_utils/PairHash.hpp>
#include <functional>
#include <vector>

#include "KurisuRenderer_global.hpp"

class KurisuRootImpl;
class KurisuTextureHandle;

class KURISURENDERER_SHARED_EXPORT KurisuBufferHandle: public KawaiiBufferHandle
{
  KurisuBufferHandle(const KurisuBufferHandle &) = delete;
  KurisuBufferHandle& operator=(const KurisuBufferHandle &) = delete;
public:
  KurisuBufferHandle(const void *ptr, size_t n, KurisuRootImpl &root, const std::initializer_list<KawaiiBufferTarget> &availableTargets);
  ~KurisuBufferHandle();

  static VkBufferUsageFlagBits trBufferTarget(KawaiiBufferTarget target);
  static VkBufferUsageFlags trBufferTarget(const std::initializer_list<KawaiiBufferTarget> &availableTargets);

  VulkanBuffer &buffer(size_t device_index);

  void bindTexture(const std::string &fieldName, KurisuTextureHandle *tex);
  void unbindTexture(const std::string &fieldName);
  void unbindAllTextures();

  const std::unordered_map<std::string, KurisuTextureHandle *> &getBindedTextures() const;

  // KawaiiBufferHandle interface
  void setBufferData(const void *ptr, size_t n) override final;
  void sendChunkToGpu(size_t offset, size_t n) override final;
  size_t getBufferSize() const override final;
  void bind(KawaiiBufferTarget target) override final;
  void unbind(KawaiiBufferTarget target) override final;
  void bindToBlock(KawaiiBufferTarget target, uint32_t bindingPoint) override final;
  void userBinding(KawaiiBufferTarget target, uint32_t bindingPoint) override final;
  void unbindFromBlock(KawaiiBufferTarget target, uint32_t bindingPoint) override final;



  //IMPLEMENT
private:
  std::vector<VulkanBuffer> buffers; //Buffers for each device

  std::unordered_map<std::string, KurisuTextureHandle*> textures;

  KurisuRootImpl &r;

  const void *data;
  size_t length;

  VkBufferUsageFlags availableTargets;
};

#endif // KURISUBUFFERHANDLE_HPP
