#!/bin/sh
SCRIPT=$(readlink -f "$0")
SRCPATH=$(dirname "$SCRIPT")
cd "$SRCPATH"

PKGEXT=.pkg.tar makepkg -f
pkexec pacman --noconfirm -U "$SRCPATH/"*.pkg*

