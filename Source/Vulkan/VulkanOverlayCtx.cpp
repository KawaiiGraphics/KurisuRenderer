#include "VulkanOverlayCtx.hpp"
#include "VulkanTexture.hpp"

#include <Kawaii3D/KawaiiConfig.hpp>
#include <sib_utils/Strings.hpp>
#include <QOpenGLExtraFunctions>
#include <QProcessEnvironment>
#include <QCoreApplication>
#include <QDebug>

namespace {
#if !defined(Q_OS_ANDROID) && !defined(Q_OS_IOS)
  constexpr GLenum gl_debug_source_api = GL_DEBUG_SOURCE_API;
  constexpr GLenum gl_debug_source_window_system = GL_DEBUG_SOURCE_WINDOW_SYSTEM;
  constexpr GLenum gl_debug_source_shader_compiler = GL_DEBUG_SOURCE_SHADER_COMPILER;
  constexpr GLenum gl_debug_source_third_party = GL_DEBUG_SOURCE_THIRD_PARTY;
  constexpr GLenum gl_debug_source_application = GL_DEBUG_SOURCE_APPLICATION;
  constexpr GLenum gl_debug_source_other = GL_DEBUG_SOURCE_OTHER;

  constexpr GLenum gl_debug_type_error = GL_DEBUG_TYPE_ERROR;
  constexpr GLenum gl_debug_type_deprecated_behavior = GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR;
  constexpr GLenum gl_debug_type_undefined_behavior = GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR;
  constexpr GLenum gl_debug_type_portability = GL_DEBUG_TYPE_PORTABILITY;
  constexpr GLenum gl_debug_type_performance = GL_DEBUG_TYPE_PERFORMANCE;
  constexpr GLenum gl_debug_type_marker = GL_DEBUG_TYPE_MARKER;
  constexpr GLenum gl_debug_type_push_group = GL_DEBUG_TYPE_PUSH_GROUP;
  constexpr GLenum gl_debug_type_pop_group = GL_DEBUG_TYPE_POP_GROUP;
  constexpr GLenum gl_debug_type_other = GL_DEBUG_TYPE_OTHER;

  constexpr GLenum gl_debug_severity_high = GL_DEBUG_SEVERITY_HIGH;
  constexpr GLenum gl_debug_severity_medium = GL_DEBUG_SEVERITY_MEDIUM;

  constexpr GLenum gl_debug_output = GL_DEBUG_OUTPUT;
  constexpr GLenum gl_debug_output_synchronous = GL_DEBUG_OUTPUT_SYNCHRONOUS;
#else
  constexpr GLenum gl_debug_source_api = GL_DEBUG_SOURCE_API_KHR;
  constexpr GLenum gl_debug_source_window_system = GL_DEBUG_SOURCE_WINDOW_SYSTEM_KHR;
  constexpr GLenum gl_debug_source_shader_compiler = GL_DEBUG_SOURCE_SHADER_COMPILER_KHR;
  constexpr GLenum gl_debug_source_third_party = GL_DEBUG_SOURCE_THIRD_PARTY_KHR;
  constexpr GLenum gl_debug_source_application = GL_DEBUG_SOURCE_APPLICATION_KHR;
  constexpr GLenum gl_debug_source_other = GL_DEBUG_SOURCE_OTHER_KHR;

  constexpr GLenum gl_debug_type_error = GL_DEBUG_TYPE_ERROR_KHR;
  constexpr GLenum gl_debug_type_deprecated_behavior = GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR_KHR;
  constexpr GLenum gl_debug_type_undefined_behavior = GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR_KHR;
  constexpr GLenum gl_debug_type_portability = GL_DEBUG_TYPE_PORTABILITY_KHR;
  constexpr GLenum gl_debug_type_performance = GL_DEBUG_TYPE_PERFORMANCE_KHR;
  constexpr GLenum gl_debug_type_marker = GL_DEBUG_TYPE_MARKER_KHR;
  constexpr GLenum gl_debug_type_push_group = GL_DEBUG_TYPE_PUSH_GROUP_KHR;
  constexpr GLenum gl_debug_type_pop_group = GL_DEBUG_TYPE_POP_GROUP_KHR;
  constexpr GLenum gl_debug_type_other = GL_DEBUG_TYPE_OTHER_KHR;

  constexpr GLenum gl_debug_severity_high = GL_DEBUG_SEVERITY_HIGH_KHR;
  constexpr GLenum gl_debug_severity_medium = GL_DEBUG_SEVERITY_MEDIUM_KHR;

  constexpr GLenum gl_debug_output = GL_DEBUG_OUTPUT_KHR;
  constexpr GLenum gl_debug_output_synchronous = GL_DEBUG_OUTPUT_SYNCHRONOUS_KHR;
#endif

  void receiveGLDebug(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *)
  {
    using sib_utils::strings::getQLatin1Str;
    const uint16_t dbg = KawaiiConfig::getInstance().getDebugLevel();

    static const std::unordered_map<GLenum, QLatin1String> sourceNames = {
      {gl_debug_source_api,             getQLatin1Str("calls to the OpenGL API")},
      {gl_debug_source_window_system,   getQLatin1Str("calls to a window-system API")},
      {gl_debug_source_shader_compiler, getQLatin1Str("compiler for a shading language")},
      {gl_debug_source_third_party,     getQLatin1Str("application associated with OpenGL")},
      {gl_debug_source_application,     getQLatin1Str("user of this application")},
      {gl_debug_source_other,           getQLatin1Str("Unknown")}
    };

    static const std::unordered_map<GLenum, QLatin1String> typeNames = {
      {gl_debug_type_error,               getQLatin1Str("Error")},
      {gl_debug_type_deprecated_behavior, getQLatin1Str("Warning (deprecated)")},
      {gl_debug_type_undefined_behavior,  getQLatin1Str("Warning (undefined behavior)")},
      {gl_debug_type_portability,         getQLatin1Str("Warning (functionality is not portable)")},
      {gl_debug_type_performance,         getQLatin1Str("Warning (performance issues)")},
      {gl_debug_type_marker,              getQLatin1Str("Command stream annotation")},
      {gl_debug_type_push_group,          getQLatin1Str("Group pushed")},
      {gl_debug_type_pop_group,           getQLatin1Str("Group poped")},
      {gl_debug_type_other,               getQLatin1Str("Unknown")}
    };

    if(length < 0 || strlen(reinterpret_cast<const char*>(message)) < static_cast<size_t>(length))
      return;

    switch(severity)
      {
      case gl_debug_severity_high:
        qCritical().setAutoInsertSpaces(true);
        qCritical() << "OpenGL debug message #" << id << '{';
        qCritical() << "\tFrom " << sourceNames.at(source);
        qCritical() << "\tType: " << typeNames.at(type);
        qCritical() << "\tText: " << reinterpret_cast<const char*>(message);
        qCritical().nospace() << '}';
        break;

      case gl_debug_severity_medium:
        if(dbg < 2) break;
        qWarning() << "OpenGL debug message #" << id << '{';
        qWarning() << "\tFrom " << sourceNames.at(source);
        qWarning() << "\tType: " << typeNames.at(type);
        qWarning() << "\tText: " << reinterpret_cast<const char*>(message);
        qWarning().nospace() << '}';
        break;

      default:
        if(dbg < 3) break;
        qInfo() << "OpenGL debug message #" << id << '{';
        qInfo() << "\tFrom " << sourceNames.at(source);
        qInfo() << "\tType: " << typeNames.at(type);
        qInfo() << "\tText: " << reinterpret_cast<const char*>(message);
        qInfo().nospace() << '}';
        break;
      }
  }

  const QSurfaceFormat sfcFmt = ([]{
      QSurfaceFormat sfcFmt = KawaiiConfig::getInstance().getPreferredSfcFormat();
      sfcFmt.setRedBufferSize(0);
      sfcFmt.setGreenBufferSize(0);
      sfcFmt.setBlueBufferSize(0);
      sfcFmt.setAlphaBufferSize(0);
      sfcFmt.setOption(QSurfaceFormat::DeprecatedFunctions, false);
      sfcFmt.setOption(QSurfaceFormat::StereoBuffers, false);
      sfcFmt.setOption(QSurfaceFormat::ResetNotification, false);
#     if !defined(Q_OS_ANDROID) && !defined(Q_OS_IOS)
      sfcFmt.setProfile(QSurfaceFormat::CoreProfile);
#     endif

      const auto systemEnv = QProcessEnvironment::systemEnvironment();
      if(systemEnv.value("SIB3D_FORCE_OPENGL", "0").toInt())
        sfcFmt.setRenderableType(QSurfaceFormat::OpenGL);
      else if(systemEnv.value("SIB3D_FORCE_COREGL", "0").toInt())
        {
          sfcFmt.setRenderableType(QSurfaceFormat::OpenGL);
          sfcFmt.setProfile(QSurfaceFormat::CoreProfile);
          sfcFmt.setVersion(3,3);
        }
      else if(systemEnv.value("SIB3D_FORCE_GLES", "0").toInt())
        {
          sfcFmt.setRenderableType(QSurfaceFormat::OpenGLES);
          sfcFmt.setProfile(QSurfaceFormat::NoProfile);
        }

      return sfcFmt;
    })();
}

VulkanOverlayCtx::VulkanOverlayCtx(bool allowVulkanInterop):
  glGenSemaphoresEXT(nullptr),
  glSignalSemaphoreEXT(nullptr),
  glDeleteSemaphoresEXT(nullptr),
  glCreateMemoryObjectsEXT(nullptr),
  glDeleteMemoryObjectsEXT(nullptr),
  glImportSemHandleEXT(nullptr),
  glImportMemHandleEXT(nullptr),
  glTexStorageMem2DEXT(nullptr),
  vulkanInterop(allowVulkanInterop)
{
}

VulkanOverlayCtx::~VulkanOverlayCtx()
{
  if(QOpenGLContext::currentContext() == &ctx)
    ctx.doneCurrent();
}

void VulkanOverlayCtx::init()
{
  if(ctx.isValid())
    return;

  sfc.setFormat(sfcFmt);
  sfc.create();
  Q_ASSERT(sfc.isValid());

  ctx.setFormat(sfcFmt);
  ctx.create();
  ctx.makeCurrent(&sfc);

  const auto glExtensions = ctx.extensions();
  if(vulkanInterop)
    {
      if(!glExtensions.contains("GL_EXT_memory_object") || !glExtensions.contains("GL_EXT_semaphore"))
        {
          qWarning().noquote() << "KurisuRenderer: OpenGL to Vulkan interop is not supported by driver!";
          vulkanInterop = false;
        }
    }

  if(vulkanInterop)
    {
      glGenSemaphoresEXT = reinterpret_cast<decltype(glGenSemaphoresEXT)>(ctx.getProcAddress("glGenSemaphoresEXT"));
      glSignalSemaphoreEXT = reinterpret_cast<decltype(glSignalSemaphoreEXT)>(ctx.getProcAddress("glSignalSemaphoreEXT"));
      glDeleteSemaphoresEXT = reinterpret_cast<decltype(glDeleteSemaphoresEXT)>(ctx.getProcAddress("glDeleteSemaphoresEXT"));
      glCreateMemoryObjectsEXT = reinterpret_cast<decltype(glCreateMemoryObjectsEXT)>(ctx.getProcAddress("glCreateMemoryObjectsEXT"));
      glDeleteMemoryObjectsEXT = reinterpret_cast<decltype(glDeleteMemoryObjectsEXT)>(ctx.getProcAddress("glDeleteMemoryObjectsEXT"));
      glTexStorageMem2DEXT = reinterpret_cast<decltype(glTexStorageMem2DEXT)>(ctx.getProcAddress("glTexStorageMem2DEXT"));

#     ifdef Q_OS_UNIX
      glImportSemHandleEXT = reinterpret_cast<decltype(glImportSemHandleEXT)>(ctx.getProcAddress("glImportSemaphoreFdEXT"));
      glImportMemHandleEXT = reinterpret_cast<decltype(glImportMemHandleEXT)>(ctx.getProcAddress("glImportMemoryFdEXT"));
#     elif defined(Q_OS_WINDOWS)
      glImportSemHandleEXT = reinterpret_cast<decltype(glImportSemHandleEXT)>(ctx.getProcAddress("glImportSemaphoreWin32HandleEXT"));
      glImportMemHandleEXT = reinterpret_cast<decltype(glImportMemHandleEXT)>(ctx.getProcAddress("glImportMemoryWin32HandleEXT"));
#     endif
    }

  if(KawaiiConfig::getInstance().getDebugLevel() > 0 &&
     (glExtensions.contains("GL_KHR_debug") || glExtensions.contains("GL_ARB_debug_output")))
    {
      ctx.functions()->glEnable(gl_debug_output);
      ctx.functions()->glEnable(gl_debug_output_synchronous);
      ctx.extraFunctions()->glDebugMessageCallback(&receiveGLDebug, nullptr);
    } else
    qWarning() << glExtensions;

  ctx.functions()->glClearColor(0,0,0,0);
}

void VulkanOverlayCtx::makeCurrent()
{
  init();
  if(QOpenGLContext::currentContext() != &ctx)
    ctx.makeCurrent(&sfc);
}

GLuint VulkanOverlayCtx::importVulkanSemaphore(VulkanDevice::Handle fd)
{
  Q_ASSERT(vulkanInterop);

  GLuint sem;
  glGenSemaphoresEXT(1, &sem);
  glImportSemHandleEXT(sem, opaqueHandleType, fd);
  return sem;
}

GlExternalTexture VulkanOverlayCtx::importVulkanTexture(const VulkanTexture &tex)
{
  Q_ASSERT(vulkanInterop);
  const auto texMemHandle = tex.getMemHandle(VulkanTexture::imgSameGpuMemoryHandleType);

  GLuint glTex;
  GLuint glMem;
  ctx.functions()->glGenTextures(1, &glTex);
  ctx.functions()->glBindTexture(GL_TEXTURE_2D, glTex);
  ctx.functions()->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_TILING_EXT,
                                   tex.isLinear()? GL_LINEAR_TILING_EXT: GL_OPTIMAL_TILING_EXT);

  glCreateMemoryObjectsEXT(1, &glMem);
  glImportMemHandleEXT(glMem, tex.getAllocatedSize(), opaqueHandleType, *texMemHandle);
  glTexStorageMem2DEXT(GL_TEXTURE_2D, 1, GL_RGBA8,
                       tex.getExtent().width, tex.getExtent().height,
                       glMem, 0);

  ctx.functions()->glBindTexture(GL_TEXTURE_2D, 0);

  return { .tex = glTex, .mem = glMem };
}

void VulkanOverlayCtx::signalGlSemaphore(GLuint sem, const GlExternalTexture &tex, GLenum dstLayout)
{
  Q_ASSERT(vulkanInterop);

  makeCurrent();
  glSignalSemaphoreEXT(sem,
                       0, nullptr,
                       1, &tex.tex, &dstLayout);
}

void VulkanOverlayCtx::free(GlExternalTexture &tex)
{
  makeCurrent();
  if(tex.tex)
    {
      ctx.functions()->glDeleteTextures(1, &tex.tex);
      tex.tex = 0;
    }
  if(tex.mem)
    {
      Q_ASSERT(glDeleteMemoryObjectsEXT != nullptr);
      glDeleteMemoryObjectsEXT(1, &tex.mem);
      tex.mem = 0;
    }
}

void VulkanOverlayCtx::deleteGlSemaphore(GLuint sem)
{
  if(sem)
    {
      Q_ASSERT(vulkanInterop);
      makeCurrent();
      glDeleteSemaphoresEXT(1, &sem);
    }
}

QOpenGLFunctions *VulkanOverlayCtx::gl()
{
  return ctx.functions();
}

bool VulkanOverlayCtx::vulkanInteropSupported() const
{
  return vulkanInterop;
}

namespace {
  inline const bool allowOverlay = (qgetenv("SIB3D_FORBID_OVERLAYS").toInt() == 0);

#ifndef Q_OS_WINDOWS
  inline const bool allowGlToVulkanInterop = qEnvironmentVariable("SIB3D_VULKAN_GL_INTEROP", "1").toInt() != 0;
#else
  inline const bool allowGlToVulkanInterop = qEnvironmentVariable("SIB3D_VULKAN_GL_INTEROP", "0").toInt() != 0;
#endif

  std::unique_ptr<VulkanOverlayCtx> globalGlCtx;
  bool globalGlCtxDestroyed = false;
}

VulkanOverlayCtx &VulkanOverlayCtx::globalOverlayCtx()
{
  if(Q_UNLIKELY(globalGlCtxDestroyed))
    throw std::runtime_error("Global overlay context no longer exists!");

  if(!globalGlCtx)
    {
      globalGlCtx = std::make_unique<VulkanOverlayCtx>(allowGlToVulkanInterop);
      qAddPostRoutine([] { globalGlCtx.reset(); globalGlCtxDestroyed = true; });
    }
  return *globalGlCtx;
}
