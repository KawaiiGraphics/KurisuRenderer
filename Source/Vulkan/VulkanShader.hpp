#ifndef VULKANGLSLSHADER_HPP
#define VULKANGLSLSHADER_HPP

#include <Kawaii3D/Shaders/KawaiiShaderType.hpp>
#include <vulkan/vulkan.h>
#include <QString>

class VulkanShader
{
  VulkanShader(const VulkanShader &orig) = delete;
  VulkanShader& operator=(const VulkanShader &orig) = delete;
public:
  VulkanShader(VkDevice dev, VkShaderStageFlagBits stage);
  VulkanShader(VulkanShader &&orig);
  VulkanShader& operator=(VulkanShader &&orig);
  ~VulkanShader();

  void setCode(const std::vector<uint32_t> &spirv_bytes);

  operator bool() const;

  VkPipelineShaderStageCreateInfo stageCreateInfo() const;

private:
  VkDevice dev;
  VkShaderModule shader;

  VkShaderStageFlagBits stage;

  void destroy();
};

#endif // VULKANGLSLSHADER_HPP
