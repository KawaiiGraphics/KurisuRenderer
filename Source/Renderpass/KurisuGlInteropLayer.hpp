#ifndef KURISUGLINTEROPLAYER_HPP
#define KURISUGLINTEROPLAYER_HPP

#include "KurisuImageSourceImpl.hpp"
#include "Vulkan/VulkanOverlayImg.hpp"
#include "Vulkan/VulkanPipeline.hpp"
#include <vector>

class KURISURENDERER_SHARED_EXPORT KurisuGlInteropLayer: public KurisuImageSourceImpl
{
public:
  KurisuGlInteropLayer(KawaiiImageSource *model);
  ~KurisuGlInteropLayer();

  void onRpDraw(size_t activeDevice);

protected:
  VulkanOverlayImg &getCurrentOverlayImg() const;

  // KawaiiImageSourceImpl interface
protected:
  void drawQuad() override final;
  bool loadPixmaps() override final;

  // KurisuImageSourceImpl interface
private:
  void writeCmdBuf(RenderingCommands &cmdBuf, size_t dev_index) override final;



  // IMPLEMENT
private:
  std::vector<std::unique_ptr<VulkanOverlayImg>> overlayImg;
  std::vector<std::unique_ptr<VulkanPipeline>> pipeline;
  std::vector<bool> painted;

protected:
  void refreshPixmaps();
};

#endif // KURISUGLINTEROPLAYER_HPP
