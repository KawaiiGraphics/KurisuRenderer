#ifndef KURISUIMAGESOURCEIMPL_HPP
#define KURISUIMAGESOURCEIMPL_HPP

#include <KawaiiRenderer/Renderpass/KawaiiImageSourceImpl.hpp>
#include "Vulkan/RenderingCommands.hpp"
#include "../KurisuRenderer_global.hpp"
#include <vector>
#include <memory>

class KURISURENDERER_SHARED_EXPORT KurisuImageSourceImpl: public virtual KawaiiImageSourceImpl
{
  KurisuImageSourceImpl(const KurisuImageSourceImpl&) = delete;
  KurisuImageSourceImpl& operator=(const KurisuImageSourceImpl&) = delete;
public:
  KurisuImageSourceImpl(KawaiiImageSource *model);
  ~KurisuImageSourceImpl();

  const std::vector<VkSemaphore>& getWaitSemaphores(size_t dev_index, size_t frameIndex) const;
  const std::vector<VkPipelineStageFlags>& getWaitStages() const;

  // KawaiiImageSourceImpl interface
private:
  void createCache() override final;
  void drawCache() override final;
  void updateCache() override final;
  void deleteCache() override final;



  // IMPLEMENT
private:
  std::vector<std::unique_ptr<RenderingCommands>> cmd;

  virtual void writeCmdBuf(RenderingCommands &cmdBuf, size_t dev_index) = 0;

  VkPipelineColorBlendAttachmentState getColorBlendAttachmentState() const;

  void bindGbufsDescriptor(size_t dev_i);

protected:
  std::vector<std::vector<std::vector<VkSemaphore>>> waitSemaphores;
  std::vector<VkPipelineStageFlags> waitStages;

  bool needsGBufs;
};

#endif // KURISUIMAGESOURCEIMPL_HPP
