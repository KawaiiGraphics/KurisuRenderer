#version 450 core

const vec2 vertices[6] = vec2[6] (
    vec2(1,1), vec2(1,-1), vec2(-1,-1),
    vec2(-1,-1), vec2(-1,1), vec2(1,1)
);

void main(void)
{
    gl_Position = vec4(vertices[gl_VertexIndex], 0.0, 1.0);
}
