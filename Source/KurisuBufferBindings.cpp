#include "KurisuBufferBindings.hpp"
#include <Kawaii3D/Shaders/KawaiiShader.hpp>

KurisuBufferBindings::KurisuBufferBindings(QObject *parent):
  QObject(parent)
{
}

void KurisuBufferBindings::bindBuffer(KawaiiBufferTarget target, KurisuBufferHandle *buf)
{
  auto el = bindings.find(target);
  if(el == bindings.end())
    el = bindings.insert({target, {}}).first;

  el->second.push_back(buf);
  emit changedBinding(el->first, el->second.back());
}

namespace {
  template <typename T>
  inline T* lastEl(const std::list<T*> &list)
  { return !list.empty()? list.back(): nullptr; }
}

void KurisuBufferBindings::unbindBuffer(KawaiiBufferTarget target, KurisuBufferHandle *buf)
{
  auto el = bindings.find(target);
  if(el == bindings.end() || el->second.empty())
    throw std::out_of_range("KurisuBufferBindings::unbindBuffer: no buffer binded");

  auto iter = std::find(el->second.begin(), el->second.end(), buf);
  bool bindingChanged = (iter == el->second.rbegin().base());
  el->second.erase(iter);
  if(bindingChanged)
    emit changedBinding(el->first, lastEl(el->second));
}

void KurisuBufferBindings::bindBufferBlock(KawaiiBufferTarget target, uint32_t block, KurisuBufferHandle *buf)
{
  auto el = blockBindings.find({target, block});
  if(el == blockBindings.end())
    el = blockBindings.insert({{target, block}, {}}).first;

  el->second.push_back(buf);
  emit changedBlockBinding(el->first.first, el->first.second, el->second.back());
}

void KurisuBufferBindings::unbindBufferBlock(KawaiiBufferTarget target, uint32_t block, KurisuBufferHandle *buf)
{
  auto el = blockBindings.find({target, block});
  if(el == blockBindings.end() || el->second.empty())
    {
      qCritical("KurisuBufferBindings::unbindBufferBlock: no buffer binded");
      return;
    }

  auto iter = std::find(el->second.begin(), el->second.end(), buf);
  bool bindingChanged = (iter == el->second.rbegin().base());
  el->second.erase(iter);
  if(bindingChanged)
    emit changedBlockBinding(el->first.first, el->first.second, lastEl(el->second));
}

void KurisuBufferBindings::bindBufferBlockUser(KawaiiBufferTarget target, uint32_t block, KurisuBufferHandle *buf)
{
  auto el = userBindings.find({target, block});
  if(el == userBindings.end())
    el = userBindings.insert({{target, block}, {}}).first;

  el->second.push_back(buf);
  emit changedUserBlockBinding(el->first.first, el->first.second, el->second.back());
}

void KurisuBufferBindings::unbindBufferBlockUser(KawaiiBufferTarget target, uint32_t block, KurisuBufferHandle *buf)
{
  auto el = userBindings.find({target, block});
  if(el == userBindings.end() || el->second.empty())
    throw std::out_of_range("KurisuBufferBindings::unbindBufferBlock: no buffer binded");

  auto iter = std::find(el->second.begin(), el->second.end(), buf);
  bool bindingChanged = (iter == el->second.rbegin().base());
  el->second.erase(iter);
  if(bindingChanged)
    emit changedUserBlockBinding(el->first.first, el->first.second, lastEl(el->second));
}

void KurisuBufferBindings::unbindBuffer(KurisuBufferHandle *buf)
{
  for(auto &i: bindings) {
      auto el = std::find(i.second.begin(), i.second.end(), buf);
      bool bindingChanged = (el == i.second.rbegin().base());
      if(el != i.second.end())
        i.second.erase(el);
      if(bindingChanged)
        emit changedBinding(i.first, i.second.back());
    }

  for(auto &i: blockBindings) {
      auto el = std::find(i.second.begin(), i.second.end(), buf);
      bool bindingChanged = (el == i.second.rbegin().base());
      if(el != i.second.end())
        i.second.erase(el);
      if(bindingChanged)
        emit changedBlockBinding(i.first.first, i.first.second, i.second.back());
    }

  for(auto &i: userBindings) {
      auto el = std::find(i.second.begin(), i.second.end(), buf);
      bool bindingChanged = (el == i.second.rbegin().base());
      if(el != i.second.end())
        i.second.erase(el);
      if(bindingChanged)
        emit changedUserBlockBinding(i.first.first, i.first.second, i.second.back());
    }
}

KurisuBufferHandle *KurisuBufferBindings::bindedBuffer(KawaiiBufferTarget target)
{
  auto el = bindings.find(target);
  if(el == bindings.end() || el->second.empty())
    return nullptr;

  return lastEl(el->second);
}

KurisuBufferHandle *KurisuBufferBindings::bindedBufferBlock(KawaiiBufferTarget target, uint32_t block)
{
  auto el = blockBindings.find({target, block});
  if(el == blockBindings.end() || el->second.empty())
    return nullptr;

  return lastEl(el->second);
}

KurisuBufferHandle *KurisuBufferBindings::bindedBufferBlockUser(KawaiiBufferTarget target, uint32_t block)
{
  auto el = userBindings.find({target, block});
  if(el == userBindings.end() || el->second.empty())
    return nullptr;

  return lastEl(el->second);
}

namespace {
  VkDescriptorType trDescriptorType(KawaiiBufferTarget type)
  {
    switch(type)
      {
      case KawaiiBufferTarget::UBO:
        return VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;

      case KawaiiBufferTarget::SSBO:
        return VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;

      case KawaiiBufferTarget::VBO:
      case KawaiiBufferTarget::CopyRead:
      case KawaiiBufferTarget::CopyWrite:
      case KawaiiBufferTarget::DrawIndirect:
      case KawaiiBufferTarget::ElementArray:
      case KawaiiBufferTarget::DispatchIndirect:
      case KawaiiBufferTarget::TextureBuffer:
      case KawaiiBufferTarget::TransformFeedback:
      case KawaiiBufferTarget::PixelPack:
      case KawaiiBufferTarget::PixelUnpack:
      case KawaiiBufferTarget::QueryBuffer:
      case KawaiiBufferTarget::AtomicCounter:
        break;
      }
    Q_UNREACHABLE();
  }
}

std::vector<std::tuple<VkDescriptorType, uint32_t, KurisuBufferHandle *> > KurisuBufferBindings::bindedBufferBlocks()
{
  std::vector<std::tuple<VkDescriptorType, uint32_t, KurisuBufferHandle *>> result;
  for(const auto &i: blockBindings)
    result.emplace_back(trDescriptorType(i.first.first), i.first.second, lastEl(i.second));
  return result;
}

std::vector<std::tuple<VkDescriptorType, uint32_t, KurisuBufferHandle *> > KurisuBufferBindings::bindedBufferBlocksUser()
{
  std::vector<std::tuple<VkDescriptorType, uint32_t, KurisuBufferHandle *>> result;
  for(const auto &i: userBindings)
    result.emplace_back(trDescriptorType(i.first.first), i.first.second, lastEl(i.second));
  return result;
}
