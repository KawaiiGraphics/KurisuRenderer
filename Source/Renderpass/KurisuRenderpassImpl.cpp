#include "KurisuRenderpassImpl.hpp"
#include "KurisuGlInteropLayer.hpp"
#include "KurisuRootImpl.hpp"
#include "Vulkan/VulkanGBuf.hpp"

KurisuRenderpassImpl::KurisuRenderpassImpl(KawaiiRenderpass *renderpass):
  KawaiiRenderpassImpl(renderpass),
  root(nullptr),
  asyncFrames(1),
  wasUpdated(false)
{
  onModelParentChanged();
  connect(renderpass, &sib_utils::TreeNode::parentUpdated, this, &KurisuRenderpassImpl::onModelParentChanged);
  setRenderingCacheble(true);
  renderpass->setReadGbufFFunc(std::bind(&KurisuRenderpassImpl::readGbufF, this, std::placeholders::_1, std::placeholders::_2));
  renderpass->setReadGbufUFunc(std::bind(&KurisuRenderpassImpl::readGbufU, this, std::placeholders::_1, std::placeholders::_2));
  renderpass->setReadGbufIFunc(std::bind(&KurisuRenderpassImpl::readGbufI, this, std::placeholders::_1, std::placeholders::_2));
}

KurisuRenderpassImpl::~KurisuRenderpassImpl()
{
  preDestruct();
}

void KurisuRenderpassImpl::create()
{
  if(Q_UNLIKELY(!root)) return;

  renderpass.reserve(root->deviceCount());
  root->forallDevices([this](VulkanDevice &dev) {
    renderpass.emplace_back(dev);
    initVulkanRenderpass(renderpass.back());
  });

  gBufs.resize(root->deviceCount());
  for(auto &i: gBufs)
    if(!i.empty())
      i.clear();
  gBufsShared.resize(root->deviceCount());
  for(auto &i: gBufsShared)
    {
      i.resize(asyncFrames);
      for(auto &j: i)
        j.clear();
    }

  std::vector<VkImageUsageFlags> gbufUsage(getModel()->gBufCount(), 0);
  getModel()->forallSubpasses([&gbufUsage] (const KawaiiRenderpass::Subpass &subpass) {
      if(subpass.depthGBuf)
        gbufUsage[*subpass.depthGBuf] |= VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
      for(auto gbuf: subpass.inputGBufs)
        gbufUsage[gbuf] |= VK_IMAGE_USAGE_INPUT_ATTACHMENT_BIT;
      for(auto gbuf: subpass.outputGBufs)
        gbufUsage[gbuf] |= VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
      for(auto gbuf: subpass.storageGBufs)
        gbufUsage[gbuf] |= VK_IMAGE_USAGE_STORAGE_BIT;
      for(auto gbuf: subpass.sampledGBufs)
        gbufUsage[gbuf] |= VK_IMAGE_USAGE_SAMPLED_BIT;
    });

  renderTargets.reserve(root->deviceCount());
  const auto &sz = getModel()->getRenderableSize();
  root->forallDevices([this, &sz](VulkanDevice &dev, size_t dev_i) {
      renderTargets.emplace_back();
      for(size_t frameI = 0; frameI < getAsyncFrames(); ++frameI)
        {
          renderTargets.back().emplace_back(&dev);
          renderTargets.back().back().setWidth(sz.x);
          renderTargets.back().back().setHeight(sz.y);
          renderTargets.back().back().setRenderpass(renderpass[dev_i].getRenderpass());
        }
    });

  root->forallDevices([this, &gbufUsage, &sz](VulkanDevice &dev, size_t dev_i) {
      const bool needOwnMem = dev_i == root->getComposerDeviceIndex();
      size_t i = 0;
      getModel()->forallGBufs([this, dev_i, needOwnMem, &gbufUsage, &i, &sz, &dev] (const KawaiiRenderpass::GBuffer &gbuf) {
          const bool shared = sharedGbufs.count(i)>0;
          if(!shared)
            {
              for(size_t frameI = 0; frameI < asyncFrames; ++frameI)
                gBufsShared[dev_i][frameI].emplace_back(nullptr);
              gBufs[dev_i].emplace_back(&dev);
              gBufs[dev_i].back().init(gbuf, sz.x, sz.y, gbufUsage[i], false);
            } else
            {
              gBufs[dev_i].emplace_back(nullptr);
              for(size_t frameI = 0; frameI < asyncFrames; ++frameI)
                {
                  gBufsShared[dev_i][frameI].emplace_back(&dev);
                  gBufsShared[dev_i][frameI].back().setLinear(true);
                  gBufsShared[dev_i][frameI].back().init(gbuf, sz.x, sz.y, gbufUsage[i], true);
                  if(needOwnMem)
                    gBufsShared[dev_i][frameI].back().allocateMem();
                }
            }
          ++i;
        });
    });

  gBufMemory.resize(root->deviceCount());
  for(size_t dev_i = 0; dev_i < gBufs.size(); ++dev_i)
    {
      const bool needOwnMem = dev_i == root->getComposerDeviceIndex();
      VkMemoryRequirements memReq = {
        .size = 0,
        .alignment = 1,
        .memoryTypeBits = 0
      };
      std::vector<VkDeviceSize> offsets(gBufs[dev_i].size(), 0);
      for(size_t i = 0; i < gBufs[dev_i].size(); ++i)
        {
          if(getExternalGbuf(dev_i, i).imgView)
            continue;

          const bool shared = sharedGbufs.count(i)>0;
          if(shared)
            {
              if(!needOwnMem)
                for(size_t frameI = 0; frameI < asyncFrames; ++frameI)
                  gBufsShared[dev_i][frameI][i].importMem(gBufsShared[root->getComposerDeviceIndex()][frameI][i]);
              continue;
            }
          const auto req = gBufs[dev_i][i].getMemoryRequirements();

          const VkDeviceSize offset = std::ceil(double(memReq.size) / double(req.alignment)) * req.alignment;
          memReq.size = offset + req.size;
          memReq.alignment = std::max<VkDeviceSize>(memReq.alignment, req.alignment);
          if(!memReq.memoryTypeBits)
            memReq.memoryTypeBits = req.memoryTypeBits;
          else
            memReq.memoryTypeBits &= req.memoryTypeBits;

          offsets[i] = offset;
        }
      auto mem = root->getDevice(dev_i).allocateMemory(memReq, 0);
      gBufMemory[dev_i] = mem.first;
      for(size_t i = 0; i < gBufs[dev_i].size(); ++i)
        if(getExternalGbuf(dev_i, i).imgView == VK_NULL_HANDLE
           && sharedGbufs.count(i)==0)
          gBufs[dev_i][i].pointToMem(mem.first, offsets[i]);

      for(size_t frameI = 0; frameI < asyncFrames; ++frameI)
        {
          std::vector<VkImageView> attachments(gBufs[dev_i].size(), VK_NULL_HANDLE);
          for(size_t i = 0; i < gBufs[dev_i].size(); ++i)
            {
              if(getExternalGbuf(dev_i, i).imgView)
                attachments[i] = externalGBufs[dev_i][i].imgView;
              else {
                  const bool shared = sharedGbufs.count(i)>0;
                  if(!shared)
                    attachments[i] = gBufs[dev_i][i].getImgView();
                  else
                    attachments[i] = gBufsShared[dev_i][frameI][i].getImgView();
                }
            }
          renderTargets[dev_i][frameI].setImgs(attachments);
        }
    }

  getModel()->forallSubpasses([this] (const KawaiiRenderpass::Subpass &subpass) {
      auto imgSrc = subpass.imgSrc;
      if(imgSrc && imgSrc->getRendererImpl())
        if(auto kurisuGlImgSrc = dynamic_cast<KurisuGlInteropLayer*>(imgSrc->getRendererImpl()); kurisuGlImgSrc)
          interopLayers.push_back(kurisuGlImgSrc);
    });
  needRedraw = std::vector<bool>(root->deviceCount(), true);
}

void KurisuRenderpassImpl::destroy()
{
  cmd.clear();
  renderTargets.clear();
  gBufs.clear();
  gBufsShared.clear();
  renderpass.clear();
  for(size_t i = 0; i < gBufMemory.size(); ++i)
    {
      root->getDevice(i).waitIdle();
      vkFreeMemory(root->getDevice(i).vkDev, gBufMemory[i], nullptr);
    }
  gBufMemory.clear();
  waitSemaphores.clear();
  waitStages.clear();
  interopLayers.clear();
  for(size_t i = 0; i < renderedFence.size(); ++i)
    {
      root->getDevice(i).waitIdle();
      for(VkFence fence: renderedFence[i])
        vkDestroyFence(root->getDevice(i).vkDev, fence, nullptr);
    }
  renderedFence.clear();
  needRedraw.clear();
}

bool KurisuRenderpassImpl::start()
{
  if(cmd.empty())
    {
      createCmd();
      getModel()->forallSubpasses([this] (const KawaiiRenderpass::Subpass &subpass) {
          auto imgSrc = subpass.imgSrc;
          if(imgSrc && imgSrc->getRendererImpl())
            if(auto kurisuImgSrc = dynamic_cast<KurisuImageSourceImpl*>(imgSrc->getRendererImpl()); kurisuImgSrc)
              {
                waitStages.insert(waitStages.end(), kurisuImgSrc->getWaitStages().cbegin(), kurisuImgSrc->getWaitStages().cend());
                if(waitSemaphores.empty() && !waitStages.empty())
                  {
                    waitSemaphores.resize(root->deviceCount());
                    for(size_t i = 0; i < waitSemaphores.size(); ++i)
                      waitSemaphores[i].resize(asyncFrames);
                  }
                for(size_t i = 0; i < waitSemaphores.size(); ++i)
                  for(size_t j = 0; j < asyncFrames; ++j)
                    waitSemaphores[i][j].insert(waitSemaphores[i][j].end(),
                                                kurisuImgSrc->getWaitSemaphores(i, j).cbegin(),
                                                kurisuImgSrc->getWaitSemaphores(i, j).cend());
              }
        });
    } else
    for(size_t i = 0; i < cmd.size(); ++i)
      {
        cmd[i]->reset();
        initCmd(i);
      }
  return true;
}

void KurisuRenderpassImpl::nextSubpass()
{
  for(auto &c: cmd)
    c->nextSubpass(VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);
}

void KurisuRenderpassImpl::finish()
{
  for(auto &c: cmd)
    {
      c->endRenderpass();
      c->finishRecording();
    }
}

void KurisuRenderpassImpl::drawCache()
{
  if(getModel()->isRedrawNeeded())
    for(size_t i = 0; i < needRedraw.size(); ++i)
      needRedraw[i] = true;

  if(!needRedraw[activeDevice]) return;

  getModel()->markRedrawn();
  needRedraw[activeDevice] = false;

  if(Q_UNLIKELY(renderedFence.empty()))
    {
      renderedFence.resize(root->deviceCount());
      for(size_t i = 0; i < renderedFence.size(); ++i)
        {
          renderedFence[i].resize(asyncFrames);
          for(VkFence &fence: renderedFence[i])
            {
              VkResult result = vkCreateFence(root->getDevice(i).vkDev, &VulkanDevice::fenceCreateInfo, nullptr, &fence);
              if(Q_UNLIKELY(result != VK_SUCCESS))
                throw std::runtime_error("Can not create fence!");
            }
        }
    }

  QReadLocker l(&root->getDevice(activeDevice).resLock);
  VkFence fence = renderedFence[activeDevice][frameIndex];
  vkWaitForFences(root->getDevice(activeDevice).vkDev, 1, &fence, VK_TRUE, std::numeric_limits<uint64_t>::max());
  vkResetFences(root->getDevice(activeDevice).vkDev, 1, &fence);

  for(auto *interopLayer: interopLayers)
    interopLayer->onRpDraw(activeDevice);
  if(waitSemaphores.empty())
    cmd[activeDevice]->render(frameIndex, frameIndex, {}, {}, {}, fence);
  else
    cmd[activeDevice]->render(frameIndex, frameIndex, waitSemaphores[activeDevice][frameIndex], waitStages, {}, fence);
  wasUpdated = true;
}

bool KurisuRenderpassImpl::getWasUpdated() const
{
  return wasUpdated;
}

VulkanBaseRenderpass& KurisuRenderpassImpl::getRenderpass(size_t device_index)
{
  return renderpass[device_index];
}

void KurisuRenderpassImpl::prepareRendering(size_t device, size_t frame_index)
{
  activeDevice = device;
  frameIndex = frame_index;
  wasUpdated = false;
}

VulkanGBuf &KurisuRenderpassImpl::getGbuf(size_t dev_i, size_t gBufIndex, size_t frame_index)
{
  if(sharedGbufs.count(gBufIndex) > 0)
    return gBufsShared[dev_i][frame_index][gBufIndex];
  else
    return gBufs[dev_i][gBufIndex];
}

void KurisuRenderpassImpl::setExternalGbuf(size_t dev_i, size_t index, VkImageView imgView, VkImage img, VkImageLayout layout, VkImageSubresourceRange subresourceRange, VkFormat fmt)
{
  if(externalGBufs.empty() && root)
    {
      externalGBufs.resize(root->deviceCount());
      for(auto &vec: externalGBufs)
        vec.resize(getModel()->gBufCount(), { .imgView = VK_NULL_HANDLE, .img = VK_NULL_HANDLE, .layout = VK_IMAGE_LAYOUT_UNDEFINED, .fmt = VK_FORMAT_UNDEFINED });
    }
  if(externalGBufs[dev_i][index].img != img
     || externalGBufs[dev_i][index].imgView != imgView
     || externalGBufs[dev_i][index].layout != layout
     || externalGBufs[dev_i][index].subresourceRange.aspectMask != subresourceRange.aspectMask
     || externalGBufs[dev_i][index].subresourceRange.baseArrayLayer != subresourceRange.baseArrayLayer
     || externalGBufs[dev_i][index].subresourceRange.baseMipLevel != subresourceRange.baseMipLevel
     || externalGBufs[dev_i][index].subresourceRange.layerCount != subresourceRange.layerCount
     || externalGBufs[dev_i][index].subresourceRange.levelCount != subresourceRange.levelCount
     || externalGBufs[dev_i][index].fmt != fmt)
    {
      externalGBufs[dev_i][index].img = img;
      externalGBufs[dev_i][index].imgView = imgView;
      externalGBufs[dev_i][index].layout = layout;
      externalGBufs[dev_i][index].subresourceRange = subresourceRange;
      externalGBufs[dev_i][index].fmt = fmt;
      preDestruct();
    }
}

KurisuRenderpassImpl::ExternalGBufDescr KurisuRenderpassImpl::getExternalGbuf(size_t dev_i, size_t index) const
{
  if(externalGBufs.empty())
    return ExternalGBufDescr {
        .imgView = VK_NULL_HANDLE,
            .img = VK_NULL_HANDLE,
            .layout = VK_IMAGE_LAYOUT_UNDEFINED
      };
  else
    return externalGBufs[dev_i][index];
}

void KurisuRenderpassImpl::unuseExternalGbufs()
{
  if(!externalGBufs.empty())
    {
      externalGBufs.clear();
      preDestruct();
    }
}

void KurisuRenderpassImpl::forallExternalGbufs(size_t dev_i, const std::function<void (size_t, const ExternalGBufDescr &)> &func)
{
  if(externalGBufs.empty()) return;

  if(Q_LIKELY(func))
    for(size_t i = 0; i < externalGBufs[dev_i].size(); ++i)
      func(i, externalGBufs[dev_i][i]);
}

void KurisuRenderpassImpl::setSharedGbufs(const std::initializer_list<size_t> &newSharedGbufs)
{
  sharedGbufs = newSharedGbufs;
}

VkImage KurisuRenderpassImpl::getGBufImage(size_t dev_i, size_t gbuf_index, size_t frame_index)
{
  if(getExternalGbuf(dev_i, gbuf_index).img)
    return externalGBufs[dev_i][gbuf_index].img;
  else
    return getGbuf(dev_i, gbuf_index, frame_index).getImg();
}

VkImageView KurisuRenderpassImpl::getGBufImageView(size_t dev_i, size_t gbuf_index, size_t frame_index)
{
  if(getExternalGbuf(dev_i, gbuf_index).imgView)
    return externalGBufs[dev_i][gbuf_index].imgView;
  else
    return getGbuf(dev_i, gbuf_index, frame_index).getImgView();
}

VkImageSubresourceRange KurisuRenderpassImpl::getGBufSubresourceRange(size_t dev_i, size_t gbuf_index)
{
  if(getExternalGbuf(dev_i, gbuf_index).img)
    return externalGBufs[dev_i][gbuf_index].subresourceRange;
  else
    return getGbuf(dev_i, gbuf_index, 0).getImageSubresourceRange();
}

std::vector<VkFramebuffer> KurisuRenderpassImpl::getFbo(size_t dev_i)
{
  std::vector<VkFramebuffer> fbo(renderTargets[dev_i].size(), VK_NULL_HANDLE);
  for(size_t frameI = 0; frameI < renderTargets[dev_i].size(); ++frameI)
    fbo[frameI] = renderTargets[dev_i][frameI].getFbo();
  return fbo;
}

KurisuRootImpl *KurisuRenderpassImpl::getRoot() const
{
  return root;
}

VkClearValue KurisuRenderpassImpl::defaultClearValue(KawaiiRenderpass::InternalImgFormat fmt)
{
  switch(fmt)
    {
    case KawaiiRenderpass::InternalImgFormat::RGBA8_Unorm:
    case KawaiiRenderpass::InternalImgFormat::RGBA8_Snorm:
    case KawaiiRenderpass::InternalImgFormat::RGBA16_F:
    case KawaiiRenderpass::InternalImgFormat::RGBA16_UInt:
    case KawaiiRenderpass::InternalImgFormat::RGBA16_SInt:
    case KawaiiRenderpass::InternalImgFormat::RGBA32_F:
    case KawaiiRenderpass::InternalImgFormat::RGBA32_UInt:
    case KawaiiRenderpass::InternalImgFormat::RGBA32_SInt:

    case KawaiiRenderpass::InternalImgFormat::RG8_Unorm:
    case KawaiiRenderpass::InternalImgFormat::RG8_Snorm:
    case KawaiiRenderpass::InternalImgFormat::RG16_F:
    case KawaiiRenderpass::InternalImgFormat::RG16_UInt:
    case KawaiiRenderpass::InternalImgFormat::RG16_SInt:
    case KawaiiRenderpass::InternalImgFormat::RG32_F:
    case KawaiiRenderpass::InternalImgFormat::RG32_UInt:
    case KawaiiRenderpass::InternalImgFormat::RG32_SInt:

    case KawaiiRenderpass::InternalImgFormat::R8_Unorm:
    case KawaiiRenderpass::InternalImgFormat::R8_Snorm:
    case KawaiiRenderpass::InternalImgFormat::R16_F:
    case KawaiiRenderpass::InternalImgFormat::R16_UInt:
    case KawaiiRenderpass::InternalImgFormat::R16_SInt:
    case KawaiiRenderpass::InternalImgFormat::R32_F:
    case KawaiiRenderpass::InternalImgFormat::R32_UInt:
    case KawaiiRenderpass::InternalImgFormat::R32_SInt:
      return VkClearValue { .color = { .float32 = { 0, 0, 0, 0 } } };

    case KawaiiRenderpass::InternalImgFormat::Depth32:
    case KawaiiRenderpass::InternalImgFormat::Depth24:
    case KawaiiRenderpass::InternalImgFormat::Depth24Stencil8:
    case KawaiiRenderpass::InternalImgFormat::Depth16:
    case KawaiiRenderpass::InternalImgFormat::Depth32Stencil8:
    case KawaiiRenderpass::InternalImgFormat::Depth16Stencil8:
    case KawaiiRenderpass::InternalImgFormat::Stencil8:
      return VkClearValue { .depthStencil = { .depth = 1.0f, .stencil = 0 } };
    }
  Q_UNREACHABLE();
}

VkAttachmentDescription KurisuRenderpassImpl::attDescription(quint32 gbuf_index)
{
  const auto &gbuf = getModel()->getGBuf(gbuf_index);
  VkAttachmentDescription attDescr = {
    .format = VulkanGBuf::trFormat(gbuf.format),
    .samples = VK_SAMPLE_COUNT_1_BIT,
    .loadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
    .storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
    .stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE,
    .stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE,
    .initialLayout = VK_IMAGE_LAYOUT_UNDEFINED,
    .finalLayout = VK_IMAGE_LAYOUT_GENERAL,
  };
  if(!externalGBufs.empty() && externalGBufs.front()[gbuf_index].img)
    attDescr.format = externalGBufs.front()[gbuf_index].fmt;

  if(gbuf.needClear)
    attDescr.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
  if(gbuf.needStore)
    {
      attDescr.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
      attDescr.finalLayout = checkGBufLayout(gbuf_index, VK_IMAGE_LAYOUT_GENERAL);
      if(!gbuf.needClear)
        {
          attDescr.loadOp = VK_ATTACHMENT_LOAD_OP_LOAD;
          attDescr.initialLayout = attDescr.finalLayout;
        }
    }
  return attDescr;
}

VkImageLayout KurisuRenderpassImpl::getInputGbufImgLayout(uint32_t subpass, uint32_t inputAttachmentIndex)
{
  if(!renderpass.empty())
    return renderpass.front().getInputAttachmentRef(subpass, inputAttachmentIndex).layout;
  else
    return VK_IMAGE_LAYOUT_UNDEFINED;
}

RenderingCommands &KurisuRenderpassImpl::getCmdbuf(size_t dev_index) const
{
  return *cmd[dev_index];
}

size_t KurisuRenderpassImpl::getActiveDevice() const
{
  return activeDevice;
}

size_t KurisuRenderpassImpl::getFrameIndex() const
{
  return frameIndex;
}

void KurisuRenderpassImpl::waitForRendered(size_t dev_i, size_t frame_i) const
{
  vkWaitForFences(root->getDevice(dev_i).vkDev, 1, &renderedFence[dev_i][frame_i], VK_TRUE, std::numeric_limits<uint64_t>::max());
}

size_t KurisuRenderpassImpl::getAsyncFrames() const
{
  return asyncFrames;
}

void KurisuRenderpassImpl::setAsyncFrames(size_t newAsyncFrames)
{
  if(newAsyncFrames != asyncFrames)
    {
      asyncFrames = newAsyncFrames;
      cmd.clear();
      setCacheDirty(true);
    }
}

void KurisuRenderpassImpl::setRoot(KurisuRootImpl *newRoot)
{
  if(newRoot != root)
    {
      if(root)
        preDestruct();
      root = newRoot;
    }
}

void KurisuRenderpassImpl::initVulkanRenderpass(VulkanBaseRenderpass &rp)
{
  size_t i = 0;
  nonlocalGBufs.clear();
  getModel()->forallSubpasses([this] (const KawaiiRenderpass::Subpass &subpass) {
      nonlocalGBufs.insert(subpass.storageGBufs.cbegin(), subpass.storageGBufs.cend());
    });
  getModel()->forallSubpasses([this] (const KawaiiRenderpass::Subpass &subpass) {
      nonlocalGBufs.insert(subpass.sampledGBufs.cbegin(), subpass.sampledGBufs.cend());
    });

  getModel()->forallGBufs([this, &rp, &i] (const KawaiiRenderpass::GBuffer &gbuf) {
      const VkAttachmentDescription attDescr = attDescription(i);
      rp.addAttachement(attDescr, defaultClearValue(gbuf.format));
      ++i;
    });

  getModel()->forallSubpasses([&rp, this] (const KawaiiRenderpass::Subpass &subpass) {
      const quint32 colorAttIndex = !subpass.outputGBufs.empty()?
            subpass.outputGBufs.front():
            VK_ATTACHMENT_UNUSED;

      const quint32 depthAttIndex = subpass.depthGBuf?
            *subpass.depthGBuf:
            VK_ATTACHMENT_UNUSED;

      std::vector<bool> isInputAttachment(getModel()->gBufCount(), false),
          isSharedAttachment(getModel()->gBufCount(), false);
      for(const auto &attachement: subpass.inputGBufs)
        isInputAttachment[attachement] = true;

      auto checkGBufLayout_ = [this, &isInputAttachment, &isSharedAttachment] (uint32_t attIndex, VkImageLayout layout) {
          if(attIndex == VK_ATTACHMENT_UNUSED)
            return layout;

          if(isInputAttachment[attIndex])
            {
              isSharedAttachment[attIndex] = true;
              return VK_IMAGE_LAYOUT_GENERAL;
            }
          else
            return checkGBufLayout(attIndex, layout);
        };

      const size_t subpass_i = rp.appendSubpass(
            VkAttachmentReference {
              .attachment = colorAttIndex,

              .layout = checkGBufLayout_(colorAttIndex, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL)
            },
            VkAttachmentReference {
              .attachment = depthAttIndex,

              .layout = checkGBufLayout_(depthAttIndex, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL)
            });

      for(size_t i = 1; i < subpass.outputGBufs.size(); ++i)
        {
          rp.addColorAttachment(subpass.outputGBufs[i],
                                checkGBufLayout_(subpass.outputGBufs[i], VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL),
                                subpass_i);
        }

      auto checkInputGBufLayout = [this, &isSharedAttachment] (uint32_t attIndex, VkImageLayout layout) {
          if(attIndex == VK_ATTACHMENT_UNUSED)
            return layout;

          if(isSharedAttachment[attIndex])
            return VK_IMAGE_LAYOUT_GENERAL;
          else
            return checkGBufLayout(attIndex, layout);
        };

      for(const auto &attachement: subpass.inputGBufs)
        {
          rp.addInputAttachment(attachement,
                                checkInputGBufLayout(attachement, VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL),
                                subpass_i);
        }
    });

  getModel()->forallDependencies([&rp] (const KawaiiRenderpass::Dependency &dep) {
      rp.addSubpassDependency(
            VkSubpassDependency {
              .srcSubpass = dep.srcSubpass,
              .dstSubpass = dep.dstSubpass,
              .srcStageMask = VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT,
              .dstStageMask = VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT,
              .srcAccessMask = VK_ACCESS_SHADER_WRITE_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
              .dstAccessMask = VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_INPUT_ATTACHMENT_READ_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_READ_BIT,
              .dependencyFlags = static_cast<VkDependencyFlags>(dep.isLocal? VK_DEPENDENCY_BY_REGION_BIT: 0)
            });
    });
}

void KurisuRenderpassImpl::createCmd()
{
  cmd.reserve(root->deviceCount());
  root->forallDevices([this](VulkanDevice &dev) {
    cmd.push_back(std::make_unique<RenderingCommands>(dev, asyncFrames, asyncFrames, true));
    initCmd(cmd.size()-1);
  });
}

void KurisuRenderpassImpl::initCmd(size_t i)
{
  const auto &sz = getModel()->getRenderableSize();
  cmd[i]->setViewport(QRect(0, 0, sz.x, sz.y));
  cmd[i]->startRecording(VK_NULL_HANDLE, { }, renderpass[i].getClearValues(), VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS, 0);

  std::vector<std::vector<VkImageMemoryBarrier>> imgBarriers(asyncFrames);
  for(size_t frameI = 0; frameI < imgBarriers.size(); ++frameI)
    for(quint32 gbuf_index: nonlocalGBufs)
      {
        imgBarriers[frameI].push_back(VkImageMemoryBarrier {
                                        .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,

                                        .srcAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT
                                        | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT
                                        | VK_ACCESS_SHADER_READ_BIT
                                        | VK_ACCESS_SHADER_WRITE_BIT,

                                        .dstAccessMask = VK_ACCESS_SHADER_READ_BIT | VK_ACCESS_SHADER_WRITE_BIT,
                                        .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                                        .newLayout = VK_IMAGE_LAYOUT_GENERAL,
                                        .srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                                        .dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED,
                                        .image = getGBufImage(i, gbuf_index, frameI),
                                        .subresourceRange = getGBufSubresourceRange(i, gbuf_index)
                                      });
      }
  cmd[i]->imageMemBarier(VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT, VK_PIPELINE_STAGE_ALL_GRAPHICS_BIT, { imgBarriers });
  cmd[i]->beginRenderpass(renderpass[i].getRenderpass(), getFbo(i), renderpass[i].getClearValues(), VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);
}

void KurisuRenderpassImpl::onModelParentChanged()
{
  KawaiiRoot *r = KawaiiRoot::getRoot(getModel());
  if(r)
    setRoot(static_cast<KurisuRootImpl*>(r->getRendererImpl()));
  else
    setRoot(nullptr);
}

VkImageLayout KurisuRenderpassImpl::checkGBufLayout(quint32 gbuf_index, VkImageLayout layout)
{
  if(gbuf_index == VK_ATTACHMENT_UNUSED)
    return layout;

  if(!externalGBufs.empty() && externalGBufs.front()[gbuf_index].img)
    return externalGBufs.front()[gbuf_index].layout;

  return (nonlocalGBufs.count(gbuf_index)>0)?
        VK_IMAGE_LAYOUT_GENERAL:
        layout;
}

std::vector<float> KurisuRenderpassImpl::readGbufF(uint32_t gbuf, const QRect &reg)
{
  if(gbuf > gBufs[activeDevice].size())
    return {};

  waitForRendered(activeDevice, frameIndex);
  return gBufs[activeDevice][gbuf].readF(reg);
}

std::vector<int32_t> KurisuRenderpassImpl::readGbufI(uint32_t gbuf, const QRect &reg)
{
  if(gbuf > gBufs[activeDevice].size())
    return {};

  waitForRendered(activeDevice, frameIndex);
  return gBufs[activeDevice][gbuf].readI(reg);
}

std::vector<uint32_t> KurisuRenderpassImpl::readGbufU(uint32_t gbuf, const QRect &reg)
{
  if(gbuf > gBufs[activeDevice].size())
    return {};

  waitForRendered(activeDevice, frameIndex);
  return gBufs[activeDevice][gbuf].readU(reg);
}
