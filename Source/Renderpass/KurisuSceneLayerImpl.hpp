#ifndef KURISUSCENELAYERIMPL_HPP
#define KURISUSCENELAYERIMPL_HPP

#include "KurisuImageSourceImpl.hpp"
#include <KawaiiRenderer/Renderpass/KawaiiSceneLayerImpl.hpp>

class KurisuRootImpl;

class KURISURENDERER_SHARED_EXPORT KurisuSceneLayerImpl: public KurisuImageSourceImpl, public KawaiiSceneLayerImpl
{
public:
  KurisuSceneLayerImpl(KawaiiSceneLayer *model);
  ~KurisuSceneLayerImpl();

  // KawaiiImageSourceImpl interface
protected:
  void drawQuad() override final;

  // KurisuImageSourceImpl interface
private:
  void writeCmdBuf(RenderingCommands &cmdBuf, size_t dev_index) override final;

  // KawaiiRendererImpl interface
private:
  void initConnections() override final;



  // IMPLEMENT
private:
  KurisuRootImpl *root;
  QMetaObject::Connection onRootBufferBindingChanged;
};

#endif // KURISUSCENELAYERIMPL_HPP
