#ifndef KURISUGLPAINTEDLAYERIMPL_HPP
#define KURISUGLPAINTEDLAYERIMPL_HPP

#include "KurisuGlInteropLayer.hpp"
#include <KawaiiRenderer/Renderpass/KawaiiGlPaintedLayerImpl.hpp>

class KURISURENDERER_SHARED_EXPORT KurisuGlPaintedLayerImpl: public KurisuGlInteropLayer, public KawaiiGlPaintedLayerImpl
{
public:
  KurisuGlPaintedLayerImpl(KawaiiGlPaintedLayer *model);
  ~KurisuGlPaintedLayerImpl() = default;

  // KawaiiGlPaintedLayerImpl interface
protected:
  void refreshPixmaps() override final;



  // IMPLEMENT
private:
  QOpenGLContext *getGlCtx();
};

#endif // KURISUGLPAINTEDLAYERIMPL_HPP
