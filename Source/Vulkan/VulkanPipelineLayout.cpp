#include "VulkanPipelineLayout.hpp"
#include <Kawaii3D/Shaders/KawaiiShader.hpp>

namespace {
  static const std::initializer_list<std::vector<VkDescriptorSetLayoutBinding>> kawaiiLayoutBindings = {
    { VkDescriptorSetLayoutBinding {
      KawaiiShader::getCameraUboLocation(), // binding
      VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // descriptorType
      1, // descriptorCount
      VK_SHADER_STAGE_ALL_GRAPHICS, // stageFlags
      nullptr // pImmutableSamplers
    }} ,
    { VkDescriptorSetLayoutBinding {
      KawaiiShader::getMaterialUboLocation(), // binding
      VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // descriptorType
      1, // descriptorCount
      VK_SHADER_STAGE_ALL_GRAPHICS, // stageFlags
      nullptr // pImmutableSamplers
    } },
    { VkDescriptorSetLayoutBinding {
      KawaiiShader::getModelUboLocation(), // binding
      VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // descriptorType
      1, // descriptorCount
      VK_SHADER_STAGE_ALL_GRAPHICS, // stageFlags
      nullptr // pImmutableSamplers
    } },
    { VkDescriptorSetLayoutBinding {
      KawaiiShader::getSurfaceUboLocation(), // binding
      VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER, // descriptorType
      1, // descriptorCount
      VK_SHADER_STAGE_ALL_GRAPHICS, // stageFlags
      nullptr // pImmutableSamplers
      } },
    {}
  };
}

VulkanPipelineLayout::VulkanPipelineLayout(VkDevice dev, std::vector<std::vector<VkDescriptorSetLayoutBinding> > &&initialDescriptorSetLayoutBindings):
  dev(dev),

  layout(VK_NULL_HANDLE),
  setLayoutBindings(std::move(initialDescriptorSetLayoutBindings)),

  layoutInfo {
    VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO, // sType
    nullptr, // pNext
    0, // flags
    0, // setLayoutCount
    nullptr, // pSetLayouts
    0, // pushConstantRangeCount
    nullptr// pPushConstantRanges
    },

  dirty(true)
{
}

VulkanPipelineLayout::VulkanPipelineLayout(VkDevice dev):
  VulkanPipelineLayout(dev, kawaiiLayoutBindings)
{
}

VulkanPipelineLayout::~VulkanPipelineLayout()
{
  if(layout || !descriptorSetLayouts.empty())
    vkDeviceWaitIdle(dev);

  if(layout)
    vkDestroyPipelineLayout(dev, layout, nullptr);

  for(auto i: descriptorSetLayouts)
    vkDestroyDescriptorSetLayout(dev, i, nullptr);
}

bool operator==(const VkDescriptorSetLayoutBinding &a, const VkDescriptorSetLayoutBinding &b)
{
  return a.descriptorType == b.descriptorType &&
      a.descriptorCount == b.descriptorCount &&
      a.binding == b.binding;
}

uint32_t VulkanPipelineLayout::addDescriptorBinding(size_t descriptorSetIndex, VkDescriptorType type, VkShaderStageFlags shaderStage)
{
  const VkDescriptorSetLayoutBinding val = {
    .binding = getFreeBindingPoint(),
    .descriptorType = type,
    .descriptorCount = 1,
    .stageFlags = shaderStage,
  };

  insertDescriptorSetLayoutBinding(descriptorSetIndex, val);
  return val.binding;
}

VkPipelineLayout VulkanPipelineLayout::getPipelineLayout()
{
  validate();
  return layout;
}

std::pair<VkDescriptorSetLayout, uint32_t> VulkanPipelineLayout::getDescriptorSetLayout(uint32_t binding)
{
  validate();
  for(size_t i = 0; i < setLayoutBindings.size(); ++i)
    for(size_t j = 0; j < setLayoutBindings[i].size(); ++j)
      if(setLayoutBindings[i][j].binding == binding)
        return { descriptorSetLayouts[i], i };

  return { VK_NULL_HANDLE, 0 };
}

const std::vector<VkDescriptorSetLayout>& VulkanPipelineLayout::getDescriptorSetLayouts()
{
  validate();
  return descriptorSetLayouts;
}

std::unordered_map<VkDescriptorType, uint32_t> VulkanPipelineLayout::getDescriptorCount(size_t descriptorSetIndex)
{
  std::unordered_map<VkDescriptorType, uint32_t> result;
  for(const auto &i: setLayoutBindings[descriptorSetIndex])
    {
      auto el = result.find(i.descriptorType);
      if(el == result.end())
        el = result.insert({i.descriptorType, i.descriptorCount}).first;
      else
        el->second += i.descriptorCount;
    }
  return result;
}

VulkanPipelineLayout::OnInvalidateFuncs::const_iterator VulkanPipelineLayout::addOnInvalidate(const std::function<void ()> &func)
{
  return onInvalidate.insert(onInvalidate.end(), func);
}

void VulkanPipelineLayout::removeOnInvalidate(const OnInvalidateFuncs::const_iterator &el)
{
  onInvalidate.erase(el);
}

void VulkanPipelineLayout::validate()
{
  if(dirty)
    {
      if(layout || !descriptorSetLayouts.empty())
        vkDeviceWaitIdle(dev);

      if(layout)
        vkDestroyPipelineLayout(dev, layout, nullptr);

      for(auto i: descriptorSetLayouts)
        vkDestroyDescriptorSetLayout(dev, i, nullptr);

      descriptorSetLayouts.resize(setLayoutBindings.size());
      for(size_t i = 0; i < setLayoutBindings.size(); ++i)
        {
          const VkDescriptorSetLayoutCreateInfo descLayoutInfo = {
            .sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
            .flags = 0,
            .bindingCount = static_cast<uint32_t>(setLayoutBindings[i].size()),
            .pBindings = setLayoutBindings[i].data()
          };

          if (vkCreateDescriptorSetLayout(dev, &descLayoutInfo, nullptr, &descriptorSetLayouts[i]) != VK_SUCCESS) {
              descriptorSetLayouts[i] = VK_NULL_HANDLE;
              for(auto i: descriptorSetLayouts)
                if(i)
                  vkDestroyDescriptorSetLayout(dev, i, nullptr);
              descriptorSetLayouts.clear();
              layout = VK_NULL_HANDLE;
              throw std::runtime_error("failed to create descriptor set layout!");
            }
        }

      if(!descriptorSetLayouts.empty())
        {
          layoutInfo.pSetLayouts = descriptorSetLayouts.data();
          layoutInfo.setLayoutCount = descriptorSetLayouts.size();
        } else
        {
          layoutInfo.pSetLayouts = nullptr;
          layoutInfo.setLayoutCount = 0;
        }

      if (vkCreatePipelineLayout(dev, &layoutInfo, nullptr, &layout) != VK_SUCCESS) {
          layout = VK_NULL_HANDLE;
          throw std::runtime_error("failed to create pipeline layout!");
        }

      dirty = false;
    }
}

void VulkanPipelineLayout::markDirty()
{
  dirty = true;
  for(const auto &i: onInvalidate)
    i();
}

uint32_t VulkanPipelineLayout::getFreeBindingPoint() const
{
  uint32_t binding = 0;
  while(true)
    {
      bool used = false;
      for(const auto &i: setLayoutBindings)
        for(const auto &j: i)
          if(j.binding == binding)
            {
              used = true;
              break;
            }
      if(used)
        ++binding;
      else
        return binding;
    }
}

void VulkanPipelineLayout::insertDescriptorSetLayoutBinding(size_t descriptorSetIndex, const VkDescriptorSetLayoutBinding &val)
{
  bool dirty = false;
  if(descriptorSetIndex >= setLayoutBindings.size())
    {
      setLayoutBindings.resize(descriptorSetIndex+1);
      dirty = true;
    }
  const auto el = std::find(setLayoutBindings[descriptorSetIndex].begin(), setLayoutBindings[descriptorSetIndex].end(), val);
  if(el == setLayoutBindings[descriptorSetIndex].end())
    {
      setLayoutBindings[descriptorSetIndex].push_back(val);
      dirty = true;
    }
  if(dirty)
    markDirty();
}
