#ifndef RENDERINGCOMMANDS_HPP
#define RENDERINGCOMMANDS_HPP

#include "VulkanDescriptorPool.hpp"
#include "VulkanSwapchain.hpp"
#include "VulkanSemaphore.hpp"
#include "VulkanTexture.hpp"
#include "VulkanBuffer.hpp"
#include <unordered_set>
#include <QFuture>
#include <QColor>
#include <vector>

//belongs to specific swapchain and readerpass

class VulkanPipelineLayout;

class RenderingCommands: public QObject
{
  Q_OBJECT
public:
  enum class State: uint8_t
  {
    Empty,
    WritingCommands,
    Compleate,
    Dirty
  };

  RenderingCommands(VulkanDevice &dev, size_t image_count=1, size_t asyncFrames=2, bool primary=false);
  RenderingCommands(const VulkanSwapchain &swapchain);
  ~RenderingCommands();

  void reset();

  void startRecording(VkRenderPass renderPass, const std::vector<VkFramebuffer> &fbo, const std::vector<VkClearValue> &clearValues, VkSubpassContents subpassContents, uint32_t subpass);
  void beginRenderpass(VkRenderPass renderPass, const std::vector<VkFramebuffer> &fbo, const std::vector<VkClearValue> &clearValues, VkSubpassContents subpassContents);

  void setViewport(const QRect &viewport);

  void bindGraphicsPipeline(VkPipeline pipeline);
  void bindVbos(uint32_t firstBinding, const std::vector<VulkanBuffer*> &buffers, const std::vector<VkDeviceSize> &offsets);
  void bindIndexBuffer(VulkanBuffer &buffer, VkIndexType indexType = VK_INDEX_TYPE_UINT32, VkDeviceSize offset = 0);

  void createDescriptorSet(uint64_t name, const std::vector<VulkanDescriptorPool::BufBinding> &bufBindings, const std::vector<VulkanDescriptorPool::ImgBinding> &imgBindings, VkDescriptorSetLayout layout, const std::unordered_map<VkDescriptorType, uint32_t> &descriptorCount);
  void createDescriptorSet(uint64_t name, const std::vector<VulkanDescriptorPool::BufBinding> &bufBindings, const std::vector<std::vector<VulkanDescriptorPool::ImgBinding>> &imgBindings, VkDescriptorSetLayout layout, const std::unordered_map<VkDescriptorType, uint32_t> &descriptorCount);

  void bindDescriptorSet(uint64_t descrSetName, uint32_t descrSetBinding, VkPipelineLayout pipelineLayout);

  void draw(uint32_t vertexCount, uint32_t instanceCount, uint32_t vertex_i, uint32_t instance_i);
  void drawIndexed(uint32_t vertexCount, uint32_t instanceCount, uint32_t vertex_i, uint32_t instance_i);
  void drawIndexedIndirect(VulkanBuffer &buffer, VkDeviceSize offset);

  void execute(RenderingCommands &commands);

  void nextSubpass(VkSubpassContents subpassContents);

  void endRenderpass();

  void finishRecording();

  void clearImage(const std::vector<VkImage> &img);
  void blitImage(const std::vector<std::vector<VkImage> > &from, VkImageLayout fromImgLayout, const std::vector<std::vector<VkImage> > &to, VkImageLayout toImgLayout, const std::vector<VkImageBlit> &regions, VkFilter filter);
  void blitImage(VkImage from, VkImageLayout fromImgLayout, VkImage to, VkImageLayout toImgLayout, const std::vector<VkImageBlit> &regions, VkFilter filter);
  void imageMemBarier(VkPipelineStageFlags srcStage, VkPipelineStageFlags dstStage, const std::vector<std::vector<std::vector<VkImageMemoryBarrier> > > &imgBarriers);

  void prepare(size_t frameIndex);
  bool isPrimary() const;
  State getState() const;
  bool isEmpty() const;

  bool hasBindedPipeline();

  void exec(uint32_t imageIndex, size_t frameIndex, const std::vector<VkSemaphore> &waitSemaphores, const std::vector<VkPipelineStageFlags> &waitStages, const std::vector<VkSemaphore> &signalSemaphores, VkFence signalFence);
  void finalize(size_t frameIndex);

  void render(uint32_t imageIndex, size_t frameIndex);
  void render(uint32_t imageIndex, size_t frameIndex, const std::vector<VkSemaphore> &waitSemaphores, const std::vector<VkPipelineStageFlags> &waitStages, const std::vector<VkSemaphore> &signalSemaphores, VkFence signalFence);

  inline const QRect& getViewport() const
  { return viewport; }

signals:
  void aboutToReset();



  //IMPLEMENT
private:
  VulkanDevice &dev;
  VkCommandPool commandPool;
  std::vector<std::vector<VkCommandBuffer>> commandBuffers;
  std::vector<std::unique_ptr<VulkanDescriptorPool>> descriptorPools;

  std::vector<std::function<void(VkCommandBuffer,size_t,size_t)>> delayedCommands;
  QFuture<void> writeCmdbufTask;

  std::vector<VulkanBuffer*> usedBufers;
  std::vector<RenderingCommands*> usedCommands;
  std::unordered_set<uint64_t> usedDescriptorSetNames;

  std::vector<VulkanSemaphore> resourcesReady;
  std::vector<bool> waitingForResources;

  QRect viewport;

  std::atomic<State> state;
  VkPipeline bindedPipeline;

  QVector<bool> isUsedOnGpu;

  bool primary;


  void waitForCmdbuf();
  VkCommandBuffer getCommandBuffer(size_t imageIndex, size_t frameIndex);

  void rewrite();
  void onDescriptorSetUpdated(uint64_t name);

  void check();
};

#endif // RENDERINGCOMMANDS_HPP
