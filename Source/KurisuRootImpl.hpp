#ifndef KURISUROOTIMPL_HPP
#define KURISUROOTIMPL_HPP

#include <KawaiiRenderer/KawaiiRootImpl.hpp>
#include "Renderpass/KurisuRenderpassImpl.hpp"
#include "Textures/KurisuOffscrRenderer.hpp"
#include "KurisuBufferHandle.hpp"
#include "KurisuBufferBindings.hpp"
#include "KurisuConfig.hpp"

#include "Vulkan/VulkanBaseRenderpass.hpp"
#include "Vulkan/VulkanDeviceManager.hpp"
#include "Vulkan/VulkanPipelineLayout.hpp"
#include "Vulkan/VulkanDescriptorPool.hpp"
#include "Vulkan/RenderingCommands.hpp"
#include "Vulkan/VulkanInstance.hpp"
#include "Vulkan/VulkanShader.hpp"

#include <sib_utils/PairHash.hpp>
#include <unordered_set>

#include <rhi/qrhi.h>

class KurisuSurfaceImpl;
class KurisuFramebufferImpl;

class KURISURENDERER_SHARED_EXPORT KurisuRootImpl: public KawaiiRootImpl
{
  Q_OBJECT

  friend class KurisuBufferHandle;
public:
  struct Binding {
    uint32_t binding;
    uint32_t set;
  };

  KurisuRootImpl(KawaiiRoot *model);
  ~KurisuRootImpl();

  KurisuBufferBindings bufBindings;

  VkSurfaceKHR getSurface(QWindow *wnd);

  void forallDevices(const std::function<void(VulkanDevice&)> &func);
  void forallDevices(const std::function<void(VulkanDevice&, size_t)> &func);

  VulkanDevice &getDevice(size_t device_index) const;
  size_t getComposerDeviceIndex() const;
  VulkanDevice &getComposerDevice() const;
  size_t getDeviceIndex(VulkanDevice *dev) const;
  uint32_t deviceCount() const;

  VulkanPipelineLayout& getScenePipelineLayout(size_t device_index);
  VulkanPipelineLayout& getOverlayPipelineLayout(size_t device_index);
  std::vector<const VulkanShader*> getOverlayShaders(size_t device_index);
  std::vector<const VulkanShader*> getArrayedOverlayShaders(size_t device_index, uint32_t layersCount);
  std::vector<const VulkanShader*> getFlippedOverlayShaders(size_t device_index);

  void activateCommands(RenderingCommands *cmd, size_t device_index);
  RenderingCommands& getActiveCommands();
  bool hasActiveCommands() const;
  void releaseCommands();

  void activateSurface(KurisuSurfaceImpl *surface);
  KurisuSurfaceImpl* getActiveSurface();
  void releaseSurface();
  void requestWindowUpdate(QWindow &wnd);

  void activateRenderpass(KurisuRenderpassImpl *rp, quint32 subpass);
  VulkanBaseRenderpass& getActiveRenderpass(size_t device_index);
  quint32 getActiveSubpass() const;

  bool isNowOffscreen() const;
  void setOffscreenTarget(KurisuOffscrRenderer *offscrRender);

  size_t getActiveDevice() const;

  KurisuConfig& getConfig() const;

  Binding getTextureLocation(const std::string &textureName, uint32_t blockBinding);
  Binding getGlobBufLocation(uint32_t number, VkDescriptorType descriptorType);
  Binding getInputGbufLocation(uint32_t number);
  Binding getStorageGbufLocation(uint32_t number);
  Binding getSampledGbufLocation(uint32_t number);
  uint32_t getDescrSetIndex(uint32_t binding);

  void registerSurface(KurisuSurfaceImpl *sfc);
  void unregisterSurface(KurisuSurfaceImpl *sfc);

  void waitForSurfacesRenderFinished();

  QReadLocker& getGpuOffscrResLocked(size_t dev_index);

  const std::vector<VkPipelineColorBlendAttachmentState> &getColorBlendAttachmentState() const;
  void setColorBlendAttachmentState(const std::vector<VkPipelineColorBlendAttachmentState> &newColorBlendAttachmentState);

  void setDepthState(VkCompareOp depthCompareOp, bool depthTest, bool depthWrite);
  bool getDepthTest() const;
  bool getDepthWrite() const;
  VkCompareOp getDepthCompareOp() const;

  VkSampler getCommonSampler(size_t dev_i);

  QRhi& getQRhi();

  static bool checkSystem();

  // KawaiiRootImpl interface
  KawaiiBufferHandle *createBuffer(const void *ptr, size_t n, const std::initializer_list<KawaiiBufferTarget> &availableTargets) override final;
  KawaiiTextureHandle *createTexture(KawaiiTexture *model) override final;

signals:
  void bufferBindingChanged();

private:
  void beginOffscreen() override final;
  void endOffscreen() override final;



  //IMPLEMENT
private:
  std::unique_ptr<VulkanInstance> inst;
  std::unique_ptr<VulkanDeviceManager> devices;
  std::vector<VulkanPipelineLayout> scenePipelineLayouts;
  std::vector<VulkanPipelineLayout> overlayPipelineLayouts;
  std::vector<VulkanShader> quadVertShaders;
  std::vector<VulkanShader> quadFlippedVertShaders;
  std::vector<VulkanShader> quadFragShaders;
  std::unordered_map<uint32_t, std::vector<VulkanShader>> quadArrayedFragShaders;

  std::vector<VkPipelineColorBlendAttachmentState> colorBlendAttachmentState;
  KurisuSurfaceImpl *currentSfc;
  RenderingCommands* currentCmd;

  KurisuOffscrRenderer *currentOffscr;
  std::vector<std::unique_ptr<QReadLocker>> offscrGpuResLock;

  size_t current_device;

  std::unique_ptr<KurisuConfig> config;

  std::unordered_map<std::pair<std::string /*name*/, uint32_t /*block binding*/>, Binding,
  sib_utils::PairHash<std::string, uint32_t>> textureLocations;

  std::unordered_map<uint32_t, Binding> inputGbufLocations;
  std::unordered_map<uint32_t, Binding> storageGbufLocations;
  std::unordered_map<uint32_t, Binding> sampledGbufLocations;

  std::vector<VulkanDescriptorPool::ImgBinding> globImgBindings;

  std::unordered_map<std::pair<uint32_t, VkDescriptorType>, Binding,
  sib_utils::PairHash<uint32_t, VkDescriptorType>> globBufLocations;

  std::unordered_set<KurisuSurfaceImpl*> surfaces;
  std::unique_ptr<QReadWriteLock> surfacesLock;

  KurisuRenderpassImpl *activeRp;
  std::vector<VkSampler> commonSampler; // One per device

  std::unique_ptr<QRhi> qrhi;

  quint32 activeSubpass;

  VkCompareOp depthCompareOp;
  bool depthTest;
  bool depthWrite;

  bool offscreen;

  struct BufferBinding {
    VkDescriptorType target;
    uint32_t block;
    KurisuBufferHandle *buf;
  };

  void bindBuffersToDescriptor(std::vector<BufferBinding> &&buffers, RenderingCommands &cmd, size_t device_index);

  void onChangedBlockBufBinding(KawaiiBufferTarget target, uint32_t block, KurisuBufferHandle *buf);
};

#endif // KURISUROOTIMPL_HPP
