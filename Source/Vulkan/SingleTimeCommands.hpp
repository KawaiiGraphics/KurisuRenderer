#ifndef SINGLETIMECOMMANDS_HPP
#define SINGLETIMECOMMANDS_HPP

#include "VulkanCommandPool.hpp"
#include "VulkanDevice.hpp"
#include <QFuture>

class VulkanTexture;

class SingleTimeCommands
{
public:
  SingleTimeCommands(const VulkanDevice &dev_info);
  ~SingleTimeCommands();

  void copyBuffer(VkBuffer srcBuffer, VkBuffer dstBuffer, std::vector<VkBufferCopy> &regions);

  void transitionImageLayout(VkImage image, VkImageSubresourceRange subresourceRange, VkImageLayout oldLayout, VkImageLayout newLayout,
                             uint32_t srcQFamily = VK_QUEUE_FAMILY_IGNORED, uint32_t dstQFamily = VK_QUEUE_FAMILY_IGNORED);

  void copyBufferToImage(VkBuffer buffer, VkImage image, VkDeviceSize bufOffset, VkOffset3D offset, VkExtent3D size, VkImageSubresourceLayers imgSubresourceLayers);
  void copyImageToBuffer(VkImage image, VkBuffer buffer, VkOffset3D offset, VkExtent3D size, VkDeviceSize bufOffset, VkImageSubresourceLayers imgSubresourceLayers);
  void copyImage(const VulkanTexture &from, const VulkanTexture &to, const std::vector<VkImageCopy> &regions);
  void copyImage(VkImage from, VkImageLayout fromInitLayout, const VulkanTexture &to, const std::vector<VkImageCopy> &regions);

  QFuture<void> write();

  static void copyImageFunc(VkCommandBuffer cmdBuf, VkImage from, VkImageLayout fromInitLayout, VkImage to, VkImageLayout toInitLayout, const std::vector<VkImageCopy> &regions, VkPipelineStageFlags sourceStage);
  static void copyImageFunc(VkCommandBuffer cmdBuf, const VulkanTexture &from, const VulkanTexture &to, const std::vector<VkImageCopy> &regions, VkPipelineStageFlags sourceStage);



private:
  VkDevice dev;
  VulkanCommandPool &pool;

  bool commited;

  std::vector<std::function<void(VkCommandBuffer)>> commands;
};

#endif // SINGLETIMECOMMANDS_HPP
